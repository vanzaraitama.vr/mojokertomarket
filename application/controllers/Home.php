<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index(){	

		$data=array(
			'base_url' 		=> base_url(),
			'title'			=> "Mojokerto Market",
			'header'		=> "Mojokerto Market",
			'mainContent' 	=> "home"
			
		);

		$this->parser->parse('master/template', $data);
	}

	public function about(){

		$data=array(
			'base_url' 		=> base_url(),
			'title'			=> "Mojokerto Market",
			'header'		=> "Mojokerto Market",
			'mainContent' 	=> "about"
			
		);

		$this->parser->parse('master/template', $data);
	}

	public function contact_us(){

		$data=array(
			'base_url' 		=> base_url(),
			'title'			=> "Mojokerto Market",
			'header'		=> "Mojokerto Market",
			'mainContent' 	=> "contact"
			
		);

		$this->parser->parse('master/template', $data);
	}

	public function categories(){
		
		$data = $this->m_categories->categories();

		echo json_encode($data);
	}
}
