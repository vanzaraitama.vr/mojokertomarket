<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index(){	

		$data=array(
			'base_url' 		=> base_url(),
			'title'			=> "Mojokerto Market",
			'header'		=> "Mojokerto Market",
			'mainContent' 	=> "product/list_product",
			'breadcrumb'	=> ""
			
		);

		$this->parser->parse('master/template', $data);
	}


	public function list(){	

		$category 		= $this->uri->segment(3);
		$id 			= $this->uri->segment(4);
		$sub_category 	= NULL;
		$breadcrumb 	= $this->fn_breadcrumb($category,$sub_category);

		$data=array(
			'base_url' 		=> base_url(),
			'title'			=> "Mojokerto Market",
			'header'		=> "Mojokerto Market",
			'mainContent' 	=> "product/list_product",
			'breadcrumb'	=> $breadcrumb,
			'category'		=> $category,
			'sub_category'	=> $sub_category,
			'id'			=> $id
			
		);

		$this->parser->parse('master/template', $data);
	}

	public function detail(){

		$category 		= $this->uri->segment(3);
		$id 			= $this->uri->segment(4);
		$sub_category	= NULL;
		$name 			= NULL;
		$slug 			= NULL;
		$description	= NULL;
		$stock 			= NULL;
		$price 			= NULL;
		$discount 		= NULL;
		$rating 		= NULL;
		$status 		= NULL;
		$category_id	= NULL;
		$price_after	= NULL;
		$data_image		= NULL;
		$data_thumbnail = NULL;
		$breadcrumb 	= $this->fn_breadcrumb($category,$sub_category);
		
		$this->load->model('m_product');

		$detail 		= $this->m_product->get_detail($id);
		//print_r($detail);	
		if($detail['status']){
			$name 		= $detail['result']['name'];
            $slug 		= $detail['result']['slug'];
            $description= $detail['result']['description'];
            $stock 		= $detail['result']['stock'];
            $price 		= $detail['result']['price'];
            $discount 	= $detail['result']['discount'];
            $rating 	= $detail['result']['rating'];
            $status 	= $detail['result']['status'];
            $category_id= $detail['result']['category_id'];
            $price_after= $detail['result']['price_after'];

            $store_name			=	$detail['result']['shop']['name'];
            $store_address  	=   $detail['result']['shop']['address'];
            $store_phone    	=   $detail['result']['shop']['phone_number'];
            $store_descriptions =   $detail['result']['shop']['description'];

            if(is_array($detail['result']['images_url'])){
            	$no =0;
            	foreach ($detail['result']['images_url'] as $key => $value) {
            		# code...
            		
            		if($no==0){
            			$data_image =	$data_image.'<div class="image_preview_container images-large">';
                        $data_image =	$data_image.'	<img id="img_zoom" data-zoom-image="'.$value.'" src="'.$value.'" alt="">';
                        $data_image =	$data_image.'	<button class="btn-zoom open_qv"><span>zoom</span></button>';
                        $data_image =	$data_image.'</div>';
            		}

            			$data_thumbnail = $data_thumbnail.'<a href="#" data-image="'.$value.'" data-zoom-image="'.$value.'">';
                        $data_thumbnail = $data_thumbnail.'		<img src="'.$value.'" data-large-image="'.$value.'" alt="">';
                        $data_thumbnail = $data_thumbnail.'</a>';

            		$no++;
            	}
            }
		}
		
		if($stock==0){
			$availibility = '<i class="fa fa-window-close" aria-hidden="true"></i> Habis';
		}else{
			$availibility = '<i class="fa fa-check-square-o" aria-hidden="true"></i> Tersedia';
		}

		$data=array(
			'base_url' 		=> base_url(),
			'title'			=> "Mojokerto Market",
			'header'		=> "Mojokerto Market",
			'mainContent' 	=> "product/detail_product",
			'breadcrumb'	=> $breadcrumb,
			'category'		=> $category,
			'sub_category'	=> $sub_category,
			'id'			=> $id,
			'name' 		  	=> $name, 		
			'slug'		  	=> $slug, 		
			'description' 	=> $description,
			'stock' 	  	=> $stock, 		
			'price' 	  	=> $price, 		
			'discount' 	  	=> $discount, 	
			'rating' 	  	=> $rating, 	
			'status' 	  	=> $status, 	
			'category_id' 	=> $category_id,
			'price_after' 	=> $price_after,
			'store_name'	=> $store_name,			
			'store_address' => $store_address, 	
			'store_phone' 	=> $store_phone,   	
			'store_descriptions' => $store_descriptions,
			'data_image'	=> $data_image,
			'data_thumbnail'=> $data_thumbnail,
			'availibility' 	=> $availibility
						
		);

		//$this->parser->parse('master/template', $data);
		$this->parser->parse('product/detail_product', $data);
	}

	public function best_seller(){

		$category 		= $this->input->get('category');
		$sub_category 	= $this->input->get('sub_category');
		
		$sub_category==''?$q=$category:$q=$sub_category;
	
		$this->load->model('m_product');

		$data = $this->m_product->best_seller($q);
			
		echo json_encode($data);
	}

	public function get_list_product(){
		
		$id 			= $this->input->get('id');
		$category 		= $this->input->get('category');
		$sub_category 	= $this->input->get('sub_category');
		$page 			= $this->input->get('page');
		$sortBy			= $this->input->get('sortBy');
		$perPage		= $this->input->get('perPage');
		
		$sub_category==''?$q=$category:$q=$sub_category;

		if($sortBy=="paling_sesuai"){
			$sortBy = "rating";
			$sortDir= "desc";
		}else if($sortBy=="penjualan"){
			$sortBy = "sales_count";
			$sortDir= "asc";
		}else if($sortBy=="termurah"){
			$sortBy = "price";
			$sortDir= "asc";
		}else if($sortBy=="termahal"){
			$sortBy = "price";
			$sortDir= "desc";
		}else if($sortBy=="terbaru"){
			$sortBy = "updated_at";
			$sortDir= "desc";
		}
		$q = "";
		$this->load->model('m_product');

		$data = $this->m_product->get_list_product($id,$page,$q,$sortBy,$sortDir,$perPage);
		//print_r($data);	
		echo json_encode($data);
	}

	public function get_detail(){
		$id 		= $this->input->get('id');

		$this->load->model('m_product');

		$data = $this->m_product->get_detail($id);

		echo json_encode($data);
	}

	public function hot_categories(){

		$id 		= $this->input->get('id');

		$this->load->model('m_product');

		$data = $this->m_product->hot_categories($id);

		
		echo json_encode($data);
	}

	public function hot_categories_7(){

		$id 		= '';

		$this->load->model('m_product');

		$data = $this->m_product->hot_categories_7();

		echo json_encode($data);
	}



	function fn_breadcrumb($category,$sub_category){

		$result = '';

		if(isset($sub_category)){
			$result = '<li>'.$category.'</li> <li class="active">'.$sub_category.'</li>';
		}else{
			$result = '<li class="active">'.$category.'</li>';
		}

		return $result;
	}
}
