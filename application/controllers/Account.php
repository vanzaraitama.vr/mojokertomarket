<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index(){	

		if($this->session->userdata('is_logged_in') == true){
			
			$data=array(
				'base_url' 		=> base_url(),
				'title'			=> "Mojokerto Market",
				'header'		=> "Mojokerto Market",
				'mainContent' 	=> "account/profile"
				
			);

			$this->parser->parse('master/template', $data);

		}else{

			redirect(base_url().'auth');
		}	
	}

	public function wishlist(){	

		if($this->session->userdata('is_logged_in') == true){

			$data=array(
				'base_url' 		=> base_url(),
				'title'			=> "Mojokerto Market",
				'header'		=> "Mojokerto Market",
				'mainContent' 	=> "account/wishlist"
				
			);

			$this->parser->parse('master/template', $data);

		}else{

			redirect(base_url().'auth');
		}
	}

	public function checkout(){	

		if($this->session->userdata('is_logged_in') == true){

			$data=array(
				'base_url' 		=> base_url(),
				'title'			=> "Mojokerto Market",
				'header'		=> "Mojokerto Market",
				'mainContent' 	=> "account/checkout"
				
			);

			$this->parser->parse('master/template', $data);

		}else{

			redirect(base_url().'auth');
		}
	}

	public function checkout_address(){	

		if($this->session->userdata('is_logged_in') == true){

			$data=array(
				'base_url' 		=> base_url(),
				'title'			=> "Mojokerto Market",
				'header'		=> "Mojokerto Market",
				'mainContent' 	=> "account/checkout_address"
				
			);

			$this->parser->parse('master/template', $data);

		}else{

			redirect(base_url().'auth');
		}
	}

	public function address(){	

		if($this->session->userdata('is_logged_in') == true){

			$data=array(
				'base_url' 		=> base_url(),
				'title'			=> "Mojokerto Market",
				'header'		=> "Mojokerto Market",
				'mainContent' 	=> "account/address"
				
			);

			$this->parser->parse('master/template', $data);

		}else{

			redirect(base_url().'auth');
		}
	}

	public function transaction(){

		if($this->session->userdata('is_logged_in') == true){

			$data=array(
				'base_url' 		=> base_url(),
				'title'			=> "Mojokerto Market",
				'header'		=> "Mojokerto Market",
				'mainContent' 	=> "account/transaction"
				
			);

			$this->parser->parse('master/template', $data);

		}else{

			redirect(base_url().'auth');
		}
	}

	public function get_profile(){

		if($this->session->userdata('is_logged_in') == true){

			$this->load->model('m_account');

			$profile = $this->m_account->get_profile();

			echo json_encode($profile);

		}else{
			exit();
		}
	}

	public function update_profile(){

		if($this->session->userdata('is_logged_in') == true){
			
			$name = $this->input->post('name');
			$phone_number = $this->input->post('phone_number');

			$this->load->model('m_account');

			$submit = $this->m_account->update_profile($name,$phone_number);
			
			echo json_encode($submit);

		}else{
			exit();
		}

	}

	public function change_password(){

		if($this->session->userdata('is_logged_in') == true){
			
			$old_password 	= $this->input->post('old_password');
			$password 		= $this->input->post('password');
			$password_confirmation = $this->input->post('password_confirmation');

			$this->load->model('m_account');

			$submit = $this->m_account->change_password($old_password,$password,$password_confirmation);
			
			echo json_encode($submit);

		}else{
			exit();
		}
	}

	public function submit_address(){
		if($this->session->userdata('is_logged_in') == true){
			$id 			= $this->input->post('id');
			$act 			= $this->input->post('act');
			$address_name 	= $this->input->post('address_name');
			$consigne 		= $this->input->post('consigne');
			$phone_number  	= $this->input->post('phone_number');
			$address 		= $this->input->post('address');
			$latitude 		= $this->input->post('latitude');
			$logitude 		= $this->input->post('logitude');


			$this->load->model('m_account');

			if($act=="Edit"){
				$submit = $this->m_account->update_address($id,$address,$latitude,$logitude,$address_name,$consigne,$phone_number);
			}else{
				$submit = $this->m_account->add_address($address,$latitude,$logitude,$address_name,$consigne,$phone_number);
			}

			echo json_encode($submit);

		}else{
			exit();
		}
	}

	public function delete_address(){
		if($this->session->userdata('is_logged_in') == true){
			
			$id 	= $this->input->post('id');

			$this->load->model('m_account');

			$submit = $this->m_account->delete_address($id);
			
			echo json_encode($submit);

		}else{
			exit();
		}
	}

	public function forms_address(){
		if($this->session->userdata('is_logged_in') == true){
			
			$id 	= $this->input->get('id');
			$act 	= $this->input->get('act');

			$this->load->model('m_account');

			$address_name 	= NULL;
			$consigne		= NULL;
			$phone_number	= NULL;
			$address 		= NULL;
			$latitude 		= '-25.363';
			$longitude 		= '131.044';

			if($act=="Edit"){
				$profile = $this->m_account->get_profile();

				if(is_array($profile['result']['address'])){

					foreach ($profile['result']['address'] as $key => $value) {
						# code...
						if($key==$id){
							
							$address_name 	=    $value['address_name'];
							$consigne		=    $value['consigne'];
							$phone_number	=    $value['phone_number'];
							$address 		=    $value['address'];
							$latitude 		=    $value['latitude'];
							$longitude 		=    $value['longitude'];
						}
						
					}
				}
			}
			

			$data=array(
				'base_url' 		=> base_url(),
				'title'			=> "Mojokerto Market",
				'header'		=> "Mojokerto Market",
				'mainContent' 	=> "account/forms_address",
				'act'			=> $act,
				'id'			=> $id,
				'address_name'	=> $address_name,
				'consigne'		=> $consigne,
				'phone_number'	=> $phone_number,
				'address'		=> $address,
				'latitude'		=> $latitude,
				'longitude'		=> $longitude
				
			);

			$this->parser->parse('master/template', $data);


		}else{
			redirect(base_url().'auth');
		}
	}

	public function add_cart(){
		if($this->session->userdata('is_logged_in') == true){
			
			$product_id 	= $this->input->post('product_id');
			$qty 			= $this->input->post('qty');

			$this->load->model('m_account');

			$submit = $this->m_account->add_cart($product_id,$qty);
			
			echo json_encode($submit);

		}else{
			exit();
		}
	}

	public function get_cart(){

		if($this->session->userdata('is_logged_in') == true){
		
			$this->load->model('m_account');

			$submit = $this->m_account->get_cart();
			
			echo json_encode($submit);

		}else{
			exit();
		}
	}

	public function update_cart(){

		if($this->session->userdata('is_logged_in') == true){
			
			$item 			= $this->input->post('item');

			if(is_array($item)){
				foreach ($item as $key => $value) {
					# code...
					$qty 	  = 0;
					$item_old = $this->input->post('itemqty_'.$value);
					$item_new = $this->input->post('itemqtyadd_'.$value);
					
					if($item_new>0){
						$product_id	= $value;
						$qty 		= $item_new - $item_old;
						//$qty 		= $item_new;
						
						$this->load->model('m_account');

						$submit = $this->m_account->update_cart($product_id,$qty);
					}	
				}
				
				echo json_encode($submit);

			}else{
				exit();
			}

		}else{
			exit();
		}
	}

	public function delete_cart(){
		if($this->session->userdata('is_logged_in') == true){
			
			$id 	= $this->input->post('id');

			$this->load->model('m_account');

			$submit = $this->m_account->delete_cart($id);
			
			echo json_encode($submit);

		}else{
			exit();
		}
	}

	public function add_wishlists(){
		if($this->session->userdata('is_logged_in') == true){
			
			$product_id 	= $this->input->post('product_id');

			$this->load->model('m_account');

			$submit = $this->m_account->add_wishlists($product_id);
			
			echo json_encode($submit);

		}else{
			exit();
		}
	}

	public function get_wishlists(){

		if($this->session->userdata('is_logged_in') == true){
		
			$this->load->model('m_account');

			$submit = $this->m_account->get_wishlists();
			
			echo json_encode($submit);

		}else{
			exit();
		}
	}

	public function delete_wishlists(){
		if($this->session->userdata('is_logged_in') == true){
			
			$id 	= $this->input->post('id');

			$this->load->model('m_account');

			$submit = $this->m_account->delete_wishlists($id);
			
			echo json_encode($submit);

		}else{
			exit();
		}
	}

	public function submit_checkout(){
		if($this->session->userdata('is_logged_in') == true){
			
			$address 			= $this->input->post('address');
			$payment_method 	= $this->input->post('payment_method');
			$distance 			= $this->input->post('distance');

			$this->load->model('m_account');

			$submit = $this->m_account->checkout($address,$payment_method,$distance);
			
			echo json_encode($submit);

		}else{
			exit();
		}
	}

	public function latlng(){
		if($this->session->userdata('is_logged_in') == true){
			
			$latlng	= $this->input->get('latlng');

			$this->load->model('m_account');

			$data = $this->m_account->latlng($latlng);
			
			$result = array('status' => 'success', 'data' => $data['results'][1]['formatted_address']);

			echo json_encode($result);

		}else{
			exit();
		}
	}

	/*public function get_list_transaction(){
		if($this->session->userdata('is_logged_in') == true){
			
			$page	= $this->input->get('page');

			$this->load->model('m_account');

			$result = $this->m_account->get_list_transaction($page);

			echo json_encode($result);

		}else{
			exit();
		}
	}*/

	public function get_list_transaction(){
		if($this->session->userdata('is_logged_in') == true){
			
			$draw=$_REQUEST['draw'];
			$length=$_REQUEST['length'];
			$start=$_REQUEST['start'];
			$search=$_REQUEST['search']["value"];
			
			$order=$_REQUEST['order'][0]["column"];
			$dir=$_REQUEST['order'][0]["dir"];

			$this->load->library('encryption');
			$this->load->model('m_account');

			$sortBy 	= "created_at";
			$sortDir	= "desc";
			$page 		= ($start/$length)+1;
			$status		= "processed";

			$data =  $this->m_account->get_list_transaction($page,$sortBy,$sortDir,$length,$status);
			
			$output=array();
			$output['draw']=$draw;
			$output['recordsTotal']=$output['recordsFiltered']=$data['result']['total'];
			$output['data']=array();
			
			
			$nomor_urut=$start+1;
			setlocale(LC_ALL, 'id_ID');
			
			foreach ($data['result']['data'] as $rows =>$row) {
				

				$iconAction = "<a href='".base_url()."account/view_transaction_detail/".$row['invoice']."' class='pointer'title='Proses' ><span>&nbsp;&nbsp;<i class='fa fa-edit'></i></span></a>";   

				if($row['status']=="processed"){
					$status= "Transaksi Sedang di Proses";
				}else if($row['status']=="paid"){
					$status= "Transaksi Sudah di bayar, menunggu Konfirmasi Penjual";
					$iconAction ="";
				}else if($row['status']=="pending_payment"){
					$status= "Menunggu Pembayaran";
				}else if($row['status']=="pending_admin_approval"){
					$status= "Menunggu Konfirmasi Admin";
					$iconAction ="";
				}else if($row['status']=="shipped"){
					$status= "Proses Pengiriman Barang";
					$iconAction = "<a onClick='del_data(".$row['id'].");' class='pointer'title='Proses' ><span>&nbsp;&nbsp;<i class='fa fa-edit'></i></span></a>"; 
				}else if($row['status']=="complete"){
					$status= "Selesai";
					$iconAction ="";
				}else{
					$status= $row['status'];
					$iconAction ="";
				}

			              			
               	$req_date 		= strftime( "%d %B %Y Pukul %H:%M", strtotime($row['order_expired_date']));
				$output['data'][]=array(
					$nomor_urut, 
					$row['invoice'], 
					$status, 
					"Rp " . number_format($row['shipping_cost'],2,',','.'),
					"Rp " . number_format($row['grandtotal'],2,',','.'),
					$req_date,
					$iconAction
				);
				$nomor_urut++;
			}
			
			echo json_encode($output);


		}else{
			exit();
		}
	}

	public function view_transaction_detail(){
		if($this->session->userdata('is_logged_in') == true){
			$this->load->library('encryption');

			$parent_page	= $this->uri->segment(1);
			$page			= $this->uri->segment(2);
			//$invoice		= $this->encryption->decrypt(base64_decode($this->uri->segment(3)));
			$invoice		= $this->uri->segment(3);
			

			$this->load->model('m_account');

			$detail = $this->m_account->account_transactions($invoice);

			$btn_action = false;
			/*if($detail['result']['status']=="processed"){
					$status_transaksi= "Transaksi Sedang di Proses";
				}else if($detail['result']['status']=="paid"){
					$status_transaksi= "Transaksi Sudah di bayar, menunggu Konfirmasi Penjual";
				}else if($detail['result']['status']=="pending_payment"){
					$status_transaksi= "Menunggu Pembayaran";
					$btn_action = true;
				}else{
					$status_transaksi= $detail['result']['status'];
				}
			*/
		
			foreach ($detail['result']['data'] as $key => $value) {
				# code...
				if($value['status']=="processed"){
					$status_transaksi= "Transaksi Sedang di Proses";
				}else if($value['status']=="paid"){
					$status_transaksi= "Transaksi Sudah di bayar, menunggu Konfirmasi Penjual";
				}else if($value['status']=="pending_admin_approval"){
					$status= "Menunggu Konfirmasi Admin";
				}else if($value['status']=="pending_payment"){
					$status_transaksi= "Menunggu Pembayaran";
					$btn_action = true;
				}else if($value['status']=="complete"){
					$status= "Selesai";
				}else{
					$status_transaksi= $value['result']['status'];
				}
			}

			setlocale(LC_ALL, 'id_ID');
				
			$data=array(
				'base_url' 		=> base_url(),
				'title'			=> "Mojokerto Market",
				'header'		=> "Mojokerto Market",
				'mainContent' 	=> "store/view_store_transaction_detail_customer",
				'btn_action'	=> $btn_action,
				'invoice' 		=> $detail['result']['invoice'],
				'detail_data' 	=> $detail['result']['data'],
				'amount'		=> $detail['result']['grandtotal'],
				'shipping_detail_user_name'		=> $detail['result']['shipping_detail']['user_name'],
				'shipping_detail_address_name'	=> $detail['result']['shipping_detail']['address_name'],
				'shipping_detail_consigne'		=> $detail['result']['shipping_detail']['consigne'],
				'shipping_detail_address'		=> $detail['result']['shipping_detail']['address'],
				'shipping_detail_phone_number'	=> $detail['result']['shipping_detail']['phone_number'],
				'shipping_detail_cost'			=> number_format($detail['result']['shipping_detail']['cost'],2,',','.'),
				'payment_method_bank_name'		=> $detail['result']['payment_method']['bank_name'],
				'payment_method_account_name'	=> $detail['result']['payment_method']['account_name'],
				'payment_method_account_number'	=> $detail['result']['payment_method']['account_number'],
				'payment_method_image_url'		=> $detail['result']['payment_method']['image_url'],
				'payment_method_method'			=> $detail['result']['payment_method']['method'],
				'grandtotal'					=> number_format($detail['result']['grandtotal'],2,',','.'),
				'expired_date'					=> strftime( "%d %B %Y Pukul %H:%M", strtotime($detail['result']['expired_date'])),
				
			);

			$this->parser->parse('master/template', array_merge($data,$detail));

		}else{
			redirect('auth');
		}
	}

	public function bank_account(){
		if($this->session->userdata('is_logged_in') == true){
			
			$this->load->model('m_account');

			$result = $this->m_account->bank_account();

			echo json_encode($result);
		}else{
			exit();
		}
	}

	public function payment_confirmation(){

		if($this->session->userdata('is_logged_in') == true){

			$invoice	= $this->input->post('invoice');
			$amount 	= $this->input->post('amount');

			if(isset($_FILES['image_url']['name'])){

				$files_name 	= $_FILES['image_url']['name'];
				$files_type 	= $_FILES['image_url']['type'];
				$files_tmp_name = $_FILES['image_url']['tmp_name'];
				$files_error	= $_FILES['image_url']['error'];
				$files_size 	= $_FILES['image_url']['size'];   

			}
			
			$this->load->model('m_account');

			$result = $this->m_account->payment_confirmation($invoice,$amount,$files_type,$files_tmp_name);

			echo $result;

		}else{
			exit();
		}
	}

	public function transaction_complete(){

		if($this->session->userdata('is_logged_in') == true){

			$id	= $this->input->post('id');

			$this->load->model('m_account');

			$result = $this->m_account->transaction_complete($id);

			echo json_encode($result);

		}else{
			exit();
		}
	}
}
