<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Store extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index(){	

		if($this->session->userdata('is_logged_in') == true){
			

			$this->load->model('m_store');

			$data = $this->m_store->store_profile();
			
			$id 		= NULL;
			$user_id 	= NULL;
			$name 		= NULL;
			$address	= NULL;
			$phone_number= NULL;
			$image		= NULL;
			$description= NULL;
			$latitude	= NULL;
			$longitude	= NULL;
			$closed		= NULL;
			$image_url	= NULL;
			$mainContent= 'store/index_not_found';

			if($data['status']=='success'){

				$id 		= $data['result']['id'];
				$user_id 	= $data['result']['user_id'];
				$name 		= $data['result']['name'];
				$address	= $data['result']['address'];
				$phone_number= $data['result']['phone_number'];
				$image		= $data['result']['image'];
				$description= $data['result']['description'];
				$latitude	= $data['result']['latitude'];
				$longitude	= $data['result']['longitude'];
				$closed		= $data['result']['closed'];
				$image_url	= $data['result']['image_url'];
				$mainContent= 'store/index';

			}

			$data=array(
				'base_url' 		=> base_url(),
				'title'			=> "Mojokerto Market",
				'header'		=> "Mojokerto Market",
				'mainContent' 	=> $mainContent,
				'id' 			=> $id,
				'user_id'		=> $user_id,
				'name'			=> $name,
				'address'		=> $address,
				'phone_number'	=> $phone_number,
				'image'			=> $image,
				'description'	=> $description,
				'latitude'		=> $latitude,
				'longitude'		=> $longitude,
				'closed'		=> $closed,
				'image_url'		=> $image_url

			);

			$this->parser->parse('master/template', $data);

		}else{
			redirect(base_url().'auth');
		}	
	}


	public function forms_store(){
		if($this->session->userdata('is_logged_in') == true){
			
			$id = $this->input->get('id');


			$this->load->model('m_store');

			$data = $this->m_store->store_profile();

			$id 		= NULL;
			$user_id 	= NULL;
			$name 		= NULL;
			$address	= NULL;
			$phone_number= NULL;
			$image		= NULL;
			$description= NULL;
			$latitude	= NULL;
			$longitude	= NULL;
			$closed		= NULL;
			$image_url	= NULL;

			if($data['status']=='success'){

				$id 		= $data['result']['id'];
				$user_id 	= $data['result']['user_id'];
				$name 		= $data['result']['name'];
				$address	= $data['result']['address'];
				$phone_number= $data['result']['phone_number'];
				$image		= $data['result']['image'];
				$description= $data['result']['description'];
				$latitude	= $data['result']['latitude'];
				$longitude	= $data['result']['longitude'];
				$closed		= $data['result']['closed'];
				$image_url	= $data['result']['image_url'];

			}

			$data=array(
				'base_url' 		=> base_url(),
				'title'			=> "Mojokerto Market",
				'header'		=> "Mojokerto Market",
				'mainContent' 	=> "store/forms_store",
				'act'			=> 'Edit',
				'id' 			=> $id,
				'user_id'		=> $user_id,
				'name'			=> $name,
				'address'		=> $address,
				'phone_number'	=> $phone_number,
				'image'			=> $image,
				'description'	=> $description,
				'latitude'		=> $latitude,
				'longitude'		=> $longitude,
				'closed'		=> $closed,
				'image_url'		=> $image_url
				
			);

			$this->parser->parse('master/template', $data);

		}else{
			exit();
		}
	}

	public function product_detail_add(){
		if($this->session->userdata('is_logged_in') == true){
			

			$categories = $this->m_categories->categories();

			$list_categories = '';
			if(is_array($categories)){
				foreach ($categories['result'] as $key => $value) {
					# code...
					# 
					$list_categories .='<option value="'.$value['id'].'" >'.$value['name'].'</option>';
				}
			}

			$data=array(

					'base_url' 		=> base_url(),
					'title'			=> "Mojokerto Market",
					'header'		=> "Mojokerto Market",
					'mainContent' 	=> "store/forms_product",
					'act'			=> 'Add',
					'id' 	 		=> '',
					'shop_id' 		=> '',
					'name' 			=> '',
					'slug' 			=> '',
					'images'		=> '',
					'description'	=> '',
					'stock' 		=> '',
					'price' 		=> '',
					'discount'		=> '0',
					'rating'		=> '',
					'status'		=> '',
					'category_id'	=> '',
					'images_url' 	=> '',
					'price_after'	=> '',
					'sales_count'	=> '',
					'list_categories' => $list_categories
					
				);

			$this->parser->parse('master/template', $data);
		}else{
			redirect('home');
		}
	}

	public function product_detail($id){
		if($this->session->userdata('is_logged_in') == true){

			$this->load->model('m_categories');

			$data = $this->store_get_product_detail($id);
			
			$categories = $this->m_categories->categories();

			$list_categories = '';
			if(is_array($categories)){
				foreach ($categories['result'] as $key => $value) {
					# code...
					$selected ='';
					if($data['result']['category_id']==$value['id']){
						$selected = 'selected';
					}

					$list_categories .='<option value="'.$value['id'].'" '.$selected.'>'.$value['name'].'</option>';
				}
			}

			if($data['status']=="success"){
			
				$data=array(

					'base_url' 		=> base_url(),
					'title'			=> "Mojokerto Market",
					'header'		=> "Mojokerto Market",
					'mainContent' 	=> "store/forms_product",
					'act'			=> 'Edit',
					'id' 	 		=> $data['result']['id'],
					'shop_id' 		=> $data['result']['shop_id'],
					'name' 			=> $data['result']['name'],
					'slug' 			=> $data['result']['slug'],
					'images'		=> $data['result']['images'],
					'description'	=> $data['result']['description'],
					'stock' 		=> $data['result']['stock'],
					'price' 		=> $data['result']['price'],
					'discount'		=> $data['result']['discount'],
					'rating'		=> $data['result']['rating'],
					'status'		=> $data['result']['status'],
					'category_id'	=> $data['result']['category_id'],
					'images_url' 	=> json_encode($data['result']['images_url']),
					'price_after'	=> $data['result']['price_after'],
					'sales_count'	=> $data['result']['sales_count'],
					'list_categories' => $list_categories
					
				);
				
				$this->parser->parse('master/template', $data);

			}else{
				redirect('store');
			}

		}else{
			redirect('home');
		}
	}

	public function store_update(){

		if($this->session->userdata('is_logged_in') == true){

			$name 		= $this->input->post('name');
			$address 	= $this->input->post('address');
			$phone_number = $this->input->post('phone_number');
			$closed 	= $this->input->post('closed');
			$latitude 	= $this->input->post('latitude');
			$longitude 	= $this->input->post('longitude');
			$description= $this->input->post('description');

			if(isset($_FILES['image_url']['name'])){

				$files_name 	= $_FILES['image_url']['name'];
				$files_type 	= $_FILES['image_url']['type'];
				$files_tmp_name = $_FILES['image_url']['tmp_name'];
				$files_error	= $_FILES['image_url']['error'];
				$files_size 	= $_FILES['image_url']['size'];   

			}
			
			$this->load->model('m_store');

			$result = $this->m_store->store_update($name,$address,$phone_number,$closed,$latitude,$longitude,$description,
											$files_name,$files_type,$files_tmp_name,$files_error,$files_size);
			echo $result;

		}else{
			exit();
		}
	}

	public function store_get_product(){

		if($this->session->userdata('is_logged_in') == true){

			$page 		= $this->input->get('page');
			$sortBy 	= $this->input->get('sortBy');
			$perPage 	= $this->input->get('perPage');

			if($sortBy=="paling_sesuai"){
				$sortBy = "name";
				$sortDir= "desc";
			}else if($sortBy=="penjualan"){
				$sortBy = "sales_count";
				$sortDir= "desc";
			}else if($sortBy=="termurah"){
				$sortBy = "price";
				$sortDir= "asc";
			}else if($sortBy=="termahal"){
				$sortBy = "price";
				$sortDir= "desc";
			}else if($sortBy=="terbaru"){
				$sortBy = "updated_at";
				$sortDir= "desc";
			}

			$this->load->model('m_store');

			$result = $this->m_store->store_product($page,$sortBy,$sortDir,$perPage);

			echo json_encode($result);

		}else{
			exit();
		}
	}

	public function store_get_product_detail($id){

		if($this->session->userdata('is_logged_in') == true){

			$this->load->model('m_store');

			$result = $this->m_store->store_product_detail($id);

			return $result;

		}else{
			exit();
		}
	}

	public function product_submit(){

		if($this->session->userdata('is_logged_in') == true){

			$act 		= $this->input->post('act');
			
			$id 		= $this->input->post('id');
			$name 		= $this->input->post('name');
			$stock 		= $this->input->post('stock');
			$price 		= str_replace(',','',$this->input->post('price'));
			$discount	= $this->input->post('discount');
			$category_id= $this->input->post('category_id');
			$description= $this->input->post('description');
			
			$this->load->model('m_store');
			$postfields['images'] = array();
			if(isset($_FILES['file_attach']['name'])){

				$files = $_FILES;
				$files_count = count($_FILES['file_attach']['name']);

				if($files_count>0){

					for($i=0; $i< $files_count; $i++){  
						$cfile =''; 
						$_FILES['file_attach']['name'] 		= $files['file_attach']['name'][$i];
					    $_FILES['file_attach']['type'] 		= $files['file_attach']['type'][$i];
					    $_FILES['file_attach']['tmp_name'] 	= $files['file_attach']['tmp_name'][$i];
					    $_FILES['file_attach']['error'] 	= $files['file_attach']['error'][$i];
					    $_FILES['file_attach']['size'] 		= $files['file_attach']['size'][$i];  
					    if($_FILES['file_attach']['tmp_name']!=''){
					    	$cfile = curl_file_create($_FILES['file_attach']['tmp_name'],$_FILES['file_attach']['type'],'image_field');
							$postfields['images'][$i] = $cfile;
					    }
					    
					}
				} 

			}
			
			if($act=='Edit'){
				
				$result = $this->m_store->store_product_update($id,$name,$stock,$price,$discount,$category_id,$description,$postfields);

			}else{

				$result = $this->m_store->store_product_add($name,$stock,$price,$discount,$category_id,$description,$postfields);
			}

			echo $result;

		}else{
			exit();
		}
	}

	public function store_product_delete(){

		if($this->session->userdata('is_logged_in') == true){

			$id 		= $this->input->post('id');

			$this->load->model('m_store');

			$result = $this->m_store->store_product_delete($id);

			echo json_encode($result);

		}else{
			exit();
		}
	}

	public function store_transaction(){

		if($this->session->userdata('is_logged_in') == true){

			$draw=$_REQUEST['draw'];
			$length=$_REQUEST['length'];
			$start=$_REQUEST['start'];
			$search=$_REQUEST['search']["value"];
			
			$order=$_REQUEST['order'][0]["column"];
			$dir=$_REQUEST['order'][0]["dir"];

			$this->load->library('encryption');
			$this->load->model('m_store');

			$sortBy 	= "created_at";
			$sortDir	= "desc";
			$page 		= ($start/$length)+1;
			$status		= "paid";

			$data =  $this->m_store->store_transaction($page,$sortBy,$sortDir,$length,$status);
			
			$output=array();
			$output['draw']=$draw;
			$output['recordsTotal']=$output['recordsFiltered']=$data['result']['total'];
			$output['data']=array();
			
			
			$nomor_urut=$start+1;
			setlocale(LC_ALL, 'id_ID');
			
			foreach ($data['result']['data'] as $rows =>$row) {
				
				
			    $iconAction = "<a href='".base_url()."store/view_store_transaction_detail/".base64_encode($this->encryption->encrypt($row['id']))."' class='pointer'title='Proses' ><span>&nbsp;&nbsp;<i class='fa fa-edit'></i></span></a>";             			
               	$req_date 		= strftime( "%d %B %Y %H:%M", strtotime($row['order_expired_date']));
				$output['data'][]=array(
					$nomor_urut, 
					$row['invoice'], 
					//$row['status'], 
					"Rp " . number_format($row['shipping_cost'],2,',','.'),
					"Rp " . number_format($row['grandtotal'],2,',','.'),
					$req_date,
					//$row['user_name'],
					$iconAction
				);
				$nomor_urut++;
			}
			
			echo json_encode($output);

		}else{
			exit();
		}
	}

	public function store_transaction_shipped(){

		if($this->session->userdata('is_logged_in') == true){

			$draw=$_REQUEST['draw'];
			$length=$_REQUEST['length'];
			$start=$_REQUEST['start'];
			$search=$_REQUEST['search']["value"];
			
			$order=$_REQUEST['order'][0]["column"];
			$dir=$_REQUEST['order'][0]["dir"];

			$this->load->library('encryption');
			$this->load->model('m_store');

			$sortBy 	= "created_at";
			$sortDir	= "desc";
			$page 		= ($start/$length)+1;
			$status		= "processed";

			$data =  $this->m_store->store_transaction_shipped($page,$sortBy,$sortDir,$length,$status);
			
			$output=array();
			$output['draw']=$draw;
			$output['recordsTotal']=$output['recordsFiltered']=$data['result']['total'];
			$output['data']=array();
			
			
			$nomor_urut=$start+1;
			setlocale(LC_ALL, 'id_ID');
			
			foreach ($data['result']['data'] as $rows =>$row) {
				
				
			    $iconAction = "<a href='".base_url()."store/view_store_transaction_detail_shipped/".base64_encode($this->encryption->encrypt($row['id']))."' class='pointer'title='Proses' ><span>&nbsp;&nbsp;<i class='fa fa-edit'></i></span></a>";             			
               	$req_date 		= strftime( "%d %B %Y %H:%M", strtotime($row['order_expired_date']));
				$output['data'][]=array(
					$nomor_urut, 
					$row['invoice'], 
					//$row['status'], 
					"Rp " . number_format($row['shipping_cost'],2,',','.'),
					"Rp " . number_format($row['grandtotal'],2,',','.'),
					$req_date,
					//$row['user_name'],
					$iconAction
				);
				$nomor_urut++;
			}
			
			echo json_encode($output);

		}else{
			exit();
		}
	}


	public function store_transaction_shipped_2(){

		if($this->session->userdata('is_logged_in') == true){

			$draw=$_REQUEST['draw'];
			$length=$_REQUEST['length'];
			$start=$_REQUEST['start'];
			$search=$_REQUEST['search']["value"];
			
			$order=$_REQUEST['order'][0]["column"];
			$dir=$_REQUEST['order'][0]["dir"];

			$this->load->library('encryption');
			$this->load->model('m_store');

			$sortBy 	= "created_at";
			$sortDir	= "desc";
			$page 		= ($start/$length)+1;
			$status		= "shipped";

			$data =  $this->m_store->store_transaction_shipped($page,$sortBy,$sortDir,$length,$status);
			
			$output=array();
			$output['draw']=$draw;
			$output['recordsTotal']=$output['recordsFiltered']=$data['result']['total'];
			$output['data']=array();
			
			
			$nomor_urut=$start+1;
			setlocale(LC_ALL, 'id_ID');
			
			foreach ($data['result']['data'] as $rows =>$row) {
				
				
			    $iconAction = "<a href='".base_url()."store/view_store_transaction_detail_shipped/".base64_encode($this->encryption->encrypt($row['id']))."' class='pointer'title='Proses' ><span>&nbsp;&nbsp;<i class='fa fa-edit'></i></span></a>";             			
               	$req_date 		= strftime( "%d %B %Y %H:%M", strtotime($row['order_expired_date']));
				$output['data'][]=array(
					$nomor_urut, 
					$row['invoice'], 
					//$row['status'], 
					"Rp " . number_format($row['shipping_cost'],2,',','.'),
					"Rp " . number_format($row['grandtotal'],2,',','.'),
					$req_date,
					//$row['user_name'],
					$iconAction
				);
				$nomor_urut++;
			}
			
			echo json_encode($output);

		}else{
			exit();
		}
	}

	public function store_transaction_all(){

		if($this->session->userdata('is_logged_in') == true){

			$draw=$_REQUEST['draw'];
			$length=$_REQUEST['length'];
			$start=$_REQUEST['start'];
			$search=$_REQUEST['search']["value"];
			
			$order=$_REQUEST['order'][0]["column"];
			$dir=$_REQUEST['order'][0]["dir"];

			$this->load->library('encryption');
			$this->load->model('m_store');

			$sortBy 	= "created_at";
			$sortDir	= "desc";
			$page 		= ($start/$length)+1;
			$status		= "";

			$data =  $this->m_store->store_transaction_shipped($page,$sortBy,$sortDir,$length,$status);
			
			$output=array();
			$output['draw']=$draw;
			$output['recordsTotal']=$output['recordsFiltered']=$data['result']['total'];
			$output['data']=array();
			
			
			$nomor_urut=$start+1;
			setlocale(LC_ALL, 'id_ID');
			
			foreach ($data['result']['data'] as $rows =>$row) {
				
				
			    $iconAction = "<a href='".base_url()."store/view_store_transaction_detail_shipped/".base64_encode($this->encryption->encrypt($row['id']))."' class='pointer'title='Proses' ><span>&nbsp;&nbsp;<i class='fa fa-edit'></i></span></a>";             			
               	$req_date 		= strftime( "%d %B %Y %H:%M", strtotime($row['order_expired_date']));
				$output['data'][]=array(
					$nomor_urut, 
					$row['invoice'], 
					//$row['status'], 
					"Rp " . number_format($row['shipping_cost'],2,',','.'),
					"Rp " . number_format($row['grandtotal'],2,',','.'),
					$req_date,
					//$row['user_name'],
					$iconAction
				);
				$nomor_urut++;
			}
			
			echo json_encode($output);

		}else{
			exit();
		}
	}

	public function view_store_transaction_detail(){
		if($this->session->userdata('is_logged_in') == true){
			$this->load->library('encryption');

			$parent_page	= $this->uri->segment(1);
			$page			= $this->uri->segment(2);
			$id				= $this->encryption->decrypt(base64_decode($this->uri->segment(3)));

			$detail 		= $this->store_transaction_detail($id);

				if($detail['result']['status']=="processed"){
					$status_transaksi= "Transaksi Sedang di Proses";
				}else if($detail['result']['status']=="paid"){
					$status_transaksi= "Transaksi Sudah di bayar, menunggu Konfirmasi Penjual";
				}else if($detail['result']['status']=="pending_payment"){
					$status_transaksi= "Menunggu Pembayaran";
				}else if($detail['result']['status']=="shipped"){
					$status_transaksi= "Sedang di Kirim";
				}else if($detail['result']['status']=="complete"){
					$status_transaksi= "Selesai";
				}else{
					$status_transaksi= $detail['result']['status'];
				}

			$data=array(
				'base_url' 		=> base_url(),
				'title'			=> "Mojokerto Market",
				'header'		=> "Mojokerto Market",
				'mainContent' 	=> "store/view_store_transaction_detail",
				'id' 			=> $id,
				'status_transaksi' => $status_transaksi
			);

			$this->parser->parse('master/template', array_merge($data,$detail));

		}else{
			redirect('auth');
		}
	}


	public function view_store_transaction_detail_shipped(){
		if($this->session->userdata('is_logged_in') == true){

			$this->load->library('encryption');

			$parent_page	= $this->uri->segment(1);
			$page			= $this->uri->segment(2);
			$id				= $this->encryption->decrypt(base64_decode($this->uri->segment(3)));

			$detail 		= $this->store_transaction_detail($id);
			$btn_action 	= true;

				if($detail['result']['status']=="processed"){
					$status_transaksi= "Transaksi Sedang di Proses";
				}else if($detail['result']['status']=="paid"){
					$status_transaksi= "Transaksi Sudah di bayar, menunggu Konfirmasi Penjual";
				}else if($detail['result']['status']=="pending_payment"){
					$status_transaksi= "Menunggu Pembayaran";
				}else if($detail['result']['status']=="shipped"){
					$status_transaksi= "Sedang di Kirim";
					$btn_action = false;
				}else if($detail['result']['status']=="complete"){
					$status_transaksi= "Selesai";
					$btn_action = false;
				}else if($detail['result']['status']=="pending_admin_approval"){
					$status_transaksi= "Menunggu Konfirmasi Admin";
					$btn_action = false;
				}else{
					$status_transaksi= $detail['result']['status'];
				}

			$data=array(
				'base_url' 		=> base_url(),
				'title'			=> "Mojokerto Market",
				'header'		=> "Mojokerto Market",
				'mainContent' 	=> "store/view_store_transaction_detail_shipped",
				'id' 			=> $id,
				'status_transaksi' => $status_transaksi,
				'btn_action' 	=> $btn_action
				
			);

			$this->parser->parse('master/template', array_merge($data,$detail));

		}else{
			redirect('auth');
		}
	}
	
	public function store_transaction_detail($id){

		if($this->session->userdata('is_logged_in') == true){

			$this->load->model('m_store');

			$result = $this->m_store->store_transaction_detail($id);

			return $result;

		}else{
			exit();
		}
	}

	public function submit_store_transaction_process(){

		if($this->session->userdata('is_logged_in') == true){

			$id 		= $this->input->post('id');

			$this->load->model('m_store');

			$result = $this->m_store->submit_store_transaction_process($id);

			echo json_encode($result);

		}else{
			exit();
		}
	}

	public function submit_store_transaction_shipped(){

		if($this->session->userdata('is_logged_in') == true){

			$id 		= $this->input->post('id');

			$this->load->model('m_store');

			$result = $this->m_store->submit_store_transaction_shipped($id);

			echo json_encode($result);

		}else{
			exit();
		}
	}

	public function submit_store_transaction_complete(){

		if($this->session->userdata('is_logged_in') == true){

			$id 		= $this->input->post('id');

			$this->load->model('m_store');

			$result = $this->m_store->submit_store_transaction_complete($id);

			echo json_encode($result);

		}else{
			exit();
		}
	}
}
