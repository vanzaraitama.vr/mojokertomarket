<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index(){	

		if($this->session->userdata('is_logged_in') == true){

			$data=array(
				'base_url' 		=> base_url(),
				'title'			=> "Mojokerto Market",
				'header'		=> "Mojokerto Market",
				'mainContent' 	=> "home"
				
			);

		}else{

			$data=array(
				'base_url' 		=> base_url(),
				'title'			=> "Mojokerto Market",
				'header'		=> "Login/Register",
				'mainContent' 	=> "auth"
				
			);
		}

		$this->parser->parse('master/template', $data);
	}

	public function forgot(){

		if($this->session->userdata('is_logged_in') == true){

			$data=array(
				'base_url' 		=> base_url(),
				'title'			=> "Mojokerto Market",
				'header'		=> "Mojokerto Market",
				'mainContent' 	=> "home"
				
			);

		}else{

			$data=array(
				'base_url' 		=> base_url(),
				'title'			=> "Mojokerto Market",
				'header'		=> "Forgot Password",
				'mainContent' 	=> "forgot"
				
			);
		}

		$this->parser->parse('master/template', $data);	
	}

	public function activation(){

		$data=array(
				'base_url' 		=> base_url(),
				'title'			=> "Mojokerto Market",
				'header'		=> "Mojokerto Market",
				'mainContent' 	=> "activation"
				
			);
		
		$this->parser->parse('master/template', $data);
	}

	public function reset_password(){

		$data=array(
				'base_url' 		=> base_url(),
				'title'			=> "Mojokerto Market",
				'header'		=> "Mojokerto Market",
				'mainContent' 	=> "reset_password"
				
			);
		
		$this->parser->parse('master/template', $data);
	}

	public function submit_activation(){

		$code 		= $this->input->post('code');

		$this->load->model('m_auth');

		$submit = $this->m_auth->activate($code);

		echo json_encode($submit);
	}

	public function submit_forgot(){

		$email 		= $this->input->post('email');

		$this->load->model('m_auth');

		$submit = $this->m_auth->forgot($email);

		echo json_encode($submit);
	}

	public function submit_reset_password(){

		$code 		= $this->input->post('code');
		$password	= $this->input->post('password');
		$password_confirmation = $this->input->post('password_confirmation');

		$this->load->model('m_auth');

		$submit = $this->m_auth->reset_password($code,$password,$password_confirmation);

		echo json_encode($submit);
	}

	public function login(){

		$email 		= $this->input->post('email');
		$password	= $this->input->post('password');

		$this->load->model('m_auth');

		$submit = $this->m_auth->login($email,$password);

		if($submit['status']=="success"){
			
				$sess	= array(
								'id'				=> $submit['result']['id'],
								'name'				=> $submit['result']['name'],
								'email'				=> $submit['result']['email'],
								'address'			=> $submit['result']['address'],
								'phone_number'		=> $submit['result']['phone_number'],
								'imei' 				=> $submit['result']['imei'],
								//'latitude' 			=> $submit['result']['latitude'],
								//'longitude'			=> $submit['result']['longitude'],
								'token' 			=> $submit['result']['token'],
								'saldo'				=> $submit['result']['saldo'],
								'is_logged_in'		=> true
							);

				$this->session->set_userdata($sess);

			$result = array("status"=>$submit['status'], "title"=>"Login Success", "message"=>"Please Wait");	

		}else{

			$result = array("status"=>$submit['status'], "title"=>"Login Fail", "message"=> $submit["message"] );	
		}

		echo json_encode($result);

	}

	public function register(){

		$email 		= $this->input->post('email');
		$password 	= $this->input->post('password');
		$password_confirmation = $this->input->post('password_confirmation');
		$name 		= $this->input->post('name');
		$phone_number		   = $this->input->post('phone_number');

		$this->load->model('m_auth');

		$submit = $this->m_auth->register($email,$password,$password_confirmation,$name,$phone_number);

		if($submit['status']=="success"){

			$result = array("status"=>$submit['status'], "title"=>"Register Success", "message"=>$submit["message"]);

		}else{

			$result = array("status"=>$submit['status'], "title"=>"Register Fail", "message"=> $submit["result"]);
		}
		
		echo json_encode($result);
	}


	public function logout(){

		$this->load->model('m_auth');

		$submit = $this->m_auth->logout();

		$this->session->sess_destroy();

		redirect(base_url());
	}
}
