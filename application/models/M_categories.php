<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_categories extends CI_Model {

	public function categories(){

		//$data = $this->restclient->post("categories", [
         
        //]);
		//var_dump($data);
		//return $data;
		//
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "http://api.mojokertomarket.com/mobile/v1/categories",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "",
		  CURLOPT_HTTPHEADER => array(
		    "Content-Type: application/x-www-form-urlencoded",
		    "Postman-Token: 624c532d-6ebd-4e2c-9823-2847392ac851",
		    "cache-control: no-cache"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		return json_decode($response,true);
	}
}