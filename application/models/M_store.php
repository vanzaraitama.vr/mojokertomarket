<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_store extends CI_Model {

	public function store_profile(){

		$data = $this->restclient->post("shop-apis/shop", [
            'token' => $this->session->userdata('token')
        ]);
		
		return $data;
	}

	public function store_update($name,$address,$phone_number,$closed,$latitude,$longitude,$description,
								$files_name,$files_type,$files_tmp_name,$files_error,$files_size){
			

		    $headers = array("Content-Type:multipart/form-data");

		    $postfields = array(
		    	"token" => $this->session->userdata('token'),
		    	"name" => $name,
		    	"address" => $address,
		    	"phone_number" => $phone_number,
		    	"closed" => $closed,
		    	"latitude" => $latitude,
		    	"longitude" => $longitude,
		    	"description" => $description
		    	
		    );

		    $url = 'http://api.mojokertomarket.com/mobile/v1/shop-apis/shop/update';
			if($files_tmp_name<>''){
				$cfile = curl_file_create("$files_tmp_name","$files_type",'image_field');
				$postfields['image'] = $cfile;	
			}
			
		    $ch = curl_init();

		    $options = array(
		        CURLOPT_URL => $url,
		        CURLOPT_HEADER => false,
		        CURLOPT_POST => 1,
		        CURLOPT_HTTPHEADER => $headers,
		        CURLOPT_POSTFIELDS => $postfields,
		        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		        CURLOPT_RETURNTRANSFER => true
		    ); 

		   curl_setopt_array($ch, $options);

		   $result = curl_exec($ch);
		   curl_close($ch);
		   
		   return $result;
	}


	public function store_product($page,$sortBy,$sortDir,$perPage){

		$data = $this->restclient->post("shop-apis/products", [
            'token' => $this->session->userdata('token'),
            'page' => $page,
            'sortBy' => $sortBy,
            'sortDir' => $sortDir,
            'perPage' => $perPage
        ]);

		return $data;
	}

	public function store_product_detail($id){

		$data = $this->restclient->post("shop-apis/products/$id", [
            'token' => $this->session->userdata('token')
        ]);

		return $data;
	}

	public function store_product_add($name,$stock,$price,$discount,$category_id,$description,$postfields_image){

		$headers = array("Content-Type:multipart/form-data");

		    $postfields = array(
		    	'token' => $this->session->userdata('token'),
		    	'name' => $name,
	            'stock' => $stock,
	            'price' => $price,
	            'discount' => $discount,
	            'category_id' => $category_id,
	            'description' => $description,
	            'weight' => '0',
	            'height' => '0'
		    	
		    );

		    $url = 'http://api.mojokertomarket.com/mobile/v1/shop-apis/products/add';


		    if(is_array($postfields_image)){
		    	$no = 0;
		    	foreach ($postfields_image['images'] as $key => $value) {
					# code...
					$postfields['images['.$no.']'] = $value;	
					$no++;
				}
		    }
			
		    $ch = curl_init();

		    $options = array(
		        CURLOPT_URL => $url,
		        CURLOPT_HEADER => false,
		        CURLOPT_POST => 1,
		        CURLOPT_HTTPHEADER => $headers,
		        CURLOPT_POSTFIELDS => $postfields,
		        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		        CURLOPT_RETURNTRANSFER => true
		    ); 

		   curl_setopt_array($ch, $options);

		   $result = curl_exec($ch);
		   curl_close($ch);
		   
		   return $result;
	}

	public function store_product_update($id,$name,$stock,$price,$discount,$category_id,$description,$postfields_image){

			$headers = array("Content-Type:multipart/form-data");

		    $postfields = array(
		    	'token' => $this->session->userdata('token'),
		    	'name' => $name,
	            'stock' => $stock,
	            'price' => $price,
	            'discount' => $discount,
	            'category_id' => $category_id,
	            'description' => $description,
	            'weight' => '0',
	            'height' => '0'
		    	
		    );
		    //print_r($postfields);

		    $url = 'http://api.mojokertomarket.com/mobile/v1/shop-apis/products/'.$id.'/update';


		    if(is_array($postfields_image)){
		    	$no = 0;
		    	foreach ($postfields_image['images'] as $key => $value) {
					# code...
					$postfields['images['.$no.']'] = $value;	
					$no++;
				}
		    }
			

		    $ch = curl_init();

		    $options = array(
		        CURLOPT_URL => $url,
		        CURLOPT_HEADER => false,
		        CURLOPT_POST => 1,
		        CURLOPT_HTTPHEADER => $headers,
		        CURLOPT_POSTFIELDS => $postfields,
		        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		        CURLOPT_RETURNTRANSFER => true
		    ); 

		   curl_setopt_array($ch, $options);

		   $result = curl_exec($ch);
		   curl_close($ch);
		   print_r($result);
		   //return $result;
	}

	public function store_product_delete($id){

		$data = $this->restclient->post("shop-apis/products/$id/delete", [
            'token' => $this->session->userdata('token')
        ]);

		return $data;
	}

	public function store_transaction($page,$sortBy,$sortDir,$length,$status){

		$data = $this->restclient->post("shop-apis/transactions", [
         	'token' => $this->session->userdata('token'),
            'page' => $page,
            'sortBy' => $sortBy,
            'sortDir' => $sortDir,
            'perPage' => $length,
            'status' => $status
        ]);

		return $data;
	}

	public function store_transaction_shipped($page,$sortBy,$sortDir,$length,$status){

		$data = $this->restclient->post("shop-apis/transactions", [
         	'token' => $this->session->userdata('token'),
            'page' => $page,
            'sortBy' => $sortBy,
            'sortDir' => $sortDir,
            'perPage' => $length,
            'status' => $status
        ]);

		return $data;
	}

	public function store_transaction_detail($id){
		//echo $id;
		$data = $this->restclient->post("shop-apis/transactions/$id", [
            'token' => $this->session->userdata('token')
        ]);
		//print_r($data);
		return $data;
	}

	public function submit_store_transaction_process($id){

		$data = $this->restclient->post("shop-apis/transactions/$id/processed", [
            'token' => $this->session->userdata('token')
        ]);

		return $data;
	}

	public function submit_store_transaction_shipped($id){

		$data = $this->restclient->post("shop-apis/transactions/$id/shipped", [
            'token' => $this->session->userdata('token')
        ]);
		
		return $data;
	}

	public function submit_store_transaction_complete($id){

		$data = $this->restclient->post("transaction-complete/$id", [
            'token' => $this->session->userdata('token')
        ]);

		return $data;
	}
}