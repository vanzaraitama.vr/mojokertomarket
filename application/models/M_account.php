<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_account extends CI_Model {

	public function get_profile(){

		$data = $this->restclient->post("get-profile", [
            'token' => $this->session->userdata('token')
        ]);

		return $data;
	}

	public function update_profile($name,$phone_number){

		$data = $this->restclient->post("update-profile", [
            'token' => $this->session->userdata('token'),
            'name' 	=> $name,
            'phone_number' => $phone_number
        ]);

		return $data;
	}

	public function change_password($old_password,$password,$password_confirmation){

		$data = $this->restclient->post("change-password", [
            'token' => $this->session->userdata('token'),
            'old_password' 	=> $old_password,
            'password' => $password,
            'password_confirmation' => $password_confirmation
        ]);

		return $data;
	}

	public function add_address($address,$latitude,$longitude,$address_name,$consigne,$phone_number){

		$data = $this->restclient->post("add-address", [
            'token' => $this->session->userdata('token'),
            'address' 	=> $address,
            'latitude' 	=> $latitude,
            'longitude' => $longitude,
            'address_name' 	=> $address_name,
            'consigne' 	=> $consigne,
            'phone_number' => $phone_number
        ]);

		return $data;
	}

	public function update_address($id,$address,$latitude,$longitude,$address_name,$consigne,$phone_number){

		$data = $this->restclient->post("update-address", [
            'token' => $this->session->userdata('token'),
            'key' 	=> $id,
            'address' 	=> $address,
            'latitude' 	=> $latitude,
            'longitude' => $longitude,
            'address_name' 	=> $address_name,
            'consigne' 	=> $consigne,
            'phone_number' => $phone_number
        ]);

		return $data;
	}

	public function delete_address($id){

		$data = $this->restclient->post("delete-address", [
            'token' => $this->session->userdata('token'),
            'key' 	=> $id
        ]);

		return $data;
	}

	public function add_cart($product_id,$qty){

		$data = $this->restclient->post("carts/add", [
            'token' => $this->session->userdata('token'),
            'product_id' => $product_id,
            'qty' => $qty
        ]);

		return $data;
	}

	public function get_cart(){

		$data = $this->restclient->post("carts", [
            'token' => $this->session->userdata('token')
        ]);

		return $data;
	}

	public function update_cart($product_id,$qty){

		$data = $this->restclient->post("carts/add", [
            'token' => $this->session->userdata('token'),
            'product_id' => $product_id,
            'qty' => $qty
        ]);

		return $data;
	}

	public function delete_cart($id){

		$data = $this->restclient->post("carts/delete", [
            'token' => $this->session->userdata('token'),
            'id' 	=> $id
        ]);

		return $data;
	}

	public function clear_cart(){

		$data = $this->restclient->post("carts/clear", [
            'token' => $this->session->userdata('token')
        ]);

		return $data;
	}

	public function add_wishlists($product_id){

		$data = $this->restclient->post("wishlists/add", [
            'token' => $this->session->userdata('token'),
            'product_id' => $product_id
        ]);

		return $data;
	}

	public function get_wishlists(){

		$data = $this->restclient->post("wishlists", [
            'token' => $this->session->userdata('token')
        ]);

		return $data;
	}

	public function delete_wishlists($id){

		$data = $this->restclient->post("wishlists/delete", [
            'token' => $this->session->userdata('token'),
            'id' 	=> $id
        ]);

		return $data;
	}

	public function checkout($address,$payment_method,$distance){

	 /*$destin_address = $this->get_profile();
		
		foreach ($destin_address['result']['address'] as $key => $value) {
			# code...
			if($key==$address){
				$desti_lat = $value['latitude'];
				$desti_lng = $value['longitude'];
			}
		}

		$origin_address = $this->get_cart();

		foreach ($origin_address['result']['data'] as $key => $value) {
			# code...
			$origin_lat = $value['shop']['latitude'];
			$origin_lng = $value['shop']['longitude'];
		}


		$distance = $this->get_distance($origin_lat,$origin_lng,$desti_lat,$desti_lng);
		//print_r($distance);
		$distance = $this->milesToKilometers(str_replace('mi', '', trim($distance['rows'][0]['elements'][0]['distance']['text'])));
		echo $distance;*/
		//$destin_address = 
		
		$data = $this->restclient->post("checkout", [
            'token' => $this->session->userdata('token'),
            'address' 	=> $address,
            'payment_method' 	=> $payment_method,
            'distance' 	=> $distance
        ]);
		
		return $data;
	}

	public function latlng($latlng){

		$data = $this->restclient->get("https://maps.googleapis.com/maps/api/geocode/json", [
            'latlng' => $latlng,
            'key' 	=> 'AIzaSyBp3E-Zv44uHcv41brVZH3gdrke4_sWFbU'
        ]);

		return $data;
	}

	public function get_list_transaction($page){

		$data = $this->restclient->post("transactions", [
            'token' => $this->session->userdata('token'),
            'search' 	=> '',
            'status' 	=> '',
            'sortBy' 	=> 'id',
            'page' 	=> $page,
        ]);
		
		return $data;
	}

	public function account_transactions($invoice){

		$data = $this->restclient->post("transactions/$invoice", [
            'token' => $this->session->userdata('token')
        ]);
		
		return $data;
	}

	public function transaction_complete($id){

		$data = $this->restclient->post("transaction-complete/$id", [
            'token' => $this->session->userdata('token')
        ]);
		
		return $data;
	}

	public function bank_account(){
		/*$data = $this->restclient->post("bank-account", [
          
        ]);
		
		return $data;*/
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "http://api.mojokertomarket.com/mobile/v1/bank-account",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "",
		  CURLOPT_HTTPHEADER => array(
		    "Content-Type: application/x-www-form-urlencoded",
		    "Postman-Token: 624c532d-6ebd-4e2c-9823-2847392ac851",
		    "cache-control: no-cache"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		return json_decode($response,true);
	}

	public function payment_confirmation($invoice,$amount,$files_type,$files_tmp_name){
			

		    $headers = array("Content-Type:multipart/form-data");
	
			$payment_date = date("Y-m-d");

		    $postfields = array(
		    	"token" => $this->session->userdata('token'),
		    	"invoice" => $invoice,
		    	"payment_date" => $payment_date,
		    	"amount" => $amount
		    );

		    $url = 'http://api.mojokertomarket.com/mobile/v1/payment-confirmation';
			if($files_tmp_name<>''){
				$cfile = curl_file_create("$files_tmp_name","$files_type",'image_field');
				$postfields['image'] = $cfile;	
			}
			
		    $ch = curl_init();

		    $options = array(
		        CURLOPT_URL => $url,
		        CURLOPT_HEADER => false,
		        CURLOPT_POST => 1,
		        CURLOPT_HTTPHEADER => $headers,
		        CURLOPT_POSTFIELDS => $postfields,
		        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		        CURLOPT_RETURNTRANSFER => true
		    ); 

		   curl_setopt_array($ch, $options);

		   $result = curl_exec($ch);
		   curl_close($ch);
		   
		   return $result;
	}


	public function get_distance($origin_lat,$origin_lng,$desti_lat,$desti_lng){
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=$origin_lat,$origin_lng&destinations=$desti_lat,$desti_lng&key=AIzaSyBp3E-Zv44uHcv41brVZH3gdrke4_sWFbU",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "Postman-Token: f491ef77-bde1-4c0c-bcd5-2a10d5af1a3c",
		    "cache-control: no-cache"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		}

		return json_decode($response,true);
	}


		function milesToKilometers($miles){
		    return int($miles) * 1.60934;
		}
}