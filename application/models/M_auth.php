<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_auth extends CI_Model {

	public function login($email,$password){

		$data = $this->restclient->post("login", [
            'email' => $email,
            'password' => $password
        ]);

		return $data;
	}

	public function register($email,$password,$password_confirmation,$name,$phone_number){

		$data = $this->restclient->post("register", [
            'email' => $email,
            'password' => $password,
            'password_confirmation' => $password_confirmation,
            'name' => $name,
            'phone_number' => $phone_number
        ]);

		return $data;	
	}

	public function logout(){

		/*$data = $this->restclient->post("logout", [
         
        ]);

		return $data;*/
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "http://api.mojokertomarket.com/mobile/v1/logout",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "",
		  CURLOPT_HTTPHEADER => array(
		    "Content-Type: application/x-www-form-urlencoded",
		    "Postman-Token: 624c532d-6ebd-4e2c-9823-2847392ac851",
		    "cache-control: no-cache"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		return json_decode($response,true);
	}

	public function activate($code){

		$data = $this->restclient->post("activate", [
            'code' => $code
        ]);

		return $data;
	}

	public function forgot($email){

		$data = $this->restclient->post("forgot-password", [
            'email' => $email
        ]);

		return $data;
	}

	public function reset_password($code,$password,$password_confirmation){
		
		$data = $this->restclient->post("reset-password", [
            'code' => $code,
			'password' => $password,
			'password_confirmation' => $password_confirmation
        ]);

		return $data;
	}
}