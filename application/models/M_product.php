<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_product extends CI_Model {

	public function best_seller($q){
		
		$data = $this->restclient->post("products/related", [
			'limit' => '3'
        ]);

		return $data;
	}

	public function get_list_product($id,$page,$q,$sortBy,$sortDir,$perPage){
		
		$data = $this->restclient->post("products", [
            'page' => $page,
			'search' => $q,
			'sortBy' => $sortBy,
			'sortDir' => $sortDir,
			'perPage' => $perPage,
			'category_id' => $id
        ]);

		return $data;
	}

	public function get_detail($id){
		
		//$data = $this->restclient->post("products/$id", [
        
        //]);
		//print_r($data);
		//return $data;
		//
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "http://api.mojokertomarket.com/mobile/v1/products/$id",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "",
		  CURLOPT_HTTPHEADER => array(
		    "Content-Type: application/x-www-form-urlencoded",
		    "Postman-Token: 624c532d-6ebd-4e2c-9823-2847392ac851",
		    "cache-control: no-cache"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		return json_decode($response,true);
	}

	public function hot_categories($id){
		$data = $this->restclient->post("products/favorite", [
			'limit' => '2',
			'category_id' => $id,
        ]);

		return $data;
	}

	public function hot_categories_7(){
		$data = $this->restclient->post("products/popular", [
			'limit' => '7'
        ]);

		return $data;
	}
	
}