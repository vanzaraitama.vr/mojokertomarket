		<!-- MAIN -->
		<main class="site-main">

            <!-- breadcrumb -->
            <div class="container breadcrumb-page">
                <ol class="breadcrumb">
                    <li><a href="{base_url}">Beranda </a></li>
                    <li class="active">Tentang</li>
                </ol>
            </div> <!-- breadcrumb -->

            <div class="page-title-base container">
                <h1 class="title-base">Tentang Kami</h1>
            </div>

            <div class="container">
              
                <div class="row">
                    <div class="col-sm-9">
                        <div class="block-about-us">
                            <div class="block-title">
                                #KotaMojokertoMelangkah
                            </div>
                            <div class="block-content row">
                                <div class="col-md-5 ">
                                    <div class="img">
                                        <img src="{base_url}assets/images/media/index1/about_mojokerto.jpg" alt="about">
                                    </div>
                                </div>

                                <div class="col-md-7 text  ">
                                    <p>Mojokertomarket merupakan salah satu e-commerce di Indonesia yang menyediakan sarana jual–beli dari konsumen ke konsumen. dan lebih di fokuskan kepada para umkm 
                                        kota Mojokerto, dan sesuai arahan dari pemerintah kota Mojokerto agar supaya dapat mendigitalkan semua pasar tradisional di kota Mojokerto 
                                        dan agar Semua orang bisa lebih mengenal kultur budaya serta keunikan kota Mojokerto 
                                        Semua warga kota Mojokerto dapat membuka toko online di mojokertomarket dan melayani pembeli dari seluruh Indonesia untuk transaksi satuan maupun banyak.</p>
                                    <p><br><br><br>Sesuai dengan Amanat :</p>
                                    <p>Undang-undang Nomor 20 Tahun 2008 tentang Usaha Mikro, Kecil dan Menengah.<br><br></p>
                                    <p>Peraturan Menteri Negera BUMN PER-05/MBU/2007 tentang Program Kemitraan Badan Usaha Milik Negara dengan Usaha Kecil dan Program Bina Lingkungan.</p>
                                    <p>Undang-Undang Nomor 7 Tahun 2014 tentang Perdangan. Yang mana di dalamnya telah Mengatur tentang "E-Commerce" (PerdaganganElektronik). Yang mana Pengaturan tentang E-Commerce memberikan Kepastian dan Kesepahaman mengenai apa yang dimaksud dengan PERDAGANGAN MELALUI SISTEM ELEKTRONIK (PMSE) dan Memberikan Perlindungan dan Kepastian kepada Pedagang, Penyelenggara PMSE dan Konsumen dalam melaksanakan Transaksi Elektronik.</p>
                                    <p>Dengan Dasar dan Alasan tersebut diatas, Pemerintah Kota Mojokerto melalui Dinas Perindustrian dan Perdagangan (DISPERINDAG) Kota Mojokerto akan Melaksanakan Launching Aplikasi E-Commerce "MojoMarket" bersamaan dengan Pameran UKM/IKM Kota Mojokerto yang Bertajuk "MojopahitFestival2019" yang akan dilaksanakan di Taman Benteng Pancasila.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="block-why-choos-us">
                            <div class="block-title">
                                Kenapa Memilih Kami
                            </div>
                            <div class="block-content">
                                <ul>
                                    <li><span>Mengangkat kearifan lokal kota mojokerto</span></li>
                                    <li><span>Mendigitalisasi para umkm kota mojokerto </span></li>
                                    <li><span>Menjangkau kalangan masyarakat di daerah daerah</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

		</main><!-- end MAIN -->
