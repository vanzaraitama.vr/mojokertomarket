        <!-- MAIN -->
        <main class="site-main">

           
            <div class="container">
                <div class="block-slide-main slide-opt-2">
                    <div class="owl-carousel" 
                        data-nav="true" 
                        data-dots="false" 
                        data-margin="0" 
                        data-items='1' 
                        data-autoplayTimeout="700" 
                        data-autoplay="true" 
                        data-loop="true">
                        <div class="item item1" >
                            <div class="container">
                                <div class="description">
                                    <span class="title banner1"></span>
                                   <!-- <span class="subtitle banner1">Flat 40% Off</span>
                                    <span class="des banner1"> Products leading fashion or participate <br> For a chance to own right</span>
                                    <a href="#" class="btn">shop now</a> -->
                                </div>
                            </div>
                        </div>
                        <div class="item item2">
                            <div class="container">
                                <div class="description">
                                    <span class="title banner2"></span>
                                   <!-- <span class="subtitle">Flat 40% Off</span>
                                    <span class="des"> Products leading fashion or participate <br>  For a chance to own right</span>
                                    <a href="#" class="btn">Shop Now</a> -->
                                </div>
                            </div>
                        </div>
                        <div class="item item3">
                            <div class="container">
                                <div class="description">
                                    <span class="title banner3"></span>
                                   <!-- <span class="subtitle">Flat 40% Off </span>
                                    <span class="des">  Products leading fashion or participate <br>  For a chance to own right</span>
                                    <a href="#" class="btn">Shop Now</a> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- block banner -->
            <div class="block-banner-main effect-banner1">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3">
                            <a href="#"  class="box-img"><img src="{base_url}assets/images/banner/image1.jpg" alt="banner1"></a>
                        </div>
                        <div class="col-sm-3">
                            <a href="#"  class="box-img"><img src="{base_url}assets/images/banner/image1.jpg" alt="banner1"></a>
                        </div>
                        <div class="col-sm-3">
                            <a href="#"  class="box-img"><img src="{base_url}assets/images/banner/image1.jpg" alt="banner1"></a>
                        </div>
                        <div class="col-sm-3">
                            <a href="#"  class="box-img"><img src="{base_url}assets/images/banner/image1.jpg" alt="banner1"></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- block banner -->            
            <style type="text/css">
                
                .block-floor-products-opt2 .col-products {
                    width: 100%;
                }

            </style>
            <!-- block -floor -products / floor :Electronics-->
            <div class="block-floor-products block-floor-products-opt2 block-floor-1" id="floor1-1">
                <div class="container">
                    <div class="block-title heading-opt-1">
                        <ul class="links">
                            <li role="presentation" class="active"><a href="#all1"  role="tab" data-toggle="tab">All Products</a></li>
                        </ul>
                        <div class="actions">
                            <a href="#" class="action action-up"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
                            <a href="#floor1-2" class="action action-down"><i class="fa fa-angle-down" aria-hidden="true"></i></a>
                        </div>
                    </div>

                    <div class="block-content">
                        <div class="col-products">
                            <div class="box-tab active in fade hot_categories_7" id="all1" role="tabpanel">
                                
                            </div>
                        </div>

                    </div>

                </div>
            </div><!-- block -floor -products / floor :Electronics-->

            
            <!-- block-hot-categori -->
            <div class="block-hot-categori">
                <div class="container">
                    <div class="block-title">
                        <span class="title">Kategori Kekinian</span>
                    </div>
                    <div class="block-content">
                        <div class="item">
                            <div class="item-info">
                                <strong class="title">
                                    <a href="#"><img src="{base_url}assets/images/icon/index1/title-hot-categori1.png" alt="hot-categori">
                                    <span>Batik</span></a>
                                </strong>
                                <ul class="kekinian_1">
                                </ul>
                            </div>
                        </div>
                        <div class="item">
                            <div class="item-info">
                                <strong class="title">
                                    <a href="#"><img src="{base_url}assets/images/icon/index1/title-hot-categori2.png" alt="hot-categori">
                                    <span>Makanan</span></a>
                                </strong>
                                <ul class="kekinian_2">
                                </ul>
                            </div>
                        </div>
                        <div class="item">
                            <div class="item-info">
                                <strong class="title">
                                    <a href="#"><img src="{base_url}assets/images/icon/index1/title-hot-categori3.png" alt="hot-categori">
                                    <span>Kerajinan</span></a>
                                </strong>
                                <ul class="kekinian_3">
                                </ul>
                            </div>
                        </div>
                        <div class="item">
                            <div class="item-info">
                                <strong class="title">
                                    <a href="#"><img src="{base_url}assets/images/icon/index1/title-hot-categori4.png" alt="hot-categori">
                                    <span>Otomotif</span></a>
                                </strong>
                                <ul class="kekinian_4">
                                </ul>
                            </div>
                        </div>
                        <div class="item">
                            <div class="item-info">
                                <strong class="title">
                                    <span>Lain-lain</span></a>
                                </strong>
                                <ul class="kekinian_4">
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- block-hot-categori -->
        </main><!-- end MAIN -->

        <script type="text/javascript">
            jQuery(document).ready(function($) {

                    $.ajax({
                         url: '{base_url}home/categories',
                         type: 'GET',
                         dataType: 'json'
                     })
                     .done(function(data) {
                         if(data.status=="success"){
                            var result = data.result;
                            $.each(result, function(index, val) {
                                //console.log(val);
                                /* iterate through array or object */
                              if(index==0){
                                //$(".banner1").text(val.name);
                                    $.each(val.banners_url, function(index_banners_url, val_banners_url) {
                                         /* iterate through array or object */
                                         if(index_banners_url==0){
                                            $('.item1').css('background-image', 'url(' + val_banners_url + ')');
                                         }
                                    });
                              }

                              if(index==1){
                                //$(".banner2").text(val.name);
                                    $.each(val.banners_url, function(index_banners_url, val_banners_url) {
                                         /* iterate through array or object */
                                         if(index_banners_url==0){
                                            $('.item2').css('background-image', 'url(' + val_banners_url + ')');
                                         }
                                    });
                              }

                              if(index==2){
                                //$(".banner3").text(val.name);
                                    $.each(val.banners_url, function(index_banners_url, val_banners_url) {
                                         /* iterate through array or object */
                                         if(index_banners_url==0){
                                            $('.item3').css('background-image', 'url(' + val_banners_url + ')');
                                         }
                                    });
                              }

                            });
                         }
                     })
                     .fail(function() {
                         console.log("error");
                     });

                    list_data ='';

                    $.ajax({
                        url: '{base_url}product/hot_categories_7',
                        type: 'GET',
                        dataType: 'json'
                    })
                    .done(function(data) {
                        var no_7 =0;
                        $.each(data.result, function(index, val) {
                             /* iterate through array or object */
                            //console.log(val);
                            var list_data ='';
                            var images_url ='';
                                $.each(val.images_url, function(index_image, val_image) {
                                     /* iterate through array or object */
                                     if(index_image=="0"){
                                        images_url = val_image;
                                     }

                                }); 
                            if(no_7==0){
                                list_data=list_data+'<div class="col-sm-6 product-item product-item-lag">';
                                list_data=list_data+'    <div class="product-item-info">';
                                list_data=list_data+'        <div class="product-item-photo">';
                                list_data=list_data+'            <a class="product-item-img" href="{base_url}product/detail/'+convertToSlug(val.name)+'/'+val.id+'"><img alt="'+val.name+'" src="'+images_url+'"></a>';
                                list_data=list_data+'        </div>';
                                list_data=list_data+'        <div class="product-item-detail">';
                                list_data=list_data+'            <strong class="product-item-name"><a href="{base_url}product/detail/'+convertToSlug(val.name)+'/'+val.id+'">'+val.name+'</a></strong>';
                                list_data=list_data+'            <a href="{base_url}product/detail/'+convertToSlug(val.name)+'/'+val.id+'" class="view-more">View More</a>';
                                list_data=list_data+'        </div>';
                                list_data=list_data+'    </div>';
                                list_data=list_data+'</div>';
                            }else{
                                list_data=list_data+'<div class="col-sm-3 product-item product-item-opt-1">';
                                list_data=list_data+'    <div class="product-item-info">';
                                list_data=list_data+'        <div class="product-item-photo">';
                                list_data=list_data+'            <a class="product-item-img" href="{base_url}product/detail/'+convertToSlug(val.name)+'/'+val.id+'"><img alt="'+val.name+'" src="'+images_url+'" width="247px" height="187px"></a>';
                                list_data=list_data+'        </div>';
                                list_data=list_data+'        <div class="product-item-detail">';
                                list_data=list_data+'            <strong class="product-item-name"><a href="{base_url}product/detail/'+convertToSlug(val.name)+'/'+val.id+'">'+val.name+'</a></strong>';
                                list_data=list_data+'            <div class="product-item-price">';
                                if(val.discount>0){
                                list_data=list_data+'                <span class="price"><font color="red"><strike>Rp.'+formatMoney(val.price)+'</strike></font><br></span>';
                                list_data=list_data+'                <span class="price">Rp.'+formatMoney(val.price_after)+'<font color="red"> '+val.discount+'%OFF</font></span>';
                                }else{
                                list_data=list_data+'                <span class="price">Rp.'+formatMoney(val.price)+'</font></span>';
                                }
                                list_data=list_data+'            </div>';
                                list_data=list_data+'        </div>';
                                list_data=list_data+'    </div>';
                                list_data=list_data+'</div>';
                            }
                            $(".hot_categories_7").append(list_data);
                            no_7++;    
                        });

                    })
                    .fail(function() {
                        console.log("error");
                    });


                    $.ajax({
                        url: '{base_url}product/hot_categories',
                        type: 'GET',
                        dataType: 'json',
                        data: {id: '1'},
                    })
                    .done(function(data) {
                        $.each(data.result, function(index, val) {
                             /* iterate through array or object */
                                //console.log(data);
                                list_data=           '    <li>';
                                list_data= list_data+'        <a href="{base_url}product/detail/'+convertToSlug(val.name)+'/'+val.id+'">';
                                val_image='';
                                $.each(val.images_url, function(index_image, val_image) {
                                     /* iterate through array or object */
                                     if(index_image=="0"){
                                        images_url = val_image;
                                     }

                                });
                                list_data= list_data+'            <img src="'+images_url+'" alt="hot-categori">';
                                list_data= list_data+'            <span>'+val.name+'</span>';
                                list_data= list_data+'        </a>';
                                list_data= list_data+'    </li>';

                                $(".kekinian_1").append(list_data);
                        });

                    })
                    .fail(function() {
                        console.log("error");
                    });

                    $.ajax({
                        url: '{base_url}product/hot_categories',
                        type: 'GET',
                        dataType: 'json',
                        data: {id: '2'},
                    })
                    .done(function(data) {
                        $.each(data.result, function(index, val) {
                             /* iterate through array or object */
                                
                                list_data=           '    <li>';
                                list_data= list_data+'        <a href="{base_url}product/detail/'+convertToSlug(val.name)+'/'+val.id+'">';
                                val_image='';
                                $.each(val.images_url, function(index_image, val_image) {
                                     /* iterate through array or object */
                                     if(index_image=="0"){
                                        images_url = val_image;
                                     }

                                });
                                list_data= list_data+'            <img src="'+images_url+'" alt="hot-categori">';
                                list_data= list_data+'            <span>'+val.name+'</span>';
                                list_data= list_data+'        </a>';
                                list_data= list_data+'    </li>';

                                $(".kekinian_2").append(list_data);
                        });

                    })
                    .fail(function() {
                        console.log("error");
                    });

                    $.ajax({
                        url: '{base_url}product/hot_categories',
                        type: 'GET',
                        dataType: 'json',
                        data: {id: '3'},
                    })
                    .done(function(data) {
                        $.each(data.result, function(index, val) {
                             /* iterate through array or object */
                                
                                list_data=           '    <li>';
                                list_data= list_data+'        <a href="{base_url}product/detail/'+convertToSlug(val.name)+'/'+val.id+'">';
                                val_image='';
                                $.each(val.images_url, function(index_image, val_image) {
                                     /* iterate through array or object */
                                     if(index_image=="0"){
                                        images_url = val_image;
                                     }

                                });
                                list_data= list_data+'            <img src="'+images_url+'" alt="hot-categori">';
                                list_data= list_data+'            <span>'+val.name+'</span>';
                                list_data= list_data+'        </a>';
                                list_data= list_data+'    </li>';

                                $(".kekinian_3").append(list_data);
                        });

                    })
                    .fail(function() {
                        console.log("error");
                    });

                    $.ajax({
                        url: '{base_url}product/hot_categories',
                        type: 'GET',
                        dataType: 'json',
                        data: {id: '4'},
                    })
                    .done(function(data) {
                        $.each(data.result, function(index, val) {
                             /* iterate through array or object */
                                
                                list_data=           '    <li>';
                                list_data= list_data+'        <a href="{base_url}product/detail/'+convertToSlug(val.name)+'/'+val.id+'">';
                                val_image='';
                                $.each(val.images_url, function(index_image, val_image) {
                                     /* iterate through array or object */
                                     if(index_image=="0"){
                                        images_url = val_image;
                                     }

                                });
                                list_data= list_data+'            <img src="'+images_url+'" alt="hot-categori">';
                                list_data= list_data+'            <span>'+val.name+'</span>';
                                list_data= list_data+'        </a>';
                                list_data= list_data+'    </li>';

                                $(".kekinian_4").append(list_data);
                        });

                    })
                    .fail(function() {
                        console.log("error");
                    });

                    $.ajax({
                        url: '{base_url}product/hot_categories',
                        type: 'GET',
                        dataType: 'json',
                        data: {id: '5'},
                    })
                    .done(function(data) {
                        $.each(data.result, function(index, val) {
                             /* iterate through array or object */
                                
                                list_data=           '    <li>';
                                list_data= list_data+'        <a href="{base_url}product/detail/'+convertToSlug(val.name)+'/'+val.id+'">';
                                val_image='';
                                $.each(val.images_url, function(index_image, val_image) {
                                     /* iterate through array or object */
                                     if(index_image=="0"){
                                        images_url = val_image;
                                     }

                                });
                                list_data= list_data+'            <img src="'+images_url+'" alt="hot-categori">';
                                list_data= list_data+'            <span>'+val.name+'</span>';
                                list_data= list_data+'        </a>';
                                list_data= list_data+'    </li>';

                                $(".kekinian_5").append(list_data);
                        });

                    })
                    .fail(function() {
                        console.log("error");
                    });

            });

            function formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
                      try {
                        decimalCount = Math.abs(decimalCount);
                        decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

                        const negativeSign = amount < 0 ? "-" : "";

                        let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
                        let j = (i.length > 3) ? i.length % 3 : 0;

                        return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
                      } catch (e) {
                        console.log(e)
                      }
            };  

            function convertToSlug(Text){
                return Text
                    .toLowerCase()
                    .replace(/ /g,'-')
                    .replace(/[^\w-]+/g,'')
                    ;
            }                     
        </script>

