        <!-- MAIN -->
		<main class="site-main">

            <!-- breadcrumb -->
            <div class="container breadcrumb-page">
                <ol class="breadcrumb">
                    <li><a href="{base_url}">Beranda </a></li>
                    <li class="">Daftar</li>
                    <li class="active">Reset Password</li>
                </ol>
            </div> <!-- breadcrumb -->

            <div class="page-title-base container">
                <h1 class="title-base">Reset Password</h1>
            </div>

            <div class="container">

                
                <div class="block-form-login">

                    <!-- block Create an Account -->
                    <div class="block-form-create">
                        <div class="block-title">
                            Reset Password
                        </div>
                        <div class="block-content">
                            <p>Silahkan Masukkan kode Rahasia dan password baru anda!</p>
                            <form action="{base_url}" name="register" method="post">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Code" name="code">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" placeholder="Password" name="password">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" placeholder="Password Confirmation" name="password_confirmation">
                            </div>
                            <button type="submit" class="btn btn-inline" id="submit_register" name="submit_register" >Reset Password</button>
                            </form>
                        </div>
                    </div><!-- block Create an Account -->

                </div>

            </div>           
		</main><!-- end MAIN -->

        <script type="text/javascript">
        
        $('form[name="register"]').submit(function(e){
            e.preventDefault();
            
            $.ajax({
                url: "{base_url}auth/submit_reset_password",
                method: "post",
                cache: false,
                data: $(this).serialize(),
                dataType: "json",
                beforeSend: function(){
                    $('#submit_register').button('loading');
                },
                success: function(data){
                    $('#submit_register').button('reset');
                    if(data.status=='success'){
                        $.notify({
                            title: '<strong>Success!</strong>',
                            message: "Reset Password Berhasil, Silahkan login kembali!!!"
                        },{
                            type: 'success',
                            placement: {
                                from: "bottom"
                            },
                        });
                        setTimeout(function(){
                            window.location.href = '{base_url}auth';
                        },1000);
                    }else{
                        $.notify({
                            title: '<strong>Gagal!</strong>',
                            message: data.message
                        },{
                            type: 'danger',
                            placement: {
                                from: "bottom"
                            },
                        });
                    }
                },
                complete: function(data){
                    $('#submit_register').button('reset');
                },
                error: function(){
                    $('#submit_register').button('reset');
                    $.notify({
                        title: '<strong>Gagal!</strong>',
                        message: "Tolong Cek Koneksi Internet Anda!"
                    },{
                        type: 'danger',
                        placement: {
                            from: "bottom"
                        },
                    });
                }
            });

        });
     </script>