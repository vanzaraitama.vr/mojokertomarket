        <!-- MAIN -->
		<main class="site-main">

            <!-- breadcrumb -->
            <div class="container breadcrumb-page">
                <ol class="breadcrumb">
                    <li><a href="{base_url}">Beranda </a></li>
                    <li class="active">Otentikasi</li>
                </ol>
            </div> <!-- breadcrumb -->

            <div class="page-title-base container">
                <h1 class="title-base">Otentikasi</h1>
            </div>

            <div class="container">

                
                <div class="block-form-login">

                    <!-- block Create an Account -->
                    <div class="block-form-create">
                        <div class="block-title">
                            Buat Akun
                        </div>
                        <div class="block-content">
                            <p>Silahkan Masukkan email anda untuk membuat akun!</p>
                            <form action="{base_url}" name="register" method="post">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Your email" name="email">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" placeholder="Password" name="password">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" placeholder="Re-type Password" name="password_confirmation">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Name" name="name">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Phone Number" name="phone_number">
                            </div>
                            <button type="submit" class="btn btn-inline" id="submit_register" name="submit_register" >Daftar</button>
                            </form>
                        </div>
                    </div><!-- block Create an Account -->

                    <!-- block Registered-->
                    <div class="block-form-registered">
                       
                        <div class="block-title">
                            Sudah Memiliki Akun?
                        </div>
                        <div class="block-content">
                            <p>Jika sudah memiliki akun silahkan masukkan user dan passoword anda!</p>
                            <form action="{base_url}" name="login" method="post">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Your email" name="email">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" placeholder="Password" name="password">
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox"><span>Ingat Saya!</span>
                                    </label>
                                </div>
                                <button type="submit" class="btn btn-inline" id="submit_login" name="submit_login" >Masuk</button>
                            </form>
                        </div>
                        
                    </div><!-- block Registered-->

                </div>

                <!-- Forgot your password -->
                <div class="block-forgot-pass">
                    Lupa Password ? <a href="{base_url}auth/forgot">Klik Disini</a>
                </div><!-- Forgot your password -->

            </div>           
		</main><!-- end MAIN -->

        <script type="text/javascript">
        
        $('form[name="login"]').submit(function(e){
            e.preventDefault();
            
            $.ajax({
                url: "{base_url}auth/login",
                method: "post",
                cache: false,
                data: $(this).serialize(),
                dataType: "json",
                beforeSend: function(){
                    $('#submit_login').button('loading');
                },
                success: function(data){
                    $('#submit_login').button('reset');
                    if(data.status=='success'){
                        $.notify({
                            title: '<strong>Success!</strong>',
                            message: "Berhasil Masuk!!!"
                        },{
                            type: 'success',
                            placement: {
                                from: "bottom"
                            },
                        });
                        setTimeout(function(){
                            window.location.reload();   
                        },1000);
                    }else{
                        $.notify({
                            title: '<strong>Gagal!</strong>',
                            message: data.message
                        },{
                            type: 'danger',
                            placement: {
                                from: "bottom"
                            },
                        });
                    }
                },
                complete: function(data){
                    $('#submit_login').button('reset');
                },
                error: function(){
                    $('#submit_login').button('reset');
                    $.notify({
                        title: '<strong>Gagal!</strong>',
                        message: "Tolong Cek koneksi internet anda!"
                    },{
                        type: 'danger',
                        placement: {
                            from: "bottom"
                        },
                    });
                }
            });

        });

        $('form[name="register"]').submit(function(e){
            e.preventDefault();
            
            $.ajax({
                url: "{base_url}auth/register",
                method: "post",
                cache: false,
                data: $(this).serialize(),
                dataType: "json",
                beforeSend: function(){
                    $('#submit_register').button('loading');
                },
                success: function(data){
                    $('#submit_login').button('reset');
                    if(data.status=='success'){
                        $.notify({
                            title: '<strong>Success!</strong>',
                            message: data.message
                        },{
                            type: 'success',
                            placement: {
                                from: "bottom"
                            },
                        });
                        setTimeout(function(){
                            //window.location.reload();   
                             window.location.href = '{base_url}auth/activation';
                        },1000);
                    }else{
                        $.each(data.message, function(index, val) {
                             /* iterate through array or object */
                            $.each(val, function(index, mess) {
                                 /* iterate through array or object */
                                 $.notify({
                                        title: '<strong>Gagal!</strong>',
                                        message: mess
                                    },{
                                        type: 'danger',
                                        placement: {
                                            from: "bottom"
                                        },
                                    });
                            });
                        });
                    }
                },
                complete: function(data){
                    $('#submit_register').button('reset');
                },
                error: function(){
                    $('#submit_register').button('reset');
                    $.notify({
                        title: '<strong>Gagal!</strong>',
                        message: "Tolong Cek koneksi internet anda!"
                    },{
                        type: 'danger',
                        placement: {
                            from: "bottom"
                        },
                    });
                }
            });

        });
     </script>