        <!-- MAIN -->
		<main class="site-main">

            <!-- breadcrumb -->
            <div class="container breadcrumb-page">
                <ol class="breadcrumb">
                    <li><a href="{base_url}">Beranda </a></li>
                    <li class="active">Hubungi Kami</li>
                </ol>
            </div> <!-- breadcrumb -->

            <div class="container">
                <div class="block-googlemap">
                    
                   <img src="{base_url}assets/images/media/index1/map_mojokerto.jpeg" alt="map">
                </div>

                <div class="row">
                    <div class="col-md-8" style="display: none;">

                        <!-- block  contact-->
                        <div class="block-contact-us">
                            <div class="block-title">
                                contact us
                            </div>
                            <div class="block-content row">
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <input type="text" placeholder="Name *" class="form-control" id="name">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" placeholder="Email *" class="form-control" id="email">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" placeholder="Subject" id="subject" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-7">
                                    <div class="form-group">
                                        <textarea placeholder="Message" rows="5" class="form-control" id="message"></textarea>
                                    </div>
                                    <div class="text-right">
                                        <button class="btn btn-inline" type="submit">send message</button>
                                    </div>
                                </div>
                            </div><!-- block  contact-->
                            
                        </div>
                    </div>

                    <div class="col-md-4">

                         <!-- block  contact-->
                        <div class="block-address">
                            <div class="block-title">
                                Alamat
                            </div>
                            <div class="block-content ">
                                <p>
                                    <b class="title">Alamat</b>
                                    Dinas Perindustrian & Perdagangan, Mergelo, Meri, Magersari, Mojokerto City, East Java 61315
                                </p>
                                <p>
                                    <b class="title">No. Telpon</b>
                                    +6282334145400
                                </p>
                                <p>
                                    <b class="title">Jam Kerja</b>
                                    Monday - Friday 10AM - 8PM
                                </p>
                            </div>
                            
                        </div><!-- block  contact-->
                    </div>
                </div>

            </div>           
		</main>
        <!-- end MAIN -->