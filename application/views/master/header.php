<!DOCTYPE html>
<html lang="en">
<head>
    <title>{title}</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="{base_url}administrator/assets/modules/core/common/img/logo-pemkot.png" type="image/x-icon" />
    <link rel="shortcut icon" href="{base_url}administrator/assets/modules/core/common/img/logo-pemkot.png" type="image/x-icon" />
    <!-- Style CSS -->
    <link rel="stylesheet" type="text/css" href="{base_url}assets/css/style.css">

    <!-- jQuery -->    
    <script type="text/javascript" src="{base_url}assets/js/jquery.min.js"></script>
    <!-- sticky -->
    <script type="text/javascript" src="{base_url}assets/js/jquery.sticky.js"></script>
    <!-- OWL CAROUSEL Slider -->    
    <script type="text/javascript" src="{base_url}assets/js/owl.carousel.min.js"></script>
    
    <!-- Countdown --> 
    <script type="text/javascript" src="{base_url}assets/js/jquery.countdown.min.js"></script>
    <!--jquery Bxslider  -->
    <script type="text/javascript" src="{base_url}assets/js/jquery.bxslider.min.js"></script>
    <!-- actual --> 
    <script type="text/javascript" src="{base_url}assets/js/jquery.actual.min.js"></script>
    <!-- jQuery UI -->
    <script type="text/javascript" src="{base_url}assets/js/jquery-ui.min.js"></script>
    <!-- Chosen jquery-->    
    <script type="text/javascript" src="{base_url}assets/js/chosen.jquery.min.js"></script>
    <!-- elevatezoom --> 
    <script type="text/javascript" src="{base_url}assets/js/jquery.elevateZoom.min.js"></script>
    <!-- fancybox -->
    <script src="{base_url}assets/js/fancybox/source/jquery.fancybox.pack.js"></script>
    <script src="{base_url}assets/js/fancybox/source/helpers/jquery.fancybox-media.js"></script>
    <script src="{base_url}assets/js/fancybox/source/helpers/jquery.fancybox-thumbs.js"></script>
    <!-- arcticmodal -->
    <script src="{base_url}assets/js/arcticmodal/jquery.arcticmodal.js"></script>
    <!-- Main -->  
    <script type="text/javascript" src="{base_url}assets/js/main.js"></script>
    <!-- Boostrap --> 
    <script type="text/javascript" src="{base_url}assets/js/bootstrap.min.js"></script>
   
    <script src="{base_url}assets/js/bootstrap-notify.min.js"></script>
    
    <link rel="stylesheet" type="text/css" href="{base_url}assets/bootstrap-sweetalert/dist/sweetalert.css">
    <script src="{base_url}assets/bootstrap-sweetalert/dist/sweetalert.min.js"></script>

    <link href="{base_url}assets/js/loadingModal/css/jquery.loadingModal.min.css" rel="stylesheet" />
    <script src="{base_url}assets/js/loadingModal/js/jquery.loadingModal.min.js"></script>
    <style type="text/css">
    	.btn-inline-blue {
		    color: #fff;
		    background-color: #1064c9;
		    border-color: #1064c9;
		}

        .header-opt-2 .header-content {
            background-color: #f5f5f5;
            padding: 0px 0px;
        }
        .header-opt-1 .header-content {
            background-color: #f5f5f5;
            padding: 0px 0px;
        } 

        div.ex1 {
          width: 350px;
          height: 350px;
          overflow: scroll;
        }

    </style>

    
</head>
<body class="cms-index-index index-opt-1">

    <div class="wrapper">

        <!-- HEADER -->
        <header class="site-header header-opt-1">

            <!-- header-top -->
            <div class="header-top">
                <div class="container">

                    <!-- hotline -->
                    <ul class="hotline nav-left" >
                        <li ><span><i class="fa fa-phone" aria-hidden="true"></i>+6282334145400</span></li>
                        <li ><span><i class="fa fa-envelope" aria-hidden="true"></i>mojokertomart@gmail.com</span></li>
                    </ul><!-- hotline -->

                    <!-- links -->
                    <ul class="links nav-right">
                        <?php 
                            if($this->session->userdata('is_logged_in') == true){
                        ?>
                        <li class="dropdown setting">
                            <a data-toggle="dropdown" role="button" href="#" class="dropdown-toggle "><span>Pengaturan</span> <i aria-hidden="true" class="fa fa-angle-down"></i></a>
                            <div class="dropdown-menu">
                                <ul class="account">
                                    <li><a href="{base_url}account/wishlist">Barang Favorit</a></li>
                                    <li><a href="{base_url}account">Akun Saya</a></li>
                                    <li><a href="{base_url}account/address">Alamat</a></li>
                                    <li><a href="{base_url}account/checkout">Keranjang Belanja</a></li>
                                    <li><a href="{base_url}account/transaction">Daftar Transaksi</a></li>
                                    <li><a href="{base_url}store">Toko Saya</a></li>
                                    <li><a href="{base_url}auth/logout">Logout</a></li>
                                </ul>
                            </div>
                        </li>
                        <?php
                            }else{
                        ?>
                            <li ><a href="{base_url}auth">Login/Daftar</a></li>
                        <?php
                            }
                        ?>
                        <li><a href="{base_url}home/about">Tentang Kami</a></li>
                        <li><a href="{base_url}home/contact_us">Hubungi Kami </a></li>
                    </ul><!-- links -->

                </div>
            </div><!-- header-top -->

            <!-- header-nav -->
            <div class=" header-nav mid-header">
                <div class="container">
                    <div class="box-header-nav">

                        <span onclick="check_menu();" data-action="toggle-nav-cat" class="nav-toggle-menu nav-toggle-cat"><span>Kategori</span><i aria-hidden="true" class="fa fa-bars"></i></span>
                        
                        <div class="block-nav-categori" id="block_1">
                            <div class="block-title" id="block_2" onclick="check_menu();">
                                <span>Kategori</span>
                            </div>
                            <!--<div class="block-content ex1" id="ex1" style="display: none;">-->
                            <div class="block-content" id="ex1">
                                <ul class="ui-categori" id="categories">
                                </ul>
                                <div class="view-all-categori">
                                    <a  class="open-cate btn-view-all">Semua Kategori</a>
                                </div>
                            </div> 
                        </div>

                        <!-- menu -->
                        <div class="block-nav-menu">
                            <ul class="ui-menu">
                                <li class="">
                                    <a href="{base_url}">Home</a>
                                    <span class="toggle-submenu"></span>
                                </li>
                            </ul>
                        </div><!-- menu -->

                        <span data-action="toggle-nav" class="nav-toggle-menu"><span>Menu</span><i aria-hidden="true" class="fa fa-bars"></i></span>
                        
                        <div class="block-minicart dropdown ">
                            <a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                <span class="cart-icon"></span>
                            </a>
                            <div class="dropdown-menu">
                                <form>
                                    <div  class="minicart-content-wrapper" >
                                        <div class="subtitle">
                                            Kamu Memiliki <span id="count_cart">0</span> barang di keranjang belanja
                                        </div>
                                        <div class="minicart-items-wrapper">
                                            <ol class="minicart-items" id="minicart_items">
                                                
                                            </ol>
                                        </div>

                                        <div class="actions">
                                            <a href="{base_url}account/checkout" class="btn btn-checkout" type="button" title="Check Out" id="btn_minicart">
                                                Checkout
                                            </a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="block-search">
                            <div class="block-title">
                                <span>Search</span>
                            </div>
                            <div class="block-content">
                                <div class="form-search">
                                    <form>
                                        <div class="box-group">
                                            <input type="text" class="form-control" placeholder="Search here...">
                                            <button class="btn btn-search" type="button"><span>search</span></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        
                        <div class="dropdown setting">
                            <a data-toggle="dropdown" role="button" href="#" class="dropdown-toggle "><span>Settings</span> <i aria-hidden="true" class="fa fa-user"></i></a>
                            <div class="dropdown-menu">
                                <ul class="account">
                                    <?php 
                                    if($this->session->userdata('is_logged_in') == true){
                                    ?>
                                    <li><a href="{base_url}account/wishlist">Barang Favorit</a></li>
                                    <li><a href="{base_url}account">Akun Saya</a></li>
                                    <li><a href="{base_url}account/address">Alamat</a></li>
                                    <li><a href="{base_url}account/checkout">Keranjang Belanja</a></li>
                                    <li><a href="{base_url}account/transaction">Daftar Transaksi</a></li>
                                    <li><a href="{base_url}store">Toko Saya</a></li>
                                    <li><a href="{base_url}auth/logout">Logout</a></li>
                                    <?php
                                    }else{
                                    ?>
                                    <li><a href="{base_url}auth">Login/Daftar</a></li>
                                    <?php
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>                        
                    </div>
                </div>
            </div><!-- header-nav -->

        </header><!-- end HEADER -->

<script type="text/javascript">
    jQuery(document).ready(function($) {
        v_check_menu = 0;

        $.ajax({
             url: '{base_url}home/categories',
             type: 'GET',
             dataType: 'json'
         })
         .done(function(data) {
             if(data.status=="success"){
                var result = data.result;
                var no = 0;
                var categories = '';
                $.each(result, function(index, val) {
                     /* iterate through array or object */
                    if(no>5){
                        var li_class = 'cat-link-orther'; 
                    }else{
                        var li_class = '';
                    }

                    categories = categories+'<li class="'+li_class+'">';
                    categories = categories+'<a href="{base_url}product/list/'+val.slug+'/'+val.id+'">';
                    categories = categories+'<span class="icon"><img src="{base_url}assets/images/icon/index1/nav-cat1.png" alt="nav-cat"></span>';
                    categories = categories+val.name;
                    categories = categories+'</a>';
                    categories = categories+'</li>';
                    no++; 

                    //$('#categori_search_option').append('<option value="'+val.name+'">'+val.name+'</option>');
                    //
                    //$(".chosen-results").append('<li class="active-result" data-option-array-index="'+no+'" style="">'+val.name+'</li>')
                });

             }else{
                categories = '';
             }

             //$("#block_search").addClass('block-search');                      
             //$("#categori_search").addClass('categori-search');                      
             //$("#categori_search_option").addClass('categori-search-option');                      
             $('#categories').html(categories);
             $('#categories').html(categories);
         })
         .fail(function() {
             console.log("error");
         });


         <?php 
            if($this->session->userdata('is_logged_in') == true){
         ?>
         $.ajax({
            url: '{base_url}account/get_cart',
            type: 'GET',
            dataType: 'json'
         })
         .done(function(data) {
            v_list_data     = '';
            v_input         = '';

            if(data.status=="success"){
                v_total_items   = data.result.total_items;
                v_loop_product  = data.result.data;
                v_grandtotal    = data.result.grandtotal;
                v_subtotal      = data.result.subtotal;
                v_total_items   = data.result.total_items;
                v_total_discount= data.result.total_discount;
                v_nom = 0;
                if(v_total_items>0){
                     $.each(v_loop_product, function(index, val) {
                        if(v_nom<3){
                            $.each(val.items, function(index, val_items) {
                                v_list_data = v_list_data+'<li class="product-item">';
                               
                                val_image='';
                                $.each(val_items.product.images_url, function(index_image, val_image) {
                                     /* iterate through array or object */
                                     if(index_image=="0"){
                                        images_url = val_image;
                                        v_list_data = v_list_data+'    <a class="product-item-photo" href="#" title="'+val_items.product.name+'">';
                                        v_list_data = v_list_data+'        <img class="product-image-photo" src="'+images_url+'" alt="'+val_items.product.name+'">';
                                        v_list_data = v_list_data+'    </a>';
                                     }
                                     
                                });

                                 v_list_data = v_list_data+'    <div class="product-item-details">';
                                 v_list_data = v_list_data+'        <strong class="product-item-name">';
                                 v_list_data = v_list_data+'            <a href="#">'+val_items.product.name+'</a>';
                                 v_list_data = v_list_data+'        </strong>';
                                 v_list_data = v_list_data+'        <div class="product-item-qty">';
                                 v_list_data = v_list_data+'            <span class="label">Quantity:</span >';
                                 v_list_data = v_list_data+'            <span class="number">'+val_items.qty+'</span>';
                                 v_list_data = v_list_data+'        </div>';
                                 v_list_data = v_list_data+'        <div class="product-item-price">';
                                 v_list_data = v_list_data+'            <span class="price">Rp.'+formatMoney(val_items.product.price)+'</span>';
                                 v_list_data = v_list_data+'        </div>';
                                 v_list_data = v_list_data+'    </div>';
                                 v_list_data = v_list_data+'</li>';
                             });
                            v_nom++;
                        }else{
                            $("#btn_minicart").text('More');
                        }
                     });
                }

                $("#count_cart").text(v_total_items);
                $('#minicart_items').html(v_list_data);
            }
         })
         .fail(function() {
             console.log("error");
         });
         <?php 
            }else{
         ?>
         $(".block-minicart").hide();
         <?php 
            }
         ?>
         
         check_menu = function(){

           /* if(v_check_menu ==0){
                v_check_menu = 1;
                $("#ex1").addClass('ex1');
                $(".ui-categori").delay(500).show();
                $(".view-all-categori").delay(500).show();
            }else{
                v_check_menu = 0;
                $("#ex1").removeClass('ex1'); 
                $(".ui-categori").delay(500).hide();
                $(".view-all-categori").delay(500).hide();  
            }    */
         /*   if($('.ui-categori').css('display') == 'none' && $('.view-all-categori').css('display') == 'none'){
                $("#ex1").show();
                $("#block_1").addClass('has-open');
                $("#block_2").addClass('active');
                $(".ui-categori").show();
                $(".view-all-categori").show();
            }else{
                $("#ex1").hide();
                $("#block_1").removeClass('has-open')
                $("#block_2").removeClass('active');
                $(".ui-categori").hide();
                $(".view-all-categori").hide();
            }*/

           /* if(v_check_menu ==0){
                v_check_menu = 1;
                
                $("#block_1").addClass('has-open');
                $("#block_2").addClass('active');
                
                if($('.ui-categori').css('display') == 'none' && $('.view-all-categori').css('display') == 'none'){
                    $("#ex1").show();
                    $(".ui-categori").show();
                    $(".view-all-categori").show();
                }else{
                    $(".ui-categori").hide();
                    $(".view-all-categori").hide();
                }

            }else{
                v_check_menu = 0;
            
                $("#block_1").removeClass('has-open')
                $("#block_2").removeClass('active');

                if($('.ui-categori').css('display') == 'none' && $('.view-all-categori').css('display') == 'none'){
                    $("#ex1").show();
                    $(".ui-categori").show();
                    $(".view-all-categori").show();
                }else{
                    $(".ui-categori").hide();
                    $(".view-all-categori").hide();
                }
            }
            console.log(v_check_menu);*/
         }
    });

        function formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
                      try {
                        decimalCount = Math.abs(decimalCount);
                        decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

                        const negativeSign = amount < 0 ? "-" : "";

                        let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
                        let j = (i.length > 3) ? i.length % 3 : 0;

                        return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
                      } catch (e) {
                        console.log(e)
                      }
            };
</script>
