       <!-- FOOTER -->
        <footer class="site-footer footer-opt-2">

            <div class="container">
                <div class="footer-column">
                
                    <div class="row">
                        <div class="col-md-4 col-lg-4 col-sm-6">
                            <strong class="logo-footer">
                               Mojokerto Market
                            </strong>

                            <table class="address">
                                <tr>
                                    <td><b>Alamat:</b></td>
                                    <td> Dinas Perindustrian & Perdagangan, Mergelo, Meri, Magersari, Mojokerto City, East Java 61315</td>
                                </tr>
                                <tr>
                                    <td><b>Telpon:</b></td>
                                    <td> 082334145400</td>
                                </tr>
                                <tr>
                                    <td><b>Email:</b></td>
                                    <td> mojokertomart@gmail.com</td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-4 col-lg-4 col-sm-12">
                            <div class="links">
                            <h3 class="title">Layanan Pelanggan</h3>
                            <ul>
                                <li><a href="{base_url}terms-condition.html">Syarat dan Ketentuan</a></li>
                                <li><a href="{base_url}privacy-policy.html">Kebijakan Privasi</a></li>
                            </ul>
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-4 col-sm-12">
                            <div class="links">
                            <h3 class="title">Produk</h3>
                            <ul>
                                <li><a href="{base_url}account/checkout">Pesanan Saya</a></li>
                                <li><a href="{base_url}account/wishlist">Barang Favorit</a></li>
                            </ul>
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-4 col-sm-6">
                            <div class="block-social">
                                <div class="block-title">Temukan Kami di </div>
                                <div class="block-content">
                                    <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-camera" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="copyright">
                    Hak Cipta Terpelihara © 2018 {title}. Desain Oleh {title} 
                </div>
            </div>
        </footer><!-- end FOOTER -->        
        
        <!--back-to-top  -->
        <a href="#" class="back-to-top">
            <i aria-hidden="true" class="fa fa-angle-up"></i>
        </a>
        
    </div>
</body>
</html>