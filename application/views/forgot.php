        <!-- MAIN -->
		<main class="site-main">

            <!-- breadcrumb -->
            <div class="container breadcrumb-page">
                <ol class="breadcrumb">
                    <li><a href="{base_url}">Beranda </a></li>
                    <li class=""><a href="{base_url}auth">Login/Daftar </a></li>
                    <li class="active">Lupa Password</li>
                </ol>
            </div> <!-- breadcrumb -->

            <div class="page-title-base container">
                <h1 class="title-base">Lupa Password</h1>
            </div>

            <div class="container">

                
                <div class="block-form-login">

                    <!-- block Create an Account -->
                    <div class="block-form-create">
                        <div class="block-title">
                            Masukkan Email Anda
                        </div>
                        <div class="block-content">
                            <p>Silahkan Masukkan alamat email anda!</p>
                            <form action="{base_url}" name="forgot" method="post">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Email" name="email">
                            </div>
                            <button type="submit" class="btn btn-inline" id="submit_register" name="submit_register" >Kirim</button>
                            </form>
                        </div>
                    </div><!-- block Create an Account -->

                </div>

            </div>           
		</main><!-- end MAIN -->

        <script type="text/javascript">
        
        $('form[name="forgot"]').submit(function(e){
            e.preventDefault();
            
            $.ajax({
                url: "{base_url}auth/submit_forgot",
                method: "post",
                cache: false,
                data: $(this).serialize(),
                dataType: "json",
                beforeSend: function(){
                    $('#submit_register').button('loading');
                },
                success: function(data){
                    $('#submit_register').button('reset');
                    if(data.status=='success'){
                        $.notify({
                            title: '<strong>Success!</strong>',
                            message: "Silahkan cek email anda!!!"
                        },{
                            type: 'success',
                            placement: {
                                from: "bottom"
                            },
                        });
                        setTimeout(function(){
                            window.location.href = '{base_url}auth/reset_password';
                        },1000);
                    }else{
                        $.notify({
                            title: '<strong>Gagal!</strong>',
                            message: data.message
                        },{
                            type: 'danger',
                            placement: {
                                from: "bottom"
                            },
                        });
                    }
                },
                complete: function(data){
                    $('#submit_register').button('reset');
                },
                error: function(){
                    $('#submit_register').button('reset');
                    $.notify({
                        title: '<strong>Gagal!</strong>',
                        message: "Tolong Cek Koneksi Internet Anda!"
                    },{
                        type: 'danger',
                        placement: {
                            from: "bottom"
                        },
                    });
                }
            });

        });
     </script>