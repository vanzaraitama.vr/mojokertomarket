    <style type="text/css">
    .pointer {
        cursor: pointer;
    }
    img {
        max-width: 30%;
    }
    </style>
    <link rel="stylesheet" type="text/css" href="{base_url}administrator/assets/vendors/dropify/dist/css/dropify.min.css">
    <!-- MAIN -->
		<main class="site-main">
            <div class="columns container">
                <div class="row">

                    <!-- Sidebar -->
                    <div class="col-lg-5 col-md-5 col-sidebar">
                        <!-- Block  Breadcrumb-->  
                        <ol class="breadcrumb no-hide">
                            <li><a href="{base_url}">Home</a></li>
                            <li><a href="{base_url}store">Toko Saya</a></li>
                            <li><a class="active">Transaksi</a></li>
                        </ol>
                       <!-- Block  Breadcrumb-->

                        <!-- block filter products -->
                        <div id="layered-filter-block" class="block-sidebar block-filter no-hide">
                            <div class="close-filter-products"><i class="fa fa-times" aria-hidden="true"></i></div>
                            <div class="block-title">
                                <strong>Detail Transaksi </strong>
                            </div>
                            <div class="block-content">
                                <!-- Filter Item  categori-->
                                <div class="filter-options-item filter-options-categori">
                                    <div class="filter-options-title">No Invoice <?php echo $result['invoice']; ?></div>
                                    <div class="filter-options-content">
                                        <ol class="items">
                                            <li class="item ">
                                                <label>
                                                    <span>Nama Toko: {shipping_detail_user_name}</span>
                                                </label>
                                            </li>
                                            <li class="item ">
                                                <label>
                                                    <span>Pemilik : {shipping_detail_address_name}</span>
                                                </label>
                                            </li>
                                            <li class="item ">
                                                <label>
                                                    <span>No. Tlp : {shipping_detail_phone_number}</span>
                                                </label>
                                            </li>
                                            <li class="item ">
                                                <label>
                                                    <span>Alamat : {shipping_detail_address}></span>
                                                </label>
                                            </li>
                                        </ol>
                                    </div>
                                </div>

                                <div class="filter-options-item filter-options-categori">
                                    <div class="filter-options-title">Metode Pembayaran {payment_method_method}</div>
                                    <div class="filter-options-content">
                                        <ol class="items">
                                            <li class="item ">
                                                <label>
                                                    <span>Nama Bank: {payment_method_bank_name}</span>
                                                </label>
                                            </li>
                                            <li class="item ">
                                                <label>
                                                    <span>No. Rekening : {payment_method_account_number}</span>
                                                </label>
                                            </li>
                                            <li class="item ">
                                                <label>
                                                    <span>Pemilik Rekening : {payment_method_account_name}</span>
                                                </label>
                                            </li>
                                        </ol>
                                    </div>
                                </div>
                                <!-- Filter Item  categori-->  
                            </div>
                        </div>
                        <!-- Filter -->
                    </div>
                    <!-- Sidebar -->

                   
                    <div class="col-lg-7 col-md-7  col-main">
                        <div class="block-form-login">
                            <div class="block-form-create">
                                <div class="block-content">

                                    <div class="block-title">
                                        <strong>Mohon Bayar Sebelum {expired_date}</strong>
                                    </div>
                                    <div class="block-content">
                                        <?php 
                                        if(is_array($detail_data)){
                                            foreach ($detail_data as $key => $value) {
                                                # code...
                                                foreach ($value['details'] as $key2 => $val2) {
                                                    # code...
                                                    echo    '<div class="product-item">';
                                                    echo    '    <div class="product-item-info">';
                                                    echo    '        <div class="product-item-photo">';
                                                    echo    '           <a href="#" class="product-item-img"><img src="'.$val2['product']['images_url'][0].'" alt="'.$val2['product_name'].'"></a>';
                                                    echo    '        </div>';
                                                    echo    '        <div class="product-item-detail">';
                                                    echo    '            <strong class="product-item-name">'.$val2['product_name'].'</strong><br>';
                                                    if($value['status']=="processed"){
                                                        $status_transaksi= "Transaksi Sedang di Proses";
                                                    }else if($value['status']=="paid"){
                                                        $status_transaksi= "Transaksi Sudah di bayar, menunggu Konfirmasi Penjual";
                                                    }else if($value['status']=="pending_payment"){
                                                        $status_transaksi= "Menunggu Pembayaran";
                                                    }else if($value['status']=="shipped"){
                                                        $status_transaksi= "Proses Pengiriman Barang";
                                                    }else if($value['status']=="complete"){
                                                        $status_transaksi= "Selesai";
                                                    }else{
                                                        $status_transaksi= $value['result']['status'];
                                                    }
                                                    echo    '            <strong class="product-item-name">'.$status_transaksi.'</strong><br>';
                                                    echo    '            <div class="product-item-price">';
                                                    echo    '                <span class="">Qty       : '.$val2['qty'].'<br></span>';
                                                    echo    '                <span class="">Harga Satuan : Rp. '.number_format($val2['product_price'],2,',','.').'<br></span>';
                                                    echo    '                <span class="">Discount  : Rp. '.number_format($val2['discount'],2,',','.').'<br></span>';
                                                    echo    '                <span class="">Harga Total : Rp. '.number_format($val2['grandtotal'],2,',','.').'</span>';
                                                    echo    '            </div>';
                                                    echo    '        </div>';
                                                    echo    '    </div>';
                                                    echo    '</div>'; 
                                                }
                                            }
                                        }
                                        ?>
                                        <br><br>
                                    </div>
                                    <form action="{base_url}" name="submit_product" id="submit_product" method="post">
                                     <input type="hidden" name="id" value="{id}">
                                     <input type="hidden" name="invoice" value="{invoice}">
                                     <input type="hidden" name="amount" value="{amount}">
                                       <ol class="items">
                                            <li class="item ">
                                                <label>
                                                    <p>Ongkos Kirim<span class="tab">: Rp. {shipping_detail_cost}</span></p>
                                                </label>
                                            </li>
                                            <li class="item ">
                                                <label>
                                                    <p>Total Bayar<span class="tab">: Rp. {grandtotal}</span></p>
                                                </label>
                                            </li>
                                        </ol>
                                        
                                        <div class="form-group row sh_confirm" style="display: none;">
                                            <div class="col-md-12">
                                                <label for="label_upload">Upload Bukti Transaksi</label>
                                            </div>
                                            <div class="col-md-4">
                                                    <input type="file" class="dropify" id="image_url" name="image_url"/>
                                            </div>
                                        </div>
                                        </div>
                                        <button type="button" class="btn btn-default pull-left" onClick="history.go(-1);" >Kembali</button>
                                        <button type="submit" class="btn btn-inline pull-right sh_confirm" id="btn_submit" name="btn_submit" style="display: none;" disabled="true">Konfirmasi Pembayaran</button>
                                    </form> 
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>

		</main><!-- end MAIN -->

         <script src="{base_url}administrator/assets/vendors/dropify/dist/js/dropify.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('.dropify').dropify();
                if('{btn_action}'==true){
                    $(".sh_confirm").show();
                    $("#btn_submit").removeAttr('disabled');
                }
                

            });

        $('form[name="submit_product"]').submit(function(e){
                e.preventDefault();
                
                var form = document.getElementById('submit_product');
                var formData = new FormData(form);

                $.ajax({
                    url: "{base_url}account/payment_confirmation",
                    enctype: 'multipart/form-data',
                    data: formData,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    crossDomain: true,
                    dataType: 'json',
                    beforeSend: function(){
                        $('#btn_submit').button('loading');
                    },
                    success: function(data){
                        $('#btn_submit').button('reset');
                        if(data.status=='success'){
                            $.notify({
                                title: '<strong>Success!</strong>',
                                message: 'Berhasil Update Status, Barang sedang di kirim.'
                            },{
                                type: 'success',
                                placement: {
                                    from: "bottom"
                                },
                            });
                            history.back();
                        }else{
                            $.notify({
                                title: '<strong>error!</strong>',
                                message: data.message
                            },{
                                type: 'danger',
                                placement: {
                                    from: "bottom"
                                },
                            });
                        }
                    },
                    complete: function(data){
                        $('#btn_submit').button('reset');
                    },
                    error: function(){
                        $('#btn_submit').button('reset');
                        $.notify({
                                title: '<strong>Error!</strong>',
                                message: "Please check your Network Connection!"
                            },{
                                type: 'danger',
                                placement: {
                                    from: "bottom"
                                },
                            });
                    }
                });

            });
        </script>