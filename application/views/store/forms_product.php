        
        <link rel="stylesheet" type="text/css" href="{base_url}administrator/assets/vendors/dropify/dist/css/dropify.min.css">
        <link rel="stylesheet" type="text/css" href="{base_url}assets/js/EasyAutocomplete/easy-autocomplete.min.css"/>       
        <!-- MAIN -->
           <style type="text/css">
            .pointer {
                cursor: pointer;
            }
            </style>
		<main class="site-main" style="background-color: #fff;">

            <!-- breadcrumb -->
            <div class="container breadcrumb-page">
                <ol class="breadcrumb">
                    <li><a href="#">Beranda </a></li>
                    <li><a href="{base_url}store">Toko Saya </a></li>
                    <li class="active">Product</li>
                </ol>
            </div> <!-- breadcrumb -->

            <div class="page-title-base container">
                <h1 class="title-base">Product</h1>
            </div>
            <div>
                <div class="container">
                    <div class="container">
                        <div class="block-form-login">
                            <div class="block-form-create">
                                <div class="block-title">
                                    Product
                                </div>
                                <div class="block-content">
                                    <form action="{base_url}" name="submit_product" id="submit_product" method="post">
                                     <input type="hidden" name="id" value="{id}">   
                                     <input type="hidden" name="shop_id" value="{shop_id}">   
                                     <input type="hidden" name="act" value="{act}">  
                                     <input type="hidden" name="closed" value="0">  
                                        <div class="form-group">
                                            <label class="col-md-3 col-form-label" for="store_name">Nama Product</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" placeholder="Nama Product" name="name" id="name" value="{name}">
                                            </div>
                                        </div>
                                        <br><br><br>
                                        <div class="form-group">
                                            <label class="col-md-3 col-form-label" for="store_name">Kategori Product</label>
                                            <div class="col-md-9">
                                                <select name="category_id" id="category_id" class="form-control">
                                                    {list_categories}
                                                </select>
                                            </div>
                                        </div> 
                                        <br><br><br>                 
                                        <div class="form-group" style="
                                            font-size: 13px;
                                            box-shadow: none;
                                            outline-style: none;
                                            outline-width: 0;
                                            color: #aaa;">
                                            <label class="col-md-3 col-form-label" for="store_name">Keterangan</label>
                                            <div class="col-md-9">
                                                <textarea name="description" id="description" rows="4" cols="43">{description}</textarea>
                                            </div>
                                        </div>
                                        <br><br><br><br>
                                        <div class="form-group">
                                            <label class="col-md-3 col-form-label" for="store_name">Stock</label>
                                            <div class="col-md-9">
                                                <input type="number" class="form-control" placeholder="Stock" name="stock" id="stock" value="{stock}">
                                            </div>
                                        </div>
                                        <br><br><br>
                                        <div class="form-group">
                                            <label class="col-md-3 col-form-label" for="store_name">Harga</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" placeholder="Harga" name="price" id="price" value="{price}">
                                            </div>
                                        </div>
                                        <br><br><br>
                                        <div class="form-group">
                                            <label class="col-md-3 col-form-label" for="store_name">Diskon</label>
                                            <div class="col-md-9">
                                                <input type="number" class="form-control" placeholder="Diskon" name="discount" id="discount" value="{discount}">
                                            </div>
                                        </div>
                                        <br><br><br>
                                        <div class="form-group">
                                            <label class="col-md-3 col-form-label add_field_button pointer btn btn-primary" for="file_attach">
                                                <font color="white">Tambah Gambar Produk</font>
                                            </label>
                                             <div class="col-md-9 input_fields_wrap">

                                             </div>
                                        </div>
                                        <br><br><br>
                                        <button type="button" class="btn btn-danger" id="btn_delete" name="btn_delete" onClick="del_data({id});">Delete</button>
                                        <button type="button" class="btn btn-default pull-right" onClick="history.go(-1);" >Kembali</button>
                                        <button type="submit" class="btn btn-inline pull-right" id="btn_submit" name="btn_submit">Simpan</button>
                                    </form> 
                                </div>
                            </div>
                        </div>
                    </div>

                </div>      
		</main>
        <!-- end MAIN -->
        <script src="{base_url}administrator/assets/vendors/dropify/dist/js/dropify.min.js"></script>
        <script type="text/javascript">
            
            del_data = function (id){   
   
            swal({
                title: "Apa kamu yakin?",
                text: "Kamu tidak dapat mengembalikan data jika sudah di hapus!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Ya, Yakin Hapus",
                cancelButtonText: "Batal",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {

                    $.ajax({
                        url: '{base_url}store/store_product_delete',
                        type: 'POST',
                        dataType: 'json',
                        data: {id: id},
                    })
                    .done(function() {
                        swal({
                            title: "Hapus!",
                            text: "Barang telah terhapus.",
                            type: "success",
                            confirmButtonClass: "btn-success"
                        });
                    })
                    .fail(function() {
                        swal({
                            title: "Error",
                            text: "Data kamu aman :). Gagal hapus barang",
                            type: "error",
                            confirmButtonClass: "btn-danger"
                        });
                    })
                    .always(function() {
                        history.back();
                    });
                              
                } else {
                    swal({
                        title: "Cancelled",
                        text: "Data kamu aman :)",
                        type: "error",
                        confirmButtonClass: "btn-danger"
                    });
                }
            });
        }

            var amount = formatMoney('{price}');
            $("#price").val(amount);

            $("#price").change(function(event) {
                /* Act on the event */
                var amount = formatMoney(this.value);
                //var amount = amount.replace(',','');
                $("#price").val(amount);
            });

            var max_fields      = 15; //maximum input boxes allowed
            var wrapper         = $(".input_fields_wrap"); //Fields wrapper
            var add_button      = $(".add_field_button"); //Add button ID
            
            var x = 1; //initlal text box count
            $(add_button).click(function(e){ //on add input button click
                e.preventDefault();
                //console.log(123);
                if(x < max_fields){ //max input box allowed
                    x++; //text box increment
                    
                    $(wrapper).append('<div class="form-group row">\
                                            <div class="col-md-4">\
                                                <input type="file" class="dropify" id="file_attach" name="file_attach[]"/>\
                                                <a href="" class="remove_field action-remove pointer"><span>Hapus</span></a>\
                                            </div>\
                                        </div>');
                }
                $('.dropify').dropify();
            });

            if('{images_url}'!=''){
                
                $.each(JSON.parse('{images_url}'), function(index, val) {
               
                     x++; //text box increment
                     var img_url = val;
                     $(wrapper).append('<div class="form-group row">\
                                            <div class="col-md-4">\
                                                <input type="file" class="dropify" data-default-file="'+img_url+'" id="file_attach" name="file_attach[]"/>\
                                                <a href="" class="remove_field action-remove pointer"><span>Hapus</span></a>\
                                            </div>\
                                        </div>');
                });
                $('.dropify').dropify();
            }

            $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
                e.preventDefault(); $(this).parent('div').remove();  x--;
            });


            $('form[name="submit_product"]').submit(function(e){
                e.preventDefault();
                
                var form = document.getElementById('submit_product');
                var formData = new FormData(form);

                $.ajax({
                    url: "{base_url}store/product_submit",
                    enctype: 'multipart/form-data',
                    data: formData,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    crossDomain: true,
                    dataType: 'json',
                    beforeSend: function(){
                        $('#btn_submit').button('loading');
                    },
                    success: function(data){
                        $('#btn_submit').button('reset');
                        if(data.status=='success'){
                            $.notify({
                                title: '<strong>Success!</strong>',
                                message: 'Berhasil merubah data toko.'
                            },{
                                type: 'success',
                                placement: {
                                    from: "bottom"
                                },
                            });
                            history.back();
                        }else{
                            $.notify({
                                title: '<strong>error!</strong>',
                                message: data.message
                            },{
                                type: 'danger',
                                placement: {
                                    from: "bottom"
                                },
                            });
                        }
                    },
                    complete: function(data){
                        $('#btn_submit').button('reset');
                    },
                    error: function(){
                        $('#btn_submit').button('reset');
                        $.notify({
                                title: '<strong>Error!</strong>',
                                message: "Please check your Network Connection!"
                            },{
                                type: 'danger',
                                placement: {
                                    from: "bottom"
                                },
                            });
                    }
                });

            });

            function formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
              try {
                decimalCount = Math.abs(decimalCount);
                decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

                const negativeSign = amount < 0 ? "-" : "";

                let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
                let j = (i.length > 3) ? i.length % 3 : 0;

                return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
              } catch (e) {
                console.log(e)
              }
            };
        </script>