        
        <link rel="stylesheet" type="text/css" href="{base_url}administrator/assets/vendors/dropify/dist/css/dropify.min.css">
        <link rel="stylesheet" type="text/css" href="{base_url}assets/js/EasyAutocomplete/easy-autocomplete.min.css"/>       
        <!-- MAIN -->
           <style type="text/css">
            .pointer {
                cursor: pointer;
            }
            </style>
		<main class="site-main" style="background-color: #fff;">

            <!-- breadcrumb -->
            <div class="container breadcrumb-page">
                <ol class="breadcrumb">
                    <li><a href="#">Beranda </a></li>
                    <li><a href="{base_url}store">Toko Saya </a></li>
                    <li class="active">Ubah</li>
                </ol>
            </div> <!-- breadcrumb -->

            <div class="page-title-base container">
                <h1 class="title-base">Ubah Toko</h1>
            </div>
            <div>

                    <div class="container">
                        <div class="block-comment-blog">
                            <div class="block-leave-reply">
                                <div class="block-title">
                                    <span class="title">Toko</span>
                                    
                                </div>
                                <div class="block-content">
                                    <form action="{base_url}" name="submit_store" id="submit_store" method="post">
                                     <input type="hidden" name="id" value="{id}">   
                                     <input type="hidden" name="act" value="{act}">  
                                     <input type="hidden" name="closed" value="{closed}">  
                                        <div class="form-group input_fields_wrap">
                                             
                                        </div>
                                        <div class="form-group">
                                            <label for="store_name">Nama Toko</label>
                                            <input type="text" class="form-control" placeholder="Nama Toko" name="name" id="name" value="{name}">
                                        </div>
                                        <div class="form-group">
                                            <label for="store_name">No. Tlp</label>
                                            <input type="text" class="form-control" placeholder="No Tlp" name="phone_number" id="phone_number" value="{phone_number}">
                                        </div>
                                        <div class="form-group">
                                            <label for="store_name">Keterangan</label>
                                            <input type="text" class="form-control" placeholder="Keterangan" name="description" id="description" value="{description}">
                                           <!-- <textarea class="form-control" id="description" name="description" rows="1" placeholder="Keterangan" style="max-width: 510px;">{description}</textarea>-->
                                        </div>

                                        <div class="form-group">
                                            <label for="store_name">Alamat</label>
                                            <textarea class="form-control" id="address" name="address" rows="1" placeholder="Alamat Toko">{address}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <input type="hidden" class="form-control" placeholder="Latitude" name="latitude" id="latitude" value="{latitude}">
                                        </div>
                                        <div class="form-group">
                                            <input type="hidden" class="form-control" placeholder="Longitude" name="longitude" id="longitude" value="{longitude}">
                                        </div>
                                        
                                        <div class="col-lg-10">
                                            <div id="map"></div>
                                        </div>
                                        <div class="col-lg-12">
                                        <button type="submit" class="btn btn-inline" id="submit_update" name="submit_update">Simpan</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>     
		</main>
        <!-- end MAIN -->
        <script src="{base_url}administrator/assets/vendors/dropify/dist/js/dropify.min.js"></script>
        <script type="text/javascript">
            
            $(document).ready(function() {
                $('.dropify').dropify();

                var max_fields      = 1; //maximum input boxes allowed
                var wrapper         = $(".input_fields_wrap"); //Fields wrapper
               
                if('{image_url}'!=''){
               
                         var img_url = '{image_url}';
                         $(wrapper).append('<div class="form-group row">\
                                                <div class="col-md-4">\
                                                    <input type="file" class="dropify" data-default-file="'+img_url+'" id="image_url" name="image_url"/>\
                                                </div>\
                                            </div>');
                    $('.dropify').dropify();
                }

            });

            $('form[name="submit_store"]').submit(function(e){
                e.preventDefault();
                
                var form = document.getElementById('submit_store');
                var formData = new FormData(form);
                
                formData.append('token', '<?php echo $this->session->userdata('token'); ?>');

                $.ajax({
                    url: "{base_url}store/store_update",
                    enctype: 'multipart/form-data',
                    data: formData,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    crossDomain: true,
                    dataType: 'json',
                    beforeSend: function(){
                        $('#submit_update').button('loading');
                    },
                    success: function(data){
                        $('#submit_update').button('reset');
                        if(data.status=='success'){
                            $.notify({
                                title: '<strong>Success!</strong>',
                                message: 'Berhasil merubah data toko.'
                            },{
                                type: 'success',
                                placement: {
                                    from: "bottom"
                                },
                            });
                            history.back();
                        }else{
                            $.notify({
                                title: '<strong>error!</strong>',
                                message: data.message
                            },{
                                type: 'danger',
                                placement: {
                                    from: "bottom"
                                },
                            });
                        }
                    },
                    complete: function(data){
                        $('#submit_update').button('reset');
                    },
                    error: function(){
                        $('#submit_update').button('reset');
                        $.notify({
                                title: '<strong>Error!</strong>',
                                message: "Please check your Network Connection!"
                            },{
                                type: 'danger',
                                placement: {
                                    from: "bottom"
                                },
                            });
                    }
                });

            });

        </script>
        <script>

        $('#map').attr('style','height:400px; width:100%');
            var map, infoWindow;
            var markers = [];
            var poligamis = [];
            var myPolygon = [];

            function initMap() {

                var myLatLng = {lat: -7.472638, lng: 112.434084};

                var map = new google.maps.Map(document.getElementById('map'), {
                  zoom: 17,
                  center: myLatLng
                });

                infoWindow = new google.maps.InfoWindow;

                var geocoder = new google.maps.Geocoder();

               // if (navigator.geolocation) {
               // navigator.geolocation.getCurrentPosition(function(position) {
                    

                    if('{act}'=='Edit'){
                        v_lat = parseInt('{latitude}');
                        v_lng = parseInt('{longitude}');
                    }else{
                        //v_lat = position.coords.latitude;
                        //v_lng = position.coords.longitude;
                        v_lat = myLatLng.lat;
                        v_lng = myLatLng.lng; 
                    }

                    var pos = {
                        lat: v_lat,
                        lng: v_lng
                    };

                    $("#latitude").val(v_lat);
                    $("#logitude").val(v_lng);

                    infoWindow.open(map);
                    map.setCenter(pos);

                    marker = new google.maps.Marker({
                      position: pos,
                      map: map,
                      draggable: true
                    }); 

                    markerCoords(marker);

               // }, function() {
                    //handleLocationError(true, infoWindow, map.getCenter());
                  /*  marker = new google.maps.Marker({
                      position: myLatLng,
                      map: map,
                      draggable: true
                    }); 

                    markerCoords(marker); */
                //});
              /*  } else {
                    handleLocationError(false, infoWindow, map.getCenter());
                    marker = new google.maps.Marker({
                      position: myLatLng,
                      map: map,
                      draggable: true
                    }); 

                    markerCoords(marker);
                } */
                
            }

            function markerCoords(markerobject){
                google.maps.event.addListener(markerobject, 'dragend', function(evt){
                   
                    $("#latitude").val(evt.latLng.lat());
                    $("#logitude").val(evt.latLng.lng());
                  
                    $.ajax({
                        url: '{base_url}account/latlng',
                        type: 'GET',
                        dataType: 'json',
                        data: {latlng: evt.latLng.lat()+','+evt.latLng.lng()},
                    })
                    .done(function(result) {
                        $("#address").val(result.data);
                    })
                    .fail(function() {
                        console.log("error");
                    });
                    
                });

                google.maps.event.addListener(markerobject, 'drag', function(evt){
                    console.log("marker is being dragged");
                });     
            }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyBp3E-Zv44uHcv41brVZH3gdrke4_sWFbU&callback=initMap&libraries=places,drawing&sensor=false"></script>

