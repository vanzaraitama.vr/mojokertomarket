    <link rel="stylesheet" type="text/css" href="{base_url}administrator/assets/vendors/datatables/media/css/dataTables.bootstrap4.css">

    <script src="{base_url}administrator/assets/vendors/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="{base_url}administrator/assets/vendors/datatables/media/js/dataTables.bootstrap4.js"></script>
    <script src="{base_url}administrator/assets/vendors/datatables-fixedcolumns/js/dataTables.fixedColumns.js"></script>
    <script src="{base_url}administrator/assets/vendors/datatables-responsive/js/dataTables.responsive.js"></script>
    <style type="text/css">
    .pointer {
        cursor: pointer;
    }
    table.dataTable thead {background-color:#ebebeb}
    table.dataTable tfoot {background-color:#ebebeb}
        
    </style>
    		<!-- MAIN -->
		<main class="site-main">

            <!-- breadcrumb -->
            <div class="container breadcrumb-page">
                <ol class="breadcrumb">
                    <li><a href="{base_url}">Beranda </a></li>
                    <li class="active">Toko Saya</li>
                </ol>
            </div> 
            <!-- breadcrumb -->

            <div class="page-title-base container">
                <!-- <h1 class="title-base">Toko Saya </h1> -->
            </div>
             <div class="col-md-9 col-md-push-3  col-main">
                <div class="product-info-detailed ">
                    <ul class="nav nav-pills" role="tablist">
                        <li role="presentation" class="active"><a href="#description"  role="tab" data-toggle="tab" onClick="tab_1()">Barang Saya  </a></li>
                        <li role="presentation"><a href="#tags"  role="tab" data-toggle="tab" onClick="tab_2()">Pesanan Baru </a></li>
                        <li role="presentation"><a href="#reviews"  role="tab" data-toggle="tab" onClick="tab_3()">Konfirmasi Pengiriman</a></li>
                        <li role="presentation"><a href="#additional"  role="tab" data-toggle="tab" onClick="tab_4()">Status Pengiriman</a></li>
                        <li role="presentation"><a href="#tab-cust"  role="tab" data-toggle="tab" onClick="tab_5()">Daftar Transaksi</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="description">
                            <div class="block-title">
                                <!-- <a  class="pointer" ><span><i class="fa fa-plus"></i>&nbsp;</span>Tambah Barang</a> --> 
                                 <button type="button" onClick="add_product()" class="btn btn-inline pull-left" >Tambah Barang</button>
                            </div>
                            <div class="block-content">
                                <div class="col-lg-12 col-md-12  col-main">
                                    <!-- Toolbar -->
                                    <div class="toolbar-products toolbar-top">

                                        <div class="btn-filter-products">
                                            <span>Filter</span>
                                        </div>
                                        <div class="modes">
                                            <strong  class="label">View as:</strong>
                                            <strong  class="modes-mode active mode-grid" title="Grid">
                                                <span>grid</span>
                                            </strong>
                                        </div>
                                        <!-- View as -->
                                       
                                        <div class="toolbar-option">

                                            <div class="toolbar-sorter ">
                                                <label  class="label">Short by:</label>
                                                <select class="sorter-options form-control" id="sortBy_top">
                                                    <option value="paling_sesuai">Paling Sesuai</option>
                                                    <option value="penjualan">Penjualan</option>
                                                    <option value="termurah">Termurah</option>
                                                    <option value="termahal">Termahal</option>
                                                    <option value="terbaru">Terbaru</option>
                                                </select>
                                            </div><!-- Short by -->

                                            <div class="toolbar-limiter">
                                                <label class="label">
                                                    <span>Show:</span>
                                                </label>
                                               
                                                <select class="limiter-options form-control" id="perPage_top">
                                                    <option value="9">9</option>
                                                    <option value="15">15</option>
                                                    <option value="30">30</option>
                                                </select>
                                                
                                            </div><!-- Show per page -->

                                        </div>

                                        <ul class="pagination">
                                            <li class="action first_page pointer">
                                                
                                            </li>

                                            <li class="active_page_before_2 pointer" style="display: none;">
                                                
                                            </li>
                                            <li class="active_page_before_1 pointer" style="display: none;">
                                                
                                            </li>
                                            
                                            <li class="active active_page pointer">
                                               
                                            </li>
                                            <li class="active_page_next_1 pointer" style="display: none;">
                                                
                                            </li>
                                            <li class="active_page_next_2 pointer" style="display: none;">
                                                
                                            </li>
                                            <li class="action last_page pointer">
                                                
                                            </li>
                                        </ul>

                                    </div><!-- Toolbar -->

                                    <!-- List Products -->
                                    <div class="products  products-grid">
                                        <ol class="product-items row product-items_tab1">
                                            
                                        </ol><!-- list product -->
                                    </div> <!-- List Products -->

                                    <!-- Toolbar -->
                                    <div class=" toolbar-products toolbar-bottom">

                                        <div class="modes">
                                            <strong  class="label">View as:</strong>
                                            <strong  class="modes-mode active mode-grid" title="Grid">
                                                <span>grid</span>
                                            </strong>
                                           <!-- <a  href="List_Products.html" title="List" class="modes-mode mode-list">
                                                <span>list</span>
                                            </a> -->
                                        </div><!-- View as -->
                                       
                                        <div class="toolbar-option">

                                            <div class="toolbar-sorter ">
                                                <label class="label">Short by:</label>
                                                <select class="sorter-options form-control" id="sortBy_bottom">
                                                    <option value="paling_sesuai">Paling Sesuai</option>
                                                    <option value="penjualan">Penjualan</option>
                                                    <option value="termurah">Termurah</option>
                                                    <option value="termahal">Termahal</option>
                                                    <option value="terbaru">Terbaru</option>
                                                </select>
                                            </div><!-- Short by -->

                                            <div class="toolbar-limiter">
                                                <label   class="label">
                                                    <span>Show:</span>
                                                </label>
                                               
                                                <select class="limiter-options form-control" id="perPage_bottom" >
                                                    <option value="9">9</option>
                                                    <option value="15">15</option>
                                                    <option value="30">30</option>
                                                </select>
                                                
                                            </div><!-- Show per page -->

                                        </div>

                                        <ul class="pagination" id="bottom_pagination">
                                            <input type="hidden" name="page_active" id="page_active" value="1">
                                            <li class="action first_page pointer">
                                               
                                            </li>
                                            <li class="active_page_before_2 pointer" style="display: none;">
                                                
                                            </li>
                                            <li class="active_page_before_1 pointer" style="display: none;">
                                                
                                            </li>
                                            <li class="active active_page pointer">
                                               
                                            </li>
                                            <li class="active_page_next_1 pointer" style="display: none;">
                                                
                                            </li>
                                            <li class="active_page_next_2 pointer" style="display: none;">
                                                
                                            </li>
                                            <li class="action last_page pointer">
                                                
                                            </li>
                                        </ul>

                                    </div>
                                    <!-- Toolbar -->

                                </div>
                           
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="tags">
                            <div class="block-content">
                                <div class="col-lg-12 col-md-12 col-main">

                                   <table class="table table-hover nowrap" id="data-table" width="100%">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Invoice</th>
                                            <th>Biaya Kirim</th>
                                            <th>Total</th>
                                            <th>Batas Waktu</th>
                                            
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>No</th>
                                            <th>Invoice</th>
                                            <th>Biaya Kirim</th>
                                            <th>Total</th>
                                            <th>Batas Waktu</th>
                                           
                                            <th>Action</th>
                                        </tr>
                                        </tfoot>
                                        <tbody id="list_queue_data" >
                                        
                                        </tbody>
                                    </table>

                                </div>
                               
                            
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="reviews">
                           
                            <div class="block-content">
                                <div class="col-lg-12 col-md-12 col-main">

                                   <table class="table table-hover nowrap" id="data-table-2" width="100%">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Invoice</th>
                                            <th>Biaya Kirim</th>
                                            <th>Total</th>
                                            <th>Batas Waktu</th>
                                            <!-- <th>Nama Pembeli</th> -->
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>No</th>
                                            <th>Invoice</th>
                                            <th>Biaya Kirim</th>
                                            <th>Total</th>
                                            <th>Batas Waktu</th>
                                            <!-- <th>Nama Pembeli</th> -->
                                            <th>Action</th>
                                        </tr>
                                        </tfoot>
                                        <tbody id="list_queue_data" >
                                        
                                        </tbody>
                                    </table>

                                </div>  
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="additional">
                           
                            <div class="block-content">
                                <div class="col-lg-12 col-md-12 col-main">

                                   <table class="table table-hover nowrap" id="data-table-3" width="100%">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Invoice</th>
                                            <th>Biaya Kirim</th>
                                            <th>Total</th>
                                            <th>Batas Waktu</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>No</th>
                                            <th>Invoice</th>
                                            <th>Biaya Kirim</th>
                                            <th>Total</th>
                                            <th>Batas Waktu</th>
                                            <th>Action</th>
                                        </tr>
                                        </tfoot>
                                        <tbody id="list_queue_data" >
                                        
                                        </tbody>
                                    </table>

                                </div> 
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="tab-cust">
                           
                            <div class="block-content">
                                <div class="col-lg-12 col-md-12 col-main">

                                   <table class="table table-hover nowrap" id="data-table-4" width="100%">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Invoice</th>
                                            <th>Biaya Kirim</th>
                                            <th>Total</th>
                                            <th>Batas Waktu</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>No</th>
                                            <th>Invoice</th>
                                            <th>Biaya Kirim</th>
                                            <th>Total</th>
                                            <th>Batas Waktu</th>
                                            <th>Action</th>
                                        </tr>
                                        </tfoot>
                                        <tbody id="list_queue_data" >
                                        
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class=" col-md-3 col-md-pull-9   col-sidebar">
                <div class="block-about-us">
                            <div class="block-title">
                                <a onClick="ch_store({id})" class="pointer" ><span>&nbsp;{name}&nbsp;<i class="fa fa-edit"></i></span></a> 
                            </div>
                            <div class="block-content row">
                                <div class="col-md-5 ">
                                    <div class="img">
                                        <img src="{image_url}" alt="store image">
                                    </div>
                                </div>

                                <div class="col-md-7 text  ">
                                    <p>{address}</p>
                                    <p>{phone_number}</p>
                                    <p>{description}</p>
                                </div>
                            </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 ">
                    <hr>
            </div>
		</main>
        <!-- end MAIN -->

<script type="text/javascript">

    tab_2 = function(){
        var baseUrl = '{base_url}';  
        var table =    
         $('#data-table').DataTable({
            bLengthChange: false,
            processing: true,
            destroy:true,
            serverSide: true,
            responsive: true,
            autoFill: true,
            autoWidth: false,
            colReorder: true,
            keys: true,
            rowReorder: false,
            columnDefs: [
                { "width": "5%", "targets": 0 },
                { className: "text-center col-with-icon", "targets": [ 5 ] },
            ],
            ajax: {
                url: baseUrl+"store/store_transaction",
                type:'POST',
            },
            bStateSave: true,
                "fnStateSave": function (oSettings, oData) {
                    localStorage.setItem('offersDataTables', JSON.stringify(oData));
                },
                "fnStateLoad": function (oSettings) {
                    return JSON.parse(localStorage.getItem('offersDataTables'));
                }
        });
    }

    tab_3 = function(){
        var baseUrl = '{base_url}';  
        var table =    
         $('#data-table-2').DataTable({
            bLengthChange: false,
            processing: true,
            destroy:true,
            serverSide: true,
            responsive: true,
            autoFill: true,
            autoWidth: false,
            colReorder: true,
            keys: true,
            rowReorder: false,
            columnDefs: [
                { "width": "5%", "targets": 0 },
                { className: "text-center col-with-icon", "targets": [ 5 ] },
            ],
            ajax: {
                url: baseUrl+"store/store_transaction_shipped",
                type:'POST',
            },
            bStateSave: true,
                "fnStateSave": function (oSettings, oData) {
                    localStorage.setItem('offersDataTables', JSON.stringify(oData));
                },
                "fnStateLoad": function (oSettings) {
                    return JSON.parse(localStorage.getItem('offersDataTables'));
                }
        });
    }

    tab_4 = function(){
        var baseUrl = '{base_url}';  
        var table =    
         $('#data-table-3').DataTable({
            bLengthChange: false,
            processing: true,
            destroy:true,
            serverSide: true,
            responsive: true,
            autoFill: true,
            autoWidth: false,
            colReorder: true,
            keys: true,
            rowReorder: false,
            columnDefs: [
                { "width": "5%", "targets": 0 },
                { className: "text-center col-with-icon", "targets": [ 5 ] },
            ],
            ajax: {
                url: baseUrl+"store/store_transaction_shipped_2",
                type:'POST',
            },
            bStateSave: true,
                "fnStateSave": function (oSettings, oData) {
                    localStorage.setItem('offersDataTables', JSON.stringify(oData));
                },
                "fnStateLoad": function (oSettings) {
                    return JSON.parse(localStorage.getItem('offersDataTables'));
                }
        });
    }
    
    tab_5 = function(){
        var baseUrl = '{base_url}';  
        var table =    
         $('#data-table-4').DataTable({
            bLengthChange: false,
            processing: true,
            destroy:true,
            serverSide: true,
            responsive: true,
            autoFill: true,
            autoWidth: false,
            colReorder: true,
            keys: true,
            rowReorder: false,
            columnDefs: [
                { "width": "5%", "targets": 0 },
                { className: "text-center col-with-icon", "targets": [ 5 ] },
            ],
            ajax: {
                url: baseUrl+"store/store_transaction_all",
                type:'POST',
            },
            bStateSave: true,
                "fnStateSave": function (oSettings, oData) {
                    localStorage.setItem('offersDataTables', JSON.stringify(oData));
                },
                "fnStateLoad": function (oSettings) {
                    return JSON.parse(localStorage.getItem('offersDataTables'));
                }
        });
    }

    ch_store = function (id){
         window.location.href = '{base_url}store/forms_store';
    }

    add_product = function(){
        window.location.href = '{base_url}store/product_detail_add';
    }

    tab_1 = function(){
        var page    =  $("#page_active").val();
        var sortBy  = $("#sortBy_top").val();
        var perPage = $("#perPage_top").val();

        fn_list_product(page,sortBy,perPage); 
    }

    change_Page = function(page){
        
        var page    = page;
        var sortBy  = $("#sortBy_top").val();
        var perPage = $("#perPage_top").val();

        $("#page_active").val(page);

        fn_list_product(page,sortBy,perPage);
    }

    fn_list_product = function (page,sortBy,perPage)  {

        $('.product-items_tab1').html('');

        $.ajax({
            url: '{base_url}store/store_get_product',
            type: 'GET',
            dataType: 'json',
            data: {page:page, sortBy:sortBy, perPage:perPage},
        })
        .done(function(data) {
            if(data.status=="success"){
                var products_grid   = '';
                var result  = data.result.data;
                var total   = data.result.total;

                first_page =            '<a onClick=change_Page(1);>';
                first_page = first_page+'   <span><i aria-hidden="true" class="fa fa-angle-left"></i></span>';
                first_page = first_page+'</a>';

                last_page =           '<a onClick=change_Page('+data.result.last_page+');>';
                last_page = last_page+'     <span><i aria-hidden="true" class="fa fa-angle-right"></i></span>';
                last_page = last_page+'</a>';
                
                $(".first_page").html(first_page);
                $(".last_page").html(last_page);


                if(data.result.current_page=="1"){
                    $(".first_page").hide();
                }else if(data.result.current_page==data.result.last_page){
                    $(".last_page").hide();
                }

                $('.active_page').html('<a href="#">'+data.result.current_page+'</a>');

                sisa = parseInt(data.result.last_page)-parseInt(data.result.current_page);
                sisa_before = parseInt(data.result.current_page)-parseInt(1);
                //console.log(sisa);
                //console.log(sisa_before);
                if(sisa>=2){
                    active_page_next_1 = parseInt(data.result.current_page)+parseInt(1);
                    active_page_next_2 = parseInt(data.result.current_page)+parseInt(2);
                    
                    $('.active_page_next_1').show();
                    $('.active_page_next_2').show();

                    $('.active_page_next_1').html('<a onClick=change_Page('+active_page_next_1+');>'+active_page_next_1+'</a>');
                    $('.active_page_next_2').html('<a onClick=change_Page('+active_page_next_2+');>'+active_page_next_2+'</a>');

                }else if(sisa<2){
                    active_page_next_1 = parseInt(data.result.current_page)+parseInt(1);
                    if(sisa>=1){
                        $('.active_page_next_1').show();
                    }else{
                        $('.active_page_next_1').hide();
                    }
                    $('.active_page_next_2').hide();
                    $(".first_page").show();
                    $('.last_page').hide();
                    
                    $('.active_page_next_1').html('<a onClick=change_Page('+active_page_next_1+');>'+active_page_next_1+'</a>');
                }

                if(sisa_before>0){
                    if(sisa_before<2){
                        active_page_before_1 = parseInt(data.result.current_page)-parseInt(1);
                        $('.active_page_before_1').html('<a onClick=change_Page('+active_page_before_1+');>'+active_page_before_1+'</a>');
                        $('.active_page_before_1').show();
                        $('.active_page_before_2').hide();
                    }else{
                        active_page_before_1 = parseInt(data.result.current_page)-parseInt(1);
                        active_page_before_2 = parseInt(data.result.current_page)-parseInt(2);
                        $('.active_page_before_1').html('<a onClick=change_Page('+active_page_before_1+');>'+active_page_before_1+'</a>');
                        $('.active_page_before_2').html('<a onClick=change_Page('+active_page_before_2+');>'+active_page_before_2+'</a>');
                        $('.active_page_before_1').show();
                        $('.active_page_before_2').show();
                    }
                }else{
                    $('.active_page_before_1').hide();
                    $('.active_page_before_2').hide();
                }

                if(sisa>2){
                    $(".last_page").show();
                }

                if(total>0){
                    
                    $.each(result, function(index, val) {
                       
                         products_grid = '';
                         products_grid = products_grid+'<li class="col-sm-3 product-item product-item-opt-0">';
                         products_grid = products_grid+'   <div class="product-item-info">';
                         products_grid = products_grid+'       <div class="product-item-photo">';

                         var images_url_no = 0;
                         $.each(val.images_url, function(index_image, val_image) {
                             
                              img ='';
                              if(index_image=="0"){
                                img =val_image;
                                products_grid = products_grid+'<a href="{base_url}store/product_detail/'+val.id+'" class="product-item-img"><img src="'+img+'" alt="'+val.name+'"></a>';
                              }     
                         });
                        
                         products_grid = products_grid+'       </div>';
                         products_grid = products_grid+'       <div class="product-item-detail">';
                         products_grid = products_grid+'           <strong class="product-item-name"><a href="{base_url}store/product_detail/'+val.id+'"><i class="fa fa-edit"></i>'+val.name+'</a></strong>';
                         products_grid = products_grid+'           <div class="product-item-price">';
                         products_grid = products_grid+'               <span class="price">Rp.'+formatMoney(val.price)+'</span>';
                         products_grid = products_grid+'           </div>';
                         products_grid = products_grid+'       </div>';
                         products_grid = products_grid+'   </div>';
                         products_grid = products_grid+'</li>';

                         $('.product-items_tab1').append(products_grid);
                    });
                }

            }else{
                var products_grid = '';
                $('.product-items_tab1').html(products_grid);
            }

        })
        .fail(function() {
            console.log("error");
        });
                        
    }

    var page    = 1;
    var sortBy  = $("#sortBy_top").val();
    var perPage = $("#perPage_top").val();

    fn_list_product(page,sortBy,perPage);

</script>