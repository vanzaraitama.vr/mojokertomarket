    <style type="text/css">
    .pointer {
        cursor: pointer;
    }
    </style>
    <!-- MAIN -->
		<main class="site-main">
            <div class="columns container">
                <div class="row">

                    <!-- Sidebar -->
                    <div class="col-lg-5 col-md-5 col-sidebar">
                        <!-- Block  Breadcrumb-->  
                        <ol class="breadcrumb no-hide">
                            <li><a href="{base_url}">Home</a></li>
                            <li><a href="{base_url}store">Toko Saya</a></li>
                            <li><a class="active">Transaksi</a></li>
                        </ol>
                       <!-- Block  Breadcrumb-->

                        <!-- block filter products -->
                        <div id="layered-filter-block" class="block-sidebar block-filter no-hide">
                            <div class="close-filter-products"><i class="fa fa-times" aria-hidden="true"></i></div>
                            <div class="block-title">
                                <strong>Detail Transaksi</strong>
                            </div>
                            <div class="block-content">
                                <!-- Filter Item  categori-->
                                <div class="filter-options-item filter-options-categori">
                                    <div class="filter-options-title">No Invoice :<?php echo $result['invoice']; ?></div>
                                    <div class="filter-options-content">
                                        <ol class="items">
                                            <li class="item ">
                                                <label>
                                                    <span>Nama Toko: <?php echo $result['shop_name']; ?></span>
                                                </label>
                                            </li>
                                            <li class="item ">
                                                <label>
                                                    <span>Pemilik : <?php echo $result['shop_owner_name']; ?></span>
                                                </label>
                                            </li>
                                            <li class="item ">
                                                <label>
                                                    <span>No. Tlp : <?php echo $result['shop_phone_number']; ?></span>
                                                </label>
                                            </li>
                                            <li class="item ">
                                                <label>
                                                    <span>Alamat : <?php echo $result['shop_address']; ?></span>
                                                </label>
                                            </li>
                                        </ol>
                                    </div>
                                </div>
                                <!-- Filter Item  categori-->  

                                <!-- Block  bestseller products-->
                                <div class="block-sidebar block-sidebar-products">
                                    <div class="block-title">
                                        <strong>Detail Produk</strong>
                                    </div>
                                    <div class="block-content">
                                        <?php 
                                        
                                        if(is_array($result['details'])){
                                            foreach ($result['details'] as $key => $value) {
                                                # code...
                                            echo    '<div class="product-item">';
                                            echo    '    <div class="product-item-info">';
                                            echo    '        <div class="product-item-photo">';
                                            echo    '           <a href="#" class="product-item-img"><img src="'.$value['product']['images_url'][0].'" alt="'.$value['product_name'].'"></a>';
                                            echo    '        </div>';
                                            echo    '        <div class="product-item-detail">';
                                            echo    '            <strong class="product-item-name">'.$value['product_name'].'</strong><br>';
                                            echo    '            <div class="product-item-price">';
                                            echo    '                <span class="">Qty       : '.$value['qty'].'<br></span>';
                                            echo    '                <span class="">Harga Satuan : Rp. '.number_format($value['product_price'],2,',','.').'<br></span>';
                                            echo    '                <span class="">Discount  : Rp. '.number_format($value['discount'],2,',','.').'<br></span>';
                                            echo    '                <span class="">Harga Total : Rp. '.number_format($value['grandtotal'],2,',','.').'</span>';
                                            echo    '            </div>';
                                            echo    '        </div>';
                                            echo    '    </div>';
                                            echo    '</div>';
                                            }
                                        }
                                        ?>
                                    </div>
                                </div>
                                <!-- Block  bestseller products-->  
                            </div>
                        </div>
                        <!-- Filter -->
                    </div>
                    <!-- Sidebar -->

                   
                    <div class="col-lg-7 col-md-7  col-main">
                        
                        <div class="block-form-login">
                            <div class="block-form-create">
                                <div class="block-content">
                                    <div class="block-title">
                                            <strong>Status: {status_transaksi}</strong>
                                    </div>
                                    <div class="filter-options-content">
                                        <ol class="items">
                                            <li class="item ">
                                                <label>
                                                    <span>Nama Pembeli: <?php echo $result['user_name']; ?></span>
                                                </label>
                                            </li>
                                            <li class="item ">
                                                <label>
                                                    <span>No. Tlp : <?php echo $result['phone_number']; ?></span>
                                                </label>
                                            </li>
                                            <li class="item ">
                                                <label>
                                                    <span>Alamat : <?php echo $result['address']; ?></span>
                                                </label>
                                            </li>
                                        </ol>
                                    </div><br><br>
                                    <form action="{base_url}" name="submit_product" id="submit_product" method="post">
                                     <input type="hidden" name="id" value="{id}">
                                       <ol class="items">
                                            <li class="item ">
                                                <label>
                                                   <p>Harga<span class="tab">: Rp. <?php echo number_format($result['subtotal'],2,',','.'); ?></span></p> 
                                                </label>
                                            </li>
                                            <li class="item ">
                                                <label>
                                                    <p>Diskon<span class="tab">: Rp. <?php echo number_format($result['discount'],2,',','.'); ?></span></p> 
                                                </label>
                                            </li>
                                            <li class="item ">
                                                <label>
                                                    <p>Biaya Kirim<span class="tab">: Rp. <?php echo number_format($result['shipping_cost'],2,',','.'); ?></span></p>
                                                </label>
                                            </li>
                                            <li class="item ">
                                                <label>
                                                    <p>Total Bayar<span class="tab">: Rp. <?php echo number_format($result['grandtotal'],2,',','.'); ?></span></p>
                                                </label>
                                            </li>
                                        </ol>
                                        <button type="button" class="btn btn-default pull-left" onClick="history.go(-1);" >Kembali</button>
                                        <button type="submit" class="btn btn-inline pull-right" id="btn_submit" name="btn_submit">Proses </button>
                                    </form> 
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>

		</main><!-- end MAIN -->


        <script type="text/javascript">
        $('form[name="submit_product"]').submit(function(e){
                e.preventDefault();
                
                var form = document.getElementById('submit_product');
                var formData = new FormData(form);

                $.ajax({
                    url: "{base_url}store/submit_store_transaction_process",
                    enctype: 'multipart/form-data',
                    data: formData,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    crossDomain: true,
                    dataType: 'json',
                    beforeSend: function(){
                        $('#btn_submit').button('loading');
                    },
                    success: function(data){
                        $('#btn_submit').button('reset');
                        if(data.status=='success'){
                            $.notify({
                                title: '<strong>Success!</strong>',
                                message: 'Berhasil Update Status, Barang sedang di proses.'
                            },{
                                type: 'success',
                                placement: {
                                    from: "bottom"
                                },
                            });
                            history.back();
                        }else{
                            $.notify({
                                title: '<strong>error!</strong>',
                                message: data.message
                            },{
                                type: 'danger',
                                placement: {
                                    from: "bottom"
                                },
                            });
                        }
                    },
                    complete: function(data){
                        $('#btn_submit').button('reset');
                    },
                    error: function(){
                        $('#btn_submit').button('reset');
                        $.notify({
                                title: '<strong>Error!</strong>',
                                message: "Please check your Network Connection!"
                            },{
                                type: 'danger',
                                placement: {
                                    from: "bottom"
                                },
                            });
                    }
                });

            });
        </script>