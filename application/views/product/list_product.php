    <style type="text/css">
    .pointer {
        cursor: pointer;
    }
    </style>
    <!-- MAIN -->
		<main class="site-main">
            <div class="columns container">
                <div class="row">

                    <!-- Sidebar -->
                    <div class="col-lg-3 col-md-3 col-sidebar">

                        <!-- Block  Breadcrumb-->
                        
                        <ol class="breadcrumb no-hide">
                            <li><a href="{base_url}">Home</a></li>
                            {breadcrumb}
                        </ol>
                       <!-- Block  Breadcrumb-->

                        <!-- Block  bestseller products-->
                        <div class="block-sidebar block-sidebar-products">
                            <div class="block-title">
                                <strong>Popular products</strong>
                            </div>
                            <div class="block-content" id="bestseller">
                                
                            </div>
                        </div>
                        <!-- Block  bestseller products-->

                    </div><!-- Sidebar -->

                    <!-- Main Content -->
                    <div class="col-lg-9 col-md-9  col-main">

                        <!-- images categori -->
                      <!--  <div class="category-view">
                            <div class="category-image">
                                <img alt="category-images" src="{base_url}assets/images/media/index1/category-images.jpg" >
                            </div>
                        </div> -->
                        <!-- images categori -->

                        <!-- Toolbar -->
                        <div class=" toolbar-products toolbar-top">

                            <div class="btn-filter-products">
                                <span>Filter</span>
                            </div>
                            <div class="modes">
                                <strong  class="label">View as:</strong>
                                <strong  class="modes-mode active mode-grid" title="Grid">
                                    <span>grid</span>
                                </strong>
                            </div>
                            <!-- View as -->
                           
                            <div class="toolbar-option">

                                <div class="toolbar-sorter ">
                                    <label  class="label">Short by:</label>
                                    <select class="sorter-options form-control" id="sortBy_top">
                                        <option value="paling_sesuai">Paling Sesuai</option>
                                        <option value="penjualan">Penjualan</option>
                                        <option value="termurah">Termurah</option>
                                        <option value="termahal">Termahal</option>
                                        <option value="terbaru">Terbaru</option>
                                    </select>
                                </div><!-- Short by -->

                                <div class="toolbar-limiter">
                                    <label class="label">
                                        <span>Show:</span>
                                    </label>
                                   
                                    <select class="limiter-options form-control" id="perPage_top">
                                        <option value="9">9</option>
                                        <option value="15">15</option>
                                        <option value="30">30</option>
                                    </select>
                                    
                                </div><!-- Show per page -->

                            </div>

                            <ul class="pagination">
                                <li class="action first_page pointer">
                                    
                                </li>

                                <li class="active_page_before_2 pointer" style="display: none;">
                                    
                                </li>
                                <li class="active_page_before_1 pointer" style="display: none;">
                                    
                                </li>
                                
                                <li class="active active_page pointer">
                                   
                                </li>
                                <li class="active_page_next_1 pointer" style="display: none;">
                                    
                                </li>
                                <li class="active_page_next_2 pointer" style="display: none;">
                                    
                                </li>
                                <li class="action last_page pointer">
                                    
                                </li>
                            </ul>

                        </div><!-- Toolbar -->

                        <!-- List Products -->
                        <div class="products  products-grid">
                            <ol class="product-items row">
                                
                            </ol><!-- list product -->
                        </div> <!-- List Products -->

                        <!-- Toolbar -->
                        <div class=" toolbar-products toolbar-bottom">

                            <div class="modes">
                                <strong  class="label">View as:</strong>
                                <strong  class="modes-mode active mode-grid" title="Grid">
                                    <span>grid</span>
                                </strong>
                               <!-- <a  href="List_Products.html" title="List" class="modes-mode mode-list">
                                    <span>list</span>
                                </a> -->
                            </div><!-- View as -->
                           
                            <div class="toolbar-option">

                                <div class="toolbar-sorter ">
                                    <label class="label">Short by:</label>
                                    <select class="sorter-options form-control" id="sortBy_bottom">
                                        <option value="paling_sesuai">Paling Sesuai</option>
                                        <option value="penjualan">Penjualan</option>
                                        <option value="termurah">Termurah</option>
                                        <option value="termahal">Termahal</option>
                                        <option value="terbaru">Terbaru</option>
                                    </select>
                                </div><!-- Short by -->

                                <div class="toolbar-limiter">
                                    <label   class="label">
                                        <span>Show:</span>
                                    </label>
                                   
                                    <select class="limiter-options form-control" id="perPage_bottom" >
                                        <option value="9">9</option>
                                        <option value="15">15</option>
                                        <option value="30">30</option>
                                    </select>
                                    
                                </div><!-- Show per page -->

                            </div>

                            <ul class="pagination" id="bottom_pagination">
                                <input type="hidden" name="page_active" id="page_active" value="1">
                                <li class="action first_page pointer">
                                   
                                </li>
                                <li class="active_page_before_2 pointer" style="display: none;">
                                    
                                </li>
                                <li class="active_page_before_1 pointer" style="display: none;">
                                    
                                </li>
                                <li class="active active_page pointer">
                                   
                                </li>
                                <li class="active_page_next_1 pointer" style="display: none;">
                                    
                                </li>
                                <li class="active_page_next_2 pointer" style="display: none;">
                                    
                                </li>
                                <li class="action last_page pointer">
                                    
                                </li>
                            </ul>

                        </div><!-- Toolbar -->

                    </div>
                    <!-- Main Content -->
                    
                </div>
            </div>

		</main><!-- end MAIN -->


        <script type="text/javascript">
            jQuery(document).ready(function($) {   

                change_Page = function(page){
                    
                    var page    = page;
                    var sortBy  = $("#sortBy_top").val();
                    var perPage = $("#perPage_top").val();

                    $("#page_active").val(page);

                    fn_list_product(page,sortBy,perPage);
                }

                fn_list_product = function (page,sortBy,perPage)  {

                    $('.product-items').html('');

                    $.ajax({
                        url: '{base_url}product/get_list_product',
                        type: 'GET',
                        dataType: 'json',
                        data: {category: '{category}', sub_category: '{sub_category}', id:'{id}',
                        page:page, sortBy:sortBy, perPage:perPage},
                    })
                    .done(function(data) {
                        if(data.status=="success"){
                            var products_grid   = '';
                            var result  = data.result.data;
                            var total   = data.result.total;

                            first_page =            '<a onClick=change_Page(1);>';
                            first_page = first_page+'   <span><i aria-hidden="true" class="fa fa-angle-left"></i></span>';
                            first_page = first_page+'</a>';

                            last_page =           '<a onClick=change_Page('+data.result.last_page+');>';
                            last_page = last_page+'     <span><i aria-hidden="true" class="fa fa-angle-right"></i></span>';
                            last_page = last_page+'</a>';
                            
                            $(".first_page").html(first_page);
                            $(".last_page").html(last_page);


                            if(data.result.current_page=="1"){
                                $(".first_page").hide();
                            }else if(data.result.current_page==data.result.last_page){
                                $(".last_page").hide();
                            }

                            $('.active_page').html('<a href="#">'+data.result.current_page+'</a>');

                            sisa = parseInt(data.result.last_page)-parseInt(data.result.current_page);
                            sisa_before = parseInt(data.result.current_page)-parseInt(1);
                            //console.log(sisa);
                            //console.log(sisa_before);
                            if(sisa>=2){
                                active_page_next_1 = parseInt(data.result.current_page)+parseInt(1);
                                active_page_next_2 = parseInt(data.result.current_page)+parseInt(2);
                                
                                $('.active_page_next_1').show();
                                $('.active_page_next_2').show();

                                $('.active_page_next_1').html('<a onClick=change_Page('+active_page_next_1+');>'+active_page_next_1+'</a>');
                                $('.active_page_next_2').html('<a onClick=change_Page('+active_page_next_2+');>'+active_page_next_2+'</a>');

                            }else if(sisa<2){
                                active_page_next_1 = parseInt(data.result.current_page)+parseInt(1);
                                if(sisa>=1){
                                    $('.active_page_next_1').show();
                                }else{
                                    $('.active_page_next_1').hide();
                                }
                                $('.active_page_next_2').hide();
                                $(".first_page").show();
                                $('.last_page').hide();
                                
                                $('.active_page_next_1').html('<a onClick=change_Page('+active_page_next_1+');>'+active_page_next_1+'</a>');
                            }

                            if(sisa_before>0){
                                if(sisa_before<2){
                                    active_page_before_1 = parseInt(data.result.current_page)-parseInt(1);
                                    $('.active_page_before_1').html('<a onClick=change_Page('+active_page_before_1+');>'+active_page_before_1+'</a>');
                                    $('.active_page_before_1').show();
                                    $('.active_page_before_2').hide();
                                }else{
                                    active_page_before_1 = parseInt(data.result.current_page)-parseInt(1);
                                    active_page_before_2 = parseInt(data.result.current_page)-parseInt(2);
                                    $('.active_page_before_1').html('<a onClick=change_Page('+active_page_before_1+');>'+active_page_before_1+'</a>');
                                    $('.active_page_before_2').html('<a onClick=change_Page('+active_page_before_2+');>'+active_page_before_2+'</a>');
                                    $('.active_page_before_1').show();
                                    $('.active_page_before_2').show();
                                }
                            }else{
                                $('.active_page_before_1').hide();
                                $('.active_page_before_2').hide();
                            }

                            if(sisa>2){
                                $(".last_page").show();
                            }

                            if(total>0){
                                
                                $.each(result, function(index, val) {
                                     /* iterate through array or object */
                                     products_grid = '';
                                     products_grid = products_grid+'<li class="col-sm-4 product-item product-item-opt-0">';
                                     products_grid = products_grid+'   <div class="product-item-info">';
                                     products_grid = products_grid+'       <div class="product-item-photo">';

                                     var images_url_no = 0;
                                     $.each(val.images_url, function(index_image, val_image) {
                                          /* iterate through array or object */
                                          img ='';
                                          if(index_image=="0"){
                                            img =val_image;
                                            products_grid = products_grid+'<a href="{base_url}product/detail/'+val.slug+'/'+val.id+'" class="product-item-img"><img src="'+img+'" alt="'+val.name+'" width="247px" height="187px"></a>';
                                          }     
                                     });
                                     //console.log(val);
                                     products_grid = products_grid+'       </div>';
                                     products_grid = products_grid+'       <div class="product-item-detail">';
                                     products_grid = products_grid+'           <strong class="product-item-name"><a href="{base_url}product/detail/'+val.slug+'/'+val.id+'">'+val.name+'</a></strong>';
                                     products_grid = products_grid+'           <div class="product-item-price">';
                                        if(val.discount>0){
                                            products_grid=products_grid+'                <span class="price"><font color="red"><strike>Rp.'+formatMoney(val.price)+'</strike></font><br></span>';
                                            products_grid=products_grid+'                <span class="price">Rp.'+formatMoney(val.price_after)+'<font color="red"> '+val.discount+'%OFF</font></span>';
                                        }else{
                                            products_grid=products_grid+'                <span class="price">Rp.'+formatMoney(val.price)+'</font></span>';
                                        }
                                     products_grid = products_grid+'           </div>';
                                     products_grid = products_grid+'       </div>';
                                     products_grid = products_grid+'   </div>';
                                     products_grid = products_grid+'</li>';

                                     $('.product-items').append(products_grid);
                                });
                            }

                        }else{
                            var products_grid = '';
                            $('.product-items').html(products_grid);
                        }

                    })
                    .fail(function() {
                        console.log("error");
                    });
                        
                }

                $("#sortBy_top").val("paling_sesuai");
                $("#sortBy_bottom").val("paling_sesuai");

                $("#perPage_top").val("9");
                $("#perPage_bottom").val("9");

                var page    = 1;
                var sortBy  = $("#sortBy_top").val();
                var perPage = $("#perPage_top").val();

                fn_list_product(page,sortBy,perPage);


                $("#sortBy_top").change(function(event) {
                    /* Act on the event */
                    var page    = $("#page_active").val();
                    var sortBy  = this.value;
                    var perPage = $("#perPage_top").val();

                    $("#sortBy_bottom").val(sortBy);
                    fn_list_product(page,sortBy,perPage);
                });

                $("#sortBy_bottom").change(function(event) {
                    /* Act on the event */
                    var page    = $("#page_active").val();
                    var sortBy  = this.value;
                    var perPage = $("#perPage_top").val();

                    $("#sortBy_top").val(sortBy);
                    fn_list_product(page,sortBy,perPage);
                });

                $("#perPage_top").change(function(event) {
                    /* Act on the event */
                    var page    = $("#page_active").val();
                    var sortBy  = $("#sortBy_top").val();
                    var perPage = this.value;

                    $("#perPage_bottom").val(perPage);
                    fn_list_product(page,sortBy,perPage);
                });

                 $("#perPage_bottom").change(function(event) {
                    /* Act on the event */
                    var page    = $("#page_active").val();
                    var sortBy  = $("#sortBy_top").val();
                    var perPage = this.value;

                    $("#perPage_top").val(perPage);
                    fn_list_product(page,sortBy,perPage);
                });

              /*  $.ajax({
                     url: '{base_url}home/categories',
                     type: 'GET',
                     dataType: 'json'
                 })
                 .done(function(data) {
                     if(data.status=="success"){
                        var result = data.result;
                
                        var categories = '';
                        $.each(result, function(index, val) {
                            
                            categories = categories+'<li class="item ">';
                            categories = categories+'<label>';
                            categories = categories+'<input type="checkbox"><span>'+val.name+'</span>';
                            categories = categories+'</label>';
                            categories = categories+'</li>';
                        });

                     }else{
                        var categories = '';
                     }
                     $('#shopby_categories').html(categories);
                 })
                 .fail(function() {
                     console.log("error");
                 });

                */
                 $.ajax({
                     url: '{base_url}product/best_seller',
                     type: 'GET',
                     dataType: 'json',
                     data: {category: '{category}', sub_category: '{sub_category}'},
                 })
                 .done(function(data) {
                     if(data.status=="success"){
                        var result  = data.result;
                        var total   = result.total;

                        var bestseller = '';
                       
                            $.each(result, function(index, val) {
                                 /* iterate through array or object */

                                bestseller= bestseller+' <div class="product-item">';
                                bestseller= bestseller+'    <div class="product-item-info">';
                                bestseller= bestseller+'        <div class="product-item-photo">';

                                 var images_url_no = 0;
                                 $.each(val.images_url, function(index_image, val_image) {
                                          /* iterate through array or object */
                                          img ='';
                                          if(index_image=="0"){
                                            img =val_image;
                                            bestseller = bestseller+'<a href="{base_url}product/detail/'+val.slug+'/'+val.id+'" class="product-item-img"><img src="'+img+'" alt="'+val.name+'" width="110px" height="70px"></a>';
                                          }     
                                     });

                                bestseller= bestseller+'        </div>';
                                bestseller= bestseller+'        <div class="product-item-detail">';
                                bestseller= bestseller+'            <strong class="product-item-name"><a href="{base_url}product/detail/'+val.slug+'/'+val.id+'">'+val.name+'</a></strong>';
                                bestseller= bestseller+'            <div class="product-item-price">';
                                        if(val.discount>0){
                                            bestseller=bestseller+'                <span class="price"><font color="red"><strike>Rp.'+formatMoney(val.price)+'</strike></font><br></span>';
                                            bestseller=bestseller+'                <span class="price">Rp.'+formatMoney(val.price_after)+'</span>';
                                        }else{
                                            bestseller=bestseller+'                <span class="price">Rp.'+formatMoney(val.price)+'</font></span>';
                                        }
                                bestseller= bestseller+'            </div>';
                                bestseller= bestseller+'        </div>';
                                bestseller= bestseller+'    </div>';
                                bestseller= bestseller+' </div>';
                            });

                     }else{
                        var bestseller = '';
                     }

                     $('#bestseller').html(bestseller);
                 })
                 .fail(function() {
                     console.log("error");
                 });

                 add_wishlist = function(id){

                    $.ajax({
                        url: '{base_url}account/add_wishlists',
                        type: 'POST',
                        dataType: 'json',
                        data: {product_id: id},
                    })
                    .done(function(data) {
                        if(data.status=="success"){
                            $.notify({
                                title: '<strong>Success!</strong>',
                                message: 'Success Add Wishlist'
                            },{
                                type: 'success',
                                placement: {
                                    from: "bottom"
                                },
                            });
                        }else{

                            $.notify({
                                title: '<strong>error!</strong>',
                                message: 'Fail Add to Wishlist, Please Try Again!'
                            },{
                                type: 'danger',
                                placement: {
                                    from: "bottom"
                                },
                            });
                        }
                    })
                    .fail(function() {
                            $.notify({
                                title: '<strong>error!</strong>',
                                message: 'Fail Add to Wishlist, Please Try Again!'
                            },{
                                type: 'danger',
                                placement: {
                                    from: "bottom"
                                },
                            });
                    });
                    
                 }

                 add_cart = function(id){

                    $.ajax({
                        url: '{base_url}account/add_cart',
                        type: 'POST',
                        dataType: 'json',
                        data: {product_id: id, qty:'1'},
                    })
                    .done(function(data) {
                        if(data.status=="success"){
                            $.notify({
                                title: '<strong>Success!</strong>',
                                message: 'Success Add Cart'
                            },{
                                type: 'success',
                                placement: {
                                    from: "bottom"
                                },
                            });
                        }else{

                            $.notify({
                                title: '<strong>error!</strong>',
                                message: 'Fail Add to Cart, Please Try Again!'
                            },{
                                type: 'danger',
                                placement: {
                                    from: "bottom"
                                },
                            });
                        }
                    })
                    .fail(function() {
                            $.notify({
                                title: '<strong>error!</strong>',
                                message: 'Fail Add to Cart, Please Try Again!'
                            },{
                                type: 'danger',
                                placement: {
                                    from: "bottom"
                                },
                            });
                    });
                 }
                 
            });

            function formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
                      try {
                        decimalCount = Math.abs(decimalCount);
                        decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

                        const negativeSign = amount < 0 ? "-" : "";

                        let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
                        let j = (i.length > 3) ? i.length % 3 : 0;

                        return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
                      } catch (e) {
                        console.log(e)
                      }
            };
        </script>