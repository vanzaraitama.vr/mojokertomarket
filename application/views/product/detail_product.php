<!DOCTYPE html>
<html lang="en">
<head>
    <title>{title}</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="{base_url}administrator/assets/modules/core/common/img/logo-pemkot.png" type="image/x-icon" />
    <link rel="shortcut icon" href="{base_url}administrator/assets/modules/core/common/img/logo-pemkot.png" type="image/x-icon" />
    <!-- Style CSS -->
    <link rel="stylesheet" type="text/css" href="{base_url}assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/bootstrap-sweetalert/dist/sweetalert.css">

</head>
<body class="index-opt-1 catalog-product-view catalog-view_default page-no-bg">

    <div class="wrapper">

        <!-- HEADER -->
        <header class="site-header header-opt-1">

            <!-- header-top -->
            <div class="header-top">
                <div class="container">

                    <!-- hotline -->
                    <ul class="hotline nav-left" >
                        <li ><span><i class="fa fa-phone" aria-hidden="true"></i>+6282334145400</span></li>
                        <li ><span><i class="fa fa-envelope" aria-hidden="true"></i>mojokertomart@gmail.com</span></li>
                    </ul><!-- hotline -->

                    <!-- links -->
                    <ul class="links nav-right">
                        <?php 
                            if($this->session->userdata('is_logged_in') == true){
                        ?>
                        <li class="dropdown setting">
                            <a data-toggle="dropdown" role="button" href="#" class="dropdown-toggle "><span>Pengaturan</span> <i aria-hidden="true" class="fa fa-angle-down"></i></a>
                            <div class="dropdown-menu">
                                <ul class="account">
                                    <li><a href="{base_url}account/wishlist">Barang Favorit</a></li>
                                    <li><a href="{base_url}account">Akun Saya</a></li>
                                    <li><a href="{base_url}account/address">Alamat</a></li>
                                    <li><a href="{base_url}account/checkout">Keranjang Belanja</a></li>
                                    <li><a href="{base_url}account/transaction">Daftar Transaksi</a></li>
                                    <li><a href="{base_url}store">Toko Saya</a></li>
                                    <li><a href="{base_url}auth/logout">Logout</a></li>
                                </ul>
                            </div>
                        </li>
                        <?php
                            }else{
                        ?>
                            <li ><a href="{base_url}auth">Login/Register</a></li>
                        <?php
                            }
                        ?>
                        <li><a href="{base_url}home/about">Tentang Kami</a></li>
                        <li><a href="{base_url}home/contact_us">Hubungi Kami </a></li>
                    </ul><!-- links -->

                </div>
            </div>
            <!-- header-top -->

            <!-- header-nav -->
            <div class=" header-nav mid-header">
                <div class="container">
                    <div class="box-header-nav">

                        <span onclick="check_menu();" data-action="toggle-nav-cat" class="nav-toggle-menu nav-toggle-cat"><span>Kategori</span><i aria-hidden="true" class="fa fa-bars"></i></span>
                        
                        <div class="block-nav-categori" id="block_1">
                            <div class="block-title" id="block_2" onclick="check_menu();">
                                <span>Kategori</span>
                            </div>
                            <!--<div class="block-content ex1" id="ex1" style="display: none;">-->
                            <div class="block-content" id="ex1">
                                <ul class="ui-categori" id="categories">
                                </ul>
                                <div class="view-all-categori">
                                    <a  class="open-cate btn-view-all">Semua Kategori</a>
                                </div>
                            </div> 
                        </div>

                        <!-- menu -->
                        <div class="block-nav-menu">
                            <ul class="ui-menu">
                                <li class="">
                                    <a href="{base_url}">Home</a>
                                    <span class="toggle-submenu"></span>
                                </li>
                            </ul>
                        </div>
                        <!-- menu -->

                        <span data-action="toggle-nav" class="nav-toggle-menu"><span>Menu</span><i aria-hidden="true" class="fa fa-bars"></i></span>
                        
                        <div class="block-minicart dropdown ">
                            <a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                <span class="cart-icon"></span>
                            </a>
                            <div class="dropdown-menu">
                                <form>
                                    <div  class="minicart-content-wrapper" >
                                        <div class="subtitle">
                                            Kamu Memiliki <span id="count_cart">0</span> barang di keranjang belanja
                                        </div>
                                        <div class="minicart-items-wrapper">
                                            <ol class="minicart-items" id="minicart_items">
                                                
                                            </ol>
                                        </div>

                                        <div class="actions">
                                            <a href="{base_url}account/checkout" class="btn btn-checkout" type="button" title="Check Out" id="btn_minicart">
                                                Checkout
                                            </a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="block-search">
                            <div class="block-title">
                                <span>Search</span>
                            </div>
                            <div class="block-content">
                                <div class="form-search">
                                    <form>
                                        <div class="box-group">
                                            <input type="text" class="form-control" placeholder="Search here...">
                                            <button class="btn btn-search" type="button"><span>search</span></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        
                        <div class="dropdown setting">
                            <a data-toggle="dropdown" role="button" href="#" class="dropdown-toggle "><span>Settings</span> <i aria-hidden="true" class="fa fa-user"></i></a>
                            <div class="dropdown-menu">
                                <ul class="account">
                                    <?php 
                                    if($this->session->userdata('is_logged_in') == true){
                                    ?>
                                    <li><a href="{base_url}account/wishlist">Barang Favorit</a></li>
                                    <li><a href="{base_url}account">Akun Saya</a></li>
                                    <li><a href="{base_url}account/address">Alamat</a></li>
                                    <li><a href="{base_url}account/checkout">Keranjang Belanja</a></li>
                                    <li><a href="{base_url}account/transaction">Daftar Transaksi</a></li>
                                    <li><a href="{base_url}store">Toko Saya</a></li>
                                    <li><a href="{base_url}auth/logout">Logout</a></li>
                                    <?php
                                    }else{
                                    ?>
                                    <li><a href="{base_url}auth">Login/Register</a></li>
                                    <?php
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>                        
                    </div>
                </div>
            </div><!-- header-nav -->

        </header><!-- end HEADER -->  

        <!-- MAIN -->
        <main class="site-main">

            <!-- breadcrumb -->
            <div class="container breadcrumb-page">
                <ol class="breadcrumb no-hide">
                    <li><a href="{base_url}">Home</a></li>
                    {breadcrumb}
                </ol>
            </div> <!-- breadcrumb -->

            <div class="container">
                <div class="row">
                    
                    <div class="col-sm-7 col-md-6 col-lg-5">

                        <div class="product-media media-horizontal">

                            {data_image}
                            
                            <div class="product_preview images-small">

                                <div class="owl-carousel thumbnails_carousel" id="thumbnails"  data-nav="true" data-dots="false" data-margin="15" data-responsive='{"0":{"items":3},"480":{"items":4},"600":{"items":5},"768":{"items":3}}'>
                                    
                                    {data_thumbnail}

                                </div>
                                <!--/ .owl-carousel-->

                            </div>

                            <!--/ .product_preview-->

                        </div><!-- image product -->
                    </div>

                    <div class="col-sm-5 col-md-6 col-lg-7"> 

                        <div class="product-info-main">

                            <h1 class="page-title">
                                {name}
                            </h1>

                            <div class="product-info-price">
                                <div class="price-box">
                                    <span class="price detail_price"></span>
                                    
                                </div>

                                <div class="product-info-stock-sku">
                                    <div class="stock available">
                                        <span class="label">Ketersediaan barang: </span>{availibility}
                                    </div>
                                </div>
                            </div>

                            <div class="product-overview">
                                <div class="overview-label">Deskripsi Produk</div>
                                <div class="overview-content">
                                    {description}
                                </div>
                            </div>

                            <div class="product-add-form">
                                <form >
                                    <div class="product-options-bottom clearfix">
                                        <div class="form-group form-qty">
                                            <label for="forSize">Qty </label>
                                            <div class="control">
                                                <input type="text" class="form-control input-qty" value='1' id="qty1" name="qty1"  maxlength="{stock}"  minlength="1">
                                                <button class="btn-number  qtyminus" data-type="minus" data-field="qty1">-</button>
                                                <button class="btn-number  qtyplus" data-type="plus" data-field="qty1">+</button>
                                            </div>
                                        </div>
                                        <div class="actions">
                                            
                                                <button onClick="add_cart({id})" type="button" title="Tambahkan Ke Keranjang" class="action btn-cart">
                                                    <span>Tambahkan Ke Keranjang</span>
                                                </button>
                                            
                                                <a onClick="add_wishlist({id})" class="action btn-wishlist" title="Wish List">
                                                    <span>Favorit</span>
                                                </a>
                                        </div>
                                       
                                    </div>

                                </form>
                            </div>

                        </div><!-- detail- product -->

                    </div><!-- Main detail -->

                </div>
            </div>

            <!-- product tab info -->
            <div class="container">
                <div class="product-info-detailed ">

                    <!-- Nav tabs -->
                    <ul class="nav nav-pills" role="tablist">
                        <li role="presentation" class="active"><a href="#store_name"  role="tab" data-toggle="tab">Nama Toko </a></li>
                        <li role="presentation" ><a href="#store_location"  role="tab" data-toggle="tab">Lokasi Toko  </a></li>
                        <li role="presentation" ><a href="#store_phone"  role="tab" data-toggle="tab">No Tlp. Toko  </a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="store_name">
                            <div class="block-title">Nama Toko</div>
                            <div class="block-content">
                                <strong id="store_name_content">{store_name}</strong>
                                <p id="store_descriptions"> {store_descriptions} </p>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="store_location">
                            <div class="block-title">Lokasi Toko</div>
                            <div class="block-content" id="store_location_content">
                                {store_address}
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="store_phone">
                            <div class="block-title">No Tlp. Toko</div>
                            <div class="block-content" id="store_phone_content">
                               {store_phone} 
                            </div>
                        </div>
                    </div>
                </div>  
            </div><!-- product tab info -->

        <!-- FOOTER -->
        <footer class="site-footer footer-opt-2">

            <div class="container">
                <div class="footer-column">
                
                    <div class="row">
                        <div class="col-md-4 col-lg-4 col-sm-6">
                            <strong class="logo-footer">
                               Mojokerto Market
                            </strong>

                            <table class="address">
                                <tr>
                                    <td><b>Address:  </b></td>
                                    <td>Dinas Perindustrian & Perdagangan, Mergelo, Meri, Magersari, Mojokerto City, East Java 61315</td>
                                </tr>
                                <tr>
                                    <td><b>Phone: </b></td>
                                    <td>+6282334145400</td>
                                </tr>
                                <tr>
                                    <td><b>Email:</b></td>
                                    <td>mojokertomart@gmail.com</td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-2 col-lg-2 col-sm-6">
                            <div class="links">
                            <h3 class="title">Products</h3>
                            <ul>
                                <li><a href="#">My Order</a></li>
                                <li><a href="#">My Wishlist</a></li>
                            </ul>
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-4 col-sm-6">
                            <div class="block-social">
                                <div class="block-title">Let’s Socialize </div>
                                <div class="block-content">
                                    <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-camera" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="copyright">
                    Copyright © 2018 {title}. All Rights Reserved. Designed by {title} 
                </div>
            </div>
        </footer><!-- end FOOTER -->        
        
        <!--back-to-top  -->
        <a href="#" class="back-to-top">
            <i aria-hidden="true" class="fa fa-angle-up"></i>
        </a>
        
    </div>

    <!-- jQuery -->    
    <script type="text/javascript" src="{base_url}assets/js/jquery.min.js"></script>

    <!-- sticky -->
    <script type="text/javascript" src="{base_url}assets/js/jquery.sticky.js"></script>

    <!-- OWL CAROUSEL Slider -->    
    <script type="text/javascript" src="{base_url}assets/js/owl.carousel.min.js"></script>

    <!-- Boostrap --> 
    <script type="text/javascript" src="{base_url}assets/js/bootstrap.min.js"></script>

    <!-- Countdown --> 
    <script type="text/javascript" src="{base_url}assets/js/jquery.countdown.min.js"></script>

    <!--jquery Bxslider  -->
    <script type="text/javascript" src="{base_url}assets/js/jquery.bxslider.min.js"></script>
    
    <!-- actual --> 
    <script type="text/javascript" src="{base_url}assets/js/jquery.actual.min.js"></script>

    <!-- jQuery UI -->
    <script type="text/javascript" src="{base_url}assets/js/jquery-ui.min.js"></script>
    
    <!-- Chosen jquery-->    
    <script type="text/javascript" src="{base_url}assets/js/chosen.jquery.min.js"></script>
    
    <!-- elevatezoom --> 
    <script type="text/javascript" src="{base_url}assets/js/jquery.elevateZoom.min.js"></script>

    <!-- fancybox -->
    <script src="{base_url}assets/js/fancybox/source/jquery.fancybox.pack.js"></script>
    <script src="{base_url}assets/js/fancybox/source/helpers/jquery.fancybox-media.js"></script>
    <script src="{base_url}assets/js/fancybox/source/helpers/jquery.fancybox-thumbs.js"></script>

    <!-- arcticmodal -->
    <script src="{base_url}assets/js/arcticmodal/jquery.arcticmodal.js"></script>
    
    <!-- Main -->  
    <script type="text/javascript" src="{base_url}assets/js/main.js"></script>

    <script src="{base_url}assets/js/bootstrap-notify.min.js"></script>
    
    <script src="{base_url}assets/bootstrap-sweetalert/dist/sweetalert.min.js"></script>


    <script type="text/javascript">
        
        jQuery(document).ready(function($) {
            if('{discount}'>0){
                $('.detail_price').html('<font color="red"><strike> Rp.'+formatMoney('{price} ')+' </strike></font> <br>Rp.'+formatMoney('{price_after}')+' <font color="red">{discount}%OFF</font>');    
            }else{
                $('.detail_price').html('Rp.'+formatMoney('{price} '));    
            }
            

            add_wishlist = function(id){

                    $.ajax({
                        url: '{base_url}account/add_wishlists',
                        type: 'POST',
                        dataType: 'json',
                        data: {product_id: id},
                    })
                    .done(function(data) {
                        if(data.status=="success"){
                            $.notify({
                                title: '<strong>Success!</strong>',
                                message: 'Success Menambahkan ke favorit'
                            },{
                                type: 'success',
                                placement: {
                                    from: "bottom"
                                },
                            });
                        }else{

                            $.notify({
                                title: '<strong>error!</strong>',
                                message: 'Gagal Menambahkan ke favorit, Silahkan Login Terlebih Dahulu!'
                            },{
                                type: 'danger',
                                placement: {
                                    from: "bottom"
                                },
                            });
                        }
                    })
                    .fail(function() {
                            $.notify({
                                title: '<strong>error!</strong>',
                                message: 'Gagal Menambahkan ke favorit, Silahkan Login Terlebih Dahulu!'
                            },{
                                type: 'danger',
                                placement: {
                                    from: "bottom"
                                },
                            });

                            swal({
                                title: "Gagal Menambahkan ke favorit!",
                                text: "Silahkan Login Terlebih Dahulu?"
                            });
                    });
                    
                 }

                 add_cart = function(id){
                    var qty = $("#qty1").val();
                    var stock = '{stock}';

                    var check_stock = parseInt(stock)-parseInt(qty);

                    if(check_stock<0){
                        $.notify({
                             title: '<strong>Stok Habis!</strong>',
                             message: 'Gagal Menambahkan ke keranjang, Stock Telah Habis!'
                         },{
                             type: 'danger',
                             placement: {
                                 from: "bottom"
                             },
                         });
                    }else{
                        $.ajax({
                            url: '{base_url}account/add_cart',
                            type: 'POST',
                            dataType: 'json',
                            data: {product_id: id, qty:qty},
                        })
                        .done(function(data) {
                            if(data.status=="success"){
                                $.notify({
                                    title: '<strong>Success!</strong>',
                                    message: 'Success Menambahkan ke keranjang'
                                },{
                                    type: 'success',
                                    placement: {
                                        from: "bottom"
                                    },
                                });
                                setTimeout(function(){
                                   window.location.href = '{base_url}account/checkout';
                                }, 3000);   
                            }else{

                                $.notify({
                                    title: '<strong>error!</strong>',
                                    message: 'Gagal Menambahkan ke keranjang, Silahkan Login Terlebih Dahulu!'
                                },{
                                    type: 'danger',
                                    placement: {
                                        from: "bottom"
                                    },
                                });
                                window.location.href = '{base_url}auth';
                                /*swal({
                                    title: "Gagal Menambahkan ke keranjang!",
                                    text: "Silahkan Login Terlebih Dahulu?"
                                });*/
                            }
                        })
                        .fail(function() {
                                $.notify({
                                    title: '<strong>error!</strong>',
                                    message: 'Gagal Menambahkan ke keranjang, Silahkan Login Terlebih Dahulu!'
                                },{
                                    type: 'danger',
                                    placement: {
                                        from: "bottom"
                                    },
                                });
                                window.location.href = '{base_url}auth';
                                /*swal({
                                    title: "Gagal Menambahkan ke keranjang!",
                                    text: "Silahkan Login Terlebih Dahulu?"
                                });*/
                        });
                    }
                 }
            
        });

    </script>

    <script type="text/javascript">
        jQuery(document).ready(function($) {
            v_check_menu = 0;

            $.ajax({
                 url: '{base_url}home/categories',
                 type: 'GET',
                 dataType: 'json'
             })
             .done(function(data) {
                 if(data.status=="success"){
                    var result = data.result;
                    var no = 0;
                    var categories = '';
                    $.each(result, function(index, val) {
                         /* iterate through array or object */
                        if(no>5){
                            var li_class = 'cat-link-orther'; 
                        }else{
                            var li_class = '';
                        }

                        categories = categories+'<li class="'+li_class+'">';
                        categories = categories+'<a href="{base_url}product/list/'+val.slug+'">';
                        categories = categories+'<span class="icon"><img src="{base_url}assets/images/icon/index1/nav-cat1.png" alt="nav-cat"></span>';
                        categories = categories+val.name;
                        categories = categories+'</a>';
                        categories = categories+'</li>';
                        no++; 

                        //$('#categori_search_option').append('<option value="'+val.name+'">'+val.name+'</option>');
                        //
                        //$(".chosen-results").append('<li class="active-result" data-option-array-index="'+no+'" style="">'+val.name+'</li>')
                    });

                 }else{
                    categories = '';
                 }

                 //$("#block_search").addClass('block-search');                      
                 //$("#categori_search").addClass('categori-search');                      
                 //$("#categori_search_option").addClass('categori-search-option');                      
                 $('#categories').html(categories);
                 $('#categories').html(categories);
             })
             .fail(function() {
                 console.log("error");
             });


             <?php 
                if($this->session->userdata('is_logged_in') == true){
             ?>
             $.ajax({
                url: '{base_url}account/get_cart',
                type: 'GET',
                dataType: 'json'
             })
             .done(function(data) {
                v_list_data     = '';
                v_input         = '';

                if(data.status=="success"){
                    v_total_items   = data.result.total_items;
                    v_loop_product  = data.result.data;
                    v_grandtotal    = data.result.grandtotal;
                    v_subtotal      = data.result.subtotal;
                    v_total_items   = data.result.total_items;
                    v_total_discount= data.result.total_discount;
                    v_nom = 0;
                    if(v_total_items>0){
                         $.each(v_loop_product, function(index, val) {
                            if(v_nom<3){
                                $.each(val.items, function(index, val_items) {
                                     v_list_data = v_list_data+'<li class="product-item">';
                                    
                                        val_image='';
                                        $.each(val_items.product.images_url, function(index_image, val_image) {
                                             /* iterate through array or object */
                                             if(index_image=="0"){
                                                images_url = val_image;
                                                v_list_data = v_list_data+'    <a class="product-item-photo" href="#" title="'+val_items.product.name+'">';
                                                v_list_data = v_list_data+'        <img class="product-image-photo" src="'+images_url+'" alt="'+val_items.product.name+'">';
                                                v_list_data = v_list_data+'    </a>';
                                             }
                                             
                                        });

                                     v_list_data = v_list_data+'    <div class="product-item-details">';
                                     v_list_data = v_list_data+'        <strong class="product-item-name">';
                                     v_list_data = v_list_data+'            <a href="#">'+val_items.product.name+'</a>';
                                     v_list_data = v_list_data+'        </strong>';
                                     v_list_data = v_list_data+'        <div class="product-item-qty">';
                                     v_list_data = v_list_data+'            <span class="label">Quantity:</span >';
                                     v_list_data = v_list_data+'            <span class="number">'+val_items.qty+'</span>';
                                     v_list_data = v_list_data+'        </div>';
                                     v_list_data = v_list_data+'        <div class="product-item-price">';
                                     v_list_data = v_list_data+'            <span class="price">Rp.'+formatMoney(val_items.product.price)+'</span>';
                                     v_list_data = v_list_data+'        </div>';
                                     v_list_data = v_list_data+'    </div>';
                                     v_list_data = v_list_data+'</li>';
                                 });
                                v_nom++;
                            }else{
                                $("#btn_minicart").text('More');
                            }
                         });
                    }

                    $("#count_cart").text(v_total_items);
                    $('#minicart_items').html(v_list_data);
                }
             })
             .fail(function() {
                 console.log("error");
             });
             <?php 
                }else{
             ?>
             $(".block-minicart").hide();
             <?php 
                }
             ?>
             
             check_menu = function(){
             }
        });

            function formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
                      try {
                        decimalCount = Math.abs(decimalCount);
                        decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

                        const negativeSign = amount < 0 ? "-" : "";

                        let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
                        let j = (i.length > 3) ? i.length % 3 : 0;

                        return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
                      } catch (e) {
                        console.log(e)
                      }
            };
    </script>
</body>
</html>