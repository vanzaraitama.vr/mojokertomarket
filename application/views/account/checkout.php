<style type="text/css">
    .pointer {cursor: pointer;}
</style>	
    	<!-- MAIN -->
		<main class="site-main" style="background-color: #fff;">

            <!-- breadcrumb -->
            <div class="container breadcrumb-page">
                <ol class="breadcrumb">
                    <li><a href="{base_url}">Beranda </a></li>
                    <li class="active">Keranjang Belanja</li>
                </ol>
            </div> <!-- breadcrumb -->


            <div class="container">

                <!-- form cart -->
                <form action="{base_url}" id="form_cart" name="form_cart" method="post" class="form-cart">
                    
                    <!-- table cart -->
                    <div class="table-cart-wrapper table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="tb-product">Products/Harga</th>
                                    <th class="tb-available">Tersedia</th>
                                    <th class="tb-qty">Qty</th>
                                    <th class="tb-total">Total</th>
                                    <th class="tb-remove">Hapus</th>
                                </tr>
                            </thead>
                            <tbody id="check_cart">
                                
                        
                            </tbody>
                        </table>
                    </div><!-- table cart -->

                    <!-- action cart -->
                    <div class="cart-actions" style="display: none;">
                        <button type="submit" title="Ubah Keranjang Belanja" class="action update" id="submit_cart" style="display: none;">
                            <span>Ubah Keranjang Belanja</span>
                        </button>
                        <button type="button" title="Proses Pembelian" class="action checkout" onClick="choose_address();">
                            <span>Proses Pembelian</span>
                        </button>
                    </div><!-- action cart -->

                </form><!-- form cart -->

            </div>
           
		</main><!-- end MAIN -->
<script type="text/javascript">
    jQuery(document).ready(function($) {

        $.ajax({
            url: '{base_url}account/get_cart',
            type: 'GET',
            dataType: 'json'
        })
        .done(function(data) {
            v_list_data     = '';
            v_input         = '';

            if(data.status=="success"){
                //console.log(data);
                v_loop_product  = data.result.data;
                v_grandtotal    = data.result.grandtotal;
                v_shipping_cost = data.result.shipping_cost;
                v_subtotal      = data.result.subtotal;
                v_total_items   = data.result.total_items;
                v_total_discount= data.result.total_discount;
                
                if(v_total_items>0){

                    $.each(v_loop_product, function(index, val) {
                         /* iterate through array or object */
                         
                         $.each(val.items, function(index, val_items) {
                              /* iterate through array or object */
                                 v_input = v_input+'<input type="hidden" name="item[]" value="'+val_items.product_id+'">';
                                 v_input = v_input+'<input type="hidden" name="itemqty_'+val_items.product_id+'" value="'+val_items.qty+'">';
                                 v_input = v_input+'<input type="hidden" name="itemqtyadd_'+val_items.product_id+'"  id="itemqtyadd_'+val_items.product_id+'" value="0">';
                                 //console.log(val_items);
                                 v_list_data = v_list_data+'<tr>';
                                 v_list_data = v_list_data+'    <td class="tb-product">';
                                 v_list_data = v_list_data+'        <div class="item">';
                                 url_image ='';
                                 //console.log(val_items);
                                 $.each(val_items.product.images_url, function(index_image, val_image) {
                                      /* iterate through array or object */
                                      if(index_image==0){
                                        url_image = val_image;
                                     }
                                 });

                                 v_list_data = v_list_data+'            <a href="#" class="item-photo"><img src="'+url_image+'" alt="cart"></a>';
                                 v_list_data = v_list_data+'            <div class="item-detail">';
                                 v_list_data = v_list_data+'                <strong class="item-name"><a href="#" >'+val_items.product.name+'  </a></strong>';
                                 v_list_data = v_list_data+'                <div class="item-price">';
                                 v_list_data = v_list_data+'                    <span class="price">Harga Normal Rp.'+formatMoney(val_items.product.price)+'</span><br>';
                                 v_list_data = v_list_data+'                    <span class="price">Diskon '+val_items.product.discount+'%</span><br>';
                                 v_list_data = v_list_data+'                    <span class="price">Setelah Diskon Rp.'+formatMoney(val_items.product.price_after)+'</span>';
                                 v_list_data = v_list_data+'                </div>';
                                 v_list_data = v_list_data+'                <br><strong class="item-name"><a href="#" >Store: '+val.shop.name+'</a> </strong>';
                                 v_list_data = v_list_data+'                <div class="item-name">Address: '+val.shop.address+'</div>';
                                 v_list_data = v_list_data+'            </div>';
                                 v_list_data = v_list_data+'        </div>';
                                 v_list_data = v_list_data+'    </td>';
                                 v_list_data = v_list_data+'    <td class="tb-available">';
                                 v_list_data = v_list_data+'        <span class="value">'+val_items.product.status+'!</span>';
                                 v_list_data = v_list_data+'    </td>';
                                 v_list_data = v_list_data+'    <td class="tb-qty">';
                                 v_list_data = v_list_data+'        <input onChange="chage_qty('+val_items.product_id+',this.value)" type="text" title="Qty" class="input-qty " value="'+val_items.qty+'">';
                                 v_list_data = v_list_data+'    </td>';
                                 v_list_data = v_list_data+'    <td class="tb-total">';
                                 v_list_data = v_list_data+'        <span class="amount">Rp.'+formatMoney(val_items.grandtotal)+'</span>';
                                 v_list_data = v_list_data+'    </td>';
                                 v_list_data = v_list_data+'    <td class="tb-remove">';
                                 v_list_data = v_list_data+'        <a onClick="del_data('+val_items.id+');" class="action-remove pointer"><span>remove</span></a>';
                                 v_list_data = v_list_data+'    </td>';
                                 v_list_data = v_list_data+'</tr>';
                         });

                    });

                    v_list_data = v_list_data+'<tr>';
                    v_list_data = v_list_data+'    <td class="tb-product">';
                    v_list_data = v_list_data+'    <span class="value">Biaya Kirim</span>';
                    v_list_data = v_list_data+'    </td>';
                    v_list_data = v_list_data+'    <td class="tb-available">';
                    v_list_data = v_list_data+'    </td>';
                    v_list_data = v_list_data+'    <td class="tb-qty">';
                    v_list_data = v_list_data+'    </td>';
                    v_list_data = v_list_data+'    <td class="tb-total">';
                    v_list_data = v_list_data+'        <span class="amount">Rp.'+formatMoney(v_shipping_cost)+'</span>';
                    v_list_data = v_list_data+'    </td>';
                    v_list_data = v_list_data+'    <td class="tb-remove">';
                    v_list_data = v_list_data+'    </td>';
                    v_list_data = v_list_data+'</tr>';

                    v_list_data = v_list_data+'<tr>';
                    v_list_data = v_list_data+'    <td class="tb-product">';
                    v_list_data = v_list_data+'    <span class="value">Total</span>';
                    v_list_data = v_list_data+'    </td>';
                    v_list_data = v_list_data+'    <td class="tb-available">';
                    v_list_data = v_list_data+'    </td>';
                    v_list_data = v_list_data+'    <td class="tb-qty">';
                    v_list_data = v_list_data+'    </td>';
                    v_list_data = v_list_data+'    <td class="tb-total">';
                    v_list_data = v_list_data+'        <span class="amount">Rp.'+formatMoney(v_grandtotal)+'</span>';
                    v_list_data = v_list_data+'    </td>';
                    v_list_data = v_list_data+'    <td class="tb-remove">';
                    v_list_data = v_list_data+'    </td>';
                    v_list_data = v_list_data+'</tr>';

                    $(".cart-actions").show();
                }

            }

            $('#check_cart').html(v_list_data);
            $('#form_cart').append(v_input);
        })
        .fail(function() {
            console.log("error");
        });

    chage_qty = function(id,qty){
        $("#itemqtyadd_"+id).val(qty);
    }    

    choose_address = function(){

        $('body').loadingModal({
                  position: 'auto',
                  text: '',
                  color: '#fff',
                  opacity: '0.7',
                  backgroundColor: 'rgb(0,0,0)',
                  text: 'Loading...',
                  animation: 'wave'
        });

        window.location.href = '{base_url}account/checkout_address';
    }

    del_data = function (id){   
   
        swal({
             title: "Apa kamu yakin?",
            text: "Kamu tidak dapat mengembalikan data jika sudah di hapus!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Ya, Yakin Hapus",
            cancelButtonText: "Batal",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm) {
            if (isConfirm) {

                $.ajax({
                    url: '{base_url}account/delete_cart',
                    type: 'POST',
                    dataType: 'json',
                    data: {id: id},
                })
                .done(function() {
                    swal({
                        title: "Hapus!",
                        text: "Data telah terhapus.",
                        type: "success",
                        confirmButtonClass: "btn-success"
                    });
                })
                .fail(function() {
                    swal({
                        title: "Gagal",
                        text: "Data kamu aman :). Gagal Hapus Data",
                        type: "error",
                        confirmButtonClass: "btn-danger"
                    });
                })
                .always(function() {
                    location.reload();
                });
                          
            } else {
                swal({
                    title: "Batal",
                    text: "Data kamu aman :)",
                    type: "error",
                    confirmButtonClass: "btn-danger"
                });
            }
        });
    }
        
    });

    $('form[name="form_cart"]').submit(function(e){
            e.preventDefault();
            
            $.ajax({
                url: "{base_url}account/update_cart",
                method: "post",
                cache: false,
                data: $(this).serialize(),
                dataType: "json",
                beforeSend: function(){
                    $('#submit_cart').button('loading');
                },
                success: function(data){
                    $('#submit_cart').button('reset');
                    if(data.status=='success'){
                        $.notify({
                            title: '<strong>Berhasil!</strong>',
                            message: 'Berhasil Merubah Keranjang Belanja.'
                        },{
                            type: 'success',
                            placement: {
                                from: "bottom"
                            },
                        });
                    }else{
                        $.notify({
                            title: '<strong>error!</strong>',
                            message: data.message
                        },{
                            type: 'danger',
                            placement: {
                                from: "bottom"
                            },
                        });
                    }
                },
                complete: function(data){
                    $('#submit_cart').button('reset');
                },
                error: function(){
                    $('#submit_cart').button('reset');
                    $.notify({
                            title: '<strong>Error!</strong>',
                            message: "Please check your Network Connection!"
                        },{
                            type: 'danger',
                            placement: {
                                from: "bottom"
                            },
                        });
                }
            });

        });

            function formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
                      try {
                        decimalCount = Math.abs(decimalCount);
                        decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

                        const negativeSign = amount < 0 ? "-" : "";

                        let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
                        let j = (i.length > 3) ? i.length % 3 : 0;

                        return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
                      } catch (e) {
                        console.log(e)
                      }
            };

</script>
