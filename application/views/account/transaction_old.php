<style type="text/css">
    .pointer {cursor: pointer;}

    .product-item-opt-0 .product-item-review .action {
        font-size: 13px;
        color: #ff0303;
        line-height: 34px;
    }

    .products.products-list .product-items .product-item .product-item-detail {
        float: right;
        width: 100%;
        padding-left: 20px;
    }
</style>	
    	
        <!-- MAIN -->
        <main class="site-main">
            <div class="columns container">
                <div class="row">

                    <!-- Sidebar -->
                    <div class="col-lg-3 col-md-3 col-lg-push-9 col-md-push-9 col-sidebar">

                    </div>
                    <!-- Sidebar -->

                    <!-- Main Content -->
                    <div class="col-lg-9 col-md-9 col-lg-pull-3 col-md-pull-3  col-main">

                        <!-- Toolbar -->
                        <div class=" toolbar-products toolbar-top">

                            <div class="btn-filter-products">
                                <span>Filter</span>
                            </div>
                            <div class="modes">
                                <strong  class="label">View as:</strong>
                                <strong  title="List" class="active modes-mode mode-list">
                                    <span>list</span>
                                </strong>
                            </div><!-- View as -->
                           
                            <div class="toolbar-option">

                            </div>

                            <ul class="pagination">
                                <li class="action first_page pointer">
                                    
                                </li>

                                <li class="active_page_before_2 pointer" style="display: none;">
                                    
                                </li>
                                <li class="active_page_before_1 pointer" style="display: none;">
                                    
                                </li>
                                
                                <li class="active active_page pointer">
                                   
                                </li>
                                <li class="active_page_next_1 pointer" style="display: none;">
                                    
                                </li>
                                <li class="active_page_next_2 pointer" style="display: none;">
                                    
                                </li>
                                <li class="action last_page pointer">
                                    
                                </li>
                            </ul>

                        </div><!-- Toolbar -->

                        <!-- List Products -->
                        <div class="products  products-list">
                            <ol class="product-items row">
                               
                            </ol>
                            <!-- list product -->
                        </div> <!-- List Products -->

                        <!-- Toolbar -->
                        <div class=" toolbar-products toolbar-bottom">

                            <div class="modes">
                                <strong  class="label">View as:</strong>
                                <strong  title="List" class="active modes-mode mode-list">
                                    <span>list</span>
                                </strong>
                            </div><!-- View as -->
                           
                            <div class="toolbar-option">

                            </div>

                            <ul class="pagination" id="bottom_pagination">
                                <input type="hidden" name="page_active" id="page_active" value="1">
                                <li class="action first_page pointer">
                                   
                                </li>
                                <li class="active_page_before_2 pointer" style="display: none;">
                                    
                                </li>
                                <li class="active_page_before_1 pointer" style="display: none;">
                                    
                                </li>
                                <li class="active active_page pointer">
                                   
                                </li>
                                <li class="active_page_next_1 pointer" style="display: none;">
                                    
                                </li>
                                <li class="active_page_next_2 pointer" style="display: none;">
                                    
                                </li>
                                <li class="action last_page pointer">
                                    
                                </li>
                            </ul>

                        </div>
                        <!-- Toolbar -->

                    </div>
                    <!-- Main Content -->
                    
                </div>
            </div>

        </main><!-- end MAIN -->

<script type="text/javascript">
    jQuery(document).ready(function($) {
        
       change_Page = function(page){
                    
            var page    = page;
            $("#page_active").val(page);

            fn_list_product(page);
        }

       fn_list_product = function (page)  {

            $('.product-items').html('');

            $.ajax({
                url: '{base_url}account/get_list_transaction',
                type: 'GET',
                dataType: 'json',
                data: {page:page},
            })
            .done(function(data) {
                if(data.status=="success"){
                    var products_grid   = '';
                    var result  = data.result.data;
                    var total   = data.result.total;

                    first_page =            '<a onClick=change_Page(1);>';
                    first_page = first_page+'   <span><i aria-hidden="true" class="fa fa-angle-left"></i></span>';
                    first_page = first_page+'</a>';

                    last_page =           '<a onClick=change_Page('+data.result.last_page+');>';
                    last_page = last_page+'     <span><i aria-hidden="true" class="fa fa-angle-right"></i></span>';
                    last_page = last_page+'</a>';
                    
                    $(".first_page").html(first_page);
                    $(".last_page").html(last_page);


                    if(data.result.current_page=="1"){
                        $(".first_page").hide();
                    }else if(data.result.current_page==data.result.last_page){
                        $(".last_page").hide();
                    }

                    $('.active_page').html('<a href="#">'+data.result.current_page+'</a>');

                    sisa = parseInt(data.result.last_page)-parseInt(data.result.current_page);
                    sisa_before = parseInt(data.result.current_page)-parseInt(1);
                   
                    if(sisa>=2){
                        active_page_next_1 = parseInt(data.result.current_page)+parseInt(1);
                        active_page_next_2 = parseInt(data.result.current_page)+parseInt(2);
                        
                        $('.active_page_next_1').show();
                        $('.active_page_next_2').show();

                        $('.active_page_next_1').html('<a onClick=change_Page('+active_page_next_1+');>'+active_page_next_1+'</a>');
                        $('.active_page_next_2').html('<a onClick=change_Page('+active_page_next_2+');>'+active_page_next_2+'</a>');

                    }else if(sisa<2){
                        active_page_next_1 = parseInt(data.result.current_page)+parseInt(1);
                        if(sisa>=1){
                            $('.active_page_next_1').show();
                        }else{
                            $('.active_page_next_1').hide();
                        }
                        $('.active_page_next_2').hide();
                        $(".first_page").show();
                        $('.last_page').hide();
                        
                        $('.active_page_next_1').html('<a onClick=change_Page('+active_page_next_1+');>'+active_page_next_1+'</a>');
                    }

                    if(sisa_before>0){
                        if(sisa_before<2){
                            active_page_before_1 = parseInt(data.result.current_page)-parseInt(1);
                            $('.active_page_before_1').html('<a onClick=change_Page('+active_page_before_1+');>'+active_page_before_1+'</a>');
                            $('.active_page_before_1').show();
                            $('.active_page_before_2').hide();
                        }else{
                            active_page_before_1 = parseInt(data.result.current_page)-parseInt(1);
                            active_page_before_2 = parseInt(data.result.current_page)-parseInt(2);
                            $('.active_page_before_1').html('<a onClick=change_Page('+active_page_before_1+');>'+active_page_before_1+'</a>');
                            $('.active_page_before_2').html('<a onClick=change_Page('+active_page_before_2+');>'+active_page_before_2+'</a>');
                            $('.active_page_before_1').show();
                            $('.active_page_before_2').show();
                        }
                    }else{
                        $('.active_page_before_1').hide();
                        $('.active_page_before_2').hide();
                    }

                    if(sisa>2){
                        $(".last_page").show();
                    }

                    if(total>0){
                        
                       $.each(result, function(index, val) {
                            console.log(val);
                            


                            $.each(val.details, function(index_detail, val_detail) {
                                 products_grid = '';

                                products_grid = products_grid+'<li class="col-sm-12 product-item product-item-opt-0">';
                                products_grid = products_grid+'    <div class="product-item-info">';
                                products_grid = products_grid+'        <div class="product-item-detail">';
                                products_grid = products_grid+'            <strong class="product-item-name"><a href="#">'+val.invoice+' - '+val_detail.product_name+'</a></strong>';
                                products_grid = products_grid+'            <div class="product-item-review">';
                                
                                var str_status = val.status;
                                var res_status = str_status.replace("_", " ");

                                products_grid = products_grid+'                <a href="#" class="action">'+res_status.toUpperCase()+'</a>';
                                products_grid = products_grid+'            </div>';
                                products_grid = products_grid+'            <div class="product-item-price">';
                                products_grid = products_grid+'                <span class="price"> Harga : Rp. '+formatMoney(val_detail.product_price)+'</span><br>';
                                products_grid = products_grid+'                <span class="price"> Biaya Kirim : Rp. '+formatMoney(val.shipping_cost)+'</span><br>';
                                products_grid = products_grid+'                <span class="price"> Total : Rp. '+formatMoney(val.grandtotal)+'</span>';
                                products_grid = products_grid+'            </div>';
                                products_grid = products_grid+'            <div class="product-item-des">';
                                products_grid = products_grid+'               '+val.address_name+', Tlp :'+val.phone_number+'<br>';
                                products_grid = products_grid+'               '+val.address+'<br>';
                                products_grid = products_grid+'            </div>';
                                products_grid = products_grid+'        </div>';
                                products_grid = products_grid+'    </div>';
                                products_grid = products_grid+'</li>';

                                $('.product-items').append(products_grid);
                            });
                        });
                    }

                }else{
                    var products_grid = '';
                    $('.product-items').html(products_grid);
                }

            })
            .fail(function() {
                console.log("error");
            });

       }


       fn_list_product('1');
    });

            function formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
                      try {
                        decimalCount = Math.abs(decimalCount);
                        decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

                        const negativeSign = amount < 0 ? "-" : "";

                        let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
                        let j = (i.length > 3) ? i.length % 3 : 0;

                        return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
                      } catch (e) {
                        console.log(e)
                      }
            };
</script>
