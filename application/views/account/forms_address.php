        <link rel="stylesheet" type="text/css" href="{base_url}assets/js/EasyAutocomplete/easy-autocomplete.min.css"/>       
        <!-- MAIN -->
		<main class="site-main" style="background-color: #fff;">

            <!-- breadcrumb -->
            <div class="container breadcrumb-page">
                <ol class="breadcrumb">
                    <li><a href="#">Pengaturan </a></li>
                    <li><a href="{base_url}account/address">Alamat </a></li>
                    <li class="active">{act}</li>
                </ol>
            </div> <!-- breadcrumb -->

            <div class="page-title-base container">
                <h1 class="title-base">{act} Alamat</h1>
            </div>
            <div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="map"></div>
                        </div>
                    </div>

                    <div class="container">
                        <div class="block-form-login">
                            <div class="block-form-create">
                                <div class="block-title">
                                    Alamat
                                </div>
                                <div class="block-content">
                                    <form action="{base_url}" name="submit_address" method="post">
                                     <input type="hidden" name="id" value="{id}">   
                                     <input type="hidden" name="act" value="{act}">  
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Address Name" name="address_name" id="address_name" value="{address_name}">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Consignee" name="consigne" id="consigne" value="{consigne}">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Phone Number" name="phone_number" id="phone_number" value="{phone_number}">
                                        </div>
                                        <div class="form-group" style="
                                            font-size: 13px;
                                            box-shadow: none;
                                            outline-style: none;
                                            outline-width: 0;
                                            color: #aaa;">
                                            <textarea name="address" id="address" rows="4" cols="43">{address}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <input type="hidden" class="form-control" placeholder="Latitude" name="latitude" id="latitude" value="{latitude}">
                                        </div>
                                        <div class="form-group">
                                            <input type="hidden" class="form-control" placeholder="Longitude" name="logitude" id="logitude" value="{longitude}">
                                        </div>
                                        <button type="submit" class="btn btn-inline" id="submit_update" name="submit_update">Submit</button>
                                    </form> 
                                </div>
                            </div>
                        </div>
                    </div>

                </div>      
		</main><!-- end MAIN -->
        <script type="text/javascript">

            $('form[name="submit_address"]').submit(function(e){
                e.preventDefault();
                
                $.ajax({
                    url: "{base_url}account/submit_address",
                    method: "post",
                    cache: false,
                    data: $(this).serialize(),
                    dataType: "json",
                    beforeSend: function(){
                        $('#submit_update').button('loading');
                    },
                    success: function(data){
                        $('#submit_update').button('reset');
                        if(data.status=='success'){
                            $.notify({
                                title: '<strong>Success!</strong>',
                                message: 'Berhasil menambahkan alamat.'
                            },{
                                type: 'success',
                                placement: {
                                    from: "bottom"
                                },
                            });
                            //window.location.href = '{base_url}account/address';
                            history.back();
                        }else{
                            $.notify({
                                title: '<strong>error!</strong>',
                                message: data.message
                            },{
                                type: 'danger',
                                placement: {
                                    from: "bottom"
                                },
                            });
                        }
                    },
                    complete: function(data){
                        $('#submit_update').button('reset');
                    },
                    error: function(){
                        $('#submit_update').button('reset');
                        $.notify({
                                title: '<strong>Error!</strong>',
                                message: "Please check your Network Connection!"
                            },{
                                type: 'danger',
                                placement: {
                                    from: "bottom"
                                },
                            });
                    }
                });

            });

        </script>
        <script>

        $('#map').attr('style','height:400px; width:100%');
            var map, infoWindow;
            var markers = [];
            var poligamis = [];
            var myPolygon = [];
            function initMap() {

                var myLatLng = {lat: -7.472638, lng: 112.434084};

                var map = new google.maps.Map(document.getElementById('map'), {
                  zoom: 17,
                  center: myLatLng
                });

                infoWindow = new google.maps.InfoWindow;

                var geocoder = new google.maps.Geocoder();

               // if (navigator.geolocation) {
               // navigator.geolocation.getCurrentPosition(function(position) {
                    

                    if('{act}'=='Edit'){
                        v_lat = parseInt('{latitude}');
                        v_lng = parseInt('{longitude}');
                    }else{
                        //v_lat = position.coords.latitude;
                        //v_lng = position.coords.longitude;
                        v_lat = myLatLng.lat;
                        v_lng = myLatLng.lng; 
                    }

                    var pos = {
                        lat: v_lat,
                        lng: v_lng
                    };

                    $("#latitude").val(v_lat);
                    $("#logitude").val(v_lng);

                    infoWindow.open(map);
                    map.setCenter(pos);

                    marker = new google.maps.Marker({
                      position: pos,
                      map: map,
                      draggable: true
                    }); 

                    markerCoords(marker);

               // }, function() {
                    //handleLocationError(true, infoWindow, map.getCenter());
                    marker = new google.maps.Marker({
                      position: myLatLng,
                      map: map,
                      draggable: true
                    }); 

                    markerCoords(marker);
                //});
              /*  } else {
                    handleLocationError(false, infoWindow, map.getCenter());
                    marker = new google.maps.Marker({
                      position: myLatLng,
                      map: map,
                      draggable: true
                    }); 

                    markerCoords(marker);
                } */
                
            }

            function markerCoords(markerobject){
                google.maps.event.addListener(markerobject, 'dragend', function(evt){
                   
                    $("#latitude").val(evt.latLng.lat());
                    $("#logitude").val(evt.latLng.lng());
                  
                    $.ajax({
                        url: '{base_url}account/latlng',
                        type: 'GET',
                        dataType: 'json',
                        data: {latlng: evt.latLng.lat()+','+evt.latLng.lng()},
                    })
                    .done(function(result) {
                        $("#address").val(result.data);
                    })
                    .fail(function() {
                        console.log("error");
                    });
                    
                });

                google.maps.event.addListener(markerobject, 'drag', function(evt){
                    console.log("marker is being dragged");
                });     
            }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyBp3E-Zv44uHcv41brVZH3gdrke4_sWFbU&callback=initMap&libraries=places,drawing&sensor=false"></script>

