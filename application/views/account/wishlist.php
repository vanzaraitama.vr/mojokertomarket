<style type="text/css">
    .pointer {cursor: pointer;}
</style>    
        <!-- MAIN -->
        <main class="site-main" style="background-color: #fff;">

            <!-- breadcrumb -->
            <div class="container breadcrumb-page">
                <ol class="breadcrumb">
                    <li><a href="{base_url}">Beranda </a></li>
                    <li class="active">Barang Favorit</li>
                </ol>
            </div> <!-- breadcrumb -->


            <div class="container">

                <!-- form cart -->
                <form class="form-cart">

                    <!-- table cart -->
                    <div class="table-cart-wrapper table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="tb-product">Products Deskripsi</th>
                                    <th class="tb-remove">Hapus</th>
                                </tr>
                            </thead>
                            <tbody id="check_cart">
                                
                        
                            </tbody>
                        </table>
                    </div><!-- table cart -->
                </form><!-- form cart -->

            </div>
           
        </main><!-- end MAIN -->

<script type="text/javascript">
    jQuery(document).ready(function($) {

        $.ajax({
            url: '{base_url}account/get_wishlists',
            type: 'GET',
            dataType: 'json'
        })
        .done(function(data) {
            v_list_data     = '';
            
            if(data.status=="success"){
            
                v_loop_product  = data.result.data;
                v_length        = data.result.total;
               
                if(v_length>0){

                    $.each(v_loop_product, function(index, val) {
                         /* iterate through array or object */

                         v_list_data = v_list_data+'<tr>';
                         v_list_data = v_list_data+'    <td class="tb-product">';
                         v_list_data = v_list_data+'        <div class="item">';
                         url_image ='';
                                // console.log(val);
                                 $.each(val.product.images_url, function(index_image, val_image) {
                                      
                                      if(index_image==0){
                                        url_image = val_image;
                                     }
                                 });
                         v_list_data = v_list_data+'            <a href="#" class="item-photo"><img src="'+url_image+'" alt="cart"></a>';
                         //v_list_data = v_list_data+'            <a href="#" class="item-photo"><img src="{base_url}assets/images/media/index1/cart.jpg" alt="cart"></a>';
                         v_list_data = v_list_data+'            <div class="item-detail">';
                         v_list_data = v_list_data+'                <strong class="item-name"><a href="#" >'+val.product.name+'  </a></strong>';
                         v_list_data = v_list_data+'                <div class="item-price">';
                         v_list_data = v_list_data+'                    <span class="price">Price: Rp.'+formatMoney(val.product.price)+'</span>';
                         v_list_data = v_list_data+'                </div>';
                         v_list_data = v_list_data+'            </div>';
                         v_list_data = v_list_data+'        </div>';
                         v_list_data = v_list_data+'    </td>';
                         v_list_data = v_list_data+'    <td class="tb-remove">';
                         v_list_data = v_list_data+'        <a onClick="del_data('+val.id+')" class="action-remove pointer"><span>remove</span></a>';
                         v_list_data = v_list_data+'    </td>';
                         v_list_data = v_list_data+'</tr>';
                    });
                }

            }

            $('#check_cart').html(v_list_data);

        })
        .fail(function() {
            console.log("error");
        });

    del_data = function (id){   
   
        swal({
            title: "Apa kamu yakin?",
            text: "Kamu tidak dapat mengembalikan data jika sudah di hapus!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Ya, Yakin Hapus",
            cancelButtonText: "Batal",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm) {
            if (isConfirm) {

                $.ajax({
                    url: '{base_url}account/delete_wishlists',
                    type: 'POST',
                    dataType: 'json',
                    data: {id: id},
                })
                .done(function() {
                    swal({
                        title: "Hapus!",
                        text: "Barang Favorit telah terhapus.",
                        type: "success",
                        confirmButtonClass: "btn-success"
                    });
                })
                .fail(function() {
                    swal({
                        title: "Error",
                        text: "Data kamu aman :). Gagal hapus barang favorit",
                        type: "error",
                        confirmButtonClass: "btn-danger"
                    });
                })
                .always(function() {
                    location.reload();
                });
                          
            } else {
                swal({
                    title: "Cancelled",
                    text: "Data kamu aman :)",
                    type: "error",
                    confirmButtonClass: "btn-danger"
                });
            }
        });
    }
        
    });

            function formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
                      try {
                        decimalCount = Math.abs(decimalCount);
                        decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

                        const negativeSign = amount < 0 ? "-" : "";

                        let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
                        let j = (i.length > 3) ? i.length % 3 : 0;

                        return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
                      } catch (e) {
                        console.log(e)
                      }
            };
</script>