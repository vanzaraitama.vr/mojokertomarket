    <link rel="stylesheet" type="text/css" href="{base_url}administrator/assets/vendors/datatables/media/css/dataTables.bootstrap4.css">

    <script src="{base_url}administrator/assets/vendors/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="{base_url}administrator/assets/vendors/datatables/media/js/dataTables.bootstrap4.js"></script>
    <script src="{base_url}administrator/assets/vendors/datatables-fixedcolumns/js/dataTables.fixedColumns.js"></script>
    <script src="{base_url}administrator/assets/vendors/datatables-responsive/js/dataTables.responsive.js"></script>
    <style type="text/css">
    .pointer {cursor: pointer;}

    .product-item-opt-0 .product-item-review .action {
        font-size: 13px;
        color: #ff0303;
        line-height: 34px;
    }

    .products.products-list .product-items .product-item .product-item-detail {
        float: right;
        width: 100%;
        padding-left: 20px;
    }

        table.dataTable thead {background-color:#ebebeb}
        table.dataTable tfoot {background-color:#ebebeb}
        
    </style>     	
        <!-- MAIN -->
        <main class="site-main" style="background-color: #fff;">

            <!-- breadcrumb -->
            <div class="container breadcrumb-page">
                <ol class="breadcrumb">
                    <li><a href="{base_url}">Beranda </a></li>
                    <li class="active">Daftar Transaksi</li>
                </ol>
            </div> 
            <!-- breadcrumb -->
            <div class="page-title-base container">
                 <h1 class="title-base">Transaksi Saya </h1>
            </div>

            <div class="col-md-12 col-main">
                <div class="col-lg-12 col-md-12 col-main">
                    <table class="table table-hover nowrap" id="data-table" width="100%">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Invoice</th>
                            <th>Status Transaksi</th>
                            <th>Biaya Kirim</th>
                            <th>Total</th>
                            <th>Batas Waktu Pembayaran</th>
                            
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Invoice</th>
                            <th>Status Transaksi</th>
                            <th>Biaya Kirim</th>
                            <th>Total</th>
                            <th>Batas Waktu Pembayaran</th>
                            
                            <th>Action</th>
                        </tr>
                        </tfoot>
                        <tbody id="list_queue_data" >
                        
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-12 col-md-12 ">
                    <hr>
                </div>
            </div>
        </main><!-- end MAIN -->

<script type="text/javascript">

    var baseUrl = '{base_url}';  
        var table =    
         $('#data-table').DataTable({
            bLengthChange: false,
            processing: true,
            destroy:true,
            serverSide: true,
            responsive: true,
            autoFill: true,
            autoWidth: false,
            colReorder: true,
            keys: true,
            rowReorder: false,
            columnDefs: [
                { "width": "5%", "targets": 0 },
                { className: "text-center col-with-icon", "targets": [ 5 ] },
            ],
            ajax: {
                url: baseUrl+"account/get_list_transaction",
                type:'POST',
            },
            bStateSave: true,
                "fnStateSave": function (oSettings, oData) {
                    localStorage.setItem('offersDataTables', JSON.stringify(oData));
                },
                "fnStateLoad": function (oSettings) {
                    return JSON.parse(localStorage.getItem('offersDataTables'));
                }
        });


        del_data = function (id){   
   
            swal({
                title: "Konfirmasi Penerimaan barang?",
                text: "Silahkan pilih Ya jika kamu sudah menerima barang!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-success",
                confirmButtonText: "Ya, Terima",
                cancelButtonText: "Batal",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {

                    $.ajax({
                        url: '{base_url}account/transaction_complete',
                        type: 'POST',
                        dataType: 'json',
                        data: {id: id},
                    })
                    .done(function() {
                        swal({
                            title: "Terima!",
                            text: "Barang suda diterima.",
                            type: "success",
                            confirmButtonClass: "btn-success"
                        });
                    })
                    .fail(function() {
                        swal({
                            title: "Error",
                            text: "Barang belum di terima",
                            type: "error",
                            confirmButtonClass: "btn-danger"
                        });
                    })
                    .always(function() {
                        table.ajax.reload();
                    });
                              
                } else {
                    swal({
                        title: "Batal",
                        text: "Barang belum di terima",
                        type: "error",
                        confirmButtonClass: "btn-danger"
                    });
                }
            });
        } 
</script>
