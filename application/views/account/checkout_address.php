<style type="text/css">
    .pointer {cursor: pointer;}
</style>	
    	<!-- MAIN -->
		<main class="site-main" style="background-color: #fff;">

            <!-- breadcrumb -->
            <div class="container breadcrumb-page">
                <ol class="breadcrumb">
                    <li><a href="{base_url}">Beranda </a></li>
                    <li class="">Keranjang Belanja</li>
                    <li class="active">Pilih Alamat</li>
                </ol>
            </div> <!-- breadcrumb -->


            <div class="container">

                <!-- form cart -->
                <form action="{base_url}" id="form_cart" name="form_cart" method="post" class="form-cart">
                    <input type="hidden" name="address" id="address" >
                    <!-- table cart -->
                    <div class="table-cart-wrapper table-responsive ch_address">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="tb-product">Alamat</th>
                                    <th class="tb-remove">Pilih</th>
                                </tr>
                            </thead>
                            <tbody id="check_cart_alamat">
                                
                        
                            </tbody>
                        </table>
                    </div>
                    <div class="table-cart-wrapper table-responsive ch_bankaccount" style="display: none;">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="tb-product">Bank</th>
                                    <th class="tb-remove">Pilih</th>
                                </tr>
                            </thead>
                            <tbody id="check_cart_bank">
                                
                        
                            </tbody>
                        </table>
                    </div>
                    <!-- table cart -->

                    <!-- action cart -->
                    <div class="cart-actions ch_address">
                       
                        <button type="button" title="Add Address" class="action checkout" id="add_address" >
                            <span>Tambah Alamat</span>
                        </button>
                    </div>
                    <!-- action cart -->

                    <div class="table-cart-wrapper table-responsive review_transfer" style="display: none;">
                        <table class="table">
                            <tbody id="check_cart_bank">
                                    <tr>
                                      <td class="tb-product"> </td>
                                      <td class="tb-product"> </td>
                                      <td class="" colspan="2" align="center">
                                          <div class="item">
                                            <strong class="item-name"> Transfer Dana Anda Sebelum</strong>
                                            <strong class="item-name" id="res_tanggal"></strong>
                                            <strong class="item-name" id="res_jam"></strong>
                                          </div>
                                      </td>
                                      <td class="tb-product"> </td>
                                      <td class="tb-product"> </td>
                                   </tr>
                                   <tr>
                                      <td class="tb-product"> </td>
                                      <td class="tb-product"> </td>
                                      <td class="" colspan="2" align="center">
                                          <div class="item">
                                            Pembayaran Menggunakan<br>
                                            <a href="#" class="item-photo res_image_account"></a>
                                          </div>
                                      </td>
                                      <td class="tb-product"> </td>
                                      <td class="tb-product"> </td>
                                   </tr>
                                   <tr>
                                      <td class="tb-product"> </td>
                                      <td class="tb-product"> </td>
                                      <td class="tb-product" style="text-align: left;">
                                          <div class="item">
                                            Metode Pembayaran<br>
                                            Nomor Rekening<br>
                                            Nama Rekening
                                          </div>
                                      </td>
                                      <td class="tb-product" style="text-align: left;">
                                          <div class="item">
                                            : Transfer<br>
                                            : <span id="res_nomor_rekening"></span><br>
                                            : <span id="res_nama_rekening"></span>
                                          </div>
                                      </td>
                                      <td class="tb-product"> </td>
                                      <td class="tb-product"> </td>
                                   </tr>
                                   <tr>
                                      <td class="tb-product"> </td>
                                      <td class="tb-product"> </td>
                                      <td class="" colspan="2" align="center">
                                          <div class="item">
                                            <strong class="item-name"> Pemesanan</strong>
                                          </div>
                                      </td>
                                      <td class="tb-product"> </td>
                                      <td class="tb-product"> </td>
                                   </tr>
                                   <tr>
                                      <td class="tb-product"> </td>
                                      <td class="tb-product"> </td>
                                      <td class="tb-product" style="text-align: left;">
                                          <div class="item">
                                            Nomor<br>
                                           <!-- Nama Pembeli<br>-->
                                            Tgl Pembelian<br>
                                            Tgl Jatuh Tempo Konfirmasi
                                          </div>
                                      </td>
                                      <td class="tb-product" style="text-align: left;">
                                          <div class="item">
                                            : <span id="res_nomor_invoice"></span><br>
                                           <!-- : <span id="res_nama_pembeli"></span><br>-->
                                            : <span id="res_tgl_pembelian"></span><br>
                                            : <span id="res_tgl_tempo"></span><br>
                                          </div>
                                      </td>
                                      <td class="tb-product"> </td>
                                      <td class="tb-product"> </td>
                                   </tr>
                                   <!--<tr>
                                      <td class="tb-product"> </td>
                                      <td class="tb-product"> </td>
                                      <td class="" colspan="2" align="center">
                                          <div class="item">
                                            <strong class="item-name"> Dikirimkan Kepada</strong>
                                          </div>
                                      </td>
                                      <td class="tb-product"> </td>
                                      <td class="tb-product"> </td>
                                   </tr>
                                   <tr>
                                      <td class="tb-product"> </td>
                                      <td class="tb-product"> </td>
                                      <td class="tb-product" colspan="2" style="text-align: left;">
                                          <div class="item">
                                            <span id="res_alamat"></span>
                                          </div>
                                      </td>
                                      <td class="tb-product"> </td>
                                      <td class="tb-product"> </td>
                                   </tr> -->
                                   <tr>
                                      <td class="tb-product"> </td>
                                      <td class="tb-product"> </td>
                                      <td class="" colspan="2" align="center">
                                          <div class="item">
                                            <strong class="item-name"> Total Pesanan Anda</strong>
                                          </div>
                                      </td>
                                      <td class="tb-product"> </td>
                                      <td class="tb-product"> </td>
                                   </tr>
                                   <tr>
                                      <td class="tb-product"> </td>
                                      <td class="tb-product"> </td>
                                      <td class="tb-product" style="text-align: left;">
                                          <div class="item">
                                            Sub Total<br>
                                            Ongkos Kirim<br>
                                            Diskon<br>
                                            Total Pembayaran
                                          </div>
                                      </td>
                                      <td class="tb-product" style="text-align: left;">
                                          <div class="item">
                                            : <span id="res_sub_total"></span><br>
                                            : <span id="res_ongkir"></span><br>
                                            : <span id="res_diskon"></span><br>
                                            : <span id="res_total"></span><br>
                                          </div>
                                      </td>
                                      <td class="tb-product"> </td>
                                      <td class="tb-product"> </td>
                                   </tr>
                            </tbody>
                        </table>
                    </div>
                </form><!-- form cart -->

            </div>
           
		</main><!-- end MAIN -->

<script type="text/javascript">
    jQuery(document).ready(function($) {

        $.ajax({
            url: '{base_url}account/get_profile',
            type: 'GET',
            dataType: 'json'
        })
        .done(function(data) {
            v_list_data     = '';

            if(data.status=="success"){
                v_loop_product  = data.result.address;  
                var no = 0;        
                $.each(v_loop_product, function(index, val) {
                
                   v_list_data = v_list_data+'<tr>';
                   v_list_data = v_list_data+'    <td class="tb-product">';
                   v_list_data = v_list_data+'        <div class="item">';
                   v_list_data = v_list_data+'            <div class="item-detail">';
                   v_list_data = v_list_data+'                <strong class="item-name">'+val.address_name+'</strong>';
                   v_list_data = v_list_data+'                <br><strong class="item-name">Address: '+val.address+'</strong>';
                   v_list_data = v_list_data+'                <div class="item-name">Phone: '+val.phone_number+'</div>';
                   v_list_data = v_list_data+'            </div>';
                   v_list_data = v_list_data+'        </div>';
                   v_list_data = v_list_data+'    </td>';
                   v_list_data = v_list_data+'    <td class="tb-remove">';
                   v_list_data = v_list_data+'        <a onClick="edit_data('+no+');" class="pointer"><span><i class="fa fa-edit"></i></span></a>';
                   v_list_data = v_list_data+'    </td>';
                   v_list_data = v_list_data+'</tr>';
                   no++;
                });
            }

            $('#check_cart_alamat').html(v_list_data);
        })
        .fail(function() {
            console.log("error");
        });

        $("#add_address").click(function(event) {
        /* Act on the event */
            $('body').loadingModal({
                position: 'auto',
                text: '',
                color: '#fff',
                opacity: '0.7',
                backgroundColor: 'rgb(0,0,0)',
                text: 'Loading...',
                animation: 'wave'
            });
            window.location.href = '{base_url}account/forms_address?act=Add';
        });

        edit_data = function(id){
            $('body').loadingModal({
                position: 'auto',
                text: '',
                color: '#fff',
                opacity: '0.7',
                backgroundColor: 'rgb(0,0,0)',
                text: 'Loading...',
                animation: 'wave'
            });

            $("#address").val(id);
            $(".ch_address").hide();
            $(".ch_bankaccount").show();
            $(".active").html('Pilih Metode Pembayaran');

            v_list_data     = '';

            $.ajax({
                url: '{base_url}account/bank_account',
                type: 'GET',
                dataType: 'json',
            })
            .done(function(data) {
                  v_loop_product  = data.result;
                $.each(v_loop_product, function(index, val) {
                   v_list_data = v_list_data+'<tr>';
                   v_list_data = v_list_data+'    <td class="tb-product">';
                   v_list_data = v_list_data+'        <div class="item">';
                   v_list_data = v_list_data+'            <a href="#" class="item-photo"><img src="'+val.image_url+'" alt="cart"></a>';
                   v_list_data = v_list_data+'            <div class="item-detail">';
                   v_list_data = v_list_data+'                <strong class="item-name"><a href="#" >'+val.account_name+'  </a></strong>';
                   v_list_data = v_list_data+'                <div class="item-price">';
                   v_list_data = v_list_data+'                    <span class="price">Nomor Rekening: '+val.account_number+'</span>';
                   v_list_data = v_list_data+'                </div>';
                   v_list_data = v_list_data+'                <div class="item-name">Bank: '+val.bank_name+'</div>';
                   v_list_data = v_list_data+'            </div>';
                   v_list_data = v_list_data+'        </div>';
                   v_list_data = v_list_data+'    </td>';
                   v_list_data = v_list_data+'    <td class="tb-remove">';
                   v_list_data = v_list_data+'        <a onClick="submit_checkout('+index+');" class="pointer"><span><i class="fa fa-edit"></i></span></a>';
                   v_list_data = v_list_data+'    </td>';
                   v_list_data = v_list_data+'</tr>';
                });
                $('body').loadingModal('destroy');
                $('#check_cart_bank').html(v_list_data);
            })
            .fail(function() {
                console.log("error");
            });
        }


        submit_checkout = function(id){

            var address = $("#address").val();

            $('body').loadingModal({
                position: 'auto',
                text: '',
                color: '#fff',
                opacity: '0.7',
                backgroundColor: 'rgb(0,0,0)',
                text: 'Loading...',
                animation: 'wave'
            });

            $.ajax({
                url: '{base_url}account/submit_checkout',
                type: 'POST',
                dataType: 'json',
                data: {address: address,payment_method:id,distance:'1'},
            })
            .done(function(data) {
                
                if(data.status=="success"){
                        $.notify({
                            title: '<strong>Success!</strong>',
                            message: 'Proses Pembelian Berhasil silahkan lakukan pembayaran'
                        },{
                            type: 'success',
                            placement: {
                                from: "bottom"
                            },
                        });
                        $("#res_tanggal").html(data.result.expired_date);
                        //$("#res_jam").html('');

                        /*$.each(data.result.banks, function(index, val) {
                           
                           if(val.bank_name==account_name){
                             var image_bank = 'http://api.mojokertomarket.com/images/bca.png';
                           }
                        });*/
                       
                        //var image_bank = 'http://api.mojokertomarket.com/images/bri.png';
                        var image_bank = data.result.payment_method.image_url;
                        
 
                        $(".res_image_account").html('<img src="'+image_bank+'" alt="cart" width="50%" height="auto">');

                        $("#res_nomor_rekening").html(data.result.payment_method.account_number);
                        $("#res_nama_rekening").html(data.result.payment_method.account_name);
                        $("#res_nomor_invoice").html(data.result.invoice);
                        //$("#res_nama_pembeli").html(data.result.data.user_name);
                        $("#res_tgl_pembelian").html(data.result.checkout_date);
                        $("#res_tgl_tempo").html(data.result.expired_date);
                        //$("#res_alamat").html(data.result.data.address);
                        var subtotal = 0;
                        $.each(data.result.data, function(index, val) {
                           /* iterate through array or object */
                           subtotal = parseInt(subtotal)+parseInt(val.subtotal);
                        });

                        $("#res_sub_total").html(formatMoney(subtotal));
                        $("#res_ongkir").html(formatMoney(data.result.shipping_detail.cost));
                        $("#res_diskon").html(formatMoney(data.result.discount));
                        $("#res_total").html(formatMoney(data.result.grandtotal));

                        setTimeout(function(){
                            $(".ch_bankaccount").hide();
                            $(".review_transfer").show();
                            //window.location.href = '{base_url}account/checkout';
                        }, 3000);   
                }else{

                  if(typeof data.result.address !== "undefined"){
                    $.each(data.result.address, function(index, val) {
                       /* iterate through array or object */
                       $.notify({
                          title: '<strong>error!</strong>',
                          message: val
                      },{
                          type: 'danger',
                          placement: {
                              from: "bottom"
                          },
                      });
                    });
                  }else{
                    $.notify({
                        title: '<strong>error!</strong>',
                        message: 'Proses Pembelian Gagal, Silahkan di coba kembali!'
                    },{
                        type: 'danger',
                        placement: {
                            from: "bottom"
                        },
                    });
                  }
                  
                }
            })
            .fail(function() {
                    $.notify({
                        title: '<strong>error!</strong>',
                        message: 'Proses Pembelian Gagal, Silahkan di coba kembali'
                    },{
                        type: 'danger',
                        placement: {
                            from: "bottom"
                        },
                    });
            }).always(function(){
                $('body').loadingModal('destroy');
            });
        } 
    });
    

            function formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
                      try {
                        decimalCount = Math.abs(decimalCount);
                        decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

                        const negativeSign = amount < 0 ? "-" : "";

                        let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
                        let j = (i.length > 3) ? i.length % 3 : 0;

                        return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
                      } catch (e) {
                        console.log(e)
                      }
            };
</script>
