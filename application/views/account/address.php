<style type="text/css">
    .pointer {cursor: pointer;}
</style>	
    	<!-- MAIN -->
		<main class="site-main" style="background-color: #fff;">

            <!-- breadcrumb -->
            <div class="container breadcrumb-page">
                <ol class="breadcrumb">
                    <li><a href="{base_url}">Beranda </a></li>
                    <li class="active">Alamat</li>
                </ol>
            </div> <!-- breadcrumb -->


            <div class="container">

                <!-- form cart -->
                <form action="{base_url}" id="form_cart" name="form_cart" method="post" class="form-cart">
                    
                    <!-- table cart -->
                    <div class="table-cart-wrapper table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="tb-product">Alamat</th>
                                    <th class="tb-remove">Ubah</th>
                                    <th class="tb-remove">Hapus</th>
                                </tr>
                            </thead>
                            <tbody id="check_cart">
                                
                        
                            </tbody>
                        </table>
                    </div><!-- table cart -->

                    <!-- action cart -->
                    <div class="cart-actions" style="display: none;">
                       
                        <button type="button" title="Add Address" class="action checkout" id="add_address" >
                            <span>Tambah Alamat</span>
                        </button>
                    </div><!-- action cart -->

                </form><!-- form cart -->

            </div>
           
		</main><!-- end MAIN -->

<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(".cart-actions").show();   
        
        $.ajax({
            url: '{base_url}account/get_profile',
            type: 'GET',
            dataType: 'json'
        })
        .done(function(data) {
            v_list_data     = '';
            v_input         = '';

            if(data.status=="success"){
                v_loop_product  = data.result.address;  
                var no = 0;        
                         $.each(v_loop_product, function(index, val) {
                                //console.log(val);
                                 v_list_data = v_list_data+'<tr>';
                                 v_list_data = v_list_data+'    <td class="tb-product">';
                                 v_list_data = v_list_data+'        <div class="item">';
                                 v_list_data = v_list_data+'            <div class="item-detail">';
                                 v_list_data = v_list_data+'                <strong class="item-name">'+val.address_name+'</strong>';
                                 v_list_data = v_list_data+'                <br><strong class="item-name">Address: '+val.address+'</strong>';
                                 v_list_data = v_list_data+'                <div class="item-name">Phone: '+val.phone_number+'</div>';
                                 v_list_data = v_list_data+'            </div>';
                                 v_list_data = v_list_data+'        </div>';
                                 v_list_data = v_list_data+'    </td>';
                                 v_list_data = v_list_data+'    <td class="tb-remove">';
                                 v_list_data = v_list_data+'        <a onClick="edit_data('+no+');" class="pointer"><span><i class="fa fa-edit"></i></span></a>';
                                 v_list_data = v_list_data+'    </td>';
                                 v_list_data = v_list_data+'    <td class="tb-remove">';
                                 v_list_data = v_list_data+'        <a onClick="del_data('+no+');" class="action-remove pointer"><span>remove</span></a>';
                                 v_list_data = v_list_data+'    </td>';
                                 v_list_data = v_list_data+'</tr>';
                                 no++;
                         });
                    
            }

            $('#check_cart').html(v_list_data);
        })
        .fail(function() {
            console.log("error");
        });

    chage_qty = function(id,qty){
        $("#itemqtyadd_"+id).val(qty);
    }    

    edit_data = function(id){
        console.log(id);
        window.location.href = '{base_url}account/forms_address?act=Edit&id='+id+'';
    }

    $("#add_address").click(function(event) {
        /* Act on the event */
        window.location.href = '{base_url}account/forms_address?act=Add';
    });

    del_data = function (id){   
   
        swal({
            title: "Apa kamu yakin?",
            text: "Kamu tidak dapat mengembalikan data jika sudah di hapus!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Ya, Yakin Hapus",
            cancelButtonText: "Batal",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm) {
            if (isConfirm) {

                $.ajax({
                    url: '{base_url}account/delete_address',
                    type: 'POST',
                    dataType: 'json',
                    data: {id: id},
                })
                .done(function() {
                    swal({
                        title: "Hapus!",
                        text: "Alamat telah terhapus.",
                        type: "success",
                        confirmButtonClass: "btn-success"
                    });
                })
                .fail(function() {
                    swal({
                        title: "Gagal",
                        text: "Data kamu aman :). Gagal hapus alamat",
                        type: "error",
                        confirmButtonClass: "btn-danger"
                    });
                })
                .always(function() {
                    location.reload();
                });
                          
            } else {
                swal({
                    title: "Batal",
                    text: "Data kamu aman :)",
                    type: "error",
                    confirmButtonClass: "btn-danger"
                });
            }
        });
    }
        
    });

</script>
