        <!-- MAIN -->
		<main class="site-main" style="background-color: #fff;">

            <!-- breadcrumb -->
            <div class="container breadcrumb-page">
                <ol class="breadcrumb">
                    <li><a href="#">Pengaturan </a></li>
                    <li class="active">Akun Saya</li>
                </ol>
            </div> <!-- breadcrumb -->

            <div class="page-title-base container">
                <h1 class="title-base">Akun Saya</h1>
            </div>

            <div class="container">

                
                <div class="block-form-login">
                    <!-- block Update an Account -->
                    <div class="block-form-create">
                        <div class="block-title">
                            Profile Saya
                        </div>
                        <div class="block-content">
                            <form action="{base_url}" name="update_profile" method="post">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Your email" name="email" id="email" disabled="true">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Name" name="name" id="name" disabled="true">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Phone Number" name="phone_number" id="phone_number" disabled="true">
                            </div>
                            <button type="submit" class="btn btn-inline" id="submit_update" name="submit_update" disabled="true">Ubah Profile</button>
                            </form>
                        </div>
                    </div>
                    <!-- block Update an Account -->

                    <!-- block Registered-->
                    <div class="block-form-registered">
                       
                        <div class="block-title">
                            Ubah Password?
                        </div>
                        <div class="block-content">
                            <form action="{base_url}" name="change_password" method="post">
                                <div class="form-group">
                                    <input type="password" class="form-control" placeholder="Password" name="old_password" id="old_password" disabled="true">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" placeholder="Password" name="password" id="password" disabled="true">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" placeholder="Ulangi Password" name="password_confirmation" id="password_confirmation" disabled="true">
                                </div>
                                <button type="submit" class="btn btn-inline" id="submit_change_pass" name="submit_change_pass" disabled="true" >Ubah Password</button>
                            </form>
                        </div>
                        
                    </div><!-- block Registered-->

                </div>

            </div>           
		</main><!-- end MAIN -->
        <script type="text/javascript">
            
            jQuery(document).ready(function($) {

                    $.ajax({
                        url: '{base_url}account/get_profile',
                        type: 'GET',
                        dataType: 'json'
                    })
                    .done(function(data) {

                        if(data.status=="success"){
                            var result = data.result;
                            $("#email").val(result.email);
                            $("#name").val(result.name);
                            $("#phone_number").val(result.phone_number);

                            $("#name").removeAttr('disabled');
                            $("#phone_number").removeAttr('disabled');

                            $("#old_password").removeAttr('disabled');
                            $("#password").removeAttr('disabled');
                            $("#password_confirmation").removeAttr('disabled');

                            $("#submit_update").removeAttr('disabled');
                            $("#submit_change_pass").removeAttr('disabled');

                        }else{
                            $("#email").val('');
                            $("#name").val('');
                            $("#phone_number").val('');

                            $("#name").attr('disabled', 'true');
                            $("#phone_number").attr('disabled', 'true');

                            $("#old_password").attr('disabled', 'true');
                            $("#password").attr('disabled', 'true');
                            $("#password_confirmation").attr('disabled', 'true');

                            $("#submit_update").attr('disabled', 'true');
                            $("#submit_change_pass").attr('disabled', 'true');
                        }
                    })
                    .fail(function() {

                            $("#email").val('');
                            $("#name").val('');
                            $("#phone_number").val('');

                            $("#name").attr('disabled', 'true');
                            $("#phone_number").attr('disabled', 'true');
                            
                            $("#old_password").attr('disabled', 'true');
                            $("#password").attr('disabled', 'true');
                            $("#password_confirmation").attr('disabled', 'true');

                            $("#submit_update").attr('disabled', 'true');
                            $("#submit_change_pass").attr('disabled', 'true');
                    });
                    
            });

            $('form[name="update_profile"]').submit(function(e){
                e.preventDefault();
                
                $.ajax({
                    url: "{base_url}account/update_profile",
                    method: "post",
                    cache: false,
                    data: $(this).serialize(),
                    dataType: "json",
                    beforeSend: function(){
                        $('#submit_update').button('loading');
                    },
                    success: function(data){
                        $('#submit_update').button('reset');
                        if(data.status=='success'){
                            $.notify({
                                title: '<strong>Success!</strong>',
                                message: 'Ubah Profile success.'
                            },{
                                type: 'success',
                                placement: {
                                    from: "bottom"
                                },
                            });
                        }else{
                            $.notify({
                                title: '<strong>error!</strong>',
                                message: data.message
                            },{
                                type: 'danger',
                                placement: {
                                    from: "bottom"
                                },
                            });
                        }
                    },
                    complete: function(data){
                        $('#submit_update').button('reset');
                    },
                    error: function(){
                        $('#submit_update').button('reset');
                        $.notify({
                                title: '<strong>Error!</strong>',
                                message: "Please check your Network Connection!"
                            },{
                                type: 'danger',
                                placement: {
                                    from: "bottom"
                                },
                            });
                    }
                });

            });

            $('form[name="change_password"]').submit(function(e){
                e.preventDefault();
                
                $.ajax({
                    url: "{base_url}account/change_password",
                    method: "post",
                    cache: false,
                    data: $(this).serialize(),
                    dataType: "json",
                    beforeSend: function(){
                        $('#submit_change_pass').button('loading');
                    },
                    success: function(data){
                        $('#submit_change_pass').button('reset');
                        if(data.status=='success'){
                            
                            $("#old_password").val('');
                            $("#password").val('');
                            $("#password_confirmation").val('');
  
                            $.notify({
                                title: '<strong>Success!</strong>',
                                message: data.message
                            },{
                                type: 'success',
                                placement: {
                                    from: "bottom"
                                },
                            });
                           
                        }else{
                            var res = data.result;
                            $.notify({
                                title: '<strong>error!</strong>',
                                message: data.message
                            },{
                                type: 'danger',
                                placement: {
                                    from: "bottom"
                                },
                            });

                            if(typeof res.old_password !== "undefined"){
                                $.each(res.old_password, function(index, val) {
                                     /* iterate through array or object */
                                     $.notify({
                                            title: '<strong>error!</strong>',
                                            message: val
                                        },{
                                            type: 'danger',
                                            placement: {
                                                from: "bottom"
                                            },
                                        });
                                });
                            }

                            if(typeof res.password !== "undefined"){
                                $.each(res.password, function(index, val) {
                                     /* iterate through array or object */
                                     $.notify({
                                            title: '<strong>error!</strong>',
                                            message: val
                                        },{
                                            type: 'danger',
                                            placement: {
                                                from: "bottom"
                                            },
                                        });
                                });
                            }
                        }
                    },
                    complete: function(data){
                        $('#submit_change_pass').button('reset');
                    },
                    error: function(){
                        $('#submit_change_pass').button('reset');
                        $.notify({
                                title: '<strong>Error!</strong>',
                                message: "Please check your Network Connection!"
                            },{
                                type: 'danger',
                                placement: {
                                    from: "bottom"
                                },
                            });
                    }
                });

            });

        </script>

