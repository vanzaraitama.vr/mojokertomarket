<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_payment_all extends CI_Controller {

	public function index(){

		if($this->session->userdata('is_logged_in') == true){

			$parent_page	=  $this->uri->segment(1);
			$page			=  $this->uri->segment(1);
				
			$this->load->library('page_render');
			
			$data=array(
				'page_content' 				=> $page,
				'base_url'					=> base_url().$page,
				'list_position'				=> $list_position,
				"tenant_id"					=> $this->session->userdata('tenant_id')
			);

			$this->parser->parse('master/content', $data);

		}else{
			redirect('login');
		}
	}


	public function json_list(){
			
			$parent_page	=  $this->uri->segment(1);
			
			$draw=$_REQUEST['draw'];
			$length=$_REQUEST['length'];
			$start=$_REQUEST['start'];
			$search=$_REQUEST['search']["value"];
			
			$order=$_REQUEST['order'][0]["column"];
			$dir=$_REQUEST['order'][0]["dir"];

			$this->load->library('encryption');
			$this->load->model('m_admin_payment_all');
			
			$data =  $this->m_admin_payment_all->list_data($length,$start,$search,$order,$dir);
			
			$output=array();
			$output['draw']=$draw;
			$output['recordsTotal']=$output['recordsFiltered']=$data['total_data'];
			$output['data']=array();
			
			
			$nomor_urut=$start+1;
			setlocale(LC_ALL, 'id_ID');
			
			foreach ($data['data'] as $rows =>$row) {
				
				$id = $row['id'];

                $iconAction = "";    

                if($row['status']=="processed"){
					$status_transaksi= "Transaksi Sedang di Proses";
				}else if($row['status']=="paid"){
					$status_transaksi= "Transaksi Sudah di bayar, menunggu Konfirmasi Penjual";
				}else if($row['status']=="pending_payment"){
					$status_transaksi= "Menunggu Pembayaran";
				}else if($row['status']=="pending_admin_approval"){
					$status_transaksi= "Menunggu Konfirmasi Admin";
				}else if($row['status']=="shipped"){
					$status_transaksi= "Sedang di Kirim";
				}else if($row['status']=="complete"){
					$status_transaksi= "Selesai";
				}else{
					$status_transaksi= $row['status'];
				}

                $iconAction = "<a href='main#".$parent_page."/view_detail/".base64_encode($this->encryption->encrypt('Edit')).'/'.base64_encode($this->encryption->encrypt($id))."' class='btn btn-icon btn-primary btn-rounded mr-2 mb-2' data-toggle='tooltip' title='View Details' aria-expanded='false'>
			                    <i class='icmn-pencil'></i>
			                  </a>";
				
				$output['data'][]=array(
					$nomor_urut, 
					$row['invoice'], 
					$status_transaksi,
					$row['nama_toko'],
					"Rp. ".$row['grandtotal'],
					$iconAction
				);

				$nomor_urut++;
			}
			
			echo json_encode($output);
	}

	public function view_detail(){

		if($this->session->userdata('is_logged_in') == true){
			
			$this->load->library('encryption');

			$parent_page	= $this->uri->segment(1);
			$page			= $this->uri->segment(2);
			$act			= $this->encryption->decrypt(base64_decode($this->uri->segment(3)));
			$id				= $this->encryption->decrypt(base64_decode($this->uri->segment(4)));

			$this->load->library('drop_down');

			$invoice 		= NULL;
			$sub_invoice 	= NULL;
			$status_transaksi= NULL;
			$payment_method = NULL;
			$shop_id 		= NULL;
			$shop_name 		= NULL;
			$shop_phone_number = NULL;
			$shop_address 	= NULL;
			$shop_owner_name= NULL;
			$user_id 		= NULL;
			$user_name 		= NULL;
			$user_email 	= NULL;
			$phone_number 	= NULL;
			$address 		= NULL;
			$address_name 	= NULL;
			$consigne 		= NULL;
			$payment_date 	= NULL;	
			$complete_date 	= NULL;
			$coupon 		= NULL;
			$subtotal 		= NULL;
			$discount 		= NULL;
			$shipping_name  = NULL;
			$shipping_cost  = NULL;
			$distance 		= NULL;
			$grandtotal 	= NULL;

			$this->load->model('m_admin_payment_all');
			
			if($act=="Edit"){

				$detail_data = $this->m_admin_payment_all->detail_data($id);

					$id 			= $detail_data[0]['id'];
					$invoice 		= $detail_data[0]['invoice'];
					$sub_invoice 	= $detail_data[0]['sub_invoice'];
					$status 		= $detail_data[0]['status'];
					$payment_method = $detail_data[0]['payment_method'];
					$shop_id 		= $detail_data[0]['shop_id'];
					$shop_name 		= $detail_data[0]['shop_name'];
					$shop_phone_number = $detail_data[0]['shop_phone_number'];
					$shop_address 	= $detail_data[0]['shop_address'];
					$shop_owner_name= $detail_data[0]['shop_owner_name'];
					$user_id 		= $detail_data[0]['user_id'];
					$user_name 		= $detail_data[0]['user_name'];
					$user_email 	= $detail_data[0]['user_email'];
					$phone_number 	= $detail_data[0]['phone_number'];
					$address 		= $detail_data[0]['address'];
					$address_name 	= $detail_data[0]['address_name'];
					$consigne 		= $detail_data[0]['consigne'];
					$payment_date 	= $detail_data[0]['payment_date'];
					$complete_date 	= $detail_data[0]['complete_date'];
					$coupon 		= $detail_data[0]['coupon'];
					$subtotal 		= $detail_data[0]['subtotal'];
					$discount 		= $detail_data[0]['discount'];
					$shipping_name  = $detail_data[0]['shipping_name'];
					$shipping_cost  = $detail_data[0]['shipping_cost'];
					$distance 		= $detail_data[0]['distance'];
					$grandtotal 	= $detail_data[0]['grandtotal'];

					if($detail_data[0]['status']=="processed"){
					$status_transaksi= "Transaksi Sedang di Proses";
					}else if($detail_data[0]['status']=="paid"){
						$status_transaksi= "Transaksi Sudah di bayar, menunggu Konfirmasi Penjual";
					}else if($detail_data[0]['status']=="pending_payment"){
						$status_transaksi= "Menunggu Pembayaran";
					}else if($detail_data[0]['status']=="pending_admin_approval"){
						$status_transaksi= "Menunggu Konfirmasi Admin";
					}else if($detail_data[0]['status']=="shipped"){
						$status_transaksi= "Sedang di Kirim";
					}else if($detail_data[0]['status']=="complete"){
						$status_transaksi= "Selesai";
					}else{
						$status_transaksi= $detail_data[0]['status'];
					}

			}

			$data = array(
				'page'				=> $parent_page,
				'page_content'		=> $parent_page.'_'.$page,
				'base_url'			=> base_url(),
				'act'				=> $act,
				'id'				=> $id,
				'invoice'			=> $invoice,
				'sub_invoice'		=> $sub_invoice,
				'status'			=> $status_transaksi,
				'payment_method'	=> $payment_method,
				'shop_id'			=> $shop_id,
				'shop_name'			=> $shop_name,
				'shop_phone_number' => $shop_phone_number,
				'shop_address'		=> $shop_address,
				'shop_owner_name'	=> $shop_owner_name,
				'user_id'			=> $user_id,
				'user_name'			=> $user_name,
				'user_email'		=> $user_email,
				'phone_number'		=> $phone_number,
				'address'			=> $address,
				'address_name'		=> $address_name,	
				'consigne'			=> $consigne,
				'payment_date'		=> $payment_date,
				'complete_date' 	=> $complete_date,
				'coupon'			=> $coupon,
				'subtotal'			=> $subtotal,
				'discount'			=> $discount,
				'shipping_name' 	=> $shipping_name,
				'shipping_cost'  	=> $shipping_cost,
				'distance'			=> $distance,
				'grandtotal'		=> $grandtotal
			);

			$this->parser->parse('master/content', $data);

		}else{
			exit();
		}

	}


	public function forms_submit(){

		if($this->session->userdata('is_logged_in') == true){

			$act 		= $this->input->post('act');
			$id 		= $this->input->post('id');
			$status		= 'paid';

			$upd 	= $this->session->userdata('userid');
			$lup 	= date('Y-m-d H:i:s');

			$this->load->model('m_admin_payment_all');

			if($act=="Edit"){
					$submit = $this->m_admin_payment_all->edit($act,$id,$status,$upd,$lup);

					if($submit['status']){
						$statusResp=true;
						$reason=$submit['reason'];
					}else{
						$statusResp="fail";
						$reason= $submit['reason'];
					}
			}
			echo json_encode(array("status"=>$statusResp, "reason"=>$reason));

		}else{
			exit();
		}

	}
}

/* End of file Admin_support_site.php */
/* Location: ./application/controllers/Admin_support_site.php */
