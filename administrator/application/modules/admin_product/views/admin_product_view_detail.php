<nav class="cat__core__top-sidebar cat__core__top-sidebar--bg">
    <span class="cat__core__title d-block mb-2">
        <span class="text-muted">Administration ·</span>
        <span class="text-muted">Product ·</span>
        <strong>{act}</strong>
    </span>
</nav>
<style type="text/css">
    .pointer {
        cursor: pointer;
    }
</style>
<!-- START: ecommerce/dashboard -->
<section class="card">
    <div class="card-header">
        <span class="cat__core__title">
            <strong>{act} Product </strong>
        </span>
    </div>
    <div class="card-block">
        <div class="row">
            <div class="col-lg-12">
                <div class="mb-5">   
            <!-- Horizontal Form -->
                    <form id="form-1" name="form-1" method="POST">
                        <input type="hidden" name="act" id="act" value="{act}">
                        <input type="hidden" name="id" id="id" value="{id}">
                        <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none"> 
                       
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="shop_id">Store</label>
                            <div class="col-md-9">
                                <select class="form-control select2"
                                            id="shop_id" 
                                            name="shop_id">
                                        <option value="">Choose</option>    
                                        {list_shop}
                                </select>
                            </div>
                        </div>

                       <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="name_product">Name</label>
                            <div class="col-md-9">
                                <input id="name_product"
                                       name="name_product"
                                       value="{name_product}" 
                                       class="form-control"
                                       placeholder="Product Name"
                                       type="text"
                                       data-validation="[NOTEMPTY]">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="description">Description</label>
                            <div class="col-md-9">
                                <textarea class="form-control" rows="3"
                                   data-validation="[NOTEMPTY, L>=6]"
                                   data-validation-message="$ cannot empty, must be more than 6 characters. No special characters allowed."
                                   id="description" 
                                   name="description">{description}</textarea> 
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="stock">Stock</label>
                            <div class="col-md-9">
                                <input id="stock"
                                       name="stock"
                                       value="{stock}" 
                                       class="form-control"
                                       placeholder="Stock"
                                       type="number"
                                       data-validation="[NOTEMPTY]">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="price">Price</label>
                            <div class="col-md-9">
                                <input id="price"
                                       name="price"
                                       value="{price}" 
                                       class="form-control"
                                       placeholder="Price"
                                       type="number"
                                       data-validation="[NOTEMPTY]">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="discount">Discount</label>
                            <div class="col-md-9">
                                <input id="discount"
                                       name="discount"
                                       value="{discount}" 
                                       class="form-control"
                                       placeholder="Price"
                                       type="number"
                                       data-validation="[NOTEMPTY]">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="rating">Rating</label>
                            <div class="col-md-9">
                                <input id="rating"
                                       name="rating"
                                       value="{rating}" 
                                       class="form-control"
                                       placeholder="Rating"
                                       type="number">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="status">Status</label>
                            <div class="col-md-9">
                                <select class="form-control select2"
                                            id="status" 
                                            name="status"> 
                                        <option value="">Choose</option>    
                                        <option value="available">Available</option>   
                                        <option value="not available">Not Available</option>   
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="category_id">Category</label>
                            <div class="col-md-9">
                                <select class="form-control select2"
                                            id="category_id" 
                                            name="category_id">
                                        <option value="">Choose</option>    
                                        {list_categories}
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                             <label class="col-md-3 col-form-label add_field_button pointer" for="file_attach"><font color="blue">Add Image</font></label>
                             <div class="col-md-9 input_fields_wrap">

                             </div>
                        </div>

                        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 offset-md-3">
                                    <button type="submit" class="btn btn-primary pull-right pointer">Submit</button>
                                    <button type="button" class="btn btn-default pull-left pointer" onclick="goBack()" >Back</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- End Horizontal Form -->
                </div>
            </div>        
        </div>
    </div>
</section>
<!-- END: ecommerce/dashboard -->
<!-- START: page scripts -->
<script>
   jQuery(document).ready(function($) {
        $('.select2').select2();
        $('.dropify').dropify();

        if('{act}'=='Edit'){
            $('#status').val('{status}').trigger('change');
        }

        var max_fields      = 15; //maximum input boxes allowed
        var wrapper         = $(".input_fields_wrap"); //Fields wrapper
        var add_button      = $(".add_field_button"); //Add button ID
        
        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                
                $(wrapper).append('<div class="form-group row">\
                                        <div class="col-md-4">\
                                            <input type="file" class="dropify" id="file_attach" name="file_attach[]"/>\
                                            <a href="" class="remove_field col-sm-3"><i class="icmn-cross"></i></a>\
                                        </div>\
                                    </div>');
            }
            $('.dropify').dropify();
        });

        if('{images}'!=''){
            $.each(JSON.parse('{images}'), function(index, val) {
                 /* iterate through array or object */
                 
                 x++; //text box increment
                 var img_url = 'http://api.mojokertomarket.com/images/products/{id}/'+val;
                 $(wrapper).append('<div class="form-group row">\
                                        <div class="col-md-4">\
                                            <input type="file" class="dropify" data-default-file="'+img_url+'" id="file_attach" name="file_attach[]"/>\
                                            <a href="" class="remove_field col-sm-3"><i class="icmn-cross"></i></a>\
                                        </div>\
                                    </div>');
            });
            $('.dropify').dropify();
        }

        $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').remove();  x--;
        });
   });

   function goBack() {
        window.history.back();
    }

    $(function(){

        
        $('#form-1').validate({
            submit: {
                settings: {
                    inputContainer: '.form-group',
                    errorListClass: 'form-control-error',
                    errorClass: 'has-danger'
                },
                callback: {
                    onBeforeSubmit: function (node) {
                        NProgress.start();    
                    },
                    onSubmit: function (node) {
                        var form = document.getElementById('form-1');
                        var formData = new FormData(form);

                        $.ajax({
                            url: '{base_url}{page}/forms_submit',
                            enctype: 'multipart/form-data',
                            data: formData,
                            processData: false,
                            contentType: false,
                            type: 'POST',
                            dataType: 'json',
                        })
                        .done(function(data) {
                            NProgress.done();
                            if(data.status==true){
                                $.notify(data.reason);
                                document.location.href="{base_url}main#{page}";
                            }else{
                                $.notify(data.reason);
                            }          
                        })
                        .fail(function() {
                            NProgress.done();
                            $.notify("Fail Save Data, Please check your connections...");
                        });
                        
                    },
                    onError: function (error) {
                        $.notify("Fail, Please Check your input...");
                    }

                }
            },
            debug: true

        });

    });
</script>
