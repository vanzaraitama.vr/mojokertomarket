<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_admin_product extends CI_Model {

	public function list_data($length,$start,$search,$order,$dir){
		
		$order_by="ORDER BY c.name, b.name, a.name";
		if($order!=0){
			$order_by="ORDER BY $order $dir";
		}
		
		$where_clause="";
		 if($search!=""){
			$where_clause=" AND (a.name like '%$search%' OR b.name like '%$search%' OR a.description like '%$search%'
			OR a.stock like '%$search%' OR a.price like '%$search%')";
		}

		$sql		= " SELECT c.name AS store, b.name AS category_name, a.name, a.description, 
						a.stock, a.price, a.discount, a.rating, a.status, a.id
						FROM products a
						INNER JOIN categories b ON a.category_id=b.id
						INNER JOIN shops c ON a.shop_id=c.id
						$where_clause
						$order_by";
						
		$query		= $this->db->query($sql . " LIMIT $start, $length");

		$numrows	= $this->db->query($sql);
		$total		= $numrows->num_rows();
		
		return array("data"=>$query->result_array(),
						"total_data"=>$total
				);
	}

	function detail_data($id){
		$this->db->query("SET sql_mode=''");
		$this->db->select('id, shop_id, name, slug, images, description, stock, price, discount, rating, status, category_id');
		$query = $this->db->get_where('products', array('id' => $id));

		$data = $query->result_array();
		
		return $data;

	}

	function deleted_item($id){

		$this->db->where('id', $id);

		$delete = $this->db->delete('products');

		if($delete){
			$result = array('status'=> 'success', 'reason'=> 'Data has been deleted');
		}else{
			$result = array('status'=> 'fail', 'reason'=> 'Failed Deleted Data');
		}

		return $result;

	}

	function get_max_id(){
		$sql ="SELECT MAX(id) AS id FROM products";

		$query	= $this->db->query($sql);

		$result = $query->result_array();

		return $result[0]['id'];
	}


	function add($id,$shop_id,$name,$slug,$images,$description,$stock,$price,$discount,$rating,$status,$category_id,$lup){

		$data = array(
		   'shop_id' => $shop_id,	
		   'name' => $name,
		   'slug' => $slug,
		   'images' => $images, 
		   'description' => $description,
		   'stock' => $stock,
		   'price' => $price,
		   'discount' => $discount,
		   'rating' => $rating,
		   'status' => $status,
		   'category_id' => $category_id,
		   'created_at' => $lup
		);

		$insert = $this->db->insert('products', $data); 

		if($insert){
			$id = $this->db->insert_id();
			$result = array('status'=> 'success', 'reason'=> 'Data has been Inserted', 'id'=>$id);
		}else{
			$result = array('status'=> 'fail', 'reason'=> 'Failed Update Data');
		}

		return $result;

	}


	function edit($act,$id,$shop_id,$name,$slug,$images,$description,$stock,$price,$discount,$rating,$status,$category_id,$lup){
		
		$data = array('shop_id' => $shop_id,
					  'name' => $name,
					  'slug' => $slug,
					  'images' => $images, 
					  'description' => $description,
					  'stock' => $stock,
					  'price' => $price,
					  'discount' => $discount,
					  'rating' => $rating,
					  'status' => $status,
					  'category_id' => $category_id,
					  'updated_at' => $lup
					);

		$this->db->where('id', $id );

		$update	= $this->db->update('products', $data); 

		if($update){
			
			$result = array('status'=> 'success', 'reason'=> 'Data has been changed');
					
		}else{
			$result = array('status'=> 'fail', 'reason'=> 'Failed Update Data');
		}

		return $result;
	}
}

/* End of file M_admin_users.php */
/* Location: ./application/models/M_admin_users.php */
