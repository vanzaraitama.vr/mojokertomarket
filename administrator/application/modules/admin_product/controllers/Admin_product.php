<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_product extends CI_Controller {

	public function index(){

		if($this->session->userdata('is_logged_in') == true){

			$parent_page	=  $this->uri->segment(1);
			$page			=  $this->uri->segment(1);
				
			$this->load->library('page_render');
			$this->load->library('drop_down');

			$data=array(
				'page_content' 				=> $page,
				'base_url'					=> base_url().$page,
				'list_position'				=> $list_position,
				"tenant_id"					=> $this->session->userdata('tenant_id')
			);

			$this->parser->parse('master/content', $data);

		}else{
			redirect('login');
		}
	}


	public function json_list(){
			
			$parent_page	=  $this->uri->segment(1);
			
			$draw=$_REQUEST['draw'];
			$length=$_REQUEST['length'];
			$start=$_REQUEST['start'];
			$search=$_REQUEST['search']["value"];
			
			$order=$_REQUEST['order'][0]["column"];
			$dir=$_REQUEST['order'][0]["dir"];

			$this->load->library('encryption');
			$this->load->model('m_admin_product');
			
			$data =  $this->m_admin_product->list_data($length,$start,$search,$order,$dir);
			
			$output=array();
			$output['draw']=$draw;
			$output['recordsTotal']=$output['recordsFiltered']=$data['total_data'];
			$output['data']=array();
			
			
			$nomor_urut=$start+1;
			setlocale(LC_ALL, 'id_ID');
			
			foreach ($data['data'] as $rows =>$row) {
				
				$id = $row['id'];
                $iconAction = "";    

                $iconAction = "<a href='main#".$parent_page."/view_detail/".base64_encode($this->encryption->encrypt('Edit')).'/'.base64_encode($this->encryption->encrypt($id))."' class='btn btn-icon btn-primary btn-rounded mr-2 mb-2' data-toggle='tooltip' title='View Details' aria-expanded='false'>
			                    <i class='icmn-pencil'></i>
			                  </a>
			                  <a onclick=del('".$row['id']."') id=$id class='btn btn-icon btn-danger btn-rounded mr-2 mb-2' data-toggle='tooltip' title='Deleted' aria-expanded='false'>
			                    <i class='icmn-bin2'></i>
			                  </a>";
				
				$output['data'][]=array(
					$nomor_urut, 
					$row['store'],
					$row['category_name'],
					$row['name'],
					$row['description'],
					$row['stock'],
					$iconAction
				);
				$nomor_urut++;
			}
			
			echo json_encode($output);
	}

	public function view_detail(){

		if($this->session->userdata('is_logged_in') == true){
			
			$this->load->library('encryption');

			$parent_page	= $this->uri->segment(1);
			$page			= $this->uri->segment(2);
			$act			= $this->encryption->decrypt(base64_decode($this->uri->segment(3)));
			$id				= $this->encryption->decrypt(base64_decode($this->uri->segment(4)));

			$this->load->library('drop_down');

			$shop_id 		= NULL;
			$name			= NULL;
			$slug			= NULL;
			$description	= NULL;
			$stock			= NULL;
			$price			= NULL;
			$discount		= NULL;
			$rating			= NULL;
			$status 		= NULL;
			$category_id	= NULL;
			$images 		= NULL;

			$this->load->model('m_admin_product');
			
			if($act=="Edit"){

				$detail_data = $this->m_admin_product->detail_data($id);

				$replace_string = array( '[', ']','"');
				$images = str_replace($replace_string,"",$detail_data[0]['images']);
				$images = explode(',', $images);
				

					$shop_id		= $detail_data[0]['shop_id'];
					$name			= $detail_data[0]['name'];
					$slug			= $detail_data[0]['slug'];
					$description	= $detail_data[0]['description'];
					$stock			= $detail_data[0]['stock'];
					$price			= $detail_data[0]['price'];
					$discount		= $detail_data[0]['discount'];
					$rating			= $detail_data[0]['rating'];
					$status 		= $detail_data[0]['status'];
					$category_id	= $detail_data[0]['category_id'];
					$images 		= json_encode($images);
			}

			$this->drop_down->select('id','name');
			$this->drop_down->from('categories');
			$list_categories = $this->drop_down->build($category_id);

			$this->drop_down->select('id','name');
			$this->drop_down->from('shops');
			$list_shop = $this->drop_down->build($shop_id);

			$data = array(
				'page'				=> $parent_page,
				'page_content'		=> $parent_page.'_'.$page,
				'base_url'			=> base_url(),
				'act'				=> $act,
				'id'				=> $id,
				'name_product'		=> $name,
				'slug'				=> $slug,
				'description'		=> $description,
				'stock'				=> $stock,
				'price'				=> $price,
				'discount'			=> $discount,
				'rating'			=> $rating,
				'status'			=> $status,
				'category_id'		=> $category_id,
				'images'			=> $images,
				'list_categories'	=> $list_categories,
				'list_shop'			=> $list_shop
			);

			$this->parser->parse('master/content', $data);

		}else{
			exit();
		}

	}


	public function forms_submit(){

		if($this->session->userdata('is_logged_in') == true){

			$act 		= $this->input->post('act');
			$id 		= $this->input->post('id');
			$shop_id 	= $this->input->post('shop_id');
			$name		= $this->input->post('name_product');
			$description= $this->input->post('description');
			$stock 		= $this->input->post('stock');
			$price 		= $this->input->post('price');
			$discount	= $this->input->post('discount');
			$rating		= $this->input->post('rating');
			$status		= $this->input->post('status');
			$category_id= $this->input->post('category_id');

			$upd 	= $this->session->userdata('userid');
			$lup 	= date('Y-m-d H:i:s');
			
			$this->load->model('m_admin_product');
			
			$slug 	= $this->slugify($name);

			$arr_image  = array();
			$images 	= "";


			if($act!="Edit"){
				$submit = $this->m_admin_product->add($id,$shop_id,$name,$slug,$images,$description,$stock,$price,$discount,$rating,$status,$category_id,$lup);

					
					if($submit['status']){
						$statusResp=true;
						$reason="Add Data Successfully...!";
						$id = $submit['id'];
					}else{
						$statusResp="fail";
						$reason= $submit['reason'];
					}	
			}

			if(isset($_FILES['file_attach']['name'])){

				$files = $_FILES;
				$files_count = count($_FILES['file_attach']['name']);
				
				if($files_count>0){
					//$path = str_replace('\\','/', APPPATH)."";
					//$path = str_replace('application','assets/images',$path);
					

					$path = "/var/www/mojomart/public/images/products/".$id;

					!is_dir($path)?mkdir($path,0777,TRUE):'';

					$final_path = $path;
					
					$config = array();
					$config['upload_path'] = $final_path;
					$config['allowed_types'] = 'gif|jpg|png|ico|jpeg';
					$config['encrypt_name'] = TRUE;

					$this->load->library('upload', $config);
					
				    for($i=0; $i< $files_count; $i++){     
				    	
				        $_FILES['file_attach']['name'] = $files['file_attach']['name'][$i];
				        $_FILES['file_attach']['type'] = $files['file_attach']['type'][$i];
				        $_FILES['file_attach']['tmp_name'] = $files['file_attach']['tmp_name'][$i];
				        $_FILES['file_attach']['error'] = $files['file_attach']['error'][$i];
				        $_FILES['file_attach']['size'] = $files['file_attach']['size'][$i];    

				        $this->upload->initialize($config);
				        
				        if (!$this->upload->do_upload('file_attach')){
				        	$result = array("status" => false, "reason" => $this->upload->display_errors() );
				        }
				        else{
				        	$file_img  = $this->upload->data('file_name');
							$orig_name = $this->upload->data('orig_name');

							array_push($arr_image,$file_img);

				        	$result = array("status" => true, "reason" => "");
				        }
				    }
				}

				$images=implode('","', $arr_image);
				$images= '["'.$images.'"]';
			}

			$submit = $this->m_admin_product->edit($act,$id,$shop_id,$name,$slug,$images,$description,$stock,$price,$discount,$rating,$status,$category_id,$lup);

			if($submit['status']){
				$statusResp=true;
				$reason=$submit['reason'];
			}else{
				$statusResp="fail";
				$reason= $submit['reason'];
			}

			echo json_encode(array("status"=>$statusResp, "reason"=>$reason));

		}else{
			exit();
		}

	}

	public function del_item(){

		if($this->session->userdata('is_logged_in') == true){

			$id 	= $this->input->post('id');
			$upd 	= $this->session->userdata('userid');
			$lup 	= date('Y-m-d H:i:s');

			$this->load->model('m_admin_product');

			$deleted_item = $this->m_admin_product->deleted_item($id);

			echo json_encode($deleted_item);

		}else{
			exit();
		}
	}

	public function slugify($text){
		  // replace non letter or digits by -
		  $text = preg_replace('~[^\pL\d]+~u', '-', $text);

		  // transliterate
		  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

		  // remove unwanted characters
		  $text = preg_replace('~[^-\w]+~', '', $text);

		  // trim
		  $text = trim($text, '-');

		  // remove duplicate -
		  $text = preg_replace('~-+~', '-', $text);

		  // lowercase
		  $text = strtolower($text);

		  if (empty($text)) {
		    return 'n-a';
		  }

		  return $text;
	}

}

/* End of file Admin_support_site.php */
/* Location: ./application/controllers/Admin_support_site.php */
