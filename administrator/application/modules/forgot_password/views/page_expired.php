<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{title}</title>
    <link href="{base_url}assets/modules/core/common/img/{favicon}" rel="shortcut icon">
    <link href="{base_url}assets/vendors/font-google/font-google.css?family=PT+Sans:400,400i,700,700i" rel="stylesheet">

    <!-- VENDORS -->
    <!-- v2.0.0 -->
    <link rel="stylesheet" type="text/css" href="{base_url}assets/vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/vendors/jscrollpane/style/jquery.jscrollpane.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/vendors/ladda/dist/ladda-themeless.min.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/vendors/bootstrap-select/dist/css/bootstrap-select.min.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/vendors/fullcalendar/dist/fullcalendar.min.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/vendors/bootstrap-sweetalert/dist/sweetalert.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/vendors/c3/c3.min.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/vendors/font-linearicons/style.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/vendors/font-icomoon/style.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/vendors/font-awesome/css/font-awesome.min.css">
    <script src="{base_url}assets/vendors/jquery/dist/jquery.min.js"></script>
    <script src="{base_url}assets/vendors/tether/dist/js/tether.min.js"></script>
    <script src="{base_url}assets/vendors/jquery-ui/jquery-ui.min.js"></script>
    <script src="{base_url}assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="{base_url}assets/vendors/jquery-mousewheel/jquery.mousewheel.min.js"></script>
    <script src="{base_url}assets/vendors/jscrollpane/script/jquery.jscrollpane.min.js"></script>
    <script src="{base_url}assets/vendors/spin.js/spin.js"></script>
    <script src="{base_url}assets/vendors/ladda/dist/ladda.min.js"></script>
    <script src="{base_url}assets/vendors/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
    <script src="{base_url}assets/vendors/select2/dist/js/select2.full.min.js"></script>
    <script src="{base_url}assets/vendors/html5-form-validation/dist/jquery.validation.min.js"></script>
    <script src="{base_url}assets/vendors/jquery-typeahead/dist/jquery.typeahead.min.js"></script>
    <script src="{base_url}assets/vendors/jquery-mask-plugin/dist/jquery.mask.min.js"></script>
    <script src="{base_url}assets/vendors/autosize/dist/autosize.min.js"></script>
    <script src="{base_url}assets/vendors/bootstrap-show-password/bootstrap-show-password.min.js"></script>
    <script src="{base_url}assets/vendors/moment/min/moment.min.js"></script>
    <script src="{base_url}assets/vendors/fullcalendar/dist/fullcalendar.min.js"></script>
    <script src="{base_url}assets/vendors/bootstrap-sweetalert/dist/sweetalert.min.js"></script>
    <script src="{base_url}assets/vendors/remarkable-bootstrap-notify/dist/bootstrap-notify.min.js"></script>


    <!-- CLEAN UI ADMIN TEMPLATE MODULES-->
    <!-- v2.0.0 -->
    <link rel="stylesheet" type="text/css" href="{base_url}assets/modules/core/common/core.cleanui.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/modules/vendors/common/vendors.cleanui.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/modules/layouts/common/layouts-pack.cleanui.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/modules/themes/common/themes.cleanui.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/modules/top-bar/common/top-bar.cleanui.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/modules/footer/common/footer.cleanui.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/modules/pages/common/pages.cleanui.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/modules/apps/common/apps.cleanui.css">
    <script src="{base_url}assets/modules/menu-left/common/menu-left.cleanui.js"></script>
    <script src="{base_url}assets/modules/menu-right/common/menu-right.cleanui.js"></script>
</head>
<body class="cat__config--vertical cat__menu-left--colorful cat__menu-left--visible">
<div class="cat__top-bar">

<div class="cat__content">
<!-- START: pages/page-500 -->
<div class="text-center">
    <div class="w-50 d-inline-block pt-5 pb-5 mt-5 mb-5">
        <h1 class="mb-4"><strong>Page Expired</strong></h1>
        <p class="mb-4">The page you are looking for is already expired</p>
        <a href="{base_url}login/{tenant_id_alias}" class="btn btn-link">Login</a>
        <a href="javascript: void(0);" class="btn btn-link">Contact Us</a>
    </div>
</div>
<!-- END: pages/page-500 -->
    <a href="javascript: void(0);" class="cat__core__scroll-top" onclick="$('body, html').animate({'scrollTop': 0}, 500);"><i class="icmn-arrow-up"></i></a>
</div>
</div>
</body>
</html>