<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_users extends CI_Controller {

	public function index(){

		if($this->session->userdata('is_logged_in') == true){

			$parent_page	=  $this->uri->segment(1);
			$page			=  $this->uri->segment(1);
				
			$this->load->library('page_render');
			
			$data=array(
				'page_content' 				=> $page,
				'base_url'					=> base_url().$page,
				'list_position'				=> $list_position,
				"tenant_id"					=> $this->session->userdata('tenant_id')
			);

			$this->parser->parse('master/content', $data);

		}else{
			redirect('login');
		}
	}


	public function json_list(){
			
			$parent_page	=  $this->uri->segment(1);
			
			$draw=$_REQUEST['draw'];
			$length=$_REQUEST['length'];
			$start=$_REQUEST['start'];
			$search=$_REQUEST['search']["value"];
			
			$order=$_REQUEST['order'][0]["column"];
			$dir=$_REQUEST['order'][0]["dir"];

			$this->load->library('encryption');
			$this->load->model('m_admin_users');
			
			$data =  $this->m_admin_users->list_data($length,$start,$search,$order,$dir);
			
			$output=array();
			$output['draw']=$draw;
			$output['recordsTotal']=$output['recordsFiltered']=$data['total_data'];
			$output['data']=array();
			
			
			$nomor_urut=$start+1;
			setlocale(LC_ALL, 'id_ID');
			
			foreach ($data['data'] as $rows =>$row) {
				
				$id = $row['id'];

                $iconAction = "";    

                $iconAction = "<a href='main#".$parent_page."/view_detail/".base64_encode($this->encryption->encrypt('Edit')).'/'.base64_encode($this->encryption->encrypt($id))."' class='btn btn-icon btn-primary btn-rounded mr-2 mb-2' data-toggle='tooltip' title='View Details' aria-expanded='false'>
			                    <i class='icmn-pencil'></i>
			                  </a>
			                  <a onclick=del('".$row['id']."') id=$id class='btn btn-icon btn-danger btn-rounded mr-2 mb-2' data-toggle='tooltip' title='Deleted' aria-expanded='false'>
			                    <i class='icmn-bin2'></i>
			                  </a>";
				
				$output['data'][]=array(
					$nomor_urut, 
					$row['name'], 
					$row['email'],
					//$row['username'],
					$row['phone_number'],
					$row['bank_name'],
					$row['account_number'],
					$iconAction
				);
				$nomor_urut++;
			}
			
			echo json_encode($output);
	}

	public function view_detail(){

		if($this->session->userdata('is_logged_in') == true){
			
			$this->load->library('encryption');

			$parent_page	= $this->uri->segment(1);
			$page			= $this->uri->segment(2);
			$act			= $this->encryption->decrypt(base64_decode($this->uri->segment(3)));
			$id				= $this->encryption->decrypt(base64_decode($this->uri->segment(4)));

			$this->load->library('drop_down');

			
			$name			= NULL;
			$email 			= NULL;
			$username		= NULL;
			$phone_number	= NULL;
			$bank_name 		= NULL;
			$account_number = NULL;

			$this->load->model('m_admin_users');
			
			if($act=="Edit"){

				$detail_data = $this->m_admin_users->detail_data($id);
				
					$name			= $detail_data[0]['name'];
					$email			= $detail_data[0]['email'];
					$username		= $detail_data[0]['username'];
					$phone_number	= $detail_data[0]['phone_number'];
					$bank_name 		= $detail_data[0]['bank_name'];
					$account_number	= $detail_data[0]['account_number'];

			}

			$data = array(
				'page'				=> $parent_page,
				'page_content'		=> $parent_page.'_'.$page,
				'base_url'			=> base_url(),
				'act'				=> $act,
				'id'				=> $id,
				'name'				=> $name,
				'email'				=> $email,
				'username'			=> $username,
				'phone_number'		=> $phone_number,
				'bank_name'			=> $bank_name,
				'account_number'	=> $account_number
			);

			$this->parser->parse('master/content', $data);

		}else{
			exit();
		}

	}


	public function forms_submit(){

		if($this->session->userdata('is_logged_in') == true){

			$act 		= $this->input->post('act');
			$id 		= $this->input->post('id');
			$name		= $this->input->post('name');
			$email		= $this->input->post('email');
			$password 	= $this->input->post('password');
			$username 	= $this->input->post('username');
			$phone_number 	= $this->input->post('phone_number');
			$bank_name  	= $this->input->post('bank_name');
			$account_number	= $this->input->post('account_number');

			$upd 	= $this->session->userdata('userid');
			$lup 	= date('Y-m-d H:i:s');

			$this->load->model('m_admin_users');

			if($act=="Edit"){

					$submit = $this->m_admin_users->edit($act,$id,$name,$email,$username,$password,$phone_number,
							$bank_name,$account_number,$upd,$lup);

					if($submit['status']){
						$statusResp=true;
						$reason=$submit['reason'];
					}else{
						$statusResp="fail";
						$reason= $submit['reason'];
					}

			}else{
				
					$submit = $this->m_admin_users->add($act,$id,$name,$email,$username,$password,$phone_number,
							$bank_name,$account_number,$upd,$lup);

					
					if($submit['status']){
						$statusResp=true;
						$reason="Add Data Successfully...!";
					}else{
						$statusResp="fail";
						$reason= $submit['reason'];
					}					
			}

			echo json_encode(array("status"=>$statusResp, "reason"=>$reason));

		}else{
			exit();
		}

	}

	public function del_item(){

		if($this->session->userdata('is_logged_in') == true){

			$id 	= $this->input->post('id');
			$status = $this->input->post('status');
			$upd 	= $this->session->userdata('userid');
			$lup 	= date('Y-m-d H:i:s');

			$this->load->model('m_admin_users');

			$deleted_item = $this->m_admin_users->deleted_item($id,$status,$upd,$lup);

			echo json_encode($deleted_item);

		}else{
			exit();
		}
	}
}

/* End of file Admin_support_site.php */
/* Location: ./application/controllers/Admin_support_site.php */
