<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_admin_users extends CI_Model {

	public function list_data($length,$start,$search,$order,$dir){
		
		$order_by="ORDER BY name";
		if($order!=0){
			$order_by="ORDER BY $order $dir";
		}
		
		$where_clause="";
		 if($search!=""){
			$where_clause=" WHERE (`name` like '%$search%' OR `email` like '%$search%' OR `username` like '%$search%'

			OR `phone_number` like '%$search%' OR `bank_name` like '%$search%' OR `account_number` like '%$search%' )";
		}

		$sql = "SELECT `name`,email,username,phone_number,bank_name,account_number,id
				FROM users
				$where_clause
				$order_by";				
						
		$query		= $this->db->query($sql . " LIMIT $start, $length");

		$numrows	= $this->db->query($sql);
		$total		= $numrows->num_rows();
		
		return array("data"=>$query->result_array(),
						"total_data"=>$total
				);
	}

	function detail_data($id){
		$this->db->query("SET sql_mode=''");
		$this->db->select('`name`,email,username,phone_number,bank_name,account_number,id');
		$query = $this->db->get_where('users', array('id' => $id));

		$data = $query->result_array();
		
		return $data;

	}

	function deleted_item($id,$status,$upd,$lup){

		/*$data = array('is_deleted' => '1',
					  'updated' => $upd,
					  'updated_at' => $lup
					);

		$this->db->where('id', $id );

		$update	= $this->db->update('m_user', $data); */
		$this->db->where('id', $id);
		$this->db->delete('users');


		if($update){
			$result = array('status'=> 'success', 'reason'=> 'Data has been deleted');
		}else{
			$result = array('status'=> 'fail', 'reason'=> 'Failed Deleted Data');
		}

		return $result;

	}


	function add($act,$id,$name,$email,$username,$password,$phone_number,
							$bank_name,$account_number,$upd,$lup){

		$password =  password_hash($password, PASSWORD_BCRYPT, ['cost' => 10]);

		$data = array(
		   'name' => $name,
		   'email' => $email,
		   'username' => $username,
		   'phone_number' => $phone_number,
		   'bank_name' => $bank_name,
		   'account_number' => $account_number,  
		   'created_at' => $lup,
		   'updated_at' => $lup
		);

		$insert = $this->db->insert('users', $data); 

		if($insert){
			$result = array('status'=> 'success', 'reason'=> 'Data has been changed');
		}else{
			$result = array('status'=> 'fail', 'reason'=> 'Failed Update Data');
		}

		return $result;

	}


	function edit($act,$id,$name,$email,$username,$password,$phone_number,
							$bank_name,$account_number,$upd,$lup){
		
		$data = array('name' => $name,
					  'email' => $email,
					  'username' => $username,
					  'phone_number' => $phone_number,
					  'bank_name' => $bank_name,
					  'account_number' => $account_number,
					  'updated_at' => $lup
					);

		$this->db->where('id', $id );

		$update	= $this->db->update('users', $data); 

		if($update){
			if($password!=NULL){

				$password =  password_hash($password, PASSWORD_BCRYPT, ['cost' => 10]);

				$data_pass = array('password'  => $password);

				$this->db->where('id', $id );

				$update	= $this->db->update('users', $data_pass); 

				$result = array('status'=> 'success', 'reason'=> 'Data User & Password has been changed');

			}else{
				$result = array('status'=> 'success', 'reason'=> 'Data has been changed');
			}
			
		}else{
			$result = array('status'=> 'fail', 'reason'=> 'Failed Update Data');
		}

		return $result;
	}
}

/* End of file M_admin_users.php */
/* Location: ./application/models/M_admin_users.php */
