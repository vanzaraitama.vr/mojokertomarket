<nav class="cat__core__top-sidebar cat__core__top-sidebar--bg">
    <span class="cat__core__title d-block mb-2">
        <span class="text-muted">Administration ·</span>
        <span class="text-muted">Users ·</span>
        <strong>{act}</strong>
    </span>
</nav>
<!-- START: ecommerce/dashboard -->
<section class="card">
    <div class="card-header">
        <span class="cat__core__title">
            <strong>{act} Users </strong>
        </span>
    </div>
    <div class="card-block">
        <div class="row">
            <div class="col-lg-12">
                <div class="mb-5">   
            <!-- Horizontal Form -->
                    <form id="form-1" name="form-1" method="POST">
                        <input type="hidden" name="act" id="act" value="{act}">
                        <input type="hidden" name="id" id="id" value="{id}">
                        <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none"> 
            
                       <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="userid">Nama</label>
                            <div class="col-md-9">
                                <input id="name"
                                       class="form-control"
                                       placeholder="Nama"
                                       name="name"
                                       type="text"
                                       value="{name}" 
                                       data-validation="[NOTEMPTY]">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="first_name">Email</label>
                            <div class="col-md-9">
                                <input id="email"
                                       class="form-control"
                                       placeholder="Email"
                                       name="email"
                                       type="text"
                                       value="{email}"
                                       data-validation="[EMAIL]">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="first_name">Username</label>
                            <div class="col-md-9">
                                <input id="username"
                                       class="form-control"
                                       placeholder="Username"
                                       name="username"
                                       type="text"
                                       value="{username}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="password">Password</label>
                            <div class="col-md-9">
                                <input id="password"
                                       class="form-control password"
                                       name="password"
                                       type="password" data-validation="[L>=6]"
                                       data-validation-message="$ must be at least 6 characters"
                                       placeholder="Password">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="password_confirm">Re-type Password</label>
                            <div class="col-md-9">
                                <input id="password_confirm"
                                   class="form-control"
                                   name="password_confirm"
                                   type="password" data-validation="[V==password]"
                                   data-validation-message="Re-type Password does not match the password"
                                   placeholder="Re-type Password">
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="phone">No. Tlp</label>
                            <div class="col-md-9">
                                <input id="phone_number"
                                       class="form-control"
                                       placeholder="No. Tlp"
                                       name="phone_number"
                                       value="{phone_number}"
                                       type="text">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="bank_name">Nama Bank</label>
                            <div class="col-md-9">
                                <input id="bank_name"
                                       class="form-control"
                                       placeholder="Nama Bank"
                                       name="bank_name"
                                       type="text"
                                       value="{bank_name}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="account_number">No. Rekening</label>
                            <div class="col-md-9">
                                <input id="account_number"
                                       class="form-control"
                                       placeholder="No. Rekening"
                                       name="account_number"
                                       type="text"
                                       value="{account_number}">
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 offset-md-3">
                                    <button type="submit" class="btn btn-primary pull-right pointer">Submit</button>
                                    <button type="button" class="btn btn-default pull-left pointer" onclick="goBack()" >Back</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- End Horizontal Form -->
                </div>
            </div>        
        </div>
    </div>
</section>
<!-- END: ecommerce/dashboard -->
<!-- START: page scripts -->
<script>
   jQuery(document).ready(function($) {
        $('.select2').select2();

        if($("#userlevel").val()!='admin'){
            $(".sh_position").show();
            $("#position").removeAttr('disabled');
        }

        $("#userlevel").change(function(event) {
            /* Act on the event */
            if($("#userlevel").val()=='admin'){
                $(".sh_position").hide();
                $("#position").attr('disabled', 'true');
            }else{    
                $(".sh_position").show();
                $("#position").removeAttr('disabled');
            }
        });

        $("#password").removeAttr('data-validation');
        $("#password").removeAttr('data-validation-message');
        $("#password_confirm").removeAttr('data-validation');
        $("#password_confirm").removeAttr('data-validation-message');

        $("#password").change(function(event) {
            /* Act on the event */
            if($("#password").val()!=''){
                $("#password").attr('data-validation', '[L>=6]');
                $("#password").attr('data-validation-message', '$ must be at least 6 characters');
                $("#password_confirm").attr('data-validation', '[V==password]');
                $("#password_confirm").attr('data-validation-message', 'Re-type Password does not match the password');
            }else{
                $("#password").removeAttr('data-validation');
                $("#password").removeAttr('data-validation-message'); 
                $("#password_confirm").removeAttr('data-validation');
                $("#password_confirm").removeAttr('data-validation-message'); 
            }
        });
   });

   function goBack() {
        window.history.back();
    }

    $(function(){

        
        $('#form-1').validate({
            submit: {
                settings: {
                    inputContainer: '.form-group',
                    errorListClass: 'form-control-error',
                    errorClass: 'has-danger'
                },
                callback: {
                    onBeforeSubmit: function (node) {
                        NProgress.start();    
                    },
                    onSubmit: function (node) {

                        $.ajax({
                            url: '{base_url}{page}/forms_submit',
                            type: 'POST',
                            dataType: 'json',
                            data: $('#form-1').serialize(),
                        })
                        .done(function(data) {
                            NProgress.done();
                            if(data.status==true){
                                $.notify(data.reason);
                                document.location.href="{base_url}main#{page}";
                            }else{
                                $.notify(data.reason);
                            }          
                        })
                        .fail(function() {
                            NProgress.done();
                            $.notify("Fail Save Data, Please check your connections...");
                        });
                        
                    },
                    onError: function (error) {
                        $.notify("Fail, Please Check your input...");
                    }

                }
            },
            debug: true

        });

    });
</script>
