<nav class="cat__core__top-sidebar cat__core__top-sidebar--bg">
    <span class="cat__core__title d-block mb-2">
        <span class="text-muted">Work Order ·</span>
        <span class="text-muted">Open ·</span>
        <strong>Ticket Details {ticket_id}</strong>
    </span>
</nav>
<!-- START: ecommerce/dashboard -->
<section class="card">
    <div class="card-header">
        <span class="cat__core__title">
            <strong>Ticket Details {ticket_id}</strong>
        </span>
    </div>
    <div class="modal fade" id="merge_modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myModalLabel">Merge Ticket</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="form-merge" name="form-merge" method="POST">
                <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none"> 
                <div class="modal-body">
                    <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l1">Ticket Id</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" placeholder="Ticket Id Original" disabled="true" id="merge_ticket_id" name="merge_ticket_id" value="{ticket_id}">
                            </div>
                    </div>
                    <div class="form-group row">   
                        <label class="col-md-3 col-form-label" for="l1">Merge to </label>   
                        <div class="col-md-9">       
                            <!--<div class="typeahead__container">
                                <div class="typeahead__field">
                                    <input  class="form-control" data-validation="[NOTEMPTY]"
                                    data-validation-message="Merge to cannot empty." 
                                            name="merge_to_ticket_id" 
                                            id="merge_to_ticket_id" 
                                            type="text" 
                                            placeholder="Search Ticket Id" 
                                            autocomplete="off">          
                                </div>
                            </div>   -->
                           
                             <select class="form-control select2" data-validation="[NOTEMPTY]"
                                     data-validation-message="Merge to cannot empty." 
                                     id="merge_to_ticket_id" 
                                     name="merge_to_ticket_id">
                                 <option value="">"Search Ticket Id</option>
                             </select>
                        </div>     
                    </div>
                    <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l1">Company</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" placeholder="Company" disabled="true" id="company_merge" name="company_merge" value="">
                            </div>
                    </div> 
                    <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l1">Tier 3</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" placeholder="Tier 3" disabled="true" id="tier3_merge" name="tier3_merge" value="">
                            </div>
                    </div>
                    <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="l1">Descriptions</label>
                            <div class="col-md-9">
                                <textarea class="form-control" rows="3" disabled="true" 
                                              id="descriptions_merge" 
                                              name="descriptions_merge"></textarea> 
                            </div>
                    </div>
                    <div class="form-group row">   
                        <label class="col-md-3 col-form-label" for="l1">Remark</label>   
                        <div class="col-md-9"> 
                                <textarea class="form-control" rows="3" 
                                              data-validation="[NOTEMPTY, L>=6]"
                                              data-validation-message="$ cannot empty, must be more than 6 characters. No special characters allowed."
                                              id="remark" 
                                              name="remark"></textarea> 
                        </div>
                    </div>                
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal">Back</button>
                    <button type="submit" class="btn btn-primary pull-right" >Merge</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <div class="card-block">
        <div class="row">
            <div class="col-xl-8 col-lg-12">
                <!-- Chart -->
                <div class="nav-tabs-horizontal mb-5">
                    <ul class="nav nav-tabs mb-3" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active font-size-16 text-black" href="javascript: void(0);" data-toggle="tab" data-target="#a1" role="tab"><strong>Ticket Descriptions</strong></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link font-size-16 text-black" href="javascript: void(0);" data-toggle="tab" data-target="#a2" role="tab"><strong>Company Information</strong></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link font-size-16 text-black" href="javascript: void(0);" data-toggle="tab" data-target="#a3" role="tab"><strong>Customer Information</strong></a>
                        </li>
                    </ul>
                    <div class="tab-content padding-top-20">
                        <div class="tab-pane active" id="a1" role="tabcard">
                            <div class="col-lg-12">
                                <!-- Horizontal Description -->
                                <div class="mb-5">
                                    <dl class="row">
                                        <dt class="col-sm-3">Ticket Id</dt>
                                        <dd class="col-sm-9">: {ticket_id}</dd>

                                        <dt class="col-sm-3">Incident Type</dt>
                                        <dd class="col-sm-9">: {incident_type}</dd>

                                        <dt class="col-sm-3">Summary</dt>
                                        <dd class="col-sm-9">: {summary}</dd>

                                        <dt class="col-sm-3">Descriptions</dt>
                                        <dd class="col-sm-9">: {descriptions}</dd>

                                        <dt class="col-sm-3">Tier 1</dt>
                                        <dd class="col-sm-9">: {tier1}</dd>
                                    
                                        <dt class="col-sm-3">Tier 2</dt>
                                        <dd class="col-sm-9">: {tier2}</dd>

                                        <dt class="col-sm-3">Tier 3</dt>
                                        <dd class="col-sm-9">: {tier3}</dd>

                                        <dt class="col-sm-3">Impact</dt>
                                        <dd class="col-sm-9">: {impact}</dd>

                                        <dt class="col-sm-3">Urgency</dt>
                                        <dd class="col-sm-9">: {urgency}</dd>

                                        <dt class="col-sm-3">Priority</dt>
                                        <dd class="col-sm-9">: {priority}</dd>

                                        <dt class="col-sm-3">Reported Source</dt>
                                        <dd class="col-sm-9">: {reported_source}</dd>

                                        <dt class="col-sm-3">SLA in Minutes</dt>
                                        <dd class="col-sm-9">: {sla}</dd>

                                        <dt class="col-sm-3">Created</dt>
                                        <dd class="col-sm-9">: {created}</dd>

                                        <dt class="col-sm-3">Created Date</dt>
                                        <dd class="col-sm-9">: {created_date}</dd>
                                    </dl>
                                </div>
                                <!-- End Horizontal Description -->
                            </div>     
                        </div>
                        <div class="tab-pane" id="a2" role="tabcard">
                            <div class="col-lg-12">
                                <!-- Horizontal Description -->
                                <div class="mb-5">
                                    <dl class="row">
                                        <dt class="col-sm-3">Company</dt>
                                        <dd class="col-sm-9">: {company}</dd>

                                        <dt class="col-sm-3">Site</dt>
                                        <dd class="col-sm-9">: {site}</dd>
                                        
                                        

                                    </dl>
                                </div>
                                <!-- End Horizontal Description -->
                            </div>
                        </div>
                        <div class="tab-pane" id="a3" role="tabcard">
                            <div class="col-lg-12">
                                <!-- Horizontal Description -->
                                <div class="mb-5">
                                    <dl class="row">
                                        <dt class="col-sm-3">Name</dt>
                                        <dd class="col-sm-9">: {customer_name}</dd>

                                        <dt class="col-sm-3">Phone</dt>
                                        <dd class="col-sm-9">: {customer_phone}</dd>
                                        
                                        <dt class="col-sm-3">Email</dt>
                                        <dd class="col-sm-9">: {customer_email}</dd>

                                        <dt class="col-sm-3">Address</dt>
                                        <dd class="col-sm-9">: {customer_address}</dd>

                                    </dl>
                                </div>
                                <!-- End Horizontal Description -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Chart -->

                <!-- Left Tables -->
                <div class="nav-tabs-horizontal mb-5">
                    <ul class="nav nav-tabs mb-3" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active font-size-16 text-black" href="javascript: void(0);" data-toggle="tab" data-target="#b1" role="tab"><strong>Last Activity</strong></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link font-size-16 text-black" href="javascript: void(0);" data-toggle="tab" data-target="#b2" role="tab"><strong>Relationship</strong></a>
                        </li>
                    <!--    <li class="nav-item">
                            <a class="nav-link font-size-16 text-black" href="javascript: void(0);" data-toggle="tab" data-target="#b3" role="tab"><strong>History By Customer</strong></a>
                        </li> -->
                    </ul>
                    <div class="tab-content padding-top-20">
                        <div class="tab-pane active" id="b1" role="tabcard">
                            <br><br>
                            <div class="cui-ecommerce-dashboard-info">
                                <div class="cui-ecommerce-dashboard-info__block">
                                    <div class="mb-5">
                                        <table class="table table-hover nowrap" id="data-table" width="100%">
                                            <thead>
                                            <tr>
                                                <th>Ticket Id</th>
                                                <th>Status</th>
                                                <th>Remark</th>
                                                <th>Updater</th>
                                                <th>Last Update</th>
                                            </tr>
                                            </thead>
                                            <tbody id="list_data_activity" >
                                            
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="b2" role="tabcard">
                            <a data-toggle="modal" data-target="#merge_modal" class="btn btn-sm btn-primary ml-2 " style="display: none" id="allow_merge">
                                        Merge Ticket 
                                    <i class="icmn-attachment ml-1"> </i>
                            </a><br><br>
                            <div class="cui-ecommerce-dashboard-info">
                                <div class="cui-ecommerce-dashboard-info__block">
                                    <table class="table table-hover nowrap" id="data-table-2" width="100%">
                                            <thead>
                                            <tr>
                                                <th>Ticket Id</th>
                                                <th>Tier 1</th>
                                                <th>Tier 2</th>
                                                <th>Tier 3</th>
                                                <th>Status</th>
                                            </tr>
                                            </thead>
                                            <tbody id="list_data_relationship" >
                                            
                                            </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="b3" role="tabcard">
                            <div class="cui-ecommerce-dashboard-info">
                                <div class="cui-ecommerce-dashboard-info__block">
                                    Tab 3 Service Information
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-4 col-lg-12">
                
                <div class="row mb-5">
                    <div class="col-xl-12 col-lg-6 col-sm-12">
                        <!-- Right Widget -->
                        <div class="bg-{ticket_color} p-4 text-white mb-3">
                            <h5 class="text-uppercase mb-3">Ticket Status</h5>
                            <div class="clearfix"></div>
                            <div>
                                <span class="pull-right font-size-24">
                                    <strong>{ticket_status}</strong>
                                </span>
                                <i class="icmn-ticket font-size-24"></i>
                            </div>
                        </div>
                        <!-- End Right Widget -->
                    </div>
                    <div class="col-xl-12 col-lg-6 col-sm-12">
                        <!-- Right Widget -->
                        <div class="bg-{priority_colour} p-4 text-white">
                            <h5 class="text-uppercase mb-3">Priority</h5>
                            <div class="clearfix"></div>
                            <div>
                                <span class="pull-right font-size-24">
                                    <strong>{priority}</strong>
                                </span>
                                <i class="icmn-pushpin font-size-24"></i>
                            </div>
                        </div>
                        <!-- End Right Widget -->
                    </div>
                </div>

                <!-- Right Table -->
                <div class="mb-5">
                    <div class="font-size-16 text-black mb-3">
                        <strong>Ticket Action</strong>
                    </div>
                    <form id="form-1" name="form-1" method="POST">
                    <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none"> 
                    <input type="hidden" name="ticket_id" id="ticket_id" value="{ticket_id}">   
                    <input type="hidden" name="id_tier3" id="id_tier3" value="{id_tier3}">   
                    <input type="hidden" name="id_category" id="id_category" value="{id_category}">   
                    <input type="hidden" name="company" id="company" value="{companyid}">   
                    <div class="cui-ecommerce-dashboard-info__block">
                        <div class="col-lg-12">
                                <div class="form-group">
                                    <label  for="ticket_status">Status</label>
                                    <select class="form-control select2" disabled="true" 
                                            data-validation="[NOTEMPTY]"
                                            id="ticket_status" 
                                            name="ticket_status">
                                        <option value="">Select Status</option>
                                        {list_status}
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label  for="assignee_support_company">Assignee Support Company</label>                                            
                                    <select class="form-control select2" disabled="true"
                                            id="assignee_support_company" 
                                            name="assignee_support_company">
                                        <option value="">Select Assignee Support Company</option>
                                    </select>        
                                </div>
                                <div class="form-group">
                                    <label  for="assignee_support_org">Assignee Support Organization</label>
                                    <select class="form-control select2" disabled="true"
                                            id="assignee_support_org" 
                                            name="assignee_support_org">
                                        <option value="">Select Assignee Support Organization</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label  for="assignee_group">Assigned Group</label>                                            
                                    <select class="form-control select2" disabled="true"
                                            id="assignee_group" 
                                            name="assignee_group">
                                        <option value="">Select Assigned Group</option>
                                    </select>        
                                </div>
                                <div class="form-group">
                                    <label  for="assignee_level">Assignee Level</label>
                                    <select class="form-control select2" disabled="true"
                                            id="assignee_level" 
                                            name="assignee_level">
                                        <option value="">Select Assignee Level</option>
                                        {assignee_level}
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label  for="assignee">Assignee</label>
                                    <select class="form-control select2" disabled="true"
                                            id="assignee" 
                                            name="assignee">
                                        <option value="">Select Assignee</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label  for="feedback">Resolution</label> 
                                    <textarea class="form-control" rows="3" disabled="true" 
                                              data-validation="[NOTEMPTY, L>=6]"
                                              data-validation-message="$ cannot empty, must be more than 6 characters. No special characters allowed."
                                              id="feedback" 
                                              name="feedback"></textarea>        
                                </div>
                                <div class="form-actions">
                                    <button type="button" onclick="goBack()" class="btn btn-default pull-left">Back</button>
                                    <button type="submit" id="btn-save" class="btn btn-primary pull-right" disabled="true" style="display: none" >Submit</button>
                                </div>
                        </div>
                    </div>
                    </form>
                </div>
                <!-- End Right Table -->
            </div>
        </div>
    </div>
</section>
<!-- END: ecommerce/dashboard -->
<!-- START: page scripts -->
<script>
   jQuery(document).ready(function($) {
        $('.select2').select2();

        if('{check_privilege}'==true){
            $("#btn-save").show();
            $("#btn-save").removeAttr('disabled');
            $("#ticket_status").removeAttr('disabled');
        }

        if('{allow_merge}'==true){
            $("#allow_merge").show();
        }

        $("#ticket_status").change(function(event) {
            /* Act on the event */
            if($("#ticket_status").val()=="Assigned"){

                $("#assignee_support_company").removeAttr('disabled');
                $("#assignee_support_org").removeAttr('disabled');
                $("#assignee_group").removeAttr('disabled');
                $("#assignee_level").removeAttr('disabled');
                $("#assignee").removeAttr('disabled');

            }else{

                $("#assignee_support_company").attr('disabled', 'true');
                $("#assignee_support_org").attr('disabled', 'true');
                $("#assignee_group").attr('disabled', 'true');
                $("#assignee_level").attr('disabled', 'true');
                $("#assignee").attr('disabled', 'true');

            }

            if($("#ticket_status").val()!=""){
                $("#feedback").removeAttr('disabled');
            }

        });

        $("#assignee_support_company").change(function(event) {
            /* Act on the event */
            $('#assignee_support_org').val('').trigger('change');
            $('#assignee_group').val('').trigger('change');
            $('#assignee_level').val('').trigger('change');
            $('#assignee').val('').trigger('change');
        });

        $("#assignee_support_org").change(function(event) {
            /* Act on the event */
            $('#assignee_group').val('').trigger('change');
            $('#assignee_level').val('').trigger('change');
            $('#assignee').val('').trigger('change');
        });

        $("#assignee_group").change(function(event) {
            /* Act on the event */
            $('#assignee_level').val('').trigger('change');
            $('#assignee').val('').trigger('change');
        });

        $("#assignee_level").change(function(event) {
            /* Act on the event */
            $('#assignee').val('').trigger('change');
        });

        $("#merge_to_ticket_id").change(function(event) {
            /* Act on the event */
            $.ajax({
                url: '{base_url}{page}/merge_find_company',
                type: 'POST',
                dataType: 'json',
                data: {merge_to_ticket_id: $("#merge_to_ticket_id").val()},
            })
            .done(function(data) {
                if(data.status==true){
                    $("#company_merge").val(data.company_name);
                    $("#tier3_merge").val(data.tier3);
                    $("#descriptions_merge").val(data.descriptions);
                }else{
                    $("#company_merge").val();
                    $("#tier3_merge").val();
                    $("#descriptions_merge").val();
                }
            })
            .fail(function() {
                $("#company_merge").val();
                $("#tier3_merge").val();
                $("#descriptions_merge").val();
            })
            .always(function() {
               
            });
            
        });

        $('#assignee_support_company').select2({
            placeholder: 'Select Assignee Support Company',
            allowClear: false,
            language: {
                 noResults: function(term) {
                    return "No Results Found";
                }
            },
            escapeMarkup: function (markup) {
                    return markup;
            },
            ajax: {
            url: "{base_url}{page}/assignee_support_company",
            method:'post',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page,
                        company: $("#company").val(),
                        tier3: $("#id_tier3").val()
                    };
                },
              processResults: function (data) {
                    return {
                    results: data
                };
              },
              cache: true
            }     
        });

        $('#assignee_support_org').select2({
            placeholder: 'Select Assignee Support Organization',
            allowClear: false,
            language: {
                 noResults: function(term) {
                    return "No Results Found";
                }
            },
            escapeMarkup: function (markup) {
                    return markup;
            },
            ajax: {
            url: "{base_url}{page}/assignee_support_org",
            method:'post',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page,
                        company: $("#company").val(),
                        assignee_support_company: $("#assignee_support_company").val(),
                        tier3: $("#id_tier3").val()
                    };
                },
              processResults: function (data) {
                    return {
                    results: data
                };
              },
              cache: true
            }     
        });

        $('#assignee_group').select2({
            placeholder: 'Select Assignee Group',
            allowClear: false,
            language: {
                 noResults: function(term) {
                    return "No Results Found";
                }
            },
            escapeMarkup: function (markup) {
                    return markup;
            },
            ajax: {
            url: "{base_url}{page}/assignee_group",
            method:'post',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page,
                        company: $("#company").val(),
                        assignee_support_company: $("#assignee_support_company").val(),
                        assignee_support_org: $("#assignee_support_org").val(),
                        tier3: $("#id_tier3").val()
                    };
                },
              processResults: function (data) {
                    return {
                    results: data
                };
              },
              cache: true
            }     
        });

        $('#assignee_level').select2({
            placeholder: 'Select Assignee Level',
            allowClear: false,
            language: {
                 noResults: function(term) {
                    return "No Results Found";
                }
            },
            escapeMarkup: function (markup) {
                    return markup;
            },
            ajax: {
            url: "{base_url}{page}/assignee_level",
            method:'post',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page,
                        assignee_support_company: $("#assignee_support_company").val(),
                        assignee_support_org: $("#assignee_support_org").val(),
                        assignee_group: $("#assignee_group").val(),
                        tier3: $("#tier3").val()
                    };
                },
              processResults: function (data) {
                    return {
                    results: data
                };
              },
              cache: true
            }     
        });

        $('#assignee').select2({
            placeholder: 'Select Assignee',
            allowClear: false,
            language: {
                 noResults: function(term) {
                    return "No Results Found";
                }
            },
            escapeMarkup: function (markup) {
                    return markup;
            },
            ajax: {
            url: "{base_url}{page}/assignee",
            method:'post',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page,
                        assignee_support_company: $("#assignee_support_company").val(),
                        assignee_support_org: $("#assignee_support_org").val(),
                        assignee_group: $("#assignee_group").val(),
                        assignee_level: $("#assignee_level").val(),
                        tier3: $("#id_tier3").val()
                    };
                },
              processResults: function (data) {
                    return {
                    results: data
                };
              },
              cache: true
            }     
        });
                   
        $('#merge_to_ticket_id').select2({
            placeholder: 'Select Ticket Id',
            allowClear: false,
            language: {
                 noResults: function(term) {
                    return "No Results Found";
                }
            },
            escapeMarkup: function (markup) {
                    return markup;
            },
            ajax: {
            url: "{base_url}{page}/search_ticket",
            method:'post',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page,
                        merge_ticket_id: $("#merge_ticket_id").val(),
                    };
                },
              processResults: function (data) {
                    return {
                    results: data
                };
              },
              cache: true
            }     
        });
   });

   function goBack() {
        window.history.back();
    }

    $(function(){
        var baseUrl = '{base_url}/{page}';  
        var table =    
         $('#data-table').DataTable({
            processing: true,
            destroy:true,
            serverSide: true,
            responsive: true,
            autoFill: true,
            autoWidth: false,
            colReorder: true,
            keys: true,
            rowReorder: true,
            ajax: {
                url: baseUrl+"/json_list_activity/{ticket_id}",
                type:'POST',
            }
        });

        var table =    
         $('#data-table-2').DataTable({
            processing: true,
            destroy:true,
            serverSide: true,
            responsive: true,
            autoFill: true,
            autoWidth: false,
            colReorder: true,
            keys: true,
            rowReorder: true,
            ajax: {
                url: baseUrl+"/json_list_relationship/{ticket_id}/{id_category}",
                type:'POST',
            }
        }); 

        $('#form-1').validate({
            submit: {
                settings: {
                    inputContainer: '.form-group',
                    errorListClass: 'form-control-error',
                    errorClass: 'has-danger'
                },
                callback: {
                    onBeforeSubmit: function (node) {
                        NProgress.start();    
                    },
                    onSubmit: function (node) {

                        $.ajax({
                            url: '{base_url}{page}/forms_submit',
                            type: 'POST',
                            dataType: 'json',
                            data: $('#form-1').serialize() ,
                        })
                        .done(function(data) {
                            NProgress.done();
                            if(data.status==true){
                                $.notify(data.reason);
                                document.location.href="{base_url}main#page_open";
                            }else{
                                $.notify(data.reason);
                            }          
                        })
                        .fail(function() {
                            NProgress.done();
                            $.notify("Fail Save Data, Please check your connections...");
                        });
                        
                    },
                    onError: function (error) {
                        $.notify("Fail, Please Check your input...");
                    }

                }
            },
            debug: true

        });

        $('#form-merge').validate({
            submit: {
                settings: {
                    inputContainer: '.form-group',
                    errorListClass: 'form-control-error',
                    errorClass: 'has-danger'
                },
                callback: {
                    onBeforeSubmit: function (node) {
                      NProgress.start();  
                    },
                    onSubmit: function (node) {

                        $.ajax({
                            url: '{base_url}{page}/forms_submit_merge',
                            type: 'POST',
                            dataType: 'json',
                            data: $('#form-merge').serialize()+'&merge_ticket_id='+$("#merge_ticket_id").val(),
                        })
                        .done(function(data) {     
                            if(data.status==true){
                                NProgress.done();
                                $('#merge_modal').modal('hide');
                                $.notify(data.reason);
                                   window.setTimeout(function(){
                                    // Move to a new location or you can do something else
                                    window.location.href = "main#page_open";
                                }, 3000);
                            }else{
                                NProgress.done();
                                $('#merge_modal').modal('hide');
                                $.notify(data.reason);
                            }      
                        })
                        .fail(function() {
                            NProgress.done();
                            $('#merge_modal').modal('hide');
                            $.notify("Fail Save Data, Please check your connections...");
                        });
                        
                    },
                    onError: function (error) {
                        $.notify("Fail, Please Check your input...");
                    }

                }
            },
            debug: true

        });

      /*  $.typeahead({
            input: '#merge_to_ticket_id',
            order: "desc",
            minLength: 3,
            dynamic: true,
            source: {
                ticket_search: {
                    ajax: {
                        url: '{base_url}{page}/search_ticket',
                        type: 'POST',
                        dataType: 'json',
                        data: $('#form-merge').serialize()+'&merge_ticket_id='+$("#merge_ticket_id").val(),
                    }
                }
            }
        });*/
    });
</script>
