<!-- START: apps/profile -->
<nav class="cat__core__top-sidebar cat__core__top-sidebar--bg">
    <div class="row">
        <div class="col-xl-8 offset-xl-4">
            <h2>
                <span class="text-black">
                    <strong>{first_name} {last_name}</strong>
                </span>
                <small class="text-muted">{email}</small>
            </h2>
            <p class="mb-1">{position}</p>
        </div>
    </div>
</nav>
<div class="row">
    <div class="col-xl-4">
        <section class="card cat__apps__profile__card" style="background-image: url(assets/modules/dummy-assets/common/img/photos/4.jpeg)">
            <div class="card-block text-center">
                <a class="cat__core__avatar cat__core__avatar--border-white cat__core__avatar--110" href="javascript:void(0);">
                        <?php
                        if($this->session->userdata('foto')!=''){
                        echo '<img src="'.base_url().'assets/img/profile/'.$this->session->userdata('tenant_id').'/'.$this->session->userdata('userid').'/'.$this->session->userdata('foto').'" />'; 
                        }else{
                        echo '<img src="'.base_url().'assets/modules/dummy-assets/common/img/avatars/1.jpg" alt="Alternative text to the image"/>';
                        }
                        ?>
                </a>
                <br />
                <br />
                <br />
                <p class="text-white">
                    Last activity: {last_activity}
                </p>
                <p class="text-white">
                    <span class="cat__core__donut cat__core__donut--success"></span>
                    Online
                </p>
            </div>
        </section>
        <section class="card">
            <div class="card-block">
                <h5 class="mb-3 text-black">
                    <strong>Information</strong>
                </h5>
                <dl class="row">
                    <dt class="col-xl-4">User Id</dt>
                    <dd class="col-xl-8">{userid}</dd>
                    <dt class="col-xl-4">User Name</dt>
                    <dd class="col-xl-8">{first_name} {last_name}</dd>
                    <dt class="col-xl-4">Email</dt>
                    <dd class="col-xl-8">{email}</dd>
                    <dt class="col-xl-4">Phone</dt>
                    <dd class="col-xl-8">{phone}</dd>
                    <dt class="col-xl-4">Position</dt>
                    <dd class="col-xl-8">{position}</dd>
                    <dt class="col-xl-4">Level</dt>
                    <dd class="col-xl-8">{group_level}</dd>
                    <dt class="col-xl-4">Company</dt>
                    <dd class="col-xl-8">{assigned_support_company}</dd>
                    <dt class="col-xl-4">Organization</dt>
                    <dd class="col-xl-8">{assigned_support_organization}</dd>
                    <dt class="col-xl-4">Group</dt>
                    <dd class="col-xl-8">{assigned_group}</dd>
                    
                    <dt class="col-xl-4">Tenant Name</dt>
                    <dd class="col-xl-8">{tenant_name}.</dd>
                </dl>
            </div>
        </section>
    </div>
    <div class="col-xl-8">
        <section class="card">
            <div class="card-block">
                <div class="nav-tabs-horizontal">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" href="javascript: void(0);" data-toggle="tab" data-target="#messaging" role="tab">
                                <i class="icmn-calendar"></i>
                                Calendar
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="javascript: void(0);" data-toggle="tab" data-target="#settings" role="tab">
                                <i class="icmn-cog"></i>
                                Settings
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content py-4">                            
                        <div class="tab-pane active" id="messaging" role="tabcard">
                                <div class="example-calendar-block"></div>
                        </div>
                        <div class="tab-pane" id="settings" role="tabcard">
                        <form id="form-reset" name="form-reset" method="#">
                        <input type="hidden" name="tenant_id" id="tenant_id" value="{tenant_id} ">
                        <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none"> 
                            <h5 class="text-black mt-4">
                                <strong>Personal Information</strong>
                            </h5>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="l0">First Name</label>
                                        <input type="text" id="first_name" name="first_name" class="form-control" value="{first_name}" data-validation="[NOTEMPTY]">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="l1">Last Name</label>
                                        <input type="text" id="last_name" name="last_name" class="form-control" value="{last_name}" data-validation="[NOTEMPTY]">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="l0">Email</label>
                                        <input type="email" id="email" name="email" class="form-control" value="{email}" data-validation="[EMAIL]">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="l1">Phone</label>
                                        <input type="text" id="phone" name="phone" class="form-control" value="{phone}">
                                    </div>
                                </div>
                            </div>
                            <h5 class="text-black mt-4">
                                <strong>Password</strong>
                            </h5>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="l3">Password</label>
                                        <input id="password"
                                           class="form-control password"
                                           name="password"
                                           type="password" data-validation="[L>=6]"
                                           data-validation-message="$ must be at least 6 characters"
                                           placeholder="Password">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="l4">Password</label>
                                        <input id="password_confirm"
                                           class="form-control"
                                           name="password_confirm"
                                           type="password" data-validation="[V==password]"
                                           data-validation-message="Re-type Password does not match the password"
                                           placeholder="Re-type Password">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <h5 class="text-black mt-4">
                                        <strong>Profile Avatar</strong>
                                    </h5>
                                    <div class="form-group">
                                        <label class="form-control-label" for="l6">File Upload</label>
                                        <input type="file" id="img_profile" name="img_profile">
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="form-group">
                                    <button type="submit" class="btn width-200 btn-primary pointer">Submit</button>
                                    <button type="button" class="btn btn-default pointer" onclick="goBack()">Cancel</button>
                                </div>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<!-- END: apps/profile -->
<!-- START: page scripts -->
<script>
    $(function() {

        $('#form-reset').validate({
            submit: {
                settings: {
                    inputContainer: '.form-group',
                    errorListClass: 'form-control-error',
                    errorClass: 'has-danger'
                },
                callback: {
                    onBeforeSubmit: function (node) {
                        NProgress.start();    
                    },
                    onSubmit: function (node) {
                        var form = document.getElementById('form-reset');
                        var formData = new FormData(form);

                        $.ajax({
                            url: '{base_url}/update_profile',
                            enctype: 'multipart/form-data',
                            data: formData,
                            processData: false,
                            contentType: false,
                            type: 'POST',
                            dataType: 'json',
                        })
                        .done(function(data) {
                            NProgress.done();
                            if(data.status=="success"){
                                $.notify(data.reason);
                                 setTimeout(function () {
                                    location.reload(true);
                                }, 1000);
                            }else{
                                $.notify(data.reason);
                            }          
                        })
                        .fail(function() {
                            NProgress.done();
                            $.notify("Fail Save Data, Please check your connections...");
                        });
                        
                    },
                    onError: function (error) {
                        $.notify("Fail, Please Check your input...");
                    }

                }
            },
            debug: true
        });

        ///////////////////////////////////////////////////////////
        // ADJUSTABLE TEXTAREA
        autosize($('.adjustable-textarea'));

        ///////////////////////////////////////////////////////////
        // CALENDAR
        $('.example-calendar-block').fullCalendar({
            //aspectRatio: 2,
            height: 450,
            header: {
                left: 'prev, next',
                center: 'title',
                right: 'month, agendaWeek, agendaDay'
            },
            buttonIcons: {
                prev: 'none fa fa-arrow-left',
                next: 'none fa fa-arrow-right',
                prevYear: 'none fa fa-arrow-left',
                nextYear: 'none fa fa-arrow-right'
            },
            Actionable: true,
            eventLimit: true, // allow "more" link when too many events
            viewRender: function(view, element) {
                if (!(/Mobi/.test(navigator.userAgent)) && jQuery().jScrollPane) {
                    $('.fc-scroller').jScrollPane({
                        autoReinitialise: true,
                        autoReinitialiseDelay: 100
                    });
                }
            },
            eventClick: function(calEvent, jsEvent, view) {
                if (!$(this).hasClass('event-clicked')) {
                    $('.fc-event').removeClass('event-clicked');
                    $(this).addClass('event-clicked');
                }
            },
            defaultDate: '{calendar}',
           /* events: [
                {
                    title: 'All Day Event',
                    start: '2016-05-01',
                    className: 'fc-event-success'
                },
                {
                    id: 999,
                    title: 'Repeating Event',
                    start: '2016-05-09T16:00:00',
                    className: 'fc-event-default'
                },
                {
                    id: 999,
                    title: 'Repeating Event',
                    start: '2016-05-16T16:00:00',
                    className: 'fc-event-success'
                },
                {
                    title: 'Conference',
                    start: '2016-05-11',
                    end: '2016-05-14',
                    className: 'fc-event-danger'
                }
            ]*/
        });

        ///////////////////////////////////////////////////////////
        // SWAL ALERTS
        $('.swal-btn-success').click(function(e){
            e.preventDefault();
            swal({
                title: "Following",
                text: "Now you are following Artour Scott",
                type: "success",
                confirmButtonClass: "btn-success",
                confirmButtonText: "Ok"
            });
        });

        $('.swal-btn-success-2').click(function(e){
            e.preventDefault();
            swal({
                title: "Friends request",
                text: "Friends request was succesfully sent to Artour Scott",
                type: "success",
                confirmButtonClass: "btn-success",
                confirmButtonText: "Ok"
            });
        });

    });

     function goBack() {
        window.history.back();
    }
</script>
<!-- END: page scripts -->