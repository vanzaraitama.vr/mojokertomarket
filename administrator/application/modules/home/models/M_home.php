<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_home extends CI_Model {

	function dashboard_card($userid,$userlevel,$group_level,$group_member,$tenant_id){
		 $this->db->query("SET sql_mode=''");
		$where_clause = "";
		
		if($userlevel!="agent"){
			$where_clause = " AND (
                    JSON_SEARCH(JSON_EXTRACT('$group_member','$.assigned_group'), 'one', assignee_group) IS NOT NULL OR
                    JSON_SEARCH(JSON_EXTRACT('$group_member','$.assigned_support_organization'), 'one', assignee_support_org) IS NOT NULL OR
                    JSON_SEARCH(JSON_EXTRACT('$group_member','$.assigned_support_company'), 'one', assignee_support_company) IS NOT NULL)
                    AND assignee_level = '$group_level' ";
		}

		$sql 	= " SELECT COUNT(*) AS jml, ticket_status FROM trans_data
					WHERE MONTH(SYSDATE())= MONTH(created_at) AND is_deleted=0 AND ticket_merge='0' 
					AND tenant_id=$tenant_id $where_clause
					GROUP BY ticket_status";
				
		$query 	= $this->db->query($sql); 	

		$result = $query->result_array();	
		$new = 0;
		$assigned = 0;
		$inprogress = 0;
		$pending = 0;
		$resolved = 0;
		$closed = 0;
		$cancelled = 0;
		if(is_array($result)){

			foreach ($result as $key => $value) {
				
				if($value['ticket_status']=="New"){
					$new = $value['jml'];
				}

				if($value['ticket_status']=="Assigned"){
					$assigned = $value['jml'];
				}

				if($value['ticket_status']=="In Progress"){
					$inprogress = $value['jml'];
				}

				if($value['ticket_status']=="Pending"){
					$pending = $value['jml'];
				}

				if($value['ticket_status']=="Resolved"){
					$resolved = $value['jml'];
				}

				if($value['ticket_status']=="Closed"){
					$closed = $value['jml'];
				}

				if($value['ticket_status']=="Cancelled"){
					$cancelled = $value['jml'];
				}
			}
		}

		return array("new" => $new,  "assigned" => $assigned, "inprogress" => $inprogress, "pending" => $pending, "resolved"=> $resolved, "closed"=> $closed, "cancelled"=> $cancelled);
	}


	function linechart_daily($userid,$userlevel,$group_level,$group_member,$tenant_id){
		$this->db->query("SET sql_mode=''");
		$where_clause = "";
		
		if($userlevel!="agent"){
			$where_clause = " AND (
                    JSON_SEARCH(JSON_EXTRACT('$group_member','$.assigned_group'), 'one', assignee_group) IS NOT NULL OR
                    JSON_SEARCH(JSON_EXTRACT('$group_member','$.assigned_support_organization'), 'one', assignee_support_org) IS NOT NULL OR
                    JSON_SEARCH(JSON_EXTRACT('$group_member','$.assigned_support_company'), 'one', assignee_support_company) IS NOT NULL)
                    AND assignee_level = '$group_level' ";
		}

		$sql = "SELECT COUNT(*) AS jml, DATE(created_at) as created_at FROM trans_data
					WHERE MONTH(SYSDATE())= MONTH(created_at) AND tenant_id=$tenant_id $where_clause
				GROUP BY DATE(created_at)";
		$query = $this->db->query($sql);
		$result= $query->result_array();

		$data = '[{"name": "Value ", "data":[';

		if(isset($result)){

			foreach ($result as $key => $value) {
				# code...
				$data .= '["'.$value['created_at'].'",'.$value['jml'].'],';
			}
				$data =substr($data, 0, -1).']}]';
		}else{
			$data = array();
		}
		
		return $data;
	}

	function piechart_source($userid,$userlevel,$group_level,$group_member,$tenant_id){
		 $this->db->query("SET sql_mode=''");
		$where_clause = "";
		
		if($userlevel!="agent"){
			$where_clause = " AND (
                    JSON_SEARCH(JSON_EXTRACT('$group_member','$.assigned_group'), 'one', a.assignee_group) IS NOT NULL OR
                    JSON_SEARCH(JSON_EXTRACT('$group_member','$.assigned_support_organization'), 'one', a.assignee_support_org) IS NOT NULL OR
                    JSON_SEARCH(JSON_EXTRACT('$group_member','$.assigned_support_company'), 'one', a.assignee_support_company) IS NOT NULL)
                    AND a.assignee_level = '$group_level' ";
		}

		$sql = "SELECT COUNT(*) AS jml, b.channel_name, a.reported_source FROM trans_data a, m_channel b
				WHERE MONTH(SYSDATE())= MONTH(a.created_at) AND a.tenant_id='$tenant_id' AND a.tenant_id = b.tenant_id AND a.reported_source=b.channel_id $where_clause
				GROUP BY a.reported_source";

		$query = $this->db->query($sql);
		$result= $query->result_array();

		$data = '[{"name": "Perc", "data":[';

		if(isset($result)){

			foreach ($result as $key => $value) {
				# code...
				$data .= '["'.$value['channel_name'].'",'.$value['jml'].'],';
			}
				$data =substr($data, 0, -1).']}]';
		}else{
			$data = array();
		}
		
		return $data;
	}

	function piechart_categorize($userid,$userlevel,$group_level,$group_member,$tenant_id){
		 $this->db->query("SET sql_mode=''");
		$where_clause = "";
		
		if($userlevel!="agent"){
			$where_clause = " AND (
                    JSON_SEARCH(JSON_EXTRACT('$group_member','$.assigned_group'), 'one', a.assignee_group) IS NOT NULL OR
                    JSON_SEARCH(JSON_EXTRACT('$group_member','$.assigned_support_organization'), 'one', a.assignee_support_org) IS NOT NULL OR
                    JSON_SEARCH(JSON_EXTRACT('$group_member','$.assigned_support_company'), 'one', a.assignee_support_company) IS NOT NULL)
                    AND a.assignee_level = '$group_level' ";
		}

		$sql = "SELECT COUNT(*) AS jml, b.tier3 AS category, a.tier3 FROM trans_data a, m_category b
				WHERE MONTH(SYSDATE())= MONTH(a.created_at) AND a.tenant_id='$tenant_id' AND a.tenant_id = b.tenant_id 
				AND a.tier3=b.id $where_clause
				GROUP BY a.tier3";

		$query = $this->db->query($sql);
		$result= $query->result_array();

		$data = '[{"name": "Perc", "data":[';

		if(isset($result)){

			foreach ($result as $key => $value) {
				# code...
				$data .= '["'.$value['category'].'",'.$value['jml'].'],';
			}
				$data =substr($data, 0, -1).']}]';
		}else{
			$data = array();
		}
		
		return $data;
	}

	
	function piechart_status($userid,$userlevel,$group_level,$group_member,$tenant_id){
		$this->db->query("SET sql_mode=''");
		$where_clause = "";
		
		if($userlevel!="agent"){
			$where_clause = " AND (
                    JSON_SEARCH(JSON_EXTRACT('$group_member','$.assigned_group'), 'one', assignee_group) IS NOT NULL OR
                    JSON_SEARCH(JSON_EXTRACT('$group_member','$.assigned_support_organization'), 'one', assignee_support_org) IS NOT NULL OR
                    JSON_SEARCH(JSON_EXTRACT('$group_member','$.assigned_support_company'), 'one', assignee_support_company) IS NOT NULL)
                    AND assignee_level = '$group_level' ";
		}

		$sql = "SELECT COUNT(*) AS jml, ticket_status FROM trans_data
					WHERE MONTH(SYSDATE())= MONTH(created_at) AND tenant_id=$tenant_id $where_clause
					GROUP BY ticket_status";

		$query = $this->db->query($sql);
		$result= $query->result_array();

		$data = '[{"name": "Perc", "data":[';

		if(isset($result)){

			foreach ($result as $key => $value) {
				# code...
				$data .= '["'.$value['ticket_status'].'",'.$value['jml'].'],';
			}
				$data =substr($data, 0, -1).']}]';
		}else{
			$data = array();
		}
		
		return $data;
	}

	function alert_ticket($group_member,$group_level,$tenant_id){

		$sql = "SELECT ticket_id FROM trans_data WHERE ticket_status IN ('New','Assigned') AND tenant_id='$tenant_id' LIMIT 1";

		$query  = $this->db->query($sql);

		$data = $query->result_array();

		if(isset($data[0])){
			$result = array("status"=> true, "reason"=> "You have new ticket with ticket id ".$data[0]['ticket_id']);
		}else{
			$result = array("status"=> false, "reason"=>"No Ticket Open & Assigned" );
		}
		
		return $result;
	}

	function alert_ticket_overdue_sla($group_member,$group_level,$tenant_id){

		$sql = "SELECT a.ticket_id FROM trans_data a, m_category b 
				WHERE a.ticket_status IN ('New','Assigned') AND a.tenant_id='$tenant_id' 
				AND a.tier3=b.id AND (b.sla -1 ) <= TIMESTAMPDIFF(MINUTE, a.created_at,SYSDATE()) LIMIT 1";

		$query  = $this->db->query($sql);

		$data = $query->result_array();

		if(isset($data[0])){
			$result = array("status"=> true, "reason"=> "You have a ticket that exceeds the sla, with ticket id ".$data[0]['ticket_id']);
		}else{
			$result = array("status"=> false, "reason"=>"No Ticket Open & Assigned that exceeds the sla" );
		}
		
		return $result;
	}

	function chart1($tenant_id,$userlevel,$group_member,$group_level){
		$this->db->query("SET sql_mode=''");
		$where_clause = "";
		
		if($userlevel!="agent"){
			$where_clause = " AND (
                    JSON_SEARCH(JSON_EXTRACT('$group_member','$.assigned_group'), 'one', a.assignee_group) IS NOT NULL OR
                    JSON_SEARCH(JSON_EXTRACT('$group_member','$.assigned_support_organization'), 'one', a.assignee_support_org) IS NOT NULL OR
                    JSON_SEARCH(JSON_EXTRACT('$group_member','$.assigned_support_company'), 'one', a.assignee_support_company) IS NOT NULL)
                    AND a.assignee_level = '$group_level' ";
		}

		$sql = "SELECT COUNT(*) AS jml, b.priority_name, b.dash_colour
			FROM trans_data a
			INNER JOIN m_priority b ON a.priority=b.priority_id AND a.tenant_id=b.tenant_id
			WHERE a.tenant_id='$tenant_id' AND a.ticket_status NOT IN ('Resolved','Closed','Cancelled') AND a.ticket_merge='0' 
			AND MONTH(SYSDATE())= MONTH(a.created_at) $where_clause
			GROUP BY b.priority_name
			ORDER BY a.priority ASC";

		$query = $this->db->query($sql);
		$result= $query->result_array();

		$data = '[{"showInLegend": false, "data":[';

		if(isset($result)){

			foreach ($result as $key => $value) {
				# code...
				$data .= '{"name":"'.$value['priority_name'].'","color":"'.$value['dash_colour'].'","y":'.$value['jml'].'},';
			}
				
				$data =substr($data, 0, -1).']}]';
		}else{
			$data = array();
		}
		
		return $data;
	}


	function chart2($tenant_id,$userlevel,$group_member,$group_level){
		$this->db->query("SET sql_mode=''");
		$where_clause = "";
		
		if($userlevel!="agent"){
			$where_clause = " AND (
                    JSON_SEARCH(JSON_EXTRACT('$group_member','$.assigned_group'), 'one', a.assignee_group) IS NOT NULL OR
                    JSON_SEARCH(JSON_EXTRACT('$group_member','$.assigned_support_organization'), 'one', a.assignee_support_org) IS NOT NULL OR
                    JSON_SEARCH(JSON_EXTRACT('$group_member','$.assigned_support_company'), 'one', a.assignee_support_company) IS NOT NULL)
                    AND a.assignee_level = '$group_level'";
		}

		$sql = "SELECT COUNT(*) AS jml , b.tier3 , c.option_name AS impact, a.tier3 as id_tier3
				FROM trans_data a
				INNER JOIN m_category b ON a.tier3=b.id AND a.tenant_id=b.tenant_id
				INNER JOIN m_option c ON a.impact=c.option_id AND b.tenant_id='$tenant_id' AND c.option_type='img_impact'
				WHERE MONTH(SYSDATE())= MONTH(a.created_at) AND a.tenant_id='$tenant_id' AND a.ticket_status NOT IN ('Resolved','Closed','Cancelled') AND a.ticket_merge='0' $where_clause
				GROUP BY b.tier3, c.option_name
				ORDER BY a.impact ASC
				LIMIT 10";

		$query = $this->db->query($sql);
		$result= $query->result_array();
		if(is_array($result)){	
			$data = array_merge(array("status"=>true),array("data"=>$result));
		}else{
			$data = array("status"=>false, "reason"=> "Error get data..!!!");
		}

		return $data;
	}

	public function list_data($length,$start,$search,$order,$dir,$group_member,$group_level,$data_type,$data_name,$upd,$tenant_id){
		
		$order_by="ORDER BY a.created_at DESC";
		if($order!=0){
			$order_by="ORDER BY $order $dir";
		}
		
		$where_clause = "";

		 if($search!=""){
			$where_clause = " AND (a.`ticket_id` like '%$search%' OR b.`incident_name` like '%$search%' OR a.`summary` like '%$search%' OR a.`ticket_status` like '%$search%' OR 
			c.`priority_name` like '%$search%' OR a.`assignee_group` like '%$search%' OR a.`assignee_support_org` like '%$search%' OR a.`assignee_support_company` like '%$search%' OR 
			a.assignee like '%$search%' OR e.first_name like '%$search%' OR e.last_name like '%$search%')";
		}
		$where_data_type = "";
		$field_name = "";
		$field_name2= "";
		if($data_type=="priority"){
			$where_data_type = " AND c.priority_name='$data_name' AND a.ticket_status NOT IN ('Resolved','Closed','Cancelled') AND a.ticket_merge='0'";
			$field_name = " c.priority_name AS priority, ";
			$field_name2 = " , c.priority_colour";
		}elseif($data_type=="impact"){
			$where_data_type = " AND a.tier3='$data_name'";
			$field_name = " f.option_name AS priority, ";
			$field_name2 = " , c.priority_colour";
			//$field_name2 = " g.option_name AS priority_colour";
		}elseif($data_type=="reported_source"){
			$where_data_type = " AND h.channel_name='$data_name'";
			$field_name = " c.priority_name AS priority, ";
			$field_name2 = " , c.priority_colour";
		}elseif($data_type=="ticket_status"){
			$where_data_type = " AND a.ticket_status=REPLACE('$data_name', '%20', ' ')";
			$field_name = " c.priority_name AS priority, ";
			$field_name2 = " , c.priority_colour";
		}
		
		$sql		= " SELECT a.ticket_id, b.incident_name, a.summary, i.tier3, a.descriptions, a.ticket_status, $field_name 
						CASE
						WHEN (a.assignee_group!='' AND a.assignee_group!='[]') THEN REPLACE(REPLACE(REPLACE(a.assignee_group,'\"',' '),'[',''),']','')
						WHEN (a.assignee_support_org!='' AND a.assignee_support_org!='[]') THEN REPLACE(REPLACE(REPLACE(a.assignee_support_org,'\"',' '),'[',''),']','')
						WHEN (a.assignee_support_company!='' AND a.assignee_support_company!='[]') THEN REPLACE(REPLACE(REPLACE(a.assignee_support_company,'\"',' '),'[',''),']','')
						ELSE ''
						END AS assignee_group, CONCAT(e.first_name,' ',e.last_name) as assignee_name , a.assignee, d.ticket_color $field_name2 ,
						CASE 
						WHEN (a.ticket_status = 'New') THEN 'page_inc_open'
						WHEN (a.ticket_status = 'Assigned') THEN 'page_inc_open'
						WHEN (a.ticket_status = 'In Progress') THEN 'page_inc_inprogress'
						WHEN (a.ticket_status = 'Pending') THEN 'page_inc_pending'
						WHEN (a.ticket_status = 'Resolved') THEN 'page_inc_resolved'
						WHEN (a.ticket_status = 'Closed') THEN 'page_inc_closed'
						WHEN (a.ticket_status = 'Cancelled') THEN 'page_inc_cancelled'
						ELSE 'home'
						END AS page_detail
						FROM trans_data a  
						INNER JOIN m_incident_type b ON a.incident_type=b.incident_id AND a.tenant_id=b.tenant_id AND b.is_deleted=0
						INNER JOIN m_priority c ON a.priority=c.priority_id AND a.tenant_id=c.tenant_id AND b.is_deleted=0 AND c.status=1
						INNER JOIN m_ticketstatus d ON a.ticket_status = d.ticket_status AND a.tenant_id=d.tenant_id AND d.is_deleted=0 AND d.status=1
						INNER JOIN m_user e ON e.userid=a.assignee AND a.tenant_id=e.tenant_id
						INNER JOIN m_option f ON f.option_type='impact' AND a.impact=f.option_id
						INNER JOIN m_option g ON g.option_type='clr_impact' AND a.impact=g.option_id
						INNER JOIN m_channel h ON h.channel_id=a.reported_source AND a.tenant_id=h.tenant_id
						INNER JOIN m_category i ON i.id=a.tier3 AND i.tenant_id=a.tenant_id
						WHERE MONTH(SYSDATE())= MONTH(a.created_at) AND a.tenant_id='$tenant_id' AND a.ticket_merge='0' $where_data_type
						$where_clause
						$order_by";
						
		$query		= $this->db->query($sql . " LIMIT $start, $length");
		
		$numrows	= $this->db->query($sql);
		$total		= $numrows->num_rows();
		
		return array("data"=>$query->result_array(),
					"total_data"=>$total
				);
	}

	public function check_id_category($data_name){

		$sql = "SELECT id FROM m_category WHERE tier3=REPLACE('$data_name', '%20', ' ')";
		$query = $this->db->query($sql);
		$data = $query->result_array();

		if(isset($data[0]['id'])){
			$result = $data[0]['id'];
		}else{
			$result = 0;
		}

		return $result;
	}

	public function check_tenant_name($tenant_id){
		$sql = "SELECT tenant_name FROM m_tenant WHERE id=$tenant_id";
		$query = $this->db->query($sql);
		$data = $query->result_array();

		if(isset($data[0]['tenant_name'])){
			$result = $data[0]['tenant_name'];
		}else{
			$result = 0;
		}

		return $result;
	}

	public function update_profile($first_name, $last_name, $email, $phone, $password, $upd, $lup, $tenant_id){

		$userid			= $this->session->userdata('userid');

		$data = array('first_name' => $first_name,
					  'last_name' => $last_name,
					  'phone' => $phone,
					  'email' => $email,
					  'updated' => $upd,
					  'updated_at' => $lup,
					);

		$this->db->where('userid', $userid );
		$this->db->where('tenant_id', $tenant_id );

		$update	= $this->db->update('m_user', $data); 

		if($update){
			if($password!=NULL){

				$password =  password_hash($password, PASSWORD_BCRYPT, ['cost' => 10]);

				$data_pass = array('password'  => $password);

				$this->db->where('userid', $userid );
				$this->db->where('tenant_id', $tenant_id );

				$update	= $this->db->update('m_user', $data_pass); 

				$result = array('status'=> 'success', 'reason'=> 'Data User & Password has been changed');

			}else{
				$result = array('status'=> 'success', 'reason'=> 'Data has been changed');
			}
			
		}else{
			$result = array('status'=> 'fail', 'reason'=> 'Failed Update Data');
		}

		return $result;
	}

	public function upload_img_profile($file_img){

		$userid			= $this->session->userdata('userid');
		$tenant_id		= $this->session->userdata('tenant_id');

		$data = array('img_profile' => $file_img);

		$this->db->where('userid', $userid );
		$this->db->where('tenant_id', $tenant_id );
		$this->session->set_userdata('foto', $file_img);
		$update	= $this->db->update('m_user', $data);
	}

}

/* End of file M_home.php */
/* Location: ./application/models/M_home.php */
