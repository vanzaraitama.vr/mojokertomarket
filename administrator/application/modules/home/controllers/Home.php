<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index(){

		if($this->session->userdata('is_logged_in') == true){

			$parent_page	=  $this->uri->segment(1);
			$page			=  $this->uri->segment(1);
			
			$userid 		= $this->session->userdata('userid');
			$userlevel 		= $this->session->userdata('userlevel');
			$tenant_id		= $this->session->userdata('tenant_id');
			$group_member	= $this->session->userdata('group_member');
			$group_level	= $this->session->userdata('group_level');

			$this->load->library('page_render');

			$this->load->model('m_home');

			//$dashboard_card = $this->m_home->dashboard_card($userid,$userlevel,$group_level,$group_member,$tenant_id);
		
			$data=array(
				
				'page_content' 				=> $page,
				'base_url'					=> base_url().$page,
				'base'						=> base_url(),
				"tenant_id"					=> $this->session->userdata('tenant_id'),
				/*"new"						=> $dashboard_card['new'],
				"assigned"					=> $dashboard_card['assigned'],
				"inprogress"				=> $dashboard_card['inprogress'],
				"pending"					=> $dashboard_card['pending'],
				"resolved"					=> $dashboard_card['resolved'],
				"closed"					=> $dashboard_card['closed'],
				"cancelled"					=> $dashboard_card['cancelled'],
				"total"						=> $dashboard_card['new']+$dashboard_card['assigned']+$dashboard_card['inprogress']+$dashboard_card['pending']+$dashboard_card['resolved']+$dashboard_card['closed']+$dashboard_card['cancelled']*/
			);

			$this->parser->parse('master/content', $data);

		}else{
			redirect('login');
		}
	}

	public function profile(){

		if($this->session->userdata('is_logged_in') == true){

			$parent_page	=  $this->uri->segment(1);
			$page			=  $this->uri->segment(1);
				
			$this->load->library('page_render');
			$this->load->model('m_home');

			$calendar=date("Y-m-d");
			$lup=date("Y-m-d H:i:s");
			$last_activity = strftime( "%d %B %Y %H:%M", strtotime($lup));

			$group_member	= json_decode($this->session->userdata('group_member'),true);

			if(is_array($group_member)){

				if(is_array($group_member['assigned_group'])){
					$assigned_group = "";

					foreach ($group_member['assigned_group'] as $key => $value) {
						# code...
						$assigned_group .= '<span class="badge badge-primary">'.$value.'</span> ';
					}

				}else{
					$assigned_group = "";
				}

				if(is_array($group_member['assigned_support_company'])){
					$assigned_support_company = "";

					foreach ($group_member['assigned_support_company'] as $key => $value) {
						# code...
						$assigned_group .= '<span class="badge badge-primary">'.$value.'</span> ';
					}

				}else{
					$assigned_support_company = '<span class="badge badge-default">Not Assigned</span> ';
				}

				if(is_array($group_member['assigned_support_organization'])){
					$assigned_support_organization = "";

					foreach ($group_member['assigned_support_organization'] as $key => $value) {
						# code...
						$assigned_group .= '<span class="badge badge-primary">'.$value.'</span> ';
					}

				}else{
					$assigned_support_organization = '<span class="badge badge-default">Not Assigned</span> ';
				}
			}

			$data=array(
				'page_content' 				=> 'profile',
				'base_url'					=> base_url().$page,
				'userid'					=> $this->session->userdata('userid'),
				'first_name'				=> $this->session->userdata('first_name'),
				'last_name'					=> $this->session->userdata('last_name'),
				'email'						=> $this->session->userdata('email'),
				'phone'						=> $this->session->userdata('phone'),
				'userlevel'					=> $this->session->userdata('userlevel'),
				'position'					=> $this->session->userdata('position'),
				'group_level'				=> $this->session->userdata('group_level'),
				"tenant_id"					=> $this->session->userdata('tenant_id'),
				"tenant_name"				=> $this->m_home->check_tenant_name($this->session->userdata('tenant_id')),
				"last_activity"				=> $last_activity,
				"assigned_group"			=> $assigned_group,
				"assigned_support_company"  => $assigned_support_company,
				"assigned_support_organization" => $assigned_support_organization,
				"calendar"					=> $calendar
			);

			$this->parser->parse('master/content', $data);

		}else{
			redirect('login');
		}
	}

	public function linechart_daily(){

		if($this->session->userdata('is_logged_in') == true){

			$userid 		= $this->session->userdata('userid');
			$userlevel 		= $this->session->userdata('userlevel');
			$tenant_id		= $this->session->userdata('tenant_id');
			$group_member	= $this->session->userdata('group_member');
			$group_level	= $this->session->userdata('group_level');

			$this->load->model('m_home');

			$linechart_daily = $this->m_home->linechart_daily($userid,$userlevel,$group_level,$group_member,$tenant_id);

			echo $linechart_daily;

		}else{
			redirect('login');
		}
	}

	public function piechart_source(){

		if($this->session->userdata('is_logged_in') == true){

			$userid 		= $this->session->userdata('userid');
			$userlevel 		= $this->session->userdata('userlevel');
			$tenant_id		= $this->session->userdata('tenant_id');
			$group_member	= $this->session->userdata('group_member');
			$group_level	= $this->session->userdata('group_level');

			$this->load->model('m_home');

			$piechart_source = $this->m_home->piechart_source($userid,$userlevel,$group_level,$group_member,$tenant_id);

			echo $piechart_source;

		}else{
			redirect('login');
		}
	}
	

	public function piechart_categorize(){

		if($this->session->userdata('is_logged_in') == true){

			$userid 		= $this->session->userdata('userid');
			$userlevel 		= $this->session->userdata('userlevel');
			$tenant_id		= $this->session->userdata('tenant_id');
			$group_member	= $this->session->userdata('group_member');
			$group_level	= $this->session->userdata('group_level');

			$this->load->model('m_home');

			$piechart_categorize = $this->m_home->piechart_categorize($userid,$userlevel,$group_level,$group_member,$tenant_id);

			echo $piechart_categorize;

		}else{
			redirect('login');
		}
	}

	public function piechart_status(){

		if($this->session->userdata('is_logged_in') == true){

			$userid 		= $this->session->userdata('userid');
			$userlevel 		= $this->session->userdata('userlevel');
			$tenant_id		= $this->session->userdata('tenant_id');
			$group_member	= $this->session->userdata('group_member');
			$group_level	= $this->session->userdata('group_level');

			$this->load->model('m_home');

			$piechart_status = $this->m_home->piechart_status($userid,$userlevel,$group_level,$group_member,$tenant_id);

			echo $piechart_status;

		}else{
			redirect('login');
		}
	}


	public function check_ticket(){

		if($this->session->userdata('is_logged_in') == true){

			$tenant_id 		= $this->session->userdata('tenant_id');
			$group_member	= $this->session->userdata('group_member');
			$group_level	= $this->session->userdata('group_level');

			$this->load->model('m_home');

			$alert_ticket = $this->m_home->alert_ticket($group_member,$group_level,$tenant_id);

			echo json_encode($alert_ticket);

		}else{
			exit();
		}
	}

	public function check_over_sla(){

		if($this->session->userdata('is_logged_in') == true){

			$tenant_id 		= $this->session->userdata('tenant_id');
			$group_member	= $this->session->userdata('group_member');
			$group_level	= $this->session->userdata('group_level');

			$this->load->model('m_home');

			$alert_ticket = $this->m_home->alert_ticket_overdue_sla($group_member,$group_level,$tenant_id);

			echo json_encode($alert_ticket);

		}else{
			exit();
		}
	}

	public function chart1(){

		if($this->session->userdata('is_logged_in') == true){

			$tenant_id 		= $this->session->userdata('tenant_id');
			$userlevel 		= $this->session->userdata('userlevel');
			$group_member	= $this->session->userdata('group_member');
			$group_level	= $this->session->userdata('group_level');

			$this->load->model('m_home');

			$data = $this->m_home->chart1($tenant_id,$userlevel,$group_member,$group_level);

			echo $data;

		}else{
			exit();
		}
	}


	public function chart2(){

		if($this->session->userdata('is_logged_in') == true){

			$tenant_id 		= $this->session->userdata('tenant_id');
			$userlevel 		= $this->session->userdata('userlevel');
			$group_member	= $this->session->userdata('group_member');
			$group_level	= $this->session->userdata('group_level');

			$this->load->model('m_home');

			$data = $this->m_home->chart2($tenant_id,$userlevel,$group_member,$group_level);

			echo json_encode($data);

		}else{
			exit();
		}
	}

	public function logout(){

		$tenant_id_alias	= $this->session->userdata('tenant_id_alias');

		$this->session->sess_destroy();

		redirect('login/'.$tenant_id_alias);
	}	

	public function detail_data(){

		if($this->session->userdata('is_logged_in') == true){

			$parent_page	= $this->uri->segment(1);
			$data_type 		= $this->uri->segment(3);
			$data_name		= $this->uri->segment(4);
			
			$tenant_id		= $this->session->userdata('tenant_id');
			$group_member	= $this->session->userdata('group_member');
			$group_level	= $this->session->userdata('group_level');
			$upd			= $this->session->userdata('userid');

			$draw=$_REQUEST['draw'];
			$length=$_REQUEST['length'];
			$start=$_REQUEST['start'];
			$search=$_REQUEST['search']["value"];
			
			$order=$_REQUEST['order'][0]["column"];
			$dir=$_REQUEST['order'][0]["dir"];

			$this->load->library('encryption');
			$this->load->model('m_home');

			$data =  $this->m_home->list_data($length,$start,$search,$order,$dir,$group_member,$group_level,$data_type,$data_name,$upd,$tenant_id);
			
			$output=array();
			$output['draw']=$draw;
			$output['recordsTotal']=$output['recordsFiltered']=$data['total_data'];
			$output['data']=array();
			
			
			$nomor_urut=$start+1;
			setlocale(LC_ALL, 'id_ID');
			
			foreach ($data['data'] as $rows =>$row) {			
               
			    $page_detail=  $row['page_detail'];            
			    $iconAction = "<a onclick='show_detail_data(\"$page_detail/view_detail/".base64_encode($this->encryption->encrypt($row['ticket_id']))."\");' class='btn btn-icon btn-primary btn-rounded mr-2 mb-2' data-toggle='tooltip' title='View Details' aria-expanded='false'>
			                    <i class='icmn-pencil2'></i>
			                   </a>";

				$output['data'][]=array(
					$nomor_urut, 
					$row['ticket_id'], 
					$row['incident_name'], 
					word_limiter($row['summary'],2), 
					$row['tier3'], 
					"<h5> <span class='badge badge-".$row['ticket_color']."'>".$row['ticket_status']."</span> </h5>", 
					"<h5> <span class='badge badge-".$row['priority_colour']."'>".$row['priority']."</span> </h5>",
					$row['assignee_group'],
					$row['assignee_name'],
					$iconAction
				);
				$nomor_urut++;
			}
			
			echo json_encode($output);

		}else{
			exit();
		}

	}


	public function update_profile(){

		if($this->session->userdata('is_logged_in') == true){

			$first_name 	= $this->input->post('first_name');
			$last_name 		= $this->input->post('last_name');
			$email 			= $this->input->post('email');
			$phone 			= $this->input->post('phone');
			$password 		= $this->input->post('password');

			$tenant_id 		= $this->session->userdata('tenant_id');
			$upd			= $this->session->userdata('userid');
			$lup			= date("Y-m-d H:i:s");

			$this->load->model('m_home');

			$path 	= str_replace('\\','/', APPPATH)."";
			$path 	= str_replace('application','assets/img/profile',$path);
			$kode_tenant_id = $tenant_id.'/';
			$kode_upd 		= $upd.'/';
			
			!is_dir($path.$kode_tenant_id.$kode_upd)?mkdir($path.$kode_tenant_id.$kode_upd,0777,TRUE):'';

			$final_path = $path.$kode_tenant_id.$kode_upd;

			$config['upload_path'] = $final_path;
			$config['allowed_types'] = 'jpg|png';
			$config['encrypt_name'] = TRUE;

			$this->load->library('upload', $config);

			if($this->upload->do_upload('img_profile')){

				$file_img	= $this->upload->data('file_name');
				$orig_name	= $this->upload->data('orig_name');	

				$upload_file= $this->m_home->upload_img_profile($file_img);
			}

			$submit = $this->m_home->update_profile($first_name, $last_name, $email, $phone, $password, $upd, $lup, $tenant_id);

			echo json_encode($submit);

		}else{
			exit();
		}
	}

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
