<!doctype html>
<!--[if IE 9]> <html class="no-js ie9 fixed-layout" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js " lang="en"> <!--<![endif]-->
<head>
    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    
    <!-- Site Metas -->
    <title>ITSM</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <!-- Site Icons -->
    <link href="{base_url}assets/modules/core/common/img/{favicon}" rel="shortcut icon">
    
    <!-- Material Design fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700,900" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{base_url}assets/helpertheme/assets/css/bootstrap.css">
    <link rel="stylesheet" href="{base_url}assets/helpertheme/assets/css/bootstrap-material-design.css">
    <link rel="stylesheet" href="{base_url}assets/helpertheme/assets/css/ripples.min.css">
    <link rel="stylesheet" href="{base_url}assets/helpertheme/assets/css/font-awesome.min.css">
    
    <!-- Site CSS -->
    <link rel="stylesheet" href="{base_url}assets/helpertheme/style.css">
    <!-- Colors CSS -->
    <link rel="stylesheet" href="{base_url}assets/helpertheme/assets/css/colors.css">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body class="">

    <!-- LOADER -->
    <div id="preloader">
        <img class="preloader" src="{base_url}helpertheme/assets/images/loader.gif" alt="">
    </div><!-- end loader -->
    <!-- END LOADER -->
    
    <div id="wrapper">
        <header class="header">
            <div class="container-fluid">
                <nav class="navbar navbar-default">
                    <div class="container">
                        <div class="navbar-collapse collapse navbar-responsive-collapse">
                            <ul class="nav navbar-nav">
                                <li class="active"><a href="{base_url}">Home</a></li>
                                <li class="dropdown hasmenu">
                                    <a href="#" data-target="#" class="dropdown-toggle" data-toggle="dropdown">Officer Login <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <?php
                                        foreach ($data_tenant as $key => $value) {
                                            # code...
                                            echo "<li><a href='{base_url}login/".$value['tenant_alias']."'>".$value['tenant_name']."</a></li>";
                                        }
                                        ?>
                                    </ul>
                                </li>
                                <li class="dropdown hasmenu">
                                    <a href="#" data-target="#" class="dropdown-toggle" data-toggle="dropdown">Create Ticket <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <?php
                                        foreach ($data_tenant as $key => $value) {
                                            # code...
                                            echo "<li><a href='{base_url}{page_content}/self_ticket/".$value['tenant_alias']."'>".$value['tenant_name']."</a></li>";
                                        }
                                        ?>
                                    </ul>
                                </li>
                            </ul>
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="#" data-toggle="modal" data-target="#LoginModal"><i class="material-icons">lock</i> Client Login</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div><!-- end container -->
        </header><!-- end header -->

        <section class="section welcome bg1">
            <div class="container">
                <div class="row">
                    <div class="content col-md-8">
                        <div class="welcome-text">
                            <h1>ITSM <strong>help-desk support</strong></h1>
                            <p>Welcome to the ITSM Portal! Search your update progress ticket with the search box above, or you can create a ticket and for more information you can contact us via chat.</p>
                        </div>
                    </div><!-- end col -->
                </div><!-- end container -->
            </div><!-- end container -->
        </section><!-- end section -->

        <section class="section lb">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        <div class="widget nopadding clearfix">
                            <div class="panel panel-primary nopadding">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Search For Ticket</h3>
                                </div>
                                <div class="panel-body">
                                    <form class="site-search">
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="focusedInput2">Check Your Progress Ticket?</label>
                                            <input class="form-control" id="focusedInput2" type="text">
                                        </div>
                                        <div class="form-group clearfix"> <!-- inline style is just to demo custom css to put checkbox below input above -->
                                            <div class="checkbox pull-left">
                                                <?php
                                                foreach ($data_tenant as $key => $value) {
                                                    # code...
                                                    //echo "<li><a href='{base_url}page_incident_cust/".$value['tenant_alias']."'>".$value['tenant_name']."</a></li>";
                                                    echo "<label>";
                                                    echo "<input type='radio' name='r_tenant' > &nbsp;".$value['tenant_name']."";
                                                    echo "</label>";
                                                }
                                                ?>
                                            </div>
                                            <div class="submit-button pull-right">
                                                <a class="btn btn-raised btn-info gr" href="#"><i class="material-icons">search</i> Search</a>
                                            </div>
                                        </div>
                                    </form><!-- end well -->
                                </div>
                            </div>
                        </div><!-- end widget -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end section -->

        <div class="stickyfooter">
            <div id="sitefooter" class="container">
                <div id="copyright" class="row">
                    <div class="col-md-6 col-sm-12 text-left">
                        <p>ITSM ® is a product from <a href="https://infomedia.co.id">Infomedia Nusantara</a>.</p>
                    </div>
                </div>
            </div>
        </div>

        <!-- CHAT -->
        <div class="chat-wrapper">
            <div class="panel panel-primary">
                <div class="panel-heading" id="chatcordion">
                    <a class="open-support btn btn-raised btn-info" data-toggle="collapse" data-parent="#chatcordion" href="#chatcordion1">
                        <i class="material-icons">chat</i>
                    </a>
                </div>
                <div class="panel-collapse collapse" id="chatcordion1">
                    <div class="panel-body">
                        <span class="chat-logo"><i class="material-icons">announcement</i> ITSM Support Chat</span>
                        <form class="sidebar-login" id="frm-1">
                            <input type="text" id="first-name" class="form-control" placeholder="Nama">
                            <input type="text" id="last-name" class="form-control" placeholder="Email">
                            <button type="button" id="chat-button" class="btn btn-raised btn-info gr">Start Chat</button>
                        </form> 
                        <div id="chat-container" style="height:370px"></div>
                    </div>
                    <!-- end panel-footer -->
                </div><!-- end panel-collapse -->
            </div><!-- end panel -->
        </div><!-- end chat-wrapper -->

        <!-- Modal -->
        <div id="LoginModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-sm">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Login Account</h4>
                    </div>
                    <div class="modal-body">
                        <div class="widget clearfix">
                            <div class="panel panel-primary">
                                <div class="panel-body">
                                    <form class="sidebar-login">
                                        <input type="text" class="form-control" placeholder="Username">
                                        <input type="text" class="form-control" placeholder="Password">
                                        <button type="button" class="btn btn-raised btn-info gr">Login</button>
                                    </form> 
                                </div>
                            </div>
                            <small>No account? <a href="login.html">Register</a></small>
                        </div><!-- end widget -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end wrapper -->
    <script src="https://apps.mypurecloud.jp/webchat/jsapi-v1.js" type="text/javascript"></script>
    <script src="{base_url}assets/helpertheme/assets/js/jquery.js"></script>
    <script src="{base_url}assets/helpertheme/assets/js/bootstrap.js"></script>
    <script src="{base_url}assets/helpertheme/assets/js/ripples.min.js"></script>
    <script src="{base_url}assets/helpertheme/assets/js/material.min.js"></script>
    <script src="{base_url}assets/helpertheme/assets/js/custom.js"></script>
<script type="text/javascript">

    $(document).ready(function initializeChat () {
        var chatConfig = {
            // Web chat application URL
            webchatAppUrl: 'https://apps.mypurecloud.jp/webchat',

            // Web chat service URL
            webchatServiceUrl: 'https://realtime.mypurecloud.jp:443',

            // Numeric organization ID
            orgId: 3285,

            // Organization name. Replace with your org name.
            orgName: 'Infomedia',

            // Requested agent language skill (Agent must have this language skill to receive chat)
            //language: 'English - Written',

            // Requested agent skills (Agent must have these skills to receive chat)
            //skills: ['Computers', 'Printers'],
            // OR
            //skills: [],

            // Priority
            priority: 0,

            // Queue Name
            queueName : 'Infomedia Nusantara',

            // Target agent email (OPTIONAL)
            //agentEmail: 'alex.agent@example.com',

            // Whether to show submit button or send message on Enter keypress
            showSubmitButton: true,

            // Log level
            logLevel: 'DEBUG',

            // Locale code
            locale: 'en',

            // Whether to allow reconnects
            reconnectEnabled: true,

            //Allowed reconnect origins
            //reconnectOrigins: ['https://example.com', 'https://help.example.com', 'https://shop.example.com'],

            // Logo used within the chat window
            companyLogoSmall: {
                width: 149,
                height: 149,
                url: 'https://www.infomedia.co.id/theme/infomedia-2016/assets/img/favicon.png'
                
            },
            // Fallback image used for agent if no agent image is defined in the agent's PureCloud profile
            agentAvatar: {
                width: 462,
                height: 462,
                url: 'https://dhqbrvplips7x.cloudfront.net/webchat/1.0.23/agent-e202505f.png'
            },

            // Text displayed with chat window is displayed
            welcomeMessage: 'Thanks for chatting.',

            // CSS class applied to the chat window
            cssClass: 'chat',

            // Custom style applied to the chat window
            css: {
                width: '100%',
                height: '100%'
            }
        };
        var chatButton = document.getElementById('chat-button');

        // Required if reconnects are enabled
        window.PURECLOUD_WEBCHAT_FRAME_CONFIG = {
            containerEl: 'chat-container'
        };

        ININ.webchat.create(chatConfig)
            .then(function (webchat) {
                // Optionally use isAutoJoined if reconnects are enabled
                if (webchat.isAutoJoined()) {
                    // Do something to disable chat button
                    $("#frm-1").hide();
                }
                chatButton.onclick = function () {
                    var firstName = document.getElementById('first-name').value;
                    var lastName = document.getElementById('last-name').value;
                    //var agentEmail = document.getElementById('agent-email').value;
                    if(firstName!=''&& lastName){
                        if (validateEmail(lastName)) {
                          // Use getConfig.setConfigProperty() for any web chat configuration property to dynamically set config values.
                            webchat.getConfig().setData({
                                firstName: firstName,
                                lastName: lastName,
                                addressStreet: '64472 Brown Street',
                                addressCity: 'Lindgrenmouth',
                                addressPostalCode: '50163-2735',
                                addressState: 'FL',
                                phoneNumber: '1-916-892-2045 x293',
                                phoneType: 'Cell',
                                customerId: 59606
                            });
                            //webchat.getConfig().setAgentEmail(agentEmail);

                            // Alternatively, call webchat.renderPopup here. Note that reconnects do not apply to popup chat.
                            webchat.renderFrame({
                                containerEl: 'chat-container'
                            });
                            $("#frm-1").hide();  
                           /* setTimeout(function() {
                                if( typeof $("#wc0-w2-input").html() =='undefined' ){
                                    alert('undefined');
                                }
                                $("#wc0-w2-input").attr('style', 'width:10px');
                                alert(123);
                            }, 5000);*/
                            
                        }else{
                            alert('Your Email Address Not Valid !!!');
                        }
                        
                    }else{
                        alert('Please Input Your Name & Email');
                    }
                    
                };
            })
            .catch(function (err){
                console.log(err);
            });

    });
    
    function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
</script>
</body>

</html>