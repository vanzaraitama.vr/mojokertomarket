<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_admin_payment_approval extends CI_Model {

	public function list_data($length,$start,$search,$order,$dir){
		
		$order_by="ORDER BY a.created_at";
		if($order!=0){
			$order_by="ORDER BY $order $dir";
		}
		
		$where_clause="";
		 if($search!=""){
			$where_clause=" AND (a.invoice like '%$search%' OR a.`status` like '%$search%' OR b.name like '%$search%'

			OR a.grandtotal like '%$search%' )";
		}

		$sql = " SELECT a.invoice,a.`status`,b.name AS nama_toko,FORMAT(a.grandtotal,2,'id_ID') AS grandtotal,a.id
				 FROM transactions a
				 INNER JOIN shops b ON a.shop_id=b.id
				 WHERE a.status='pending_admin_approval'
				$where_clause
				$order_by";				
						
		$query		= $this->db->query($sql . " LIMIT $start, $length");

		$numrows	= $this->db->query($sql);
		$total		= $numrows->num_rows();
		
		return array("data"=>$query->result_array(),
						"total_data"=>$total
				);
	}

	function detail_data($id){
		$this->db->query("SET sql_mode=''");
		$this->db->select("a.id,
						  a.invoice,
						  a.sub_invoice,
						  a.`status`,
						  a.payment_method,
						  a.shop_id,
						  a.shop_name,
						  a.shop_phone_number,
						  a.shop_address,
						  a.shop_owner_name,
						  a.user_id,
						  a.user_name,
						  a.user_email,
						  a.phone_number,
						  a.address,
						  a.address_name,
						  a.consigne,
						  a.payment_date,
						  a.complete_date,
						  a.coupon,
						  FORMAT(a.subtotal,2,'id_ID') AS subtotal,
						  FORMAT(a.discount,2,'id_ID') AS discount,
						  a.shipping_name,
						  FORMAT(a.shipping_cost,2,'id_ID') AS shipping_cost,
						  a.distance,
						  FORMAT(a.grandtotal,2,'id_ID') AS grandtotal,
						  a.created_at,
						  a.updated_at");
		$query = $this->db->get_where('transactions a', array('a.id' => $id));

		$data = $query->result_array();
		
		return $data;

	}

	function edit($act,$id,$status,$upd,$lup){
		
		$data = array('status' => $status,
					  'updated_at' => $lup
					);

		$this->db->where('id', $id );

		$update	= $this->db->update('transactions', $data); 

		if($update){
		
			$result = array('status'=> 'success', 'reason'=> 'Data has been changed');
			
		}else{
			$result = array('status'=> 'fail', 'reason'=> 'Failed Update Data');
		}

		return $result;
	}
}

/* End of file M_admin_users.php */
/* Location: ./application/models/M_admin_users.php */
