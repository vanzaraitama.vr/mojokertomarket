<nav class="cat__core__top-sidebar cat__core__top-sidebar--bg">
    <span class="cat__core__title d-block mb-2">
        <span class="text-muted">Transaction ·</span>
        <span class="text-muted">Detail ·</span>
        <strong>{invoice}</strong>
    </span>
</nav>
<!-- START: ecommerce/dashboard -->
<section class="card">
    <div class="card-header">
        <span class="cat__core__title">
            <strong>Transaction {invoice}</strong>
        </span>
    </div>
    <div class="card-block">
        <div class="row">
            <div class="col-lg-12">
                <div class="mb-5">   
            <!-- Horizontal Form -->
                    <form id="form-1" name="form-1" method="POST">
                        <input type="hidden" name="act" id="act" value="{act}">
                        <input type="hidden" name="id" id="id" value="{id}">
                        <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none"> 
            
                       <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="userid">Invoice</label>
                            <div class="col-md-9">
                                <input id="name" disabled="true" 
                                       class="form-control"
                                       name="invoice"
                                       type="text"
                                       value="{invoice}" 
                                       data-validation="[NOTEMPTY]">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="first_name">Sub Invoice</label>
                            <div class="col-md-9">
                                <input id="sub_invoice" disabled="true"
                                       class="form-control"
                                       name="sub_invoice"
                                       type="text"
                                       value="{sub_invoice}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="first_name">Status </label>
                            <div class="col-md-9">
                                <input id="status" disabled="true"
                                       class="form-control"
                                       name="status"
                                       type="text"
                                       value="{status}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="first_name">Nama Toko </label>
                            <div class="col-md-9">
                                <input id="shop_name" disabled="true"
                                       class="form-control"
                                       name="shop_name"
                                       type="text"
                                       value="{shop_name}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="first_name">Tlp. Toko </label>
                            <div class="col-md-9">
                                <input id="shop_phone_number" disabled="true"
                                       class="form-control"
                                       name="shop_phone_number"
                                       type="text"
                                       value="{shop_phone_number}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="first_name">Alamat Toko </label>
                            <div class="col-md-9">
                                <textarea disabled="true" class="form-control" name="shop_address">{shop_address}</textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="first_name">Pemilik Toko </label>
                            <div class="col-md-9">
                                <input id="shop_owner_name" disabled="true"
                                       class="form-control"
                                       name="shop_owner_name"
                                       type="text"
                                       value="{shop_owner_name}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="first_name">Nama Pembeli</label>
                            <div class="col-md-9">
                                <input id="user_name" disabled="true"
                                       class="form-control"
                                       name="user_name"
                                       type="text"
                                       value="{user_name}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="first_name">Tlp. Pembeli</label>
                            <div class="col-md-9">
                                <input id="phone_number" disabled="true"
                                       class="form-control"
                                       name="phone_number"
                                       type="text"
                                       value="{phone_number}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="first_name">Alamat Pembeli</label>
                            <div class="col-md-9">
                              <textarea disabled="true" class="form-control" name="shop_address">{address}</textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="first_name">Waktu Pembayaran</label>
                            <div class="col-md-9">
                                <input id="payment_date" disabled="true"
                                       class="form-control"
                                       name="payment_date"
                                       type="text"
                                       value="{payment_date}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="first_name">Sub Total</label>
                            <div class="col-md-9">
                                <input id="payment_date" disabled="true"
                                       class="form-control"
                                       name="payment_date"
                                       type="text"
                                       value="{payment_date}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="first_name">Kupon</label>
                            <div class="col-md-9">
                                <input id="coupon" disabled="true"
                                       class="form-control"
                                       name="coupon"
                                       type="text"
                                       value="{coupon}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="first_name">Diskon</label>
                            <div class="col-md-9">
                                <input id="discount" disabled="true"
                                       class="form-control"
                                       name="discount"
                                       type="text"
                                       value="{discount}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="first_name">Biaya Kirim</label>
                            <div class="col-md-9">
                                <input id="shipping_cost" disabled="true"
                                       class="form-control"
                                       name="shipping_cost"
                                       type="text"
                                       value="{shipping_cost}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="first_name">Total Keseluruhan</label>
                            <div class="col-md-9">
                                <input id="grandtotal" disabled="true"
                                       class="form-control"
                                       name="grandtotal"
                                       type="text"
                                       value="{grandtotal}">
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 offset-md-3">
                                    <button type="submit" class="btn btn-primary pull-right pointer">Sudah di Bayar</button>
                                    <button type="button" class="btn btn-default pull-left pointer" onclick="goBack()" >Back</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- End Horizontal Form -->
                </div>
            </div>        
        </div>
    </div>
</section>
<!-- END: ecommerce/dashboard -->
<!-- START: page scripts -->
<script>
   jQuery(document).ready(function($) {
        $('.select2').select2();

        if($("#userlevel").val()!='admin'){
            $(".sh_position").show();
            $("#position").removeAttr('disabled');
        }

        $("#userlevel").change(function(event) {
            /* Act on the event */
            if($("#userlevel").val()=='admin'){
                $(".sh_position").hide();
                $("#position").attr('disabled', 'true');
            }else{    
                $(".sh_position").show();
                $("#position").removeAttr('disabled');
            }
        });

        $("#password").removeAttr('data-validation');
        $("#password").removeAttr('data-validation-message');
        $("#password_confirm").removeAttr('data-validation');
        $("#password_confirm").removeAttr('data-validation-message');

        $("#password").change(function(event) {
            /* Act on the event */
            if($("#password").val()!=''){
                $("#password").attr('data-validation', '[L>=6]');
                $("#password").attr('data-validation-message', '$ must be at least 6 characters');
                $("#password_confirm").attr('data-validation', '[V==password]');
                $("#password_confirm").attr('data-validation-message', 'Re-type Password does not match the password');
            }else{
                $("#password").removeAttr('data-validation');
                $("#password").removeAttr('data-validation-message'); 
                $("#password_confirm").removeAttr('data-validation');
                $("#password_confirm").removeAttr('data-validation-message'); 
            }
        });
   });

   function goBack() {
        window.history.back();
    }

    $(function(){

        
        $('#form-1').validate({
            submit: {
                settings: {
                    inputContainer: '.form-group',
                    errorListClass: 'form-control-error',
                    errorClass: 'has-danger'
                },
                callback: {
                    onBeforeSubmit: function (node) {
                        NProgress.start();    
                    },
                    onSubmit: function (node) {

                        $.ajax({
                            url: '{base_url}{page}/forms_submit',
                            type: 'POST',
                            dataType: 'json',
                            data: $('#form-1').serialize(),
                        })
                        .done(function(data) {
                            NProgress.done();
                            if(data.status==true){
                                $.notify(data.reason);
                                document.location.href="{base_url}main#{page}";
                            }else{
                                $.notify(data.reason);
                            }          
                        })
                        .fail(function() {
                            NProgress.done();
                            $.notify("Fail Save Data, Please check your connections...");
                        });
                        
                    },
                    onError: function (error) {
                        $.notify("Fail, Please Check your input...");
                    }

                }
            },
            debug: true

        });

    });
</script>
