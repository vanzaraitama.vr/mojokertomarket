<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link rel="icon" href="{base_url}assets/modules/core/common/img/{favicon}" type="image/x-icon" />
    <link rel="shortcut icon" href="{base_url}assets/modules/core/common/img/{favicon}" type="image/x-icon" />
    <title>{title}</title>
    <link href="{base_url}assets/vendors/font-google/font-google.css?family=PT+Sans:400,400i,700,700i" rel="stylesheet">

    <!-- VENDORS -->
    <!-- v2.0.0 -->
    <link rel="stylesheet" type="text/css" href="{base_url}assets/vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/vendors/jscrollpane/style/jquery.jscrollpane.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/vendors/ladda/dist/ladda-themeless.min.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/vendors/bootstrap-select/dist/css/bootstrap-select.min.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/vendors/select2/dist/css/select2.min.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/vendors/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/vendors/fullcalendar/dist/fullcalendar.min.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/vendors/bootstrap-sweetalert/dist/sweetalert.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/vendors/summernote/dist/summernote.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/vendors/owl.carousel/dist/assets/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/vendors/ionrangeslider/css/ion.rangeSlider.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/vendors/datatables/media/css/dataTables.bootstrap4.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/vendors/c3/c3.min.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/vendors/chartist/dist/chartist.min.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/vendors/nprogress/nprogress.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/vendors/jquery-steps/demo/css/jquery.steps.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/vendors/dropify/dist/css/dropify.min.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/vendors/font-linearicons/style.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/vendors/font-icomoon/style.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/vendors/cleanhtmlaudioplayer/src/player.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/vendors/cleanhtmlvideoplayer/src/player.css">
    <script src="{base_url}assets/vendors/jquery/dist/jquery.min.js"></script>
    <script src="{base_url}assets/vendors/tether/dist/js/tether.min.js"></script>
    <script src="{base_url}assets/vendors/jquery-ui/jquery-ui.min.js"></script>
    <script src="{base_url}assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="{base_url}assets/vendors/jquery-mousewheel/jquery.mousewheel.min.js"></script>
    <script src="{base_url}assets/vendors/jscrollpane/script/jquery.jscrollpane.min.js"></script>
    <script src="{base_url}assets/vendors/spin.js/spin.js"></script>
    <script src="{base_url}assets/vendors/ladda/dist/ladda.min.js"></script>
    <script src="{base_url}assets/vendors/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
    <script src="{base_url}assets/vendors/select2/dist/js/select2.full.min.js"></script>
    <script src="{base_url}assets/vendors/html5-form-validation/dist/jquery.validation.min.js"></script>
    <script src="{base_url}assets/vendors/jquery-typeahead/dist/jquery.typeahead.min.js"></script>
    <script src="{base_url}assets/vendors/jquery-mask-plugin/dist/jquery.mask.min.js"></script>
    <script src="{base_url}assets/vendors/autosize/dist/autosize.min.js"></script>
    <script src="{base_url}assets/vendors/bootstrap-show-password/bootstrap-show-password.min.js"></script>
    <script src="{base_url}assets/vendors/moment/min/moment.min.js"></script>
    <script src="{base_url}assets/vendors/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script src="{base_url}assets/vendors/fullcalendar/dist/fullcalendar.min.js"></script>
    <script src="{base_url}assets/vendors/bootstrap-sweetalert/dist/sweetalert.min.js"></script>
    <script src="{base_url}assets/vendors/remarkable-bootstrap-notify/dist/bootstrap-notify.min.js"></script>
    <script src="{base_url}assets/vendors/summernote/dist/summernote.min.js"></script>
    <script src="{base_url}assets/vendors/owl.carousel/dist/owl.carousel.min.js"></script>
    <script src="{base_url}assets/vendors/ionrangeslider/js/ion.rangeSlider.min.js"></script>
    <script src="{base_url}assets/vendors/nestable/jquery.nestable.js"></script>
    <script src="{base_url}assets/vendors/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="{base_url}assets/vendors/datatables/media/js/dataTables.bootstrap4.js"></script>
    <script src="{base_url}assets/vendors/datatables-fixedcolumns/js/dataTables.fixedColumns.js"></script>
    <script src="{base_url}assets/vendors/datatables-responsive/js/dataTables.responsive.js"></script>
    <script src="{base_url}assets/vendors/editable-table/mindmup-editabletable.js"></script>
    <script src="{base_url}assets/vendors/d3/d3.min.js"></script>
    <script src="{base_url}assets/vendors/c3/c3.min.js"></script>
    <script src="{base_url}assets/vendors/chartist/dist/chartist.min.js"></script>
    <script src="{base_url}assets/vendors/peity/jquery.peity.min.js"></script>
    <script src="{base_url}assets/vendors/chartist-plugin-tooltip/dist/chartist-plugin-tooltip.min.js"></script>
    <script src="{base_url}assets/vendors/jquery-countTo/jquery.countTo.js"></script>
    <script src="{base_url}assets/vendors/nprogress/nprogress.js"></script>
    <script src="{base_url}assets/vendors/jquery-steps/build/jquery.steps.min.js"></script>
    <script src="{base_url}assets/vendors/chart.js/dist/Chart.bundle.min.js"></script>
    <script src="{base_url}assets/vendors/dropify/dist/js/dropify.min.js"></script>
    <script src="{base_url}assets/vendors/cleanhtmlaudioplayer/src/jquery.cleanaudioplayer.js"></script>
    <script src="{base_url}assets/vendors/cleanhtmlvideoplayer/src/jquery.cleanvideoplayer.js"></script>
    <script src="{base_url}assets/vendors/hashchange/jquery.hashchange.js"></script>


    <!-- CLEAN UI ADMIN TEMPLATE MODULES-->
    <!-- v2.0.0 -->
    <link rel="stylesheet" type="text/css" href="{base_url}assets/modules/core/common/core.cleanui.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/modules/vendors/common/vendors.cleanui.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/modules/layouts/common/layouts-pack.cleanui.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/modules/themes/common/themes.cleanui.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/modules/menu-left/common/menu-left.cleanui.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/modules/menu-right/common/menu-right.cleanui.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/modules/top-bar/common/top-bar.cleanui.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/modules/footer/common/footer.cleanui.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/modules/pages/common/pages.cleanui.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/modules/ecommerce/common/ecommerce.cleanui.css">
    <link rel="stylesheet" type="text/css" href="{base_url}assets/modules/apps/common/apps.cleanui.css">
    <script src="{base_url}assets/modules/menu-left/common/menu-left.cleanui.js"></script>
    <script src="{base_url}assets/modules/menu-right/common/menu-right.cleanui.js"></script>

    <!-- <script src="{base_url}assets/js/highcharts.js"></script>
    <script src="{base_url}assets/js/highcharts-3d.js"></script> -->
</head>
<style type="text/css">
    .pointer {
        cursor: pointer;
    }
</style>
<body class="cat__config--vertical cat__menu-left--colorful cat__menu-left--visible">
    <nav class="cat__menu-left">
        <div class="cat__menu-left__lock cat__menu-left__action--menu-toggle">
            <div class="cat__menu-left__pin-button">
                <div><!-- --></div>
            </div>
        </div>
        <div class="cat__menu-left__logo" align="center">
            <a href="{base_url}main#home">
                <img src="{base_url}assets/modules/dummy-assets/common/img/{logo_full}" />
            </a>
        </div>
        {menu}
    </nav>
<div class="cat__top-bar">
    <!-- left aligned items -->
    <div class="cat__top-bar__left">
        <div class="cat__top-bar__logo">
            <a href="{base_url}main#home">
                <img src="{base_url}assets/modules/dummy-assets/common/img/{logo}" />
            </a>
        </div>        
    </div>
    <!-- right aligned items -->
    <div class="cat__top-bar__right">
        <div class="cat__top-bar__item">
            <div class="dropdown cat__top-bar__avatar-dropdown">
                <a href="javascript: void(0);" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <span class="cat__top-bar__avatar" href="javascript:void(0);">
                        <?php
                        if($this->session->userdata('foto')!=''){
                        echo '<img src="'.base_url().'assets/img/profile/'.$this->session->userdata('tenant_id').'/'.$this->session->userdata('userid').'/'.$this->session->userdata('foto').'" />'; 
                        }else{
                        echo '<img src="'.base_url().'assets/modules/dummy-assets/common/img/avatars/1.jpg" />';
                        }
                        ?>
                    </span>
                </a>
                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="" role="menu">
                    <a class="dropdown-item" href="main#home/profile"><i class="dropdown-icon icmn-user"></i> {first_name} {last_name} </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{base_url}home/logout"><i class="dropdown-icon icmn-exit"></i> Logout</a>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="cat__content">
    <!--{page_content}-->
    <!-- begin #ajax-content -->
        <div id="ajax-content" style="min-height: 95%; height: auto;"></div>
    <!-- end #ajax-content -->
</div>
<!-- END: page scripts -->
<div class="cat__footer">
    <div class="cat__footer__bottom">
        <div class="row">
            <div class="col-md-12">
                <div class="cat__footer__company">
                    <img class="cat__footer__company-logo" src="{base_url}assets/modules/dummy-assets/common/img/{logo}" target="_blank" title="{title}">
                    <span>
                        © {copyright} <a href="https://infomedia.co.id" target="_blank">{footer}</a>
                        <br>
                        All rights reserved
                    </span>
                </div>
            </div>
        </div>
    </div>
    <a href="javascript: void(0);" class="cat__core__scroll-top" onclick="$('body, html').animate({'scrollTop': 0}, 500);"><i class="icmn-arrow-up"></i></a>
</div>
</div>
<script src="{base_url}assets/apps.js"></script>
    <script>
        $(document).ready(function() {
            var csfrData = {};
            csfrData['<?php echo $this->security->get_csrf_token_name(); ?>'] = '<?php echo $this->security->get_csrf_hash(); ?>';
            $.ajaxSetup({
            data: csfrData
            });
            App.init();

            var timerId_NewTicket = setInterval(function(){
               //check_ticket();
            }, 1000*60*3); //1000ms. = 1sec.

            var timerId_OverSla = setInterval(function(){
               //check_over_sla();
            }, 1000*60*60); //1000ms. = 1sec.

        });


        function check_ticket(){

            $.ajax({
                url: '{base_url}home/check_ticket',
                type: 'POST',
                dataType: 'json'
            })
            .done(function(data) {
                if(data.status==true){
                    var audio = new Audio("assets/Alarm.mp3");

                    $.notify({
                        // options
                        message: data.reason
                    },{
                        // settings
                        type: 'warning',
                        onClose: audio.pause(),
                        onClosed: audio.pause(),
                        onShow: audio.play(),
                        onShown: audio.play(),
                        
                    });
                }   
            })
            .fail(function() {
                $.notify(data.reason);
            });
            
        }


        function check_over_sla(){

            $.ajax({
                url: '{base_url}home/check_over_sla',
                type: 'POST',
                dataType: 'json'
            })
            .done(function(data) {
                if(data.status==true){
                    var audio = new Audio("assets/Alarm.mp3");

                    $.notify({
                        // options
                        message: data.reason
                    },{
                        // settings
                        type: 'danger',
                        onClose: audio.pause(),
                        onClosed: audio.pause(),
                        onShow: audio.play(),
                        onShown: audio.play(),
                        
                    });
                }   
            })
            .fail(function() {
                $.notify(data.reason);
            });
        }

    </script>    
</body>
</html>