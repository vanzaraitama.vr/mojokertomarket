<nav class="cat__core__top-sidebar cat__core__top-sidebar--bg">
    <span class="cat__core__title d-block mb-2">
        <span class="text-muted">Administration ·</span>
        <span class="text-muted">Store ·</span>
        <strong>{act}</strong>
    </span>
</nav>
<style type="text/css">
    .pointer {
        cursor: pointer;
    }
</style>
<!-- START: ecommerce/dashboard -->
<section class="card">
    <div class="card-header">
        <span class="cat__core__title">
            <strong>{act} Store </strong>
        </span>
    </div>
    <div class="card-block">
        <div class="row">
            <div class="col-lg-12">
                <div class="mb-5">   
            <!-- Horizontal Form -->
                    <form id="form-1" name="form-1" method="POST">
                        <input type="hidden" name="act" id="act" value="{act}">
                        <input type="hidden" name="id" id="id" value="{id}">
                        <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none"> 
                       
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="user_id">User Name (Pemilik Store)</label>
                            <div class="col-md-9">
                                <select class="form-control select2"
                                            id="user_id" 
                                            name="user_id">
                                        <option value="">Choose</option>    
                                        {list_user}
                                </select>
                            </div>
                        </div>

                       <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="store_name">Name</label>
                            <div class="col-md-9">
                                <input id="store_name"
                                       name="store_name"
                                       value="{store_name}" 
                                       class="form-control"
                                       placeholder="Store Name"
                                       type="text"
                                       data-validation="[NOTEMPTY]">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="address">Address</label>
                            <div class="col-md-9">
                                <textarea class="form-control" rows="3"
                                   data-validation="[NOTEMPTY, L>=6]"
                                   data-validation-message="$ cannot empty, must be more than 6 characters. No special characters allowed."
                                   id="address" 
                                   name="address">{address}</textarea> 
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="phone_number">Phone Number</label>
                            <div class="col-md-9">
                                <input id="phone_number"
                                       name="phone_number"
                                       value="{phone_number}" 
                                       class="form-control"
                                       placeholder="Phone Number"
                                       type="text"
                                       data-validation="[NOTEMPTY]">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="description">Descriptions</label>
                            <div class="col-md-9">
                                <textarea class="form-control" rows="3"
                                   data-validation="[NOTEMPTY, L>=6]"
                                   data-validation-message="$ cannot empty, must be more than 6 characters. No special characters allowed."
                                   id="description" 
                                   name="description">{description}</textarea> 
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="form-group row">
                                <div class="col-md-9 offset-md-3">
                                    <button type="submit" class="btn btn-primary pull-right pointer">Submit</button>
                                    <button type="button" class="btn btn-default pull-left pointer" onclick="goBack()" >Back</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- End Horizontal Form -->
                </div>
            </div>        
        </div>
    </div>
</section>
<!-- END: ecommerce/dashboard -->
<!-- START: page scripts -->
<script>
   jQuery(document).ready(function($) {
        $('.select2').select2();
   });

   function goBack() {
        window.history.back();
    }

    $(function(){

        
        $('#form-1').validate({
            submit: {
                settings: {
                    inputContainer: '.form-group',
                    errorListClass: 'form-control-error',
                    errorClass: 'has-danger'
                },
                callback: {
                    onBeforeSubmit: function (node) {
                        NProgress.start();    
                    },
                    onSubmit: function (node) {
                        var form = document.getElementById('form-1');
                        var formData = new FormData(form);

                        $.ajax({
                            url: '{base_url}{page}/forms_submit',
                            enctype: 'multipart/form-data',
                            data: formData,
                            processData: false,
                            contentType: false,
                            type: 'POST',
                            dataType: 'json',
                        })
                        .done(function(data) {
                            NProgress.done();
                            if(data.status==true){
                                $.notify(data.reason);
                                document.location.href="{base_url}main#{page}";
                            }else{
                                $.notify(data.reason);
                            }          
                        })
                        .fail(function() {
                            NProgress.done();
                            $.notify("Fail Save Data, Please check your connections...");
                        });
                        
                    },
                    onError: function (error) {
                        $.notify("Fail, Please Check your input...");
                    }

                }
            },
            debug: true

        });

    });
</script>
