<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_admin_store extends CI_Model {

	public function list_data($length,$start,$search,$order,$dir){
		
		$order_by="ORDER BY a.name";
		if($order!=0){
			$order_by="ORDER BY $order $dir";
		}
		
		$where_clause="";
		 if($search!=""){
			$where_clause=" AND (a.name like '%$search%' OR b.name like '%$search%' OR a.phone_number like '%$search%' )";
		}

		$sql		= " SELECT a.name, a.phone_number, b.name AS nama_user, a.id
						FROM shops a 
						INNER JOIN users b ON a.user_id=b.id
						$where_clause
						$order_by";
						
		$query		= $this->db->query($sql . " LIMIT $start, $length");

		$numrows	= $this->db->query($sql);
		$total		= $numrows->num_rows();
		
		return array("data"=>$query->result_array(),
						"total_data"=>$total
				);
	}

	function detail_data($id){
		$this->db->query("SET sql_mode=''"); 
		$this->db->select('id, user_id, name, address, phone_number, description');
		$query = $this->db->get_where('shops', array('id' => $id));

		$data = $query->result_array();
		
		return $data;

	}

	function deleted_item($id){

		$this->db->where('id', $id);

		$delete = $this->db->delete('shops');

		if($delete){
			$result = array('status'=> 'success', 'reason'=> 'Data has been deleted');
		}else{
			$result = array('status'=> 'fail', 'reason'=> 'Failed Deleted Data');
		}

		return $result;

	}


	function add($id,$user_id,$name,$address,$phone_number,$description,$lup){
	
		$data = array(
		   'user_id' => $user_id,	
		   'name' => $name,
		   'address' => $address,
		   'phone_number' => $phone_number, 
		   'description' => $description,
		   'image' => 'image.jpg',
		   'banners' => '["banner1.jpg","banner2.jpg","banner3.jpg"]',
		   'created_at' => $lup
		);

		$insert = $this->db->insert('shops', $data); 

		if($insert){
			$result = array('status'=> 'success', 'reason'=> 'Data has been Inserted');
		}else{
			$result = array('status'=> 'fail', 'reason'=> 'Failed Update Data');
		}

		return $result;

	}


	function edit($act,$id,$user_id,$name,$address,$phone_number,$description,$lup){
		
		$data = array('user_id' => $user_id,	
					   'name' => $name,
					   'address' => $address,
					   'phone_number' => $phone_number, 
					   'description' => $description,
					  'updated_at' => $lup
					);

		$this->db->where('id', $id );

		$update	= $this->db->update('shops', $data); 

		if($update){
			
			$result = array('status'=> 'success', 'reason'=> 'Data has been changed');
					
		}else{
			$result = array('status'=> 'fail', 'reason'=> 'Failed Update Data');
		}

		return $result;
	}
}

/* End of file M_admin_users.php */
/* Location: ./application/models/M_admin_users.php */
