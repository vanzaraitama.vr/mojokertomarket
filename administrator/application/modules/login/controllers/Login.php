<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index(){
		
		redirect('error_page');
		
	}

	public function tenant($page){

		$this->load->model('q_login');

		$tenant = $this->q_login->check_tenant($page);
		
		if($tenant['status']){
			
			$data = array( "base_url" => base_url() , "tenant_id" => $tenant['tenant_id'], "tenant_id_alias" => $page , "favicon" => $tenant['favicon'], "title" => $tenant['title'] );
			$this->parser->parse('login', $data);
		
		}else{
			redirect('error_page');
		}

	}


	public function check_login(){

		$userid		= $this->input->post('validation[email]');
		$password	= $this->input->post('validation[password]');
		$tenant_id	= $this->input->post('tenant_id');
		$tenant_id_alias	= $this->input->post('tenant_id_alias');


		$this->load->model('q_login');

		$check_login = $this->q_login->check_login($userid,$password,$tenant_id);
		
		if(is_array($check_login)){				

				if($check_login['status']==TRUE){
						$sess	= array(
									'userid'				=> $check_login['userid'],
									'first_name'			=> $check_login['first_name'],
									'last_name'				=> $check_login['last_name'],
									'group_level'			=> $check_login['group_level'],
									'group_member'			=> $check_login['group_member'],
									'userlevel' 			=> $check_login['userlevel'],
									'position' 				=> $check_login['position'],
									'email' 				=> $check_login['email'],
									'phone' 				=> $check_login['phone'],
									'foto'					=> $check_login['foto'],
									'tenant_id'				=> $check_login['tenant_id'],
									'tenant_id_alias'		=> $tenant_id_alias,
									'is_logged_in'			=> true
									);

						$this->session->set_userdata($sess);
						
						$result = array("status"=>"success","title"=>"Login Success","reason"=>"Please Wait");
				}else{

					$result = array("status"=>"fail","title"=>"Login Fail","reason"=>"mohon cek kembali userid & password anda, atau ulangi beberapa saat lagi");		
				
				}	

		}else{
			
			$result = array("status"=>"error","title"=>"Login Fail","reason"=>"error");
		
		}

		echo json_encode($result);
	}

	public function check_login_api(){

		$this->load->library('encryption');

		$tenant_id_alias = $this->uri->segment(3);
		$tenant_id 		 = $this->encryption->decrypt(base64_decode($this->uri->segment(4)));
		$userid 		 = $this->encryption->decrypt(base64_decode($this->uri->segment(5)));
		$ticket_id 		 = $this->uri->segment(6);
		$ticket_status 	 = $this->uri->segment(7);


		$this->load->model('q_login');

		$check_login = $this->q_login->check_login_api($userid,$tenant_id);
		
		if(is_array($check_login)){				

				if($check_login['status']==TRUE){
						$sess	= array(
									'userid'				=> $check_login['userid'],
									'first_name'			=> $check_login['first_name'],
									'last_name'				=> $check_login['last_name'],
									'group_level'			=> $check_login['group_level'],
									'group_member'			=> $check_login['group_member'],
									'userlevel' 			=> $check_login['userlevel'],
									'position' 				=> $check_login['position'],
									'email' 				=> $check_login['email'],
									'phone' 				=> $check_login['phone'],
									'foto'					=> $check_login['foto'],
									'tenant_id'				=> $check_login['tenant_id'],
									'tenant_id_alias'		=> $tenant_id_alias,
									'is_logged_in'			=> true
									);

						$this->session->set_userdata($sess);
						
						$page = $this->q_login->find_page($ticket_status);
						$url  = "main#".$page."/view_detail/".base64_encode($this->encryption->encrypt($ticket_id)); 
						redirect($url,'refresh');	
				}else{

					redirect('login','refresh');	
				
				}	

		}else{
			redirect('login','refresh');
		}
		//echo json_encode($result);
	}

	public function check_login_genesys(){

		$tenant_id_alias= $this->uri->segment(3);
		$email 			= $this->input->get('email');
		$tenant_id 		= $this->input->get('tenant_id');

		$url_key		= $this->input->get('key');
		$data			= $this->input->get('data');

		$userid 		= $this->session->userdata('userid');
		
		$this->load->library('encryption');

		if(isset($data)){
			$data_ = json_decode($data,true);

			if(is_array($data_)){
				foreach ($data_ as $key => $value) {
					# code...
					if($key=='email'){
						$email = $value;
					}
					if($key=='ani'){
						$ani = $value;
						$ani = substr($ani, 4);
					}
					if($key=='tenant'){
						$tenant_id= $value;
					}
					if($key=='interaction_type'){
						$interaction_type= $value;
					}
					if($key=='from_email'){
						$cust_email= $value;
					}

					if($key=='interaction_id'){
						$interaction_id= $value;
					}
				}
			}  

			if(is_array($data_)){
				if($interaction_type!="Email" && $interaction_type!="Call"){
						foreach ($data_ as $key => $value) {
						if($key=='chat_lastname'){
							$cust_email= $value;
							$ani = '';
						}
					}
				}
			}
		}

		$this->load->model('q_login');
		if(isset($userid)){
			$url  = base_url()."page_incident/v2?ani=$ani&email=$cust_email&interaction_type=$interaction_type&interaction_id=$interaction_id"; 
			redirect($url,'refresh');	
		}else{
			$check_login = $this->q_login->check_login_api($email,$tenant_id);

			if(is_array($check_login)){				

					if($check_login['status']==TRUE){
							$sess	= array(
										'userid'				=> $check_login['userid'],
										'first_name'			=> $check_login['first_name'],
										'last_name'				=> $check_login['last_name'],
										'group_level'			=> $check_login['group_level'],
										'group_member'			=> $check_login['group_member'],
										'userlevel' 			=> $check_login['userlevel'],
										'position' 				=> $check_login['position'],
										'email' 				=> $check_login['email'],
										'phone' 				=> $check_login['phone'],
										'foto'					=> $check_login['foto'],
										'tenant_id'				=> $check_login['tenant_id'],
										'tenant_id_alias'		=> $tenant_id_alias,
										'is_logged_in'			=> true
										);

							$this->session->set_userdata($sess);
							
							$url  = base_url()."page_incident/v2?".base64_encode($this->encryption->encrypt($ani)); 
							redirect($url,'refresh');	
					}else{

						redirect('login','refresh');				
					}	

			}else{
				redirect('login','refresh');
			}
		}
	}
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */
