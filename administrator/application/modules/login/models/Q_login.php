<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Q_login extends CI_Model {

	function check_login($userid,$password,$tenant_id){

		$this->db->select('userid, first_name, last_name, password, group_level, group_member , userlevel, position, tenant_id, email, phone, img_profile');
		$query = $this->db->get_where('m_user', array('userid' => $userid, 'tenant_id' => $tenant_id, 'status' => '1', 'is_deleted' => '0'));
		
		$data = $query->result_array();
		
		if(isset($data[0]['userid'])){
			if (password_verify($password, $data[0]['password'])) {
				$result = array("status"	=> true,
							"userid"		=> $data[0]['userid'],
							"first_name"	=> $data[0]['first_name'],
							"last_name"		=> $data[0]['last_name'],
							"group_level"	=> $data[0]['group_level'],
							"group_member"	=> $data[0]['group_member'],
							"userlevel"		=> $data[0]['userlevel'],
							"position"		=> $data[0]['position'],
							"email"			=> $data[0]['email'],
							"phone"			=> $data[0]['phone'],
							"foto"			=> $data[0]['img_profile'],
							"tenant_id"		=> $data[0]['tenant_id']
							);
			}else{
				$result = array("status"	=> false,
							"userid"		=> '',
							"first_name"	=> '',
							"last_name"		=> '',
							"group_level"	=> '',
							"group_member"	=> '',
							"userlevel"		=> '',
							"position"		=> '',
							"email"			=> '',
							"phone"			=> '',
							"foto"			=> '',
							"tenant_id"		=> ''
							);
			}
		}else{
				$result = array("status"	=> false,
							"userid"		=> '',
							"first_name"	=> '',
							"last_name"		=> '',
							"group_level"	=> '',
							"group_member"	=> '',
							"userlevel"		=> '',
							"position"		=> '',
							"email"			=> '',
							"phone"			=> '',
							"foto"			=> '',
							"tenant_id"		=> ''
							);
		}	
		
		return $result;
	}

	function check_login_api($userid,$tenant_id){

		$this->db->select('userid, first_name, last_name, password, group_level, group_member , userlevel, position, tenant_id, email, phone');
		$query = $this->db->get_where('m_user', array('userid' => $userid, 'tenant_id' => $tenant_id, 'status' => '1', 'is_deleted' => '0'));

		$data = $query->result_array();
		
		if(isset($data[0]['userid'])){

				$result = array("status"	=> true,
							"userid"		=> $data[0]['userid'],
							"first_name"	=> $data[0]['first_name'],
							"last_name"		=> $data[0]['last_name'],
							"group_level"	=> $data[0]['group_level'],
							"group_member"	=> $data[0]['group_member'],
							"userlevel"		=> $data[0]['userlevel'],
							"position"		=> $data[0]['position'],
							"email"			=> $data[0]['email'],
							"phone"			=> $data[0]['phone'],
							"foto"			=> '',
							"tenant_id"		=> $data[0]['tenant_id']
							);
			
		}else{
				$result = array("status"	=> false,
							"userid"		=> '',
							"first_name"	=> '',
							"last_name"		=> '',
							"group_level"	=> '',
							"group_member"	=> '',
							"userlevel"		=> '',
							"position"		=> '',
							"email"			=> '',
							"phone"			=> '',
							"foto"			=> '',
							"tenant_id"		=> ''
							);
		}	
		
		return $result;
	}

	function check_login_admin($userid,$password){

		$this->db->select('userid, first_name, last_name, password, group_level, group_member , userlevel, position, tenant_id, email, phone');
		$query = $this->db->get_where('m_user', array('userid' => $userid, 'tenant_id' => 0, 'userlevel' => 'admin', 'status' => '1' , 'is_deleted' => '0'));

		$data = $query->result_array();
		
		if(isset($data[0]['userid'])){
			if (password_verify($password, $data[0]['password'])) {
				$result = array("status"	=> true,
							"userid"		=> $data[0]['userid'],
							"first_name"	=> $data[0]['first_name'],
							"last_name"		=> $data[0]['last_name'],
							"group_level"	=> $data[0]['group_level'],
							"group_member"	=> $data[0]['group_member'],
							"userlevel"		=> $data[0]['userlevel'],
							"position"		=> $data[0]['position'],
							"email"			=> $data[0]['email'],
							"phone"			=> $data[0]['phone'],
							"foto"			=> '',
							"tenant_id"		=> $data[0]['tenant_id']
							);
			}else{
				$result = array("status"	=> false,
							"userid"		=> '',
							"first_name"	=> '',
							"last_name"		=> '',
							"group_level"	=> '',
							"group_member"	=> '',
							"userlevel"		=> '',
							"position"		=> '',
							"email"			=> '',
							"phone"			=> '',
							"foto"			=> '',
							"tenant_id"		=> ''
							);
			}
		}else{
				$result = array("status"	=> false,
							"userid"		=> '',
							"first_name"	=> '',
							"last_name"		=> '',
							"group_level"	=> '',
							"group_member"	=> '',
							"userlevel"		=> '',
							"position"		=> '',
							"email"			=> '',
							"phone"			=> '',
							"foto"			=> '',
							"tenant_id"		=> ''
							);
		}	
		
		return $result;
	}


	function check_tenant($page){
		
		$this->db->select('tenant_id, favicon, logo, title ');
		$query = $this->db->get_where('master_setting', array('tenant_id_alias'=> $page));

		$data = $query->result_array();

		if(isset($data[0]['tenant_id'])){
				$result = array("status" => true, "tenant_id" => $data[0]['tenant_id'], "favicon" => $data[0]['favicon'] , "logo" => $data[0]['logo'], "title" => $data[0]['title']);
		}else{
				$result = array("status" => false );
		}

		return $result;
	}

	function find_page($ticket_status){

		$sql ="	SELECT flow_page
				FROM m_page_config 
				WHERE JSON_SEARCH(JSON_EXTRACT(flow_group,'$.order_status'), 'one', '$ticket_status') IS NOT NULL
				LIMIT 1";
		$query = $this->db->query($sql);
		
		$result = $query->result_array();

		return $result[0]['flow_page'];

	}


}

/* End of file q_login.php */
/* Location: ./application/models/q_login.php */
