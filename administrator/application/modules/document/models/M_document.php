<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_document extends CI_Model {


	function download_attachment_incident($id){

		$sql = "SELECT filename,orig_name,path_file FROM trans_data_attachments WHERE id=$id";
		
		$query = $this->db->query($sql);
		$result= $query->result_array();
		
		if(isset($result[0])){
			$result = $result;
		}else{
			$result = array();
		}
		
		return $result;
	}

	function download_attachment_problem($id){

		$sql = "SELECT filename,orig_name,path_file FROM trans_data_problem_attachments WHERE id=$id";
		
		$query = $this->db->query($sql);
		$result= $query->result_array();
		
		if(isset($result[0])){
			$result = $result;
		}else{
			$result = array();
		}
		
		return $result;
	}

	function download_attachment_email($id){

		$sql = "SELECT filename,orig_name,path_file FROM trans_email_attach WHERE id=$id";
		
		$query = $this->db->query($sql);
		$result= $query->result_array();
		
		if(isset($result[0])){
			$result = $result;
		}else{
			$result = array();
		}
		
		return $result;
	}

	function download_attachment_telegram($id){

		$sql = "SELECT file_name_download AS filename,file_name AS orig_name,file_path_download AS path_file FROM trans_telegram_attachment WHERE message_id=$id";
		
		$query = $this->db->query($sql);
		$result= $query->result_array();
		
		if(isset($result[0])){
			$result = $result;
		}else{
			$result = array();
		}
		
		return $result;
	}

}