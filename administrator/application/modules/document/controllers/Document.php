<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Document extends CI_Controller {

	public function index()
	{
		if($this->session->userdata('is_logged_in') == true){

			$parent_page	=  $this->uri->segment(1);
			$page			=  $this->uri->segment(1);
			$baseUrl		=  base_url();	
			$this->load->library('page_render');
			
			$this->page_render->render($parent_page,$page);

		}else{
			redirect('login');
		}

	}	

	public function download_attachment_incident(){

		$this->load->library('encryption');

		$id		= $this->encryption->decrypt(base64_decode($this->uri->segment(3)));
	
		$this->load->model('m_document');
		
		$files = $this->m_document->download_attachment_incident($id);
		
		if(is_array($files)){
				if(isset($files[0])){

					$file		= $files[0]['path_file'].'/'.$files[0]['filename'];

					$data		= file_get_contents($file);
					$orig_name	= $files[0]['orig_name'];
					
					$this->load->helper('download');
					force_download($orig_name, $data, TRUE);
				}else{
					exit('file not found');
				}
		}else{
			exit('file not found');
		}

	}

	public function download_attachment_problem(){

		$this->load->library('encryption');

		$id		= $this->encryption->decrypt(base64_decode($this->uri->segment(3)));
	
		$this->load->model('m_document');
		
		$files = $this->m_document->download_attachment_problem($id);
		
		if(is_array($files)){
				if(isset($files[0])){

					$file		= $files[0]['path_file'].'/'.$files[0]['filename'];

					$data		= file_get_contents($file);
					$orig_name	= $files[0]['orig_name'];
					
					$this->load->helper('download');
					force_download($orig_name, $data, TRUE);
				}else{
					exit('file not found');
				}
		}else{
			exit('file not found');
		}

	}

	public function download_attachment_email(){

		$this->load->library('encryption');

		$id		= $this->uri->segment(3);
	
		$this->load->model('m_document');
		
		$files = $this->m_document->download_attachment_email($id);
		
		if(is_array($files)){
				if(isset($files[0])){

					$file		= $files[0]['path_file'].'/'.$files[0]['filename'];

					$data		= file_get_contents($file);
					$orig_name	= $files[0]['orig_name'];
					
					$this->load->helper('download');
					force_download($orig_name, $data, TRUE);
				}else{
					exit('file not found');
				}
		}else{
			exit('file not found');
		}

	}

	public function download_attachment_telegram(){

		$this->load->library('encryption');

		$id		= $this->uri->segment(3);
	
		$this->load->model('m_document');
		
		$files = $this->m_document->download_attachment_telegram($id);
		
		if(is_array($files)){
				if(isset($files[0])){

					$file		= $files[0]['path_file'].'/'.$files[0]['filename'];

					$data		= file_get_contents($file);
					$orig_name	= $files[0]['orig_name'];
					
					$this->load->helper('download');
					force_download($orig_name, $data, TRUE);
				}else{
					exit('file not found');
				}
		}else{
			exit('file not found');
		}

	}
}

/* End of file document.php */
/* Location: ./application/controllers/document.php */
