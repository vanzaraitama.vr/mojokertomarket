<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_admin_categories extends CI_Model {

	public function list_data($length,$start,$search,$order,$dir){
		
		$order_by="ORDER BY a.name";
		if($order!=0){
			$order_by="ORDER BY $order $dir";
		}
		
		$where_clause="";
		 if($search!=""){
			$where_clause=" AND (a.name like '%$search%' OR b.name like '%$search%')";
		}

		$sql		= " SELECT a.name, a.slug, b.name as parent, a.id
						FROM categories a
						LEFT OUTER JOIN categories b ON a.parent_id=b.id
						$where_clause
						$order_by";
						
		$query		= $this->db->query($sql . " LIMIT $start, $length");

		$numrows	= $this->db->query($sql);
		$total		= $numrows->num_rows();
		
		return array("data"=>$query->result_array(),
						"total_data"=>$total
				);
	}

	function detail_data($id){
		$this->db->query("SET sql_mode=''");
		$this->db->select('id, name, slug, parent_id');
		$query = $this->db->get_where('categories', array('id' => $id));

		$data = $query->result_array();
		
		return $data;

	}

	function deleted_item($id){

		$this->db->where('id', $id);

		$delete = $this->db->delete('categories');

		if($delete){
			$result = array('status'=> 'success', 'reason'=> 'Data has been deleted');
		}else{
			$result = array('status'=> 'fail', 'reason'=> 'Failed Deleted Data');
		}

		return $result;

	}


	function add($id,$name,$slug,$parent_id,$lup){

		$data = array(
		   'name' => $name,
		   'slug' => $slug,
		   'parent_id' => $parent_id,
		   'created_at' => $lup
		);

		$insert = $this->db->insert('categories', $data); 

		if($insert){
			$result = array('status'=> 'success', 'reason'=> 'Data has been Inserted');
		}else{
			$result = array('status'=> 'fail', 'reason'=> 'Failed Update Data');
		}

		return $result;

	}


	function edit($act,$id,$name,$slug,$parent_id,$lup){
		
		$data = array('name' => $name,
					  'slug' => $slug,
					  'parent_id' => $parent_id,
					  'updated_at' => $lup
					);

		$this->db->where('id', $id );

		$update	= $this->db->update('categories', $data); 

		if($update){
			
			$result = array('status'=> 'success', 'reason'=> 'Data has been changed');
					
		}else{
			$result = array('status'=> 'fail', 'reason'=> 'Failed Update Data');
		}

		return $result;
	}
}

/* End of file M_admin_users.php */
/* Location: ./application/models/M_admin_users.php */
