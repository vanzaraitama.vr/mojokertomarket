<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_categories extends CI_Controller {

	public function index(){

		if($this->session->userdata('is_logged_in') == true){

			$parent_page	=  $this->uri->segment(1);
			$page			=  $this->uri->segment(1);
				
			$this->load->library('page_render');
			$this->load->library('drop_down');

			$data=array(
				'page_content' 				=> $page,
				'base_url'					=> base_url().$page,
				'list_position'				=> $list_position,
				"tenant_id"					=> $this->session->userdata('tenant_id')
			);

			$this->parser->parse('master/content', $data);

		}else{
			redirect('login');
		}
	}


	public function json_list(){
			
			$parent_page	=  $this->uri->segment(1);
			
			$draw=$_REQUEST['draw'];
			$length=$_REQUEST['length'];
			$start=$_REQUEST['start'];
			$search=$_REQUEST['search']["value"];
			
			$order=$_REQUEST['order'][0]["column"];
			$dir=$_REQUEST['order'][0]["dir"];

			$this->load->library('encryption');
			$this->load->model('m_admin_categories');
			
			$data =  $this->m_admin_categories->list_data($length,$start,$search,$order,$dir);
			
			$output=array();
			$output['draw']=$draw;
			$output['recordsTotal']=$output['recordsFiltered']=$data['total_data'];
			$output['data']=array();
			
			
			$nomor_urut=$start+1;
			setlocale(LC_ALL, 'id_ID');
			
			foreach ($data['data'] as $rows =>$row) {
				
				$id = $row['id'];
                $iconAction = "";    

                $iconAction = "<a href='main#".$parent_page."/view_detail/".base64_encode($this->encryption->encrypt('Edit')).'/'.base64_encode($this->encryption->encrypt($id))."' class='btn btn-icon btn-primary btn-rounded mr-2 mb-2' data-toggle='tooltip' title='View Details' aria-expanded='false'>
			                    <i class='icmn-pencil'></i>
			                  </a>
			                  <a onclick=del('".$row['id']."') id=$id class='btn btn-icon btn-danger btn-rounded mr-2 mb-2' data-toggle='tooltip' title='Deleted' aria-expanded='false'>
			                    <i class='icmn-bin2'></i>
			                  </a>";
				
				$output['data'][]=array(
					$nomor_urut, 
					$row['name'], 
					$row['slug'],
					$row['parent'],
					$iconAction
				);
				$nomor_urut++;
			}
			
			echo json_encode($output);
	}

	public function view_detail(){

		if($this->session->userdata('is_logged_in') == true){
			
			$this->load->library('encryption');

			$parent_page	= $this->uri->segment(1);
			$page			= $this->uri->segment(2);
			$act			= $this->encryption->decrypt(base64_decode($this->uri->segment(3)));
			$id				= $this->encryption->decrypt(base64_decode($this->uri->segment(4)));

			$this->load->library('drop_down');

			
			$name			= NULL;
			$slug			= NULL;
			$parent_id 		= NULL;

			$this->load->model('m_admin_categories');
			
			if($act=="Edit"){

				$detail_data = $this->m_admin_categories->detail_data($id);
				
					$name			= $detail_data[0]['name'];
					$slug			= $detail_data[0]['slug'];
					$parent_id		= $detail_data[0]['parent_id'];
			}

			$this->drop_down->select('id','name');
			$this->drop_down->from('categories');
			$list_parent_id = $this->drop_down->build($parent_id);

			$data = array(
				'page'				=> $parent_page,
				'page_content'		=> $parent_page.'_'.$page,
				'base_url'			=> base_url(),
				'act'				=> $act,
				'id'				=> $id,
				'name_categories'	=> $name,
				'slug'				=> $slug,
				'parent_id'			=> $parent_id,
				'list_parent_id'	=> $list_parent_id
			);

			$this->parser->parse('master/content', $data);

		}else{
			exit();
		}

	}


	public function forms_submit(){

		if($this->session->userdata('is_logged_in') == true){

			$act 		= $this->input->post('act');
			$id 		= $this->input->post('id');
			$name		= $this->input->post('name_categories');
			$parent_id	= $this->input->post('parent_id');

			$upd 	= $this->session->userdata('userid');
			$lup 	= date('Y-m-d H:i:s');
			
			$this->load->model('m_admin_categories');
			
			$slug 	= $this->slugify($name);
			
			if($parent_id==''){
				$parent_id = NULL;
			}

			if($act=="Edit"){

					$submit = $this->m_admin_categories->edit($act,$id,$name,$slug,$parent_id,$lup);

					if($submit['status']){
						$statusResp=true;
						$reason=$submit['reason'];
					}else{
						$statusResp="fail";
						$reason= $submit['reason'];
					}

			}else{
				
					$submit = $this->m_admin_categories->add($id,$name,$slug,$parent_id,$lup);

					
					if($submit['status']){
						$statusResp=true;
						$reason="Add Data Successfully...!";
					}else{
						$statusResp="fail";
						$reason= $submit['reason'];
					}					
			}

			echo json_encode(array("status"=>$statusResp, "reason"=>$reason));

		}else{
			exit();
		}

	}

	public function del_item(){

		if($this->session->userdata('is_logged_in') == true){

			$id 	= $this->input->post('id');
			$upd 	= $this->session->userdata('userid');
			$lup 	= date('Y-m-d H:i:s');

			$this->load->model('m_admin_categories');

			$deleted_item = $this->m_admin_categories->deleted_item($id);

			echo json_encode($deleted_item);

		}else{
			exit();
		}
	}

	public function slugify($text){
		  // replace non letter or digits by -
		  $text = preg_replace('~[^\pL\d]+~u', '-', $text);

		  // transliterate
		  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

		  // remove unwanted characters
		  $text = preg_replace('~[^-\w]+~', '', $text);

		  // trim
		  $text = trim($text, '-');

		  // remove duplicate -
		  $text = preg_replace('~-+~', '-', $text);

		  // lowercase
		  $text = strtolower($text);

		  if (empty($text)) {
		    return 'n-a';
		  }

		  return $text;
	}

}

/* End of file Admin_support_site.php */
/* Location: ./application/controllers/Admin_support_site.php */
